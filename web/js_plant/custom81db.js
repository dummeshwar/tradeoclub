/************************************
    File Name: custom.js
    Template Name: NurseryPlant
    Created By: Themestall
    Envato Profile: http://themeforest.net/user/themestall
    Version: 1.0.0
************************************/

(function($) {
    "use strict";

    $(window).load(function() {
        $("#preloader").on(500).fadeOut();
        $(".preloader").on(600).fadeOut("slow");
		
		
    });

    $('select').selectpicker();
    $('[data-toggle="tooltip"]').tooltip();

      /* ==============================================
    AFFIX -->
    =============================================== */
        $('.header-sticky').affix({offset: {top: 150} }); 
        $('.fixmymap').affix({offset: {top: 150} });
	
	$('.owl-hotels').each(function () {
		var column = $(this).data('column');
		var rtl = $(this).data('rtl');
		$(this).owlCarousel({
			rtl:rtl,
            loop:true,
            margin:30,
            nav:true,
            dots:false,
            navText: [
               "<i class='fa fa-angle-left'></i>",
               "<i class='fa fa-angle-right'></i>"
            ],
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1080:{
                    items:column
                },
                1180:{
                    items:column
                }
            }
        })
	})
	
	$('.owl-client').each(function () {
		var column = $(this).data('column');
		var rtl = $(this).data('rtl');
		$(this).owlCarousel({
			rtl:rtl,
            loop:true,
			autoplay: true,
            margin:0,
            nav:false,
            dots:false,
            navText: [
               "<i class='fa fa-angle-left'></i>",
               "<i class='fa fa-angle-right'></i>"
            ],
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1080:{
                    items:column
                },
                1180:{
                    items:column
                }
            }
        })
	})
	
	$('.testi-carousel').each(function () {
		var column = $(this).data('column');
		var rtl = $(this).data('rtl');
		$(this).owlCarousel({
			rtl:rtl,
            loop:true,
			autoplay: true,
            margin:10,
            nav:false,
            dots:false,
            navText: [
               "<i class='fa fa-angle-left'></i>",
               "<i class='fa fa-angle-right'></i>"
            ],
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1080:{
                    items:column
                },
                1180:{
                    items:column
                }
            }
        })
	});
	
	$('.team-carousel').each(function () {
		var column = $(this).data('column');
		var rtl = $(this).data('rtl');
		$(this).owlCarousel({
			rtl:rtl,
			center: true,
            loop:true,
			autoplay: true,
            margin:10,
            nav:false,
            dots:false,
            navText: '',
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1080:{
                    items:column
                },
                1180:{
                    items:column
                }
            }
        })
	});
	
	$('.project-loop').each(function () {
		var column = $(this).data('column');
		var rtl = $(this).data('rtl');
		$(this).owlCarousel({
			rtl:rtl,
            loop:true,
			autoplay: true,
            margin:30,
            nav:true,
            dots:false,
            navText: [
               "<i class='fa fa-angle-left'></i>",
               "<i class='fa fa-angle-right'></i>"
            ],
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1080:{
                    items:column
                },
                1180:{
                    items:column
                }
            }
        })
	});
	
	/*$('.owl-banner-slider').each(function () {
		var navigation = $(this).data('navigation');
		var rtl = $(this).data('rtl');
		$(this).owlCarousel({
			rtl:rtl,
			center: true,
            loop:true,
			autoplay: true,
            margin:10,
            nav:navigation,
            dots:false,
            navText: [
               "<i class='fa fa-angle-left'></i>",
               "<i class='fa fa-angle-right'></i>"
            ],
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1080:{
                    items:1
                },
                1180:{
                    items:1
                }
            }
        })
	});*/
	
	/* ==============================================
VIDEO BG -->
=============================================== */ 

    $('.youtubevideo').each(function(){
		var videoid = $(this).data('videoid');
	
   var Video_back = new video_background($(".youtubevideo"), { 
        "position": "absolute",    //Follow page scroll
        "z-index": "1",        //Behind everything
        "loop": true,          //Loop when it reaches the end
        "autoplay": true,       //Autoplay at start
        "muted": true,         //Muted at start
        "youtube": videoid,   //Youtube video id
        "start":2,                 //Start with 6 seconds offset (to pass black introduction in this case for example)
        "video_ratio": 1.7778,      // width/height -> If none provided sizing of the video is set to adjust
        "fallback_image": "images/dummy.png",    //Fallback image path
        });
        //Toggle play status
        $("#playvideo").on('click',function(){
        Video_back.toggle_play();
        });
        //Toggle mute
        $("#opensound").on('click',function(){
        Video_back.toggle_mute();
    });
	
	});
	
	
	
	$('#myChart').each(function(){
		var labels = $(this).data('labels');
		var numbers = $(this).data('numbers');
		var colors = $(this).data('colors');
		
		var ctx = document.getElementById("myChart").getContext('2d');
		var myChart = new Chart(ctx, {
		  type: 'doughnut',
		  data: {
			labels: ["Red", "T", "W", "T", "F", "S", "S"],
			datasets: [{
			  backgroundColor: ["#2ecc71","#3498db","#95a5a6","#9b59b6","#f1c40f","#e74c3c","#34495e"],
			  data: [12, 19, 3, 17, 28, 14, 7]
			}]
		  },
		  options: {cutoutPercentage: 40, maintainAspectRatio: true}
		});
	});

})(jQuery);

// Owl Slider
 function owlSlider() {
	 
	 "use strict";

	(function($) {

		"use strict";

		if ($('.owl-carousel').length) {		    

			  $(".owl-carousel").each(function (index) {

				var autoplay = $(this).data('autoplay');

				var timeout = $(this).data('delay');

				var slidemargin = $(this).data('margin');

				var slidepadding = $(this).data('stagepadding');

				var items = $(this).data('items');

				var animationin = $(this).data('animatein');

				var animationout = $(this).data('animateout');

				var itemheight = $(this).data('autoheight');

				var itemwidth = $(this).data('autowidth');

				var itemmerge = $(this).data('merge');

				var navigation = $(this).data('nav');

				var pagination = $(this).data('dots');

				var infinateloop = $(this).data('loop');

				var itemsdesktop = $(this).data('desktop');

				var itemsdesktopsmall = $(this).data('desktopsmall');

				var itemstablet = $(this).data('tablet');

				var itemsmobile = $(this).data('mobile');

				$(this).on('initialized.owl.carousel changed.owl.carousel',function(property){

					var current = property.item.index;

					$(property.target).find(".owl-item").eq(current).find(".animated").each(function(){

						var elem = $(this);

						var animation = elem.data('animate');

						if ( elem.hasClass('visible') ) {

							elem.removeClass( animation + ' visible');

						}

						if ( !elem.hasClass('visible') ) {

							var animationDelay = elem.data('animation-delay');

							if ( animationDelay ) {			

								setTimeout(function(){

								 elem.addClass( animation + " visible" );

								}, animationDelay);				

							} else {

								elem.addClass( animation + " visible" );

							}

						}

					});					

				}).owlCarousel({ 

					autoplay: autoplay,

					autoplayTimeout:timeout,

					items : items,

					margin:slidemargin,

					autoHeight:itemheight,

					animateIn: animationin,

					animateOut: animationout,

					autoWidth:itemwidth,

					stagePadding:slidepadding,

					merge:itemmerge,

					nav:navigation,

					dots:pagination,

					loop:infinateloop,

					responsive:{

						479:{

							items:itemsmobile,

						},

						768:{

							items:itemstablet,

						},

						980:{

							items:itemsdesktopsmall,

						},

						1199:{

							items:itemsdesktop,

						}

					}

				});

			});

		}  

	})(jQuery);

}

// Animation 
function dataAnimations() {
	"use strict";
  $('[data-animation]').each(function() {
		var element = $(this);
		element.addClass('animated');
		element.appear(function() {
			var delay = ( element.data('delay') ? element.data('delay') : 1 );
			if( delay > 1 ) element.css('animation-delay', delay + 'ms');				
			element.addClass( element.data('animation') );
			setTimeout(function() {
				element.addClass('visible');
			}, delay);
		});
  });
}

jQuery(window).load(function() {
	
	"use strict";
	
	owlSlider();

});	