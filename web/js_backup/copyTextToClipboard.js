$(document).ready( function() {
	$("#copy-l-link").click(function() {
		var text = document.getElementById("l-link");
		var selection = window.getSelection();
		var range = document.createRange();
		range.selectNodeContents(text);
		selection.removeAllRanges();
		selection.addRange(range);
		//add to clipboard.

		document.execCommand('copy');
		$(this).html('Copied');
		$(this).addClass('btn btn-success');
   });

   $("#copy-r-link").click(function() {
		var text = document.getElementById("r-link");
		var selection = window.getSelection();
		var range = document.createRange();
		range.selectNodeContents(text);
		selection.removeAllRanges();
		selection.addRange(range);
		//add to clipboard.

		document.execCommand('copy');
		$(this).html('Copied');
		$(this).addClass('btn btn-success');
   });

})