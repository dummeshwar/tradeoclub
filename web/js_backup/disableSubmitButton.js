$(document).ready( function() {
   $(":submit").mouseup(function() {
      $(this).addClass('btn btn-primary');
      $(this).attr("disabled",true);
      $(this).html('Processing...');
      this.form.submit();
   });
})