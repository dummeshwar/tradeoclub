<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\ResetMasterPinForm;
use app\models\User;
use app\models\Order;
use \app\models\UserProfile;
use yii\web\UploadedFile;
use app\models\Balance;
use app\models\ContactForm;
use app\models\UserProfileForm;
use app\models\UserAddFundForm;
use app\models\Transaction;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Utils;
use yii\data\Pagination;
use yii\db\Query;
use app\models\OrderSearch;
use app\models\SearchUserTreeForm;
use app\models\Package;
use yii\helpers\ArrayHelper;
use app\models\BuyTcgPointsForm;
use app\models\BroadcastMessage;
use app\models\ProfitCalculatorForm;

class UserController extends Controller {

    public $layout = "user/main";

    public function beforeAction($action) {
      if ($action->id == 'process-add-fund-request' || $action->id == 'getd-default-sponsor-id') {
        $this->enableCsrfValidation = false;
      }
      return parent::beforeAction($action);
    }



    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'profile', 'index', 'view', 'getd-default-sponsorId', 'reset-password', 'my-tree', 'contact', 'my-downline-users', 'add-fund', 'process-add-fund-request', 'add-tcg-points', 'profit-calculator'],
                'rules' => [
                    [
                        'actions' => ['logout', 'profile', 'index', 'view', 'getd-default-sponsorId', 'reset-password', 'my-tree', 'contact', 'my-downline-users', 'add-fund', 'process-add-fund-request', 'add-tcg-points', 'profit-calculator'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    //'cryptobox-callback' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $userOrderCount = Order::find()->where(['status' => 1, 'user_id' => Yii::$app->user->identity->id])->count();

        $totalUserPackageCost = Transaction::find()->where(['user_id' => Yii::$app->user->identity->id, 'status' => 1, 'transaction_mode' => 'PACKAGE PURCHASE'])->sum('paid_amount');

        
        
//        package details added
        
        $query = Package::find()
        ->where(['status' => 1])
        ->orderBy(['updated_at' => SORT_DESC]);

        // get the total number of articles (but do not fetch the article data yet)
        $count = $query->count();

        // create a pagination object with the total count
        $pagination = new Pagination(['totalCount' => $count, 'pageSize' => Yii::$app->params['pageSize']]);


        // limit the query using the pagination and retrieve the packages
        $packages = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        
        
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'userObj' => User::findOne(Yii::$app->user->identity->id),
            'userOrderCount' => $userOrderCount,
            'totalUserPackageCost' => $totalUserPackageCost,
             'packages' => $packages,
             'pages' => $pagination,
        ]);
    }

    public function actionBenefits() {
      return $this->render('benefits');
    }

    public function actionMemberarea() {
      return $this->render('memberarea');
    }

    public function actionPackage() {
      return $this->render('package');
    }

    public function actionCurrent() {
      return $this->render('current');
    }

    public function actionCalculator() {
      return $this->render('calculator');
    }

    public function actionSummary() {
      return $this->render('summary');
    }

    public function actionPayment() {
      return $this->render('payment');
    }
    
    public function actionMarketCapitalizations() {
      return $this->render('market_capitalizations');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $this->redirect(['index']);
        }
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();
        $this->redirect(['login']);
        //return $this->goHome();
    }

    /**
     * Get default Sponser Id.
     */
    public function actionGetdDefaultSponsorId() {
        if (Yii::$app->request->isAjax) {
            $sponsorId = User::find()
                    ->where(['status' => 1, 'is_admin' => 1])
                    ->orderBy(['id' => SORT_ASC])
                    ->one();
            if (!empty($sponsorId['username'])) {
                $response = Yii::$app->response;
                $response->format = \yii\web\Response::FORMAT_JSON;
                $response->data = ['sponsorId' => $sponsorId['username']];
                $response->statusCode = 200;
                return $response;
            }
        } else
            throw new \yii\web\BadRequestHttpException;
    }

    /**
     * User Profile.
     *
     */
    public function actionProfile() {

      $UserProfile = UserProfileForm::findOne([
          'user_id' => Yii::$app->user->id
      ]);

      $User = User::findOne([
        'id' => Yii::$app->user->id
      ]);

      $existing_profile_image = !empty($UserProfile->picture) ? $UserProfile->picture : NULL;
      $existing_statement_image = !empty($UserProfile->bank_statement) ? $UserProfile->bank_statement : NULL;

      if ($UserProfile->load(Yii::$app->request->post())) {

            // IF new Image uploaded, User profile pic.
            $UserProfile->picture = UploadedFile::getInstance($UserProfile, 'picture');
            if (!empty($UserProfile->picture) && $UserProfile->upload()) {
                $UserProfile->picture = $UserProfile->picture->name;
            } else {
                // Use old image/Null.
                $UserProfile->picture = $existing_profile_image;
            }

            // Bank statement.
            $UserProfile->bank_statement = UploadedFile::getInstance($UserProfile, 'bank_statement');
            if (!empty($UserProfile->bank_statement) && $UserProfile->bankStatementUpload()) {
                $UserProfile->bank_statement = $UserProfile->bank_statement->name;
            } else {
                // Use old image/Null.
                $UserProfile->bank_statement = $existing_statement_image;
            }

            // Do the operation.
            $UserProfile->date_of_birth = date('Y-m-d', strtotime($UserProfile->date_of_birth));

            if (!$UserProfile->save(false)) {
              throw new Exception("Something went wrong!. Please try again later.");
            }

            Yii::$app->session->setFlash('profileUpdate');
            return $this->refresh();
      }

      return $this->render('profile', [
          'user' => $User,
          'profile' => $UserProfile
      ]);
    }

    /**
     * User password reset.
     */

    public function actionResetPassword() {
        $User = User::findOne(Yii::$app->user->id);
        $ResetPasswordModel = new ResetPasswordForm($User, FALSE);

        if ($ResetPasswordModel->load(Yii::$app->request->post()) && $ResetPasswordModel->validate() && $ResetPasswordModel->resetPasswordForLoggedInUser()) {
            Yii::$app->session->setFlash('password-reset', 'Password Reset successful.');
            return $this->refresh();
        }
        return $this->render('reset_password', [
            'model' => $ResetPasswordModel
        ]);

    }

    /**
     * User password reset.
     */

    public function actionResetMasterPin() {
        $User = User::findOne(Yii::$app->user->id);
        $ResetMasterPinModel = new ResetMasterPinForm($User, FALSE);

        if ($ResetMasterPinModel->load(Yii::$app->request->post()) && $ResetMasterPinModel->validate() && $ResetMasterPinModel->resetMasterPinForLoggedInUser()) {
            Yii::$app->session->setFlash('master-pin-reset', 'Key Pin Reset successful.');
            return $this->refresh();
        }

        return $this->render('reset_master_pin', [
            'model' => $ResetMasterPinModel
        ]);

    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
                    'userObj' => User::findOne(Yii::$app->user->identity->id),
        ]);
    }

    /**
     * Get user downlines
     */
    public function actionMyDownlineUsers() {
      $current_userId = Yii::$app->user->identity->id;

        // Get all users which are direct descendent to current user.
        $query = User::find()
                ->where(['parent_id' => $current_userId])
                ->orderBy(['created_at' => SORT_DESC]);

        // get the total number of articles (but do not fetch the article data yet)
        $count = $query->count();

        // create a pagination object with the total count
        $pagination = new Pagination(['totalCount' => $count, 'pageSize' => Yii::$app->params['pageSize']]);


        // limit the query using the pagination and retrieve the users
        $users = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('user_downlines', [
             'users' => $users,
             'pages' => $pagination,
        ]);

    }


    public function actionMyTree($username = null) {
      $model = new SearchUserTreeForm();
      $rootUserObject = User::findOne(Yii::$app->user->identity->id);
      if (!empty($username) && $rootUserObject->username != $username) {
          $currentUserObject = User::findOne([
                  'username' => $username
          ]);
          $tree_data = $this->printSubTree($currentUserObject, $rootUserObject);
      } else {
          $currentUserObject = $rootUserObject;
          $tree_data = $this->printTree($currentUserObject);
      }

      if ($model->load(Yii::$app->request->post()) && $model->validate()) {
          $currentUserObject = User::findOne([
                  'username' => $model->username
          ]);

          if ($model->username != $rootUserObject->username) {
            $tree_data = $this->printSubTree($currentUserObject, $rootUserObject);
          } else {
            $tree_data = $this->printTree($currentUserObject);
          }
      }


        //return $this->render('tree_view', ['tree_data' => $tree_data]);
        //return $this->render('downline_tree', ['tree_data' => $tree_data, 'root_user' => $currentUserObject->username, 'downlineData' => $this->getUserDownlinestats()]);

         return $this->render('tree_view', ['tree_data' => $tree_data, 'root_user' => $currentUserObject->username, 'downlineData' => $this->getUserDownlinestats(), 'model' => $model]);
    }

    public function printSubTree($curUserObject, $rootUserObject) {
      //echo $curUserObject->username . " : " . $rootUserObject->username;
      $tree_data = [];

      // Zeroth level - 0
      $tree_data[] = $curUserObject->username;


      // First level - 1

      if (!empty($curUserObject->left_user)) {
          $leftUser = User::find()
            ->where(['username' => $curUserObject->left_user])->one();

          // if (!empty($leftUser) && ($leftUser->parent_id == $curUserObject->id || $leftUser->parent_id == $rootUserObject->id)) {
          if (!empty($leftUser) ) {
            $tree_data[] = $leftUser->username;
          } else {

            $flag = false;
            // Loop until you find direct referral in tree.
            while (isset($leftUser->left_user_id) && $leftUser->left_user_id != null) {
              $leftUser = User::findOne($leftUser->left_user_id);

              // if ( (isset($leftUser->parent_id) && isset($curUserObject->id) && isset($rootUserObject->id)) && ($rootNode->id == $leftUser->parent_id || $leftUser->parent_id == $rootUserObject->id) ) {
              if ( (isset($leftUser->parent_id) && isset($curUserObject->id) && isset($rootUserObject->id)) ) {
                $flag = true;
                $tree_data[] = $leftUser->username;
                break;
              }
            }
            if (!$flag) {
              $tree_data[] = "N";
            }
          }

      } else {
          $tree_data[] = "N";
      }


      if (!empty($curUserObject->right_user)) {
          $rightUser = User::find()
            ->where(['username' => $curUserObject->right_user])->one();

          //if (!empty($rightUser) && ($rightUser->parent_id == $curUserObject->id || $rightUser->parent_id == $rootUserObject->id)) {
          if (!empty($rightUser)) {
            $tree_data[] = $rightUser->username;
          } else {

            $flag = false;
            // Loop until you find direct referral in tree.
            while (isset($rightUser->right_user_id) && $rightUser->right_user_id != null) {
              $rightUser = User::findOne($rightUser->right_user_id);

              // if ( (isset($rightUser->parent_id) && isset($curUserObject->id)  && isset($rootUserObject->id)) && ($curUserObject->id == $rightUser->parent_id || $rightUser->parent_id == $rootUserObject->id) ) {
              if ( (isset($rightUser->parent_id) && isset($curUserObject->id)  && isset($rootUserObject->id)) ) {
                $flag = true;
                $tree_data[] = $rightUser->username;
                break;
              }
            }
            if (!$flag) {
              $tree_data[] = "N";
            }
          }

      } else {
          $tree_data[] = "N";
      }



    // Second Level

    // Left user -  users.

    if (!empty($tree_data[1]) && $tree_data[1] != 'N') {
      $userObj = User::find()
            ->where(['username' => $tree_data[1]])->one();


      if (!empty($userObj) && !empty($userObj->left_user)) {

          $leftUser = User::find()
            ->where(['username' => $userObj->left_user])->one();

          // if ( !empty($leftUser) && (($leftUser->parent_id == $curUserObject->id) || ($leftUser->parent_id == $userObj->id) || ($leftUser->parent_id == $rootUserObject->id)) ) {
          if ( !empty($leftUser) ) {
            $tree_data[] = $leftUser->username;
          } else {

            $flag = false;
            // Loop until you find direct referral in tree.
            while (isset($leftUser->left_user_id) && $leftUser->left_user_id != null) {
              $leftUser = User::findOne($leftUser->left_user_id);

              // if ( !empty($leftUser) && (($leftUser->parent_id == $curUserObject->id) || ($leftUser->parent_id == $userObj->id) || ($leftUser->parent_id == $rootUserObject->id)) ) {
              if ( !empty($leftUser) ) {
                $flag = true;
                $tree_data[] = $leftUser->username;
                break;
              }
            }
            if (!$flag) {
              $tree_data[] = "N";
            }
          }


      } else {
        $tree_data[] = "N";
      }

      if (!empty($userObj) && !empty($userObj->right_user)) {

          $rightUser = User::find()
            ->where(['username' => $userObj->right_user])->one();

          // if ( !empty($rightUser) && (($rightUser->parent_id == $curUserObject->id) || ($rightUser->parent_id == $userObj->id) || ($rightUser->parent_id == $rootUserObject->id) ) ) {
          if ( !empty($rightUser) ) {
            $tree_data[] = $rightUser->username;
          } else {

            $flag = false;
            // Loop until you find direct referral in tree.
            while (isset($rightUser->left_user_id) && $rightUser->left_user_id != null) {
              $rightUser = User::findOne($rightUser->right_user_id);

              // if ( !empty($rightUser) && (($rightUser->parent_id == $curUserObject->id) || ($rightUser->parent_id == $userObj->id) || ($rightUser->parent_id == $rootUserObject->id)) ) {
              if ( !empty($rightUser) ) {
                $flag = true;
                $tree_data[] = $rightUser->username;
                break;
              }
            }
            if (!$flag) {
              $tree_data[] = "N";
            }
          }


      } else {
        $tree_data[] = "N";
      }

    }

    if (!empty($tree_data[2]) && $tree_data[2] != 'N') {
      $userObj = User::find()
            ->where(['username' => $tree_data[2]])->one();


      if (!empty($userObj) && !empty($userObj->left_user)) {

          $leftUser = User::find()
            ->where(['username' => $userObj->left_user])->one();

          //if ( !empty($leftUser) && (($leftUser->parent_id == $curUserObject->id) || ($leftUser->parent_id == $userObj->id) || ($leftUser->parent_id == $rootUserObject->id)) ) {
          if ( !empty($leftUser) ) {
            $tree_data[] = $leftUser->username;
          } else {

            $flag = false;
            // Loop until you find direct referral in tree.
            while (isset($leftUser->left_user_id) && $leftUser->left_user_id != null) {
              $leftUser = User::findOne($leftUser->left_user_id);

              //if ( !empty($leftUser) && (($leftUser->parent_id == $curUserObject->id) || ($leftUser->parent_id == $userObj->id) || ($leftUser->parent_id == $rootUserObject->id)) ) {
              if ( !empty($leftUser) ) {
                $flag = true;
                $tree_data[] = $leftUser->username;
                break;
              }
            }
            if (!$flag) {
              $tree_data[] = "N";
            }
          }


      } else {
        $tree_data[] = "N";
      }

      if (!empty($userObj) && !empty($userObj->right_user)) {

          $rightUser = User::find()
            ->where(['username' => $userObj->right_user])->one();

          //if ( !empty($rightUser) && (($rightUser->parent_id == $curUserObject->id) || ($rightUser->parent_id == $userObj->id) || ($rightUser->parent_id == $rootUserObject->id)) ) {
          if ( !empty($rightUser) ) {
            $tree_data[] = $rightUser->username;
          } else {

            $flag = false;
            // Loop until you find direct referral in tree.
            while (isset($rightUser->left_user_id) && $rightUser->left_user_id != null) {
              $rightUser = User::findOne($rightUser->right_user_id);

              //if ( !empty($rightUser) && (($rightUser->parent_id == $curUserObject->id) || ($rightUser->parent_id == $userObj->id) || ($rightUser->parent_id == $rootUserObject->id)) ) {
              if ( !empty($rightUser) ) {
                $flag = true;
                $tree_data[] = $rightUser->username;
                break;
              }
            }
            if (!$flag) {
              $tree_data[] = "N";
            }
          }


      } else {
        $tree_data[] = "N";
      }

    }

    return $tree_data;

    }

    public function printTree($rootNode) {


        $tree_data = [];

        // Zeroth level - 0
        $tree_data[] = $rootNode->username;


        // First level - 1

        if (!empty($rootNode->left_user)) {
            $leftUser = User::find()
              ->where(['username' => $rootNode->left_user])->one();

            // if (!empty($leftUser) && $leftUser->parent_id == $rootNode->id) {
            if (!empty($leftUser) ) {
              $tree_data[] = $leftUser->username;
            } else {

              $flag = false;
              // Loop until you find direct referral in tree.
              while (isset($leftUser->left_user_id) && $leftUser->left_user_id != null) {
                $leftUser = User::findOne($leftUser->left_user_id);

                /* if ( (isset($leftUser->parent_id) && isset($rootNode->id)) && ($rootNode->id == $leftUser->parent_id) ) {
                  $flag = true;
                  $tree_data[] = $leftUser->username;
                  break;
                } */
                
                /* VT Code Start */
                
                if ( (isset($leftUser->parent_id) && isset($rootNode->id)) ) {
                  $flag = true;
                  $tree_data[] = $leftUser->username;
                  break;
                } 
                
                /* VT Code End */
                
              }
              if (!$flag) {
                $tree_data[] = "N";
              }
            }

        } else {
            $tree_data[] = "N";
        }


        if (!empty($rootNode->right_user)) {
            $rightUser = User::find()
              ->where(['username' => $rootNode->right_user])->one();

            // if (!empty($rightUser) && $rightUser->parent_id == $rootNode->id) {
            if (!empty($rightUser) ) {
              $tree_data[] = $rightUser->username;
            } else {

              $flag = false;
              // Loop until you find direct referral in tree.
              while (isset($rightUser->right_user_id) && $rightUser->right_user_id != null) {
                $rightUser = User::findOne($rightUser->right_user_id);

                /* if ( (isset($rightUser->parent_id) && isset($rootNode->id)) && ($rootNode->id == $rightUser->parent_id) ) {
                  $flag = true;
                  $tree_data[] = $rightUser->username;
                  break;
                } */
                
                if ( (isset($rightUser->parent_id) && isset($rootNode->id)) ) {
                  $flag = true;
                  $tree_data[] = $rightUser->username;
                  break;
                }
                
              }
              if (!$flag) {
                $tree_data[] = "N";
              }
            }

        } else {
            $tree_data[] = "N";
        }



      // Second Level

      // Left user -  users.

      if (!empty($tree_data[1]) && $tree_data[1] != 'N') {
        $userObj = User::find()
              ->where(['username' => $tree_data[1]])->one();


        if (!empty($userObj) && !empty($userObj->left_user)) {

            $leftUser = User::find()
              ->where(['username' => $userObj->left_user])->one();

            //if ( !empty($leftUser) && (($leftUser->parent_id == $rootNode->id) || ($leftUser->parent_id == $userObj->id)) ) {
            if ( !empty($leftUser) ) {
              $tree_data[] = $leftUser->username;
            } else {

              $flag = false;
              // Loop until you find direct referral in tree.
              while (isset($leftUser->left_user_id) && $leftUser->left_user_id != null) {
                $leftUser = User::findOne($leftUser->left_user_id);

                // if ( !empty($leftUser) && (($leftUser->parent_id == $rootNode->id) || ($leftUser->parent_id == $userObj->id)) ) {
                if ( !empty($leftUser) ) {
                  $flag = true;
                  $tree_data[] = $leftUser->username;
                  break;
                }
              }
              if (!$flag) {
                $tree_data[] = "N";
              }
            }


        } else {
          $tree_data[] = "N";
        }

        if (!empty($userObj) && !empty($userObj->right_user)) {

            $rightUser = User::find()
              ->where(['username' => $userObj->right_user])->one();

            //if ( !empty($rightUser) && (($rightUser->parent_id == $rootNode->id) || ($rightUser->parent_id == $userObj->id)) ) {
            if ( !empty($rightUser) ) {
              $tree_data[] = $rightUser->username;
            } else {

              $flag = false;
              // Loop until you find direct referral in tree.
              while (isset($rightUser->left_user_id) && $rightUser->left_user_id != null) {
                $rightUser = User::findOne($rightUser->right_user_id);

                // if ( !empty($rightUser) && (($rightUser->parent_id == $rootNode->id) || ($rightUser->parent_id == $userObj->id)) ) {
                if ( !empty($rightUser) ) {
                  $flag = true;
                  $tree_data[] = $rightUser->username;
                  break;
                }
              }
              if (!$flag) {
                $tree_data[] = "N";
              }
            }


        } else {
          $tree_data[] = "N";
        }

      }

      if (!empty($tree_data[2]) && $tree_data[2] != 'N') {
        $userObj = User::find()
              ->where(['username' => $tree_data[2]])->one();


        if (!empty($userObj) && !empty($userObj->left_user)) {

            $leftUser = User::find()
              ->where(['username' => $userObj->left_user])->one();

            //if ( !empty($leftUser) && (($leftUser->parent_id == $rootNode->id) || ($leftUser->parent_id == $userObj->id)) ) {
            if ( !empty($leftUser) ) {
              $tree_data[] = $leftUser->username;
            } else {

              $flag = false;
              // Loop until you find direct referral in tree.
              while (isset($leftUser->left_user_id) && $leftUser->left_user_id != null) {
                $leftUser = User::findOne($leftUser->left_user_id);

                // if ( !empty($leftUser) && (($leftUser->parent_id == $rootNode->id) || ($leftUser->parent_id == $userObj->id)) ) {
                if ( !empty($leftUser) ) {
                  $flag = true;
                  $tree_data[] = $leftUser->username;
                  break;
                }
              }
              if (!$flag) {
                $tree_data[] = "N";
              }
            }


        } else {
          $tree_data[] = "N";
        }

        if (!empty($userObj) && !empty($userObj->right_user)) {

            $rightUser = User::find()
              ->where(['username' => $userObj->right_user])->one();

            // if ( !empty($rightUser) && (($rightUser->parent_id == $rootNode->id) || ($rightUser->parent_id == $userObj->id)) ) {
            if ( !empty($rightUser) ) {
              $tree_data[] = $rightUser->username;
            } else {

              $flag = false;
              // Loop until you find direct referral in tree.
              while (isset($rightUser->left_user_id) && $rightUser->left_user_id != null) {
                $rightUser = User::findOne($rightUser->right_user_id);

                // if ( !empty($rightUser) && (($rightUser->parent_id == $rootNode->id) || ($rightUser->parent_id == $userObj->id)) ) {
                if ( !empty($rightUser) ) {
                  $flag = true;
                  $tree_data[] = $rightUser->username;
                  break;
                }
              }
              if (!$flag) {
                $tree_data[] = "N";
              }
            }


        } else {
          $tree_data[] = "N";
        }

      }

      return $tree_data;
    }



    /**
     * Get user Downline stats.
     */
    public function  getUserDownlinestats() {
      $downlines = [];
      $current_userId = Yii::$app->user->identity->id;

      // Left user count and their package purchase count.
      $curUserObj = $Userobj = User::findOne($current_userId);
      $downlines['left_user_count'] = $downlines['right_user_count']= 0;
      $downlines['left_user_package_purchase_count'] = $downlines['right_user_package_purchase_count'] = 0;
      $downlines['left_user_total_package_purchase_amount'] = $downlines['right_user_total_package_purchase_amount'] = 0;
      $Btree_users = $this->getBinaryTreeUsers($curUserObj);
// echo "<pre>";
// print_r($Btree_users);
// exit;
      if (!empty($Btree_users) && (count($Btree_users) > 0)) {
        $this->processUsers($Btree_users, $curUserObj, $downlines);
      }

      return $downlines;
    }

    /**
     * Get users level by level.
     */
    public function getBinaryTreeUsers($rootNode) {
      $Btree_users['L'] = [];
      $Btree_users['R'] = [];

      // Process left subtree.
      // 2 meaning both active and in active users.
      $leftUserNode = $this->getImmediateChild($rootNode, 'LEFT');
      if (!empty($leftUserNode)) {
        $Btree_users['L'] = $this->processSubtree($leftUserNode, $rootNode);
      }


      // Process Right subtree.
      $rightUserNode = $this->getImmediateChild($rootNode, 'RIGHT');
      if (!empty($rightUserNode)) {
        $Btree_users['R'] = $this->processSubtree($rightUserNode, $rootNode);
      }

      return $Btree_users;
    }

    /**
     * Get her own immidiate left OR Right child.
     */
    public function getImmediateChild($rootNode, $location) {

      if ($location == 'LEFT') {
        // Left Side.
        $flag = false;
        $leftUserObj = User::findByUsername($rootNode->left_user, 2);

        //if ( isset($leftUserObj->parent_id) && isset($rootNode->id) && ($rootNode->id == $leftUserObj->parent_id) ) {
        if ( isset($leftUserObj->parent_id) ) {
          return $leftUserObj;
        } else {
          // Loop until you find direct referral in tree.
          while (isset($leftUserObj->left_user_id) && $leftUserObj->left_user_id != null) {
            $leftUserObj = User::findOne($leftUserObj->left_user_id);

            //if ( (isset($leftUserObj->parent_id) && isset($rootNode->id)) && (($rootNode->id == $leftUserObj->parent_id)) ) {
            if ( (isset($leftUserObj->parent_id) && isset($rootNode->id)) ) {
              $flag = true;
              break;
            }
          }

          if ($flag) {
            return $leftUserObj;
          }
        }
        return null;
      }

      if ($location == 'RIGHT') {
        // Right side.
        $flag = false;
        $rightUserNode = User::findByUsername($rootNode->right_user, 2);

        // if ( (isset($rightUserNode->parent_id) && isset($rootNode->id)) && (($rootNode->id == $rightUserNode->parent_id)) ) {
        if ( (isset($rightUserNode->parent_id) && isset($rootNode->id)) ) {
          return $rightUserNode;
        } else {
          // Loop until you find direct referral in tree.
          while (isset($rightUserNode->right_user_id) && $rightUserNode->right_user_id != null) {
            $rightUserNode = User::findOne($rightUserNode->right_user_id);

            //if ( (isset($rightUserNode->parent_id) && isset($rootNode->id)) && (($rootNode->id == $rightUserNode->parent_id)) ) {
            if ( (isset($rightUserNode->parent_id) && isset($rootNode->id)) ) {
              $flag = true;
              break;
            }
          }

          if ($flag) {
            return $rightUserNode;
          }
        }
        return null;
      }

    }


    /***
     * Process sub-trees.
     */
     public function processSubtree($treeNode, $rootNode) {
       $children = $root_node_children_ids = ArrayHelper::getColumn(User::find()->where(['parent_id' => $rootNode->id])->all(), 'id');

       $q = new \SplQueue();
       $q->enqueue($treeNode->username);
       $level = 0;
       $users_at_each_level = [];
       array_push($users_at_each_level, $treeNode->username);

       while (1) {
           $node_count = count($q);

           if ($node_count == 0) {
               return array_filter($users_at_each_level);
               break;
           }

           $flag = false;

           // Dequeue all nodes of current level and Enqueue all
           // nodes of next level
           while ($node_count > 0)
           {
               $username = $q->dequeue();
               // 2. Consider both active and inctive users.
               $node = User::findByUsername($username, 2);

               if (!empty($node->left_user_id) &&  $node->left_user_id!= NULL) {

                   // Current left user object.
                   $leftUserObj = User::findByUsername($node->left_user, 2);

                   //if ( (isset($node->id) && isset($leftUserObj->parent_id) && isset($rootNode->id)) && (($node->id == $leftUserObj->parent_id) || ($rootNode->id == $leftUserObj->parent_id) || (in_array($leftUserObj->parent_id, $root_node_children_ids)) || (in_array($leftUserObj->parent_id, $children))) ) {
                   if ( (isset($node->id) && isset($leftUserObj->parent_id) && isset($rootNode->id)) && ((in_array($leftUserObj->parent_id, $root_node_children_ids)) || (in_array($leftUserObj->parent_id, $children))) ) {
                     array_push($users_at_each_level, $leftUserObj->username);
                     array_push($children, $leftUserObj->id);
                     $q->enqueue($leftUserObj->username);
                   } else {

                     // Loop until you find direct referral in tree.
                     while (isset($leftUserObj->left_user_id) && $leftUserObj->left_user_id != null) {
                       $leftUserObj = User::findOne($leftUserObj->left_user_id);

                       //if ( (isset($node->id) && isset($leftUserObj->parent_id) && isset($rootNode->id)) && (($node->id == $leftUserObj->parent_id) || ($rootNode->id == $leftUserObj->parent_id) || (in_array($leftUserObj->parent_id, $root_node_children_ids)) || (in_array($leftUserObj->parent_id, $children))) ) {
                       if ( (isset($node->id) && isset($leftUserObj->parent_id) && isset($rootNode->id)) && ((in_array($leftUserObj->parent_id, $root_node_children_ids)) || (in_array($leftUserObj->parent_id, $children))) ) {
                         $flag = true;
                         $q->enqueue($leftUserObj->username);
                         break;
                       }
                     }

                     if ($flag) {
                       array_push($users_at_each_level, $leftUserObj->username);
                       array_push($children, $leftUserObj->id); // Push all children.
                     }

                     $flag = false;
                   }
               }

               if (!empty($node->right_user_id) &&  $node->right_user_id!= NULL) {

                   // Current right user object.
                   $rightUserObj = User::findByUsername($node->right_user, 2);

                   //if ( (isset($node->id) && isset($rightUserObj->parent_id) && isset($rootNode->id)) && (($node->id == $rightUserObj->parent_id) || ($rootNode->id == $rightUserObj->parent_id) || (in_array($rightUserObj->parent_id, $root_node_children_ids)) || (in_array($rightUserObj->parent_id, $children))) ) {
                   if ( (isset($node->id) && isset($rightUserObj->parent_id) && isset($rootNode->id)) && ((in_array($rightUserObj->parent_id, $root_node_children_ids)) || (in_array($rightUserObj->parent_id, $children))) ) {
                     array_push($users_at_each_level, $rightUserObj->username);
                     array_push($children, $rightUserObj->id);
                     $q->enqueue($rightUserObj->username);
                   } else {
                     // Loop until you find direct referral in tree.
                     while (isset($rightUserObj->right_user_id) && $rightUserObj->right_user_id != null) {
                       $rightUserObj = User::findByUsername($rightUserObj->right_user, 2);

                       //if ( (isset($node->id) && isset($rightUserObj->parent_id) && isset($rootNode->id)) && (($node->id == $rightUserObj->parent_id) || ($rootNode->id == $rightUserObj->parent_id) || (in_array($rightUserObj->parent_id, $root_node_children_ids)) || (in_array($rightUserObj->parent_id, $children))) ) {
                       if ( (isset($node->id) && isset($rightUserObj->parent_id) && isset($rootNode->id)) && ((in_array($rightUserObj->parent_id, $root_node_children_ids)) || (in_array($rightUserObj->parent_id, $children))) ) {
                         $flag = true;
                         $q->enqueue($rightUserObj->username);
                         break;
                       }
                     }

                     if ($flag) {
                       array_push($users_at_each_level, $rightUserObj->username);
                       array_push($children, $rightUserObj->id);
                     }

                     $flag = false;
                   }
               }
               $node_count--;
           }

           // Increase the level.
           $level++;
       }
     }

    /**
     * Process tree.
     */
    public function processUsers($user, $curUserObj, &$downlines) {
      $lc_fund = 0;
      $rc_fund = 0;
      $lc_fund += $this->processTreeChildren($user['L'], $curUserObj, $downlines, 'L');
      $rc_fund += $this->processTreeChildren($user['R'], $curUserObj, $downlines, 'R');

      $downlines['left_user_total_package_purchase_amount'] = $lc_fund;
      $downlines['right_user_total_package_purchase_amount'] = $rc_fund;
    }

    /**
     * Calculate package sum.
     */
    public function processTreeChildren($users, $rootNode, &$downlines, $is_left_or_right) {
      $children_fund = 0;
      $order_count = 0;
      if (count($users) > 0) {
        foreach ($users as $user) {
          $userObj = User::findByUsername($user, 2);
          if (empty($userObj)) {
            // Not an user.
            continue;
          }
          // Find any packages purchased.
          $package_ids = ArrayHelper::getColumn(Order::find()->where(['status' => 1, 'user_id' => $userObj->id])->all(), 'package_id');
          // Get order ids.
          $result_ids = ArrayHelper::getColumn(Order::find()->where(['status' => 1, 'user_id' => $userObj->id])->all(), 'id');

          $order_count = $order_count + count($result_ids);

          if (count($result_ids) > 0) {
              // Get only newly created packages.
              $package_ids = ArrayHelper::getColumn(Order::find()->where(['id' => $result_ids])->all(), 'package_id');
              $children_fund += Package::find()->where(['id' => $package_ids])->sum('price');
          }
        }
      }
      if ($is_left_or_right == 'L') {
        $downlines['left_user_count'] = count($users);
        $downlines['left_user_package_purchase_count'] += $order_count;
      } else {
        $downlines['right_user_count'] = count($users);
        $downlines['right_user_package_purchase_count'] += $order_count;
      }
      return $children_fund;
    }

    /**
     * Debit money from user.
     */
    public function actionAddFund() {
        $model = new UserAddFundForm();
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

          try {
                $curUserObj = Yii::$app->user->identity;

                // Create a transaction.
                $TransactionObj = Transaction::createNewTransaction($curUserObj->id, null, Yii::$app->params['wallets']['CASH_WALLET'], Yii::$app->params['transaction_modes']['user_add_fund'], $model->amount, $model->amount, Yii::$app->params['transaction_modes']['user_add_fund']);

// echo '<pre>'; print_r($TransactionObj); exit;

                // All ok.
                $transaction->commit();
                $this->redirect(['user/process-add-fund-request','amount' => $model->amount, 'tid' => base64_encode('order-' . $TransactionObj->id)]);

          } catch(\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch(\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }

        }

        return $this->render('add_fund', [
            'model' => $model
        ]);
    }

    /**
     * Process add fund request.
     */
     public function actionProcessAddFundRequest($amount = 0, $tid = null) {

      if ($tid == null || $amount == 0) {
          throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
      }

      return $this->render('process_add_fund_request', [
            'amount' => $amount,
            'user_id' => Yii::$app->user->id,
            'order_id' => base64_decode($tid)
        ]);
     }



     /**
      * Success callback from BitCOIN api.
      */
    public function actionCryptoboxCallback() {

      $connection = \Yii::$app->db;

      // a. check if private key valid
      $valid_key = false;
      if (isset($_POST["private_key_hash"]) && strlen($_POST["private_key_hash"]) == 128 && preg_replace('/[^A-Za-z0-9]/', '', $_POST["private_key_hash"]) == $_POST["private_key_hash"])
      {
          $keyshash = array();
          $arr = explode("^", CRYPTOBOX_PRIVATE_KEYS);
          foreach ($arr as $v) $keyshash[] = strtolower(hash("sha512", $v));
          if (in_array(strtolower($_POST["private_key_hash"]), $keyshash)) $valid_key = true;
      }

      // b. alternative - ajax script send gourl.io json data
      if (!$valid_key && isset($_POST["json"]) && $_POST["json"] == "1")
      {
          $data_hash = $boxID = "";
          if (isset($_POST["data_hash"]) && strlen($_POST["data_hash"]) == 128 && preg_replace('/[^A-Za-z0-9]/', '', $_POST["data_hash"]) == $_POST["data_hash"]) { $data_hash = strtolower($_POST["data_hash"]); unset($_POST["data_hash"]); }
          if (isset($_POST["box"]) && is_numeric($_POST["box"]) && $_POST["box"] > 0) $boxID = intval($_POST["box"]);

          if ($data_hash && $boxID)
          {
              $private_key = "";
              $arr = explode("^", CRYPTOBOX_PRIVATE_KEYS);
              foreach ($arr as $v) if (strpos($v, $boxID."AA") === 0) $private_key = $v;

              if ($private_key)
              {
                  $data_hash2 = strtolower(hash("sha512", $private_key.json_encode($_POST).$private_key));
                  if ($data_hash == $data_hash2) $valid_key = true;
              }
              unset($private_key);
          }

          if (!$valid_key) die("Error! Invalid Json Data sha512 Hash!");

      }

      // c.
      if ($_POST) foreach ($_POST as $k => $v) if (is_string($v)) $_POST[$k] = trim($v);



      // d.
      if (isset($_POST["plugin_ver"]) && !isset($_POST["status"]) && $valid_key)
      {
        echo "cryptoboxver_" . (CRYPTOBOX_WORDPRESS ? "wordpress_" . GOURL_VERSION : "php_" . CRYPTOBOX_VERSION);
        die;
      }

      // e.
      if (isset($_POST["status"]) && in_array($_POST["status"], array("payment_received", "payment_received_unrecognised")) &&
          $_POST["box"] && is_numeric($_POST["box"]) && $_POST["box"] > 0 && $_POST["amount"] && is_numeric($_POST["amount"]) && $_POST["amount"] > 0 && $valid_key)
      {

        foreach ($_POST as $k => $v)
        {
          if ($k == "datetime")             $mask = '/[^0-9\ \-\:]/';
          elseif (in_array($k, array("err", "date", "period")))   $mask = '/[^A-Za-z0-9\.\_\-\@\ ]/';
          else                $mask = '/[^A-Za-z0-9\.\_\-\@]/';
          if ($v && preg_replace($mask, '', $v) != $v)  $_POST[$k] = "";
        }

        if (!$_POST["amountusd"] || !is_numeric($_POST["amountusd"])) $_POST["amountusd"] = 0;
        if (!$_POST["confirmed"] || !is_numeric($_POST["confirmed"])) $_POST["confirmed"] = 0;


        $dt     = gmdate('Y-m-d H:i:s');

        $model = $connection->createCommand("select paymentID, txConfirmed from crypto_payments where boxID = ".$_POST["box"]." && orderID = '".$_POST["order"]."' && userID = '".$_POST["user"]."' && txID = '".$_POST["tx"]."' && amount = ".$_POST["amount"]." && addr = '".$_POST["addr"]."' limit 1");
        $obj = $model->queryOne();

        //$obj    = run_sql("select paymentID, txConfirmed from crypto_payments where boxID = ".$_POST["box"]." && orderID = '".$_POST["order"]."' && userID = '".$_POST["user"]."' && txID = '".$_POST["tx"]."' && amount = ".$_POST["amount"]." && addr = '".$_POST["addr"]."' limit 1");


        $paymentID    = ($obj) ? $obj->paymentID : 0;
        $txConfirmed  = ($obj) ? $obj->txConfirmed : 0;

        // Save new payment details in local database
        if (!$paymentID)
        {
          $sql = "INSERT INTO crypto_payments (boxID, boxType, orderID, userID, countryID, coinLabel, amount, amountUSD, unrecognised, addr, txID, txDate, txConfirmed, txCheckDate, recordCreated) VALUES (".$_POST["box"].", '".$_POST["boxtype"]."', '".$_POST["order"]."', '".$_POST["user"]."', '".$_POST["usercountry"]."', '".$_POST["coinlabel"]."', ".$_POST["amount"].", ".$_POST["amountusd"].", ".($_POST["status"]=="payment_received_unrecognised"?1:0).", '".$_POST["addr"]."', '".$_POST["tx"]."', '".$_POST["datetime"]."', ".$_POST["confirmed"].", '$dt', '$dt')";

          $paymentID = $connection->createCommand($sql)->execute();

          $box_status = "cryptobox_newrecord";
        }
        // Update transaction status to confirmed
        elseif ($_POST["confirmed"] && !$txConfirmed)
        {
          $sql = "UPDATE crypto_payments SET txConfirmed = 1, txCheckDate = '$dt' WHERE paymentID = $paymentID LIMIT 1";
          run_sql($sql);

          $connection->createCommand($sql)->execute();

          $box_status = "cryptobox_updated";
        }
        else
        {
          $box_status = "cryptobox_nochanges";
        }


        /**
         *  User-defined function for new payment - cryptobox_new_payment(...)
         *  For example, send confirmation email, update database, update user membership, etc.
         *  You need to modify file - cryptobox.newpayment.php
         *  Read more - https://gourl.io/api-php.html#ipn
               */

        if (in_array($box_status, array("cryptobox_newrecord", "cryptobox_updated"))) {
          $this->cryptobox_new_payment($paymentID, $_POST, $box_status);
        }

      }

      else
        $box_status = "Only POST Data Allowed";


        echo $box_status; // don't delete it
    }


    function cryptobox_new_payment($paymentID = 0, $payment_details = array(), $box_status = "")
    {
      if ($payment_details['order'] && $payment_details['user']) {
          $id = end(explode('-',$payment_details['order']));
          $Transaction = Transaction::find()
              ->where(['id' => $id, 'user_id' => $payment_details['user']])
              ->one();
          if ($Transaction) {
            $Transaction->status = 1;
            $transaction->admin_marked_status = 1;
            $Transaction->save(false);
          }
      }
    }

    /**
     * Debit money from user.
     */
    public function actionAddTcgPoints() {
        $model = new BuyTcgPointsForm();
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        $btc_val_of_tcg_point = Yii::$app->params['points_to_BTC']; // 1 tcg point equals.

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
          $btc_value = $btc_val_of_tcg_point * $model->points;
          try {
                $curUserObj = Yii::$app->user->identity;

                // Create a transaction and debit the money from cash wallet.
                $TransactionObj = Transaction::createNewTransaction(null, Yii::$app->params['wallets']['CASH_WALLET'], null, Yii::$app->params['transaction_modes']['points_purchased'], $btc_value, $btc_value, Yii::$app->params['transaction_modes']['points_purchased'], $curUserObj->id);
                Balance::findAndUpdate($curUserObj->id, Yii::$app->params['wallets']['CASH_WALLET'], $btc_value, 'DEBIT');

                // Wallet.
                $TransactionObj = Transaction::createNewTransaction($curUserObj->id, null, Yii::$app->params['secondary_wallets']['PROMO_WALLET'], Yii::$app->params['transaction_modes']['points_added'], $model->points, $model->points, Yii::$app->params['transaction_modes']['points_added']);
                Balance::findAndUpdate($curUserObj->id, Yii::$app->params['secondary_wallets']['PROMO_WALLET'], $model->points, 'CREDIT');

                // All ok.
                $transaction->commit();
                Yii::$app->session->setFlash('tcgPointsPurchaseSuccess');
                return $this->refresh();

          } catch(\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('tcgPointsPurchaseSuccessFailed');
                throw $e;
          } catch(\Throwable $e) {
              $transaction->rollBack();
              Yii::$app->session->setFlash('tcgPointsPurchaseSuccessFailed');
              throw $e;
          }

        }

        return $this->render('add_points', [
            'model' => $model
        ]);
    }

    /**
     * Profit calculater.
     *
     * @return string
     */
    public function actionProfitCalculator() {
      $model = new ProfitCalculatorForm();
      if ($model->load(Yii::$app->request->post()) && $model->validate()) {

        $results = Package::find()
                ->where(['status' => 1])
                ->where(['<=', 'price', $model->amount])
                ->orderBy(['id' => SORT_ASC])
                ->all();
        return $this->render('profit_calculator', [
          'model' => $model,
          'success' => true,
          'results' => $results
        ]);
      }
      return $this->render('profit_calculator', [
        'model' => $model,
        'success' => false,
      ]);
    }
    
    
    
     public function actionPackageDetail($id) {
        $id = base64_decode($id);
//echo Yii::$app->user->identity->id; exit;
        if (empty($id)) { // item does not exist
            throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
        }

        // Check user has sufficient balance OR not to purchase this package.
        $userCash = Utils::walletMoney(Yii::$app->params['wallets']['CASH_WALLET']);

        // build a DB query to get all packages with status = 1 and updated_at desc order.
        $package = Package::find()
                ->where(['id' => $id, 'status' => 1])
                ->orderBy(['updated_at' => SORT_DESC])->one();

        if (empty($package)) { // item does not exist
            throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
        }

        // Check whether user already purchase the package OR not.
        $order = Order::find()
                ->where(['user_id' => Yii::$app->user->identity->id, 'package_id' => $package->id, 'status' => 1])->one();

//        return $this->redirect('site/packagedetail',[
        return $this->render('packagedetail', [
             'package' => $package,
             'booking_confirm_link' => Url::to(['site/confirm-booking', 'id' => base64_encode($id)]),
             'is_user_has_sufficient_fund' => ($userCash >= $package->price) ? TRUE : FALSE,
             'already_purchased' => empty($order->id) ? TRUE : FALSE,
        ]);
    }


}
