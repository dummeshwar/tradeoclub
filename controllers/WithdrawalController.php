<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\ResetMasterPinForm;
use app\models\User;
use app\models\Order;
use \app\models\MoneyTransfer;
use yii\web\UploadedFile;
use app\models\Balance;
use app\models\ContactForm;
use app\models\WithdrawalRequestSearch;
use app\models\Transaction;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Utils;
use yii\data\Pagination;
use app\models\WithdrawalRequestForm;
use app\models\PaymentGateway;
use yii\helpers\ArrayHelper;

class WithdrawalController extends Controller {

    public $layout = "user/main";

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['request', 'summary'],
                'rules' => [
                    [
                        'actions' => ['request', 'summary'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }



    /**
     * Withdrawal request.
     */
    public function actionRequest() {
        $model = new WithdrawalRequestForm();
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        $withdrawalWallets = Yii::$app->params['money_withdrawal_allowed_wallets'];
        $e_currencies = ArrayHelper::getColumn(PaymentGateway::find()->where(['status' => 1])->all(), 'name');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            try {

                $userObj = Yii::$app->user->identity;

                $withdrawalCharges = Yii::$app->params['transaction_charges']['withdrawal'];
                $actual_amount = $model->amount + (($model->amount * $withdrawalCharges) / 100);
                Transaction::createNewTransaction(null, $model->from_wallet, null, $model->comment, $model->amount, $actual_amount, Yii::$app->params['transaction_modes']['withdrawal_request']);

                Balance::findAndUpdate($userObj->id, $model->from_wallet, $actual_amount, 'DEBIT');
                // All ok.
                $transaction->commit();
                Yii::$app->session->setFlash('withdrawalRequestSuccess');
                return $this->refresh();
            }  catch(\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('withdrawalRequestFailed');
                throw $e;
            } catch(\Throwable $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('withdrawalRequestFailed');
                throw $e;
            }
        }


        return $this->render('request', [
                    'activeWallets' => $withdrawalWallets,
                    'model' => $model,
                    'e_currencies' => $e_currencies,
        ]);
    }


    /**
     * Withdrawal summary.
     */
    public function actionSummary() {
        $searchModel = new WithdrawalRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('summary', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}
