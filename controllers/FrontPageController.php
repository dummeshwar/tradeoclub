<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\RegistrationForm;
use app\models\User;
use app\models\UserProfile;
use app\models\Balance;
use yii\base\Exception;
use app\models\CreateNewPasswordMasterPinForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\MasterPinResetRequestForm;
use app\models\ResetMasterPinForm;
use app\models\Package;
use app\models\UserContactForm;
use app\models\AdminPopUps;
use app\models\BroadcastMessage;


class FrontPageController extends Controller
{
    public $layout = "frontpage/main_trading";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

   
    
    
    
     public function actionIndex()
    {
//          echo "hi"; exit;
         
//          echo '<pre>'; print_r(Yii::$app->request->post());exit;
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            //return $this->goBack();
            return $this->redirect(['user/index']);
        }
        
        
        
        $userRegistrationModel = new RegistrationForm();
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();

        if (!empty($ref)) {
            $userRegistrationModel->parent_id = $ref;
        }

        if (!empty($pos)) {
            $userRegistrationModel->position = $pos;
        }


        // Get all active users.
        $activeUsers = User::getActiveUsers();

        if ($userRegistrationModel->load(Yii::$app->request->post()) && $userRegistrationModel->validate()) {
            try {
                // Since sponsor_id/parent_id is string, so we need to convert
                // back parent user id.
                $userRegistrationModel->parent_id = User::findByUsername($userRegistrationModel->parent_id)->id;
                $userObj = User::createNewUser($userRegistrationModel->attributes);

                // Create User Profile  using relation data.
                UserProfile::createNewUserProfile('user', $userObj, $userRegistrationModel->attributes);

                // Create all wallets.
                Balance::createWallets($userObj);

                // Build a relationship tree.
                User::buildTree($userObj);

                // All ok.
                $transaction->commit();
                Yii::$app->session->setFlash('registrationSuccessful');
                return $this->refresh();
            }  catch(\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('registrationFailed');
                throw $e;
            } catch(\Throwable $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('registrationFailed');
                throw $e;
            }
        }
        
        
//        echo '<pre>'; print_r($model); exit;
        return $this->render('index', [
            'model' => $model,
            'userRegistrationModelForm' => $userRegistrationModel,
            'activeUsers' => $activeUsers
        ]); 
         
//        return $this->render('index');
    }
    
    
   
   
    
    

}
