<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\ResetMasterPinForm;
use app\models\User;
use app\models\Order;
use \app\models\UserProfile;
use yii\web\UploadedFile;
use app\models\Balance;
use app\models\ContactForm;
use app\models\PackagePurchaseForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Utils;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use \app\models\Package;
use \app\models\Transaction;

class PackageController extends Controller {

    public $layout = "user/main";

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['purchase', 'bitcoin-to-usd', ''],
                'rules' => [
                    [
                        'actions' => ['purchase', 'bitcoin-to-usd'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionPurchase($packageId = false) {
//        echo '<pre>'; print_r($_REQUEST); exit;
        $model = new PackagePurchaseForm();
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();

        $package_prices = [];
        $packages = Package::find()->where(['status' => 1])->all();
        $package_data = Package::find()->where(['status' => 1])->all();

        if (!empty($packages)) {
            foreach($packages as $package) {
                if (is_numeric($package->id)) {
                    $package_prices[$package->id] = "Name : " . $package->name . ", Price : " . $package->price;
                }
            }
        }

        if ($packageId) {
            $packageId = base64_decode($packageId);
            $packageObj = Package::findOne($packageId);

            if (empty($packageObj)) { // item does not exist
                throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
            }
            $packagePrice = $packageObj->price;
            $priceInUSD = Utils::convertBTCtoUSD($packagePrice);
            $packageName = $packageObj->name;
        }

//         $data=array();
//        $data["PackagePurchaseForm"]= isset($_REQUEST['package'])? $_REQUEST:'';
//        echo '<pre>'; print_r($data); exit;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//        if ($model->load($data) && $model->validate()) {
            try {
//echo 'HI'; exit;
                $price = Utils::extract_package_price($model->package);

                if (empty($price)) {
                    throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
                }

                $packageObj = Package::find()->where(['status' => 1, 'price' => $price])->one();

                if (empty($packageObj)) { // item does not exist
                    throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
                }

                $curUserObj = Yii::$app->user->identity;

                // Create a transaction.
                $TransactionObj = Transaction::createNewTransaction(null, Yii::$app->params['wallets']['CASH_WALLET'], null, "Package Purchase", $packageObj->price, $packageObj->price, Yii::$app->params['transaction_modes']['package_purchase']);
                
                // Package purchase is only from cash wallet.
                Balance::findAndUpdate($curUserObj->id, Yii::$app->params['wallets']['CASH_WALLET'], $packageObj->price, 'DEBIT');  

                // Create a new ORDER.
                Order::createNewOrder($curUserObj->id, $TransactionObj->id, $packageObj);

                // Add package tgc tokens to TGC wallet.
                //if (is_numeric($packageObj->points) && ( $packageObj->points > 0) ) {
                //  $TransactionObj = Transaction::createNewTransaction($curUserObj->id, Yii::$app->params['secondary_wallets']['PROMO_WALLET'], Yii::$app->params['secondary_wallets']['PROMO_WALLET'], Yii::$app->params['transaction_modes']['points_from_package_purchase'], $packageObj->points, $packageObj->points, Yii::$app->params['transaction_modes']['points_from_package_purchase'], 1);
                //  Balance::findAndUpdate($curUserObj->id, Yii::$app->params['secondary_wallets']['PROMO_WALLET'], $packageObj->points, 'CREDIT');
                //}
                // Give Direct comission.
                $parentObj = User::findOne($curUserObj->parent_id);

                // Do only if user is active.
                if (!empty($parentObj) && $parentObj->status == 1) {
                    $direct_commission_fund = ( $packageObj->price * Yii::$app->params['direct_commission'] ) / 100;
                    Transaction::createNewTransaction($parentObj->id, Yii::$app->params['wallets']['REFERRAL_WALLET'], Yii::$app->params['wallets']['REFERRAL_WALLET'], Yii::$app->params['transaction_modes']['direct_commission'], $direct_commission_fund, $direct_commission_fund, Yii::$app->params['transaction_modes']['direct_commission']);

                    Balance::findAndUpdate($parentObj->id, Yii::$app->params['wallets']['REFERRAL_WALLET'], $direct_commission_fund, 'CREDIT');
                }

                // All ok.
                $transaction->commit();
                Yii::$app->session->setFlash('packagePurchaseSuccess');
                return $this->refresh();

            } catch(\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('packagePurchaseSuccessFailed');
                throw $e;
            } catch(\Throwable $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('packagePurchaseSuccessFailed');
                throw $e;
            }
        }

        return $this->render('purchase', [
            'packagePrices' => $package_prices,
            'packageName' => !empty($packageName) ? $packageName : '',
            'defaultPackagePrice' => !empty($packagePrice) ? $packagePrice : '',
            'defaultPackageUSDPrice' => !empty($priceInUSD) ? $priceInUSD : '',
            'packageData' =>$package_data,
            'model' => $model
        ]);
    }

    /**
     * Api call - Function to convert bit coin value to USD.
     */
    public static function actionBitcoinToUsd($bitcoin = 0) {
        try {
            $jsonData = file_get_contents(Yii::$app->params['API']['BitcoinToUSD']);
            $jsonObj = json_decode($jsonData);
            return ($jsonObj->USD->last * $bitcoin);
        }  catch(\Exception $e) {
            return null;
        }

    }

}
