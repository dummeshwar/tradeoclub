<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\ResetMasterPinForm;
use app\models\User;
use app\models\Order;
use \app\models\MoneyTransfer;
use yii\web\UploadedFile;
use app\models\Balance;
use app\models\ContactForm;
use app\models\MoneyTransferForm;
use app\models\Transaction;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Utils;
use yii\data\Pagination;
use app\models\MoneyTransferSearch;

class MoneyController extends Controller {

    public $layout = "user/main";

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['transfer', 'transactions'],
                'rules' => [
                    [
                        'actions' => ['transfer', 'transactions'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Transfer money.
     *
     * @return string
     */
    public function actionTransfer() {
        $model = new MoneyTransferForm();
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();


        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            try {

                $curUserObj = Yii::$app->user->identity;
                // Check for himself OR other user.
                $toUserObj = !empty($model->to_myself) ? Yii::$app->user->identity : User::findByUsername($model->to_user);


                // User Transaction.
                $money_transfer_charges = Yii::$app->params['transaction_charges']['money_transfer'];
                $user_paid_amount = $model->amount + (($model->amount * $money_transfer_charges) / 100);
                $user_actual_amount = $model->amount;
                $userTransaction = Transaction::createNewTransaction($toUserObj->id, $model->from_wallet, $model->to_wallet, $model->comment, $user_actual_amount, $user_paid_amount, Yii::$app->params['transaction_modes']['money_trasnfer']);

                
                // Admin transaction
                $actual_amount = $paid_amount = ($model->amount * $money_transfer_charges) / 100;
                $adminTransaction = Transaction::createNewTransaction(Yii::$app->params['admin_user_id'], $model->from_wallet, Yii::$app->params['wallets']['CASH_WALLET'], "ADMIN CHARGES", $actual_amount, $paid_amount, Yii::$app->params['transaction_modes']['money_trasnfer']);

                // Find out existing balance of a user - and update Balance.
                // For current user.
                Balance::findAndUpdate($curUserObj->id, $model->from_wallet, $user_paid_amount, 'DEBIT');

                // To user balance update
                Balance::findAndUpdate($toUserObj->id, $model->to_wallet, $model->amount, 'CREDIT');

                // Admin user balance update
                Balance::findAndUpdate(Yii::$app->params['admin_user_id'], Yii::$app->params['wallets']['CASH_WALLET'], $actual_amount, 'CREDIT');

                
                // For user.
                MoneyTransfer::createNewMoneyTransferEntry($curUserObj->id, $toUserObj->id, $model, $model->from_wallet, $model->to_wallet, $userTransaction, $model->amount, Yii::$app->params['ACTIVE']);

                // Admin
                MoneyTransfer::createNewMoneyTransferEntry($curUserObj->id, Yii::$app->params['admin_user_id'], $model, $model->from_wallet, Yii::$app->params['wallets']['CASH_WALLET'], $adminTransaction, $adminTransaction->actual_amount, Yii::$app->params['ACTIVE']);



                // All ok.
                $transaction->commit();
                Yii::$app->session->setFlash('moneyTransferSuccess');
                return $this->refresh();
            }  catch(\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('moneyTransferFailed');
                throw $e;
            } catch(\Throwable $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('moneyTransferFailed');
                throw $e;
            }
        }


        return $this->render('transfer', [
            'model' => $model,
            'allUsers' => User::getActiveUsers(),
            'fromWallet' => Yii::$app->params['from_wallets'],
            'toWallet' => Yii::$app->params['to_wallets'],
        ]);
    }


    /**
     * List all money transfers.
     */
    public function actionTransactions() {
        $searchModel = new MoneyTransferSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('transactions', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


}
