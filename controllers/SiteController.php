<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\RegistrationForm;
use app\models\User;
use app\models\UserProfile;
use app\models\Balance;
use yii\base\Exception;
use app\models\CreateNewPasswordMasterPinForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\MasterPinResetRequestForm;
use app\models\ResetMasterPinForm;
use app\models\ResendPasswordSecurityPinResetLinkForm;
use yii\data\Pagination;
use app\models\Package;
use yii\helpers\Url;
use \app\models\CreatePasswordMasterPinForm;
use \app\models\Genealogy;
use app\models\Transaction;
use app\models\Order;
use Yii\helpers\Utils;

class SiteController extends Controller
{
//    public $layout = "site/main";
     public $layout = "frontpage/main_trading";
//     public $layout = "frontpage/main_mining";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','package-detail'],
                'rules' => [
                    [
                        'actions' => ['logout','package-detail'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
//        if (!Yii::$app->user->isGuest) {
//            return $this->goHome();
//        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            //return $this->goBack();
            return $this->redirect(['user/index']);
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
//        echo "hi"; exit;
        return $this->render('about');
    }
    
    public function actionBlog()
    {
        
        return $this->render('blog');
    }

    public function actionShop()
    {
//        echo "hello"; exit;
        // build a DB query to get all packages with status = 1 and updated_at desc order.
        $query = Package::find()
        ->where(['status' => 1])
        ->orderBy(['updated_at' => SORT_DESC]);

        // get the total number of articles (but do not fetch the article data yet)
        $count = $query->count();

        // create a pagination object with the total count
        $pagination = new Pagination(['totalCount' => $count, 'pageSize' => Yii::$app->params['pageSize']]);


        // limit the query using the pagination and retrieve the packages
        $packages = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('shop', [
             'packages' => $packages,
             'pages' => $pagination,
        ]);
    }

    public function actionPackageDetail($id) {
        $id = base64_decode($id);
//echo Yii::$app->user->identity->id; exit;
        if (empty($id)) { // item does not exist
            throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
        }

        // Check user has sufficient balance OR not to purchase this package.
        $userCash = Utils::walletMoney(Yii::$app->params['wallets']['CASH_WALLET']);

        // build a DB query to get all packages with status = 1 and updated_at desc order.
        $package = Package::find()
                ->where(['id' => $id, 'status' => 1])
                ->orderBy(['updated_at' => SORT_DESC])->one();

        if (empty($package)) { // item does not exist
            throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
        }

        // Check whether user already purchase the package OR not.
        $order = Order::find()
                ->where(['user_id' => Yii::$app->user->identity->id, 'package_id' => $package->id, 'status' => 1])->one();


        return $this->render('packagedetail', [
             'package' => $package,
             'booking_confirm_link' => Url::to(['site/confirm-booking', 'id' => base64_encode($id)]),
             'is_user_has_sufficient_fund' => ($userCash >= $package->price) ? TRUE : FALSE,
             'already_purchased' => empty($order->id) ? TRUE : FALSE,
        ]);
    }

    public function actionConfirmBooking($id) {
        $id = base64_decode($id);

        if (empty($id)) { // item does not exist
            throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
        }

        // build a DB query to get all packages with status = 1 and updated_at desc order.
        $model = Package::find()
                ->where(['id' => $id, 'status' => 1])
                ->orderBy(['updated_at' => SORT_DESC])->one();

        if (empty($model)) { // item does not exist
            throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
        }

        $userObj = Yii::$app->user->identity;

        // Transaction
        //$transactionObj = new Transaction();
        $firstInnerTransaction = Transaction::getDb()->beginTransaction();
        try {
            // $TransactionObj = Transaction::createNewTransaction(null, Yii::$app->params['wallets']['CASH_WALLET'], null, "Package Purchase", $packageObj->price, $packageObj->price, Yii::$app->params['transaction_modes']['package_purchase']);
            $transactionObj = Transaction::createNewTransaction(null, Yii::$app->params['wallets']['CASH_WALLET'], null, Yii::$app->params['transaction_modes']['package_purchase'], $model->price, $model->price, Yii::$app->params['transaction_modes']['package_purchase']);
            // $transactionObj->createNewTransaction($userObj->id, $model, $model->price, $model->price, 'Purchase');            
            $transactionObj->save();
            $firstInnerTransaction->commit();
            
            // Find out existing balance of a user - Balance.
            // Package purchase is only from cash wallet.
            $balanceObj = Balance::find()->where(['user_id' => $userObj->id, 'wallet_id' => Yii::$app->params['wallets']['CASH_WALLET']])->one();
            $secondInnerTransaction = Balance::getDb()->beginTransaction();
            try {
                $fund = $balanceObj->amount - $model->price;
                $balanceObj->updateNewBalance($fund);
                $balanceObj->save(FALSE);
                $secondInnerTransaction->commit();

                // MoneyTransfer
                $order = new Order();
                $thirdInnerTransaction = Order::getDb()->beginTransaction();
                try {
                    $order->createPackageOrder($userObj, $model, $transactionObj);
                    $order->save();
                    $thirdInnerTransaction->commit();

                    // Give direct commission to his parent/ancestor.
                    if(!empty($userObj->parent_id)) {
                        $parentObj = User::findOne($userObj->parent_id);
                    } else {
                        $parentObj = User::findOne(1);
                    }

                    // Make an entry to the UserViewedAds.
                    // UserViewedAds::createEntry($userObj, $model, $order);
                    // Do only if user is active.
                    if ($parentObj->status == 1) {
                        // Transaction entry - For direct commission.
                        $direct_commission_perc = !empty($order->package->direct_commission) ? $order->package->direct_commission : Yii::$app->params['direct_commission'];
                        $direct_commission_fund = ( $order->package->price * $direct_commission_perc ) / 100;
                        $transactionObj = new Transaction();
                        //$transactionObj->createNewEntryInTransaction(1, \Yii::$app->params['payment_gateways']['manual_payment'], $direct_commission_fund, $direct_commission_fund, "Direct Commission", 1, 1, "Direct Commission", "Direct Commission", 3, 3, 1, $parentObj->id);
                        $transactionObj->createNewTransaction($userObj->id, Yii::$app->params['wallets']['REFERRAL_WALLET'], Yii::$app->params['wallets']['REFERRAL_WALLET'], Yii::$app->params['transaction_modes']['direct_commission'], $direct_commission_fund, $direct_commission_fund, Yii::$app->params['transaction_modes']['direct_commission'], $from_user_id = Null);

//$transactionObj->createNewEntryInTransaction(1, \Yii::$app->params['payment_gateways']['manual_payment'], $direct_commission_fund, $direct_commission_fund, "Direct Commission", 1, 1, "Direct Commission", "Direct Commission", 3, 3, 1, $parentObj->id);
//$transactionObj->createNewTransaction($curUserObj->id, Yii::$app->params['wallets']['BINARY_WALLET'], Yii::$app->params['wallets']['BINARY_WALLET'], Yii::$app->params['transaction_modes']['binary_commission'], $binary_commission_fund, $binary_commission_fund, Yii::$app->params['transaction_modes']['binary_commission'], Yii::$app->params['admin_user_id']);
    

                        // Balance update.
                        Balance::updateUserBalance($parentObj->id, 2, $direct_commission_fund);
                    }

                    Yii::$app->session->setFlash('order-success', 'Order sucessfull. Thank you!!!');
                    return $this->redirect(['order/my-orders']);
                } catch(\Throwable $e) {
                    $thirdInnerTransaction->rollBack();
                    throw $e;
                }
            } catch(\Throwable $e) {
                $secondInnerTransaction->rollBack();
                throw $e;
            }

        } catch(\Throwable $e) {
            $firstInnerTransaction->rollBack();
            throw $e;
        }
    }


    public function actionMyacount()
    {
        return $this->render('myacount');
    }
    public function actionContactus()
    {
        return $this->render('contactus');
    }
    public function actionPricing()
    {
        return $this->render('pricing');
    }

    public function actionPayment()
    {
        return $this->render('payment');
    }
    
    

    /**
     * Register action.
     *
     */
    public function actionRegister($ref = null, $pos = null) {
        $userRegistrationModel = new RegistrationForm();
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();

        if (!empty($ref)) {
            $userRegistrationModel->parent_id = $ref;
        }

        if (!empty($pos)) {
            $userRegistrationModel->position = $pos;
        }


        // Get all active users.
        $activeUsers = User::getActiveUsers();

//         echo '<pre>'; print_r($userRegistrationModel->validate()); exit;
        if ($userRegistrationModel->load(Yii::$app->request->post()) && $userRegistrationModel->validate()) {
          
            try {
                // Since sponsor_id/parent_id is string, so we need to convert
                // back parent user id.
                $userRegistrationModel->parent_id = User::findByUsername($userRegistrationModel->parent_id)->id;
                $userObj = User::createNewUser($userRegistrationModel->attributes);
             
                // Create User Profile  using relation data.
                UserProfile::createNewUserProfile('user', $userObj, $userRegistrationModel->attributes);

                // Create all wallets.
                Balance::createWallets($userObj);

                // Build a relationship tree.
                User::buildTree($userObj);

                // All ok.
                $transaction->commit();
                Yii::$app->session->setFlash('registrationSuccessful');
//                return $this->redirect('createNewPasswordMasterpin');
//                $this->generatePasswordResetToken();
                 
//                echo Yii::$app->security->generateRandomString() . '_' . time() ; exit;
               return  Yii::$app->response->redirect(['site/create-new-password-masterpin','token' =>$userObj->password_reset_token ]);
                exit;
                return $this->refresh();
            }  catch(\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('registrationFailed');
                throw $e;
            } catch(\Throwable $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('registrationFailed');
                throw $e;
            }
        }
        return $this->render('register', [
                    'userRegistrationModelForm' => $userRegistrationModel,
                    'activeUsers' => $activeUsers
        ]);
    }


    
   
    public function actionCreateNewPasswordMasterpin($token) {

        try {
            $model = new CreateNewPasswordMasterPinForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->createNewPasswordMasterPinByToken()) {
            // Update only when he creates his full account.
            //User::buildTree($model->_user);
            Yii::$app->session->setFlash('successNewPwdMasterPin');
            //return $this->goHome();
            return $this->redirect(['login']);
        }

        return $this->render('create_new_password_masterpin', [
                    'model' => $model,
        ]);
    }

    /**
     * Password reset action.
     *
     * @return string
     */
    public function actionRequestPasswordReset() {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                //return $this->goHome();
                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }
        return $this->render('request_password_reset_token', [
                    'model' => $model,
        ]);
    }

    /**
     * Resent Password and Key Pin create action.
     * resend-password-security-reset-link
     * @return string
     */
    public function actionResendPasswordSecurityResetLink() {
        $model = new ResendPasswordSecurityPinResetLinkForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
          $user= User::findOne(['email' => $model->email, 'status' => 0, 'is_admin_blocked' => 0]);
            if (!empty($user)) {
              $user->generatePasswordResetToken();
              $user->save(false);
              if ($user->sendEmail(true)) {
                  Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                  //return $this->goHome();
                  return $this->refresh();
              } else {
                  Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
              }
            }

        }
        return $this->render('resend_password_security_reset_link', [
                    'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {
        try {
            $model = new ResetPasswordForm(FALSE, $token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPasswordByToken()) {
            Yii::$app->session->setFlash('success', 'New password saved. Now try login with new credentials.');
            //return $this->goHome();
            return $this->redirect(['login']);
        }
        return $this->render('reset_password', [
                    'model' => $model,
        ]);
    }

    /**
     * Password reset action.
     *
     * @return string
     */
    public function actionRequestMasterPinReset() {
        $model = new MasterPinResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                //return $this->goHome();
                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }
        return $this->render('request_master_pin_reset_token', [
                    'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetMasterPin($token) {
        try {
            $model = new ResetMasterPinForm(FALSE, $token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetMasterPinByToken()) {
            Yii::$app->session->setFlash('success', 'New key pin saved. Now try login with new credentials.');
            //return $this->goHome();
            return $this->redirect(['login']);
        }
        return $this->render('reset_master_pin', [
                    'model' => $model,
        ]);
    }

    /**
     * contact us.
     */
     public function actionBusiness() {
        
        
        return $this->render('business');
     }
}

