<?php

namespace app\controllers\admin;

use Yii;
use app\models\WithdrawalRequestSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\base\Exception;
use app\models\Transaction;
use app\models\User;
use app\models\Balance;
use app\models\ProcessWithdrawalRequestForm;

/**
 * WithDrawal Controller.
 */
class WithdrawalController extends Controller
{
    public $layout = "admin/main";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['process', 'index'],
                'rules' => [
                    [
                        'actions' => ['process', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (isset(Yii::$app->user->identity->is_admin) && Yii::$app->user->identity->is_admin) {
                                return true;
                            }
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * All withdrawal requensts.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WithdrawalRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('withdrawals', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Process withdrawal requests..
     * @return mixed
     */
    public function actionProcess($id)
    {
        if (empty($id) || !is_numeric($id)) {
            throw new NotFoundHttpException("Not found exeception.");
        }

        $transactionObj = Transaction::findOne($id);

        if (!$transactionObj) {
            throw new NotFoundHttpException("Not found exeception.");
        }

        $userObj = User::findOne($transactionObj->user_id);


        if (!$userObj) {
            throw new NotFoundHttpException("Not found exeception.");
        }

        $processWithdralRequest = new ProcessWithdrawalRequestForm();
        $adminApprovalStatus = Yii::$app->params['adminApprovalStatus'];
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();

        if ($processWithdralRequest->load(Yii::$app->request->post()) && $processWithdralRequest->validate()) {
            try {

                if ($processWithdralRequest->is_approved == 1) {
                    // Update the existing transactionObj with admin entered status.
                    // Deduct balance from requested user.
                    Transaction::updateExistingTransaction($transactionObj, $processWithdralRequest);
                    //Balance::findAndUpdate($userObj->id, $transactionObj->from_wallet_id, $transactionObj->paid_amount, 'DEBIT');
                } else {
                    // Update the transation status.
                    Transaction::updateExistingTransaction($transactionObj, $processWithdralRequest);
                    Transaction::createNewTransaction($userObj->id, $transactionObj->from_wallet_id, $transactionObj->from_wallet_id, Yii::$app->params['transaction_modes']['withdrawal_request_rejected'], $transactionObj->actual_amount, $transactionObj->paid_amount, Yii::$app->params['transaction_modes']['withdrawal_request_rejected'], 1);
                    Balance::findAndUpdate($userObj->id, $transactionObj->from_wallet_id, $transactionObj->paid_amount, 'CREDIT');
                }
                // All ok.
                $transaction->commit();
                Yii::$app->session->setFlash('withdrawalRequestSuccess');
                return $this->redirect(['index']);
            }  catch(\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('withdrawalRequestDeclined');
                throw $e;
            } catch(\Throwable $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('withdrawalRequestDeclined');
                throw $e;
            }
        }

        return $this->render('process', [
            'processWithdrawal' => $processWithdralRequest,
            'transaction' => $transactionObj,
            'approvalStatus' => $adminApprovalStatus,
        ]);

    }

}
