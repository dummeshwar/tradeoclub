<?php

namespace app\controllers\admin;

use Yii;
use app\models\AdminPopUps;
use app\models\AdminPopUpsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\base\Exception;

/**
 * AdminPopUpController implements the CRUD actions for AdminPopUps model.
 */
class AdminPopUpController extends Controller
{
  public $layout = "admin/main";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
          'access' => [
              'class' => AccessControl::className(),
              'only' => ['index', 'view', 'create', 'update', 'delete', 'model', 'status-change'],
              'rules' => [
                  [
                      'actions' => ['index', 'view', 'create', 'update', 'delete', 'model', 'status-change'],
                      'allow' => true,
                      'roles' => ['@'],
                      'matchCallback' => function ($rule, $action) {
                          if (isset(Yii::$app->user->identity->is_admin) && Yii::$app->user->identity->is_admin) {
                              return true;
                          }
                      }
                  ],
              ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['POST'],
              ],
          ],
        ];
    }

    /**
     * Lists all AdminPopUps models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdminPopUpsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AdminPopUps model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AdminPopUps model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AdminPopUps();
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
          try {

              $model->image_desktop = UploadedFile::getInstance($model, 'image_desktop');
              $model->image_mobile = UploadedFile::getInstance($model, 'image_mobile');

              if (!empty($model->image_desktop) && $model->upload_desktop_image()) {
                  $model->image_desktop = $model->image_desktop->name;
              }

              if (!empty($model->image_mobile) && $model->upload_mobile_image()) {
                  $model->image_mobile = $model->image_mobile->name;
              }

              if (!$model->save()) {
                  throw new Exception("Something went wrong!. Please try again later.");
              }

              // All ok.
              $transaction->commit();
              Yii::$app->session->setFlash('popUpCreated', 'PopUp created successfully.');
              return $this->refresh();
          }  catch(\Exception $e) {
              $transaction->rollBack();
              Yii::$app->session->setFlash('popUpCreationFailed');
              throw $e;
          } catch(\Throwable $e) {
              $transaction->rollBack();
              Yii::$app->session->setFlash('popUpCreationFailed');
              throw $e;
          }

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AdminPopUps model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $existing_image_mobile = !empty($model->image_mobile) ? $model->image_mobile : NULL;
        $existing_image_desktop = !empty($model->image_desktop) ? $model->image_desktop : NULL;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

          // IF new Image uploaded.
          $model->image_mobile = UploadedFile::getInstance($model, 'image_mobile');
          if (!empty($model->image_mobile) && $model->upload_mobile_image()) {
              $model->image_mobile = $model->image_mobile->name;
          } else {
              // Use old image/Null.
              $model->image_mobile = $existing_image_mobile;
          }

          // IF new Image uploaded.
          $model->image_desktop = UploadedFile::getInstance($model, 'image_desktop');
          if (!empty($model->image_desktop) && $model->upload_desktop_image()) {
              $model->image_desktop = $model->image_desktop->name;
          } else {
              // Use old image/Null.
              $model->image_desktop = $existing_image_desktop;
          }

          if (!$model->save()) {
            throw new Exception("Something went wrong!. Please try again later.");
          }

          return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AdminPopUps model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AdminPopUps model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AdminPopUps the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AdminPopUps::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Change package status.
     */
    public function actionStatusChange() {
        // @todo : Need to change this code.
        $action = Yii::$app->request->post('action');
        $selection = (array)Yii::$app->request->post('selection'); //typecasting
        if (isset($action) && !empty($selection)) {
            foreach($selection as $id){
                $popUpObj = AdminPopUps::findOne((int)$id); //make a typecasting
                // change status to selected action.
                $popUpObj->status = $action;
                $popUpObj->save();
            }
        }
        return $this->redirect(['index']);
    }

}
