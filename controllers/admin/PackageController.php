<?php

namespace app\controllers\admin;

use Yii;
use app\models\Package;
use app\models\PackageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\base\Exception;

/**
 * PackageController implements the CRUD actions for Package model.
 */
class PackageController extends Controller
{
    public $layout = "admin/main";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete', 'model', 'status-change'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'model', 'status-change'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (isset(Yii::$app->user->identity->is_admin) && Yii::$app->user->identity->is_admin) {
                                return true;
                            }
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Package models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PackageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Package model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Package model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
//        echo '<pre>'; print_r($_REQUEST); exit;
      $model = new Package();
      if ($model->load(Yii::$app->request->post()) && $model->validate()) {
        $model->image = UploadedFile::getInstance($model, 'image');

        if (!empty($model->image) && $model->upload()) {
            $model->image = $model->image->name;
        }

        if (!$model->save()) {
            throw new Exception("Something went wrong!. Please try again later.");
        }

        Yii::$app->session->setFlash('packageCreated', 'Package created successfully.');
        return $this->refresh();
      } 
      return $this->render('create', [
          'model' => $model
      ]);
    }

    /**
     * Updates an existing Package model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $existing_package_image = !empty($model->image) ? $model->image : NULL;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // IF new Image uploaded.
            $model->image = UploadedFile::getInstance($model, 'image');
            if (!empty($model->image) && $model->upload()) {
                $model->image = $model->image->name;
            } else {
                // Use old image/Null.
                $model->image = $existing_package_image;
            }

            if (!$model->save()) {
              throw new Exception("Something went wrong!. Please try again later.");
            }
            
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Package model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Package model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Package the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Package::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    /**
     * Change package status.
     */
    public function actionStatusChange() {
        // @todo : Need to change this code.
        $action = Yii::$app->request->post('action');
        $selection = (array)Yii::$app->request->post('selection'); //typecasting
        if (isset($action) && !empty($selection)) {
            foreach($selection as $id){
                $packageObj = Package::findOne((int)$id); //make a typecasting
                // change status to selected action.
                $packageObj->status = $action;
                $packageObj->save();
            }
        }
        return $this->redirect(['index']);
    }
}
