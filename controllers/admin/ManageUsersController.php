<?php

namespace app\controllers\admin;

use Yii;
use app\models\ManageUsers;
use app\models\UserProfile;
use app\models\User;
use app\models\ManageUsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Balance;
use borales\extensions\phoneInput\PhoneInputValidator;
use yii\base\Exception;

/**
 * WithDrawal Controller.
 */
class ManageUsersController extends Controller
{
    public $layout = "admin/main";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete', 'model', 'status-change'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'model', 'status-change'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (isset(Yii::$app->user->identity->is_admin) && Yii::$app->user->identity->is_admin) {
                                return true;
                            }
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ManageUsers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ManageUsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ManageUsers model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ManageUsers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user = new ManageUsers();
        $user->scenario = ManageUsers::SCENARIO_USER_CREATION;
        $userProfile = new UserProfile();
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();


        // Get all active users.
        $activeUsers = User::getActiveUsers();


        if ($user->load(Yii::$app->request->post()) && $userProfile->load(Yii::$app->request->post()) && $user->validate()) {

            try {

                // Create new user by admin.
                $user->createNewUser();
                // Create user profile.
                $userProfile->createNewUserProfileByadmin($user);

                // Create all wallets.
                Balance::createWallets($user);

                // Build a relationship tree.
                User::buildTree($user);
              
                // All ok.
                $transaction->commit();
                return $this->redirect(['view', 'id' => $user->id]);
            }  catch(\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('userCreationFailed');
                throw $e;
            } catch(\Throwable $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('userCreationFailed');
                throw $e;
            }

        }

        return $this->render('create', [
                        'model' => $user,
                        'userProfile' => $userProfile,
                        'activeUsers' => $activeUsers,
        ]);
    }

    /**
     * Updates an existing ManageUsers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        $model = $this->findModel($id);

        $userProfile = UserProfile::find()->where(['id' => $model->userProfiles->id])->one();

        if (!$userProfile) {
            throw new NotFoundHttpException("The user has no profile.");
        }

        // Get all active users.
        $allActiveUsers = User::getActiveUsers();

        $existing_profile_image = !empty($userProfile->picture) ? $userProfile->picture : NULL;
        $existing_statement_image = !empty($userProfile->bank_statement) ? $userProfile->bank_statement : NULL;

        if ($model->load(Yii::$app->request->post()) && $userProfile->load(Yii::$app->request->post()) && $model->validate()) {

            try {

                // Update user (Admin managed users).
                $model->updateUserByAdmin();

                // update user profile.
                $userProfile->updateUserProfileByadmin($model, $existing_profile_image, $existing_statement_image);

                // All ok.
                $transaction->commit();
                return $this->redirect(['view', 'id' => $model->id]);
            }  catch(\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('userUpdationFailed');
                throw $e;
            } catch(\Throwable $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('userUpdationFailed');
                throw $e;
            }
        }

        return $this->render('update', [
            'model' => $model,
            'userProfile' => $userProfile,
            'activeUsers' => $allActiveUsers,
        ]);
    }

    /**
     * Deletes an existing ManageUsers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ManageUsers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ManageUsers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ManageUsers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

     /**
     * Change package status.
     */
    public function actionStatusChange() {
        // @todo : Need to change this code.
        $action = Yii::$app->request->post('action');
        $selection = (array)Yii::$app->request->post('selection'); //typecasting
        if (isset($action) && !empty($selection)) {
            foreach($selection as $id){
                $userObj = User::findOne((int)$id); //make a typecasting
                // change status to selected action.
                $userObj->status = $action;
                $userObj->save(false);
            }
        }
        return $this->redirect(['index']);
    }

}
