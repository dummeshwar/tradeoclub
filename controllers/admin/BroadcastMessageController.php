<?php

namespace app\controllers\admin;

use Yii;
use app\models\BroadcastMessage;
use app\models\BroadcastMessageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * BroadcastMessageController implements the CRUD actions for BroadcastMessage model.
 */
class BroadcastMessageController extends Controller
{
    public $layout = "admin/main";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete', 'model', 'status-change'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'model', 'status-change'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (isset(Yii::$app->user->identity->is_admin) && Yii::$app->user->identity->is_admin) {
                                return true;
                            }
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BroadcastMessage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BroadcastMessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BroadcastMessage model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BroadcastMessage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BroadcastMessage();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
          Yii::$app->session->setFlash('messageCreated', 'BroadcastMessage created successfully.');
          return $this->refresh();
            //return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing BroadcastMessage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BroadcastMessage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BroadcastMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BroadcastMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BroadcastMessage::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Change package status.
     */
    public function actionStatusChange() {
        // @todo : Need to change this code.
        $action = Yii::$app->request->post('action');
        $selection = (array)Yii::$app->request->post('selection'); //typecasting
        if (isset($action) && !empty($selection)) {
            foreach($selection as $id){
                $BroadcastMessageObj = BroadcastMessage::findOne((int)$id); //make a typecasting
                // change status to selected action.
                $BroadcastMessageObj->status = $action;
                $BroadcastMessageObj->save();
            }
        }
        return $this->redirect(['index']);
    }

}
