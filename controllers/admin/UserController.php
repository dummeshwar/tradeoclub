<?php

namespace app\controllers\admin;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\AddFundForm;
use app\models\Wallet;
use app\models\DeductFundForm;
use app\models\Transaction;
use app\models\Balance;
use app\models\MoneyTransfer;
use app\models\BalanceSearch;


class UserController extends Controller
{
    public $layout = "admin/main";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'index', 'tree', 'add-fund', 'deduct-fund', 'wallet-summary'],
                'rules' => [
                    [
                        'actions' => ['logout', 'index','tree', 'add-fund', 'deduct-fund', 'wallet-summary'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (isset(Yii::$app->user->identity->is_admin) && Yii::$app->user->identity->is_admin) {
                                return true;
                            }
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['userCount' => User::find()->where(['status' => 1, 'is_deleted' => 0, 'is_admin' => 0])->count()]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        // Pass extra parameter saying that I'm an admin.
        if ($model->load(Yii::$app->request->post()) && $model->login(true)) {
            $this->redirect(['index']);
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        $this->redirect(['login']);
        //return $this->goHome();
    }

    public function actionTree($username = null) {
        if (!empty($username)) {
            $currentUserObject = User::findOne([
                    'username' => $username
            ]);
        } else {
            $currentUserObject = User::findOne(Yii::$app->user->identity->id);
        }

        $tree_data = $this->printTree($currentUserObject);
        return $this->render('tree_view', ['tree_data' => $tree_data]);
    }


    /**
     * Debit money from user.
     */
    public function actionAddFund() {
        $model = new AddFundForm();
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            try {
                $tragetUserObj = User::findByUsername($model->to_user);

                // Create a transaction.
                $TransactionObj = Transaction::createNewTransaction($tragetUserObj->id, Yii::$app->params['wallets']['CASH_WALLET'], ($model->to_wallet + 1), Yii::$app->params['transaction_modes']['amount_credited'], $model->amount, $model->amount, Yii::$app->params['transaction_modes']['amount_credited']);

                Balance::findAndUpdate($tragetUserObj->id, ($model->to_wallet + 1), $model->amount, 'CREDIT');

                // Make a transfer from admin.
                MoneyTransfer::createNewMoneyTransferEntry(1, $tragetUserObj->id, $model, Yii::$app->params['wallets']['CASH_WALLET'], ($model->to_wallet + 1), $TransactionObj, $model->amount, Yii::$app->params['ACTIVE']);

                // All ok.
                $transaction->commit();
                Yii::$app->session->setFlash('creditSuccessful');
                return $this->refresh();
            }  catch(\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('creditFailed');
                throw $e;
            } catch(\Throwable $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('creditFailed');
                throw $e;
            }
        }

        return $this->render('add_fund', [
            'model' => $model,
            'allUsers' => User::getActiveUsers(),
            'activeWallets' => Wallet::getAllActiveWallets(),
        ]);
    }

    /**
     * Debit money from user.
     */
    public function actionDeductFund() {
        $model = new DeductFundForm();
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            try {
                $tragetUserObj = User::findByUsername($model->from_user);

                // Create a transaction.
                $TransactionObj = Transaction::createNewTransaction(1, ( $model->from_wallet + 1 ), Yii::$app->params['wallets']['CASH_WALLET'], Yii::$app->params['transaction_modes']['amount_debited'], $model->amount, $model->amount, Yii::$app->params['transaction_modes']['amount_debited'], $tragetUserObj->id);

                Balance::findAndUpdate($tragetUserObj->id, ( $model->from_wallet + 1 ), $model->amount, 'DEBIT');
                Balance::findAndUpdate(1, Yii::$app->params['wallets']['CASH_WALLET'], $model->amount, 'CREDIT');

                // Make a transfer from admin to user.
                MoneyTransfer::createNewMoneyTransferEntry($tragetUserObj->id, 1, $model, ( $model->from_wallet + 1 ), Yii::$app->params['wallets']['CASH_WALLET'], $TransactionObj, $model->amount, Yii::$app->params['ACTIVE']);

                // All ok.
                $transaction->commit();
                Yii::$app->session->setFlash('debitSuccessful');
                return $this->refresh();
            }  catch(\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('debittFailed');
                throw $e;
            } catch(\Throwable $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('debittFailed');
                throw $e;
            }
        }
        return $this->render('deduct_fund', [
            'model' => $model,
            'allUsers' => User::getActiveUsers(),
            'activeWallets' => Wallet::getAllActiveWallets(),
        ]);
    }

    /**
     * Lists all Balance models.
     * @return mixed
     */
    public function actionWalletSummary()
    {
        $searchModel = new BalanceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('wallet-summary', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}
