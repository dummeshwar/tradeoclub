<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\RegistrationForm;
use app\models\User;
use app\models\UserProfile;
use app\models\Balance;
use yii\base\Exception;
use app\models\CreateNewPasswordMasterPinForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\MasterPinResetRequestForm;
use app\models\ResetMasterPinForm;
use app\models\Package;
use app\models\UserContactForm;
use app\models\AdminPopUps;
use app\models\BroadcastMessage;


class InnerPageController extends Controller
{
    public $layout = "innerpage/main_mining";

    /**
     * @inheritdoc
     */
    
    public function beforeAction($action) 
{ 
    $this->enableCsrfValidation = false; 
    return parent::beforeAction($action); 
}
    
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
//    public function actionIndex()
//    {
//      $contactFormModel = new UserContactForm();
//      if ($contactFormModel->load(Yii::$app->request->post()) && $contactFormModel->validate() && $contactFormModel->contact(Yii::$app->params['adminEmail'])) {
//          Yii::$app->session->setFlash('contactFormSubmitted');
//          return $this->refresh();
//      }
//
//      $model = Package::find()
//              ->where(['status' => 1])
//              ->orderBy([
//                  'price'=>SORT_ASC,
//              ])
//              ->limit(Yii::$app->params['packageLimit'])
//              ->all();
//
//     $popUps = AdminPopUps::findOne(['status' => 1]);
//
//      return $this->render('index', [
//          'packages' => $model,
//          'contactFormModel' => $contactFormModel,
//          'popUpsObject' => $popUps,
//      ]);
//    }
    
    
    
     public function actionIndex()
    {
//          echo "hi"; exit;
        return $this->render('index');
    }
     public function actionBusiness()
    {
        $layout = "frontpage/main";
        return $this->render('business');
    }
    
    public function actionProfitrex()
    {
        
        return $this->render('profitrex');
    }

    public function actionOpportunity()
    {
        
        return $this->render('opportunity');
    }
    public function actionShop()
    {

        return $this->render('shop');
    }
    
    
    public function actionContactus()
    {

        return $this->render('contactus');
    }
    public function actionFaq()
    {

        return $this->render('faq');
    }
    
    
    public function actionPolicy()
    {

        return $this->render('policy');
    }
    
    public function actionConditions()
    {

        return $this->render('conditions');
    }
    public function actionServices()
    {

        return $this->render('services');
    }
    public function actionTeam()
    {

        return $this->render('team');
    }
    
    public function actionAbout()
    {

        return $this->render('about');
    }
    
    
    public function actionLogin()
    {
//        if (!Yii::$app->user->isGuest) {
//            return $this->goHome();
//        }

       
        $data=array();
        $data["LoginForm"]=Yii::$app->request->post();
        $model = new LoginForm();
        if ($model->load($data) && $model->login()) {
            //return $this->goBack();
//            return $this->redirect(['user/index']);
            return "success";
        }else{
            return "error";
        }
//        return $this->render('login', [
//            'model' => $model,
//        ]);
    }
    
    
    public function actionRegister() {
        
//        echo '<pre>'; print_r(Yii::$app->request->post()); exit;
        $userRegistrationModel = new RegistrationForm();
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
//         echo '<pre>'; print_r($transaction); exit;

        if (!empty($_REQUEST['parent_id'])) {
            $userRegistrationModel->parent_id = (isset($_REQUEST['parent_id']) && $_REQUEST['parent_id'] !=''? $_REQUEST['parent_id']:'');
        }

        if (!empty($_REQUEST['position'])) {
            $userRegistrationModel->position = (isset($_REQUEST['position']) && $_REQUEST['position'] !=''? $_REQUEST['position']:'');
        }


        // Get all active users.
        $activeUsers = User::getActiveUsers();
        $data=array();
        $data["RegistrationForm"]=Yii::$app->request->post();


     
//echo '<pre>'; print_r($userRegistrationModel->load(Yii::$app->request->post())) exit;
        if ($userRegistrationModel->load($data) && $userRegistrationModel->validate()) {
//         echo "hello"; exit;   
            try {  //echo "hi"; exit;  
                // Since sponsor_id/parent_id is string, so we need to convert
                // back parent user id.
                $userRegistrationModel->parent_id = User::findByUsername($userRegistrationModel->parent_id)->id;
                $userObj = User::createNewUser($userRegistrationModel->attributes);

                // Create User Profile  using relation data.
                UserProfile::createNewUserProfile('user', $userObj, $userRegistrationModel->attributes);

                // Create all wallets.
                Balance::createWallets($userObj);

                // Build a relationship tree.
                User::buildTree($userObj);

                // All ok.
                
                $transaction->commit();
                Yii::$app->session->setFlash('registrationSuccessful');
//                return $this->refresh();
                echo "success"; 
                exit;
            }  catch(\Exception $e) {  
                $transaction->rollBack();
                Yii::$app->session->setFlash('registrationFailed');
                
                throw $e;
                
            } catch(\Throwable $e) {   
                $transaction->rollBack();
                Yii::$app->session->setFlash('registrationFailed');
               
                throw $e;
            }
        }
        echo "incorrectDetails" ; exit;
    
//        return $this->render('register', [
//                    'userRegistrationModelForm' => $userRegistrationModel,
//                    'activeUsers' => $activeUsers
//        ]);
    }
    
    
       public function actionRequestPasswordReset() {
//        echo '<pre>'; print_r($_REQUEST); exit;
        $data=array();
        $data["PasswordResetRequestForm"]=Yii::$app->request->post();
        $model = new PasswordResetRequestForm();
        if ($model->load($data) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                //return $this->goHome();
                return $this->refresh();
            } else {
                echo "syserror"; exit;
              
            }
        }
        echo "nouser"; exit;
    }

   
}
