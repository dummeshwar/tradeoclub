<?php

namespace app\controllers;

use Yii;
use app\models\Transaction;
use app\models\TransactionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\DirectReferralSearch;
use app\models\BinaryReferralSearch;
use app\models\DailyReturnsSearch;
use app\models\UserFundSearch;
use app\models\WalletSummarySearch;
use yii\filters\AccessControl;
use app\models\TgsPointsSearch;
use app\models\TgsPointsPurchaseSearch;

/**
 * TransactionController implements the CRUD actions for Transaction model.
 */
class TransactionController extends Controller
{

    public $layout = "user/main";


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['direct-referral-summary', 'binary-referral-summary', 'daily-returns-summary', 'user-fund-summary', 'wallet-summary', 'tgs-points-summary', 'tgs-points-purchase-summary'],
                'rules' => [
                    [
                        'actions' => ['direct-referral-summary', 'binary-referral-summary', 'daily-returns-summary', 'user-fund-summary', 'wallet-summary', 'tgs-points-summary', 'tgs-points-purchase-summary'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Transaction models.
     * @return mixed
     */
    public function actionDirectReferralSummary()
    {
        $searchModel = new DirectReferralSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('direct-referral-summary', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Transaction models.
     * @return mixed
     */
    public function actionBinaryReferralSummary()
    {
        $searchModel = new BinaryReferralSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('binary-referral-summary', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Transaction models.
     * @return mixed
     */
    public function actionDailyReturnsSummary()
    {
        $searchModel = new DailyReturnsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('daily-returns-summary', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Transaction models.
     * @return mixed
     */
    public function actionTgsPointsSummary()
    {
        $searchModel = new TgsPointsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('tgs-points-summary', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Transaction models.
     * @return mixed
     */
    public function actionTgsPointsPurchaseSummary()
    {
        $searchModel = new TgsPointsPurchaseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('tgs-points-purchase-summary', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Transaction models.
     * @return mixed
     */
    public function actionUserFundSummary()
    {
        $searchModel = new UserFundSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('user-add-fund-summary', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Lists all Transaction models.
     * @return mixed
     */
    public function actionWalletSummary()
    {
        $searchModel = new WalletSummarySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('wallet-summary', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


}
