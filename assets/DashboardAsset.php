<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
        'dashboard-assets/plugins/bootstrap/bootstrap.min.css',
        'dashboard-assets/fonts/cryptocoins.css',
        'dashboard-assets/css/simple-line-icons.css',
        'dashboard-assets/css/font-awesome.min.css',
        'dashboard-assets/css/font-awesome-animation.min.css',
        'dashboard-assets/plugins/select2/select2.min.css',
        'dashboard-assets/css/custom.css',
        'dashboard-assets/css/skin-colors/skin-yellow.css',
        'dashboard-assets/css/media.css',
        'dashboard-assets/plugins/rickshaw/rickshaw.min.css',
        'dashboard-assets/css/tree_view.css',
        'https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i',
    ];
    public $js = [
        //'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js',
        //'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
      //  'dashboard-assets/js/jquery.min.js',
        'dashboard-assets/js/jquery.scrollbar.min.js',
        'dashboard-assets/plugins/modernizr/modernizr.custom.js',
        'dashboard-assets/plugins/classie/classie.js',
        'dashboard-assets/plugins/bootstrap/bootstrap.min.js',
        'dashboard-assets/plugins/select2/select2.min.js',
        'dashboard-assets/plugins/highcharts/highcharts.js',
        'dashboard-assets/plugins/highcharts/exporting.js',
        'dashboard-assets/js/charts.js',
        'dashboard-assets/plugins/amcharts/amcharts.js',
        'dashboard-assets/plugins/amcharts/depthChart/serial.js',
        'dashboard-assets/plugins/amcharts/depthChart/export.min.js',
        'dashboard-assets/plugins/amcharts/depthChart/light.js',
        'dashboard-assets/js/charts-amcharts.js',
        'dashboard-assets/js/custom.min.js',
        'dashboard-assets/js/preloader.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}