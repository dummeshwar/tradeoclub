<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use app\assets\AppAsset;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SiteAsset extends AppAsset {

    // public $publishOptions = [
    //     'forceCopy' => true,
    // ];

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
        'css/bootstrap.min.css',
        'css/font-awesome.min.css',
        'css/magnific-popup.css',
        'css/shortcodes/shortcodes.css',
        'css/owl.carousel.css',
        'css/owl.theme.css',
        'css/style.css?4',
        'css/blog.css',
        'css/style-responsive.css',
        'css/default-theme.css',
    ];
    public $js = [
        //'js/jquery-1.10.2.min.js',
        'js/menuzord.js',
        'js/jquery.flexslider-min.js',
        'js/owl.carousel.min.js',
        'js/jquery.flexslider-min.js',
        'js/jquery.isotope.js',
        'js/imagesloaded.js?2',
        'js/jquery.magnific-popup.min.js',
        'js/jquery.countTo.js',
        'js/visible.js',
        'js/smooth.js',
        'js/wow.min.js',
        'js/imagesloaded.js',
        'js/scripts.js?6',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
    ];

}
