<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Utils;
?>


<!-- Cash Wallet: style can be found in dropdown.less -->
<li class="messages-menu bonus-wallet">
    <a href="#" title="Tradeoclub Wallet">
        <i class="fa fa-gift"></i>
        <span class="label label-success"><i class="fa fa-usd"></i><?php echo Utils::walletMoney(Yii::$app->params['wallets']['TRADEOCLUB_WALLET']); ?></span>
    </a>
</li>
