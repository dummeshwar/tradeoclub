<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Utils;
?>


<!-- Cash Wallet: style can be found in dropdown.less -->
<li class="messages-menu referral-wallet">
    <a href="#" title="Referral Wallet">
        <i class="fa fa-users"></i>
        <span class="label label-success"><i class="fa fa-usd"></i><?php echo Utils::walletMoney(Yii::$app->params['wallets']['REFERRAL_WALLET']); ?></span>
    </a>
</li>
