<style>
li{
    /*font-family: "Times New Roman", Times, serif;*/
    font-size: 18px;
}
</style>
<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */
?>
<link rel="icon" type="image/icon" href="/images/favicon/favicon.png">
<div class="top_nav">
            <div class="nav_menu">
              <ul class="nav navbar-nav navbar-left new-navbar-right">
                <li class="toggle-li">
                  <div class="nav toggle burger-nav">
                    <a id="menu_toggle">
                      <div class="burger">
                        <span></span>
                        <span></span>
                        <span></span>
                      </div>
                    </a>
                  </div>
                </li>
              </ul>
                
                    <ul class="nav navbar-nav navbar-right">
                        <?=
                        $this->render('_PROMO_WALLET', ['directoryAsset' => $directoryAsset]);
                        ?>
                        <?=
                        $this->render('_cash_wallet', ['directoryAsset' => $directoryAsset]);
                        ?>
                        <?=
                        $this->render('_binary_wallet', ['directoryAsset' => $directoryAsset]);
                        ?>
                        <?=
                        $this->render('_referral_wallet', ['directoryAsset' => $directoryAsset]);
                        ?>
                        <?=
                        $this->render('_bonus_wallet', ['directoryAsset' => $directoryAsset]);
                        ?>
                    </ul>
            </div>
          </div>