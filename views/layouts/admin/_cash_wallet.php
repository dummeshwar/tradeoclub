<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Utils;
?>


<!-- Cash Wallet: style can be found in dropdown.less -->
<li class="messages-menu cash-wallet">
    <a href="#" title="Cash Wallet">
        <i class="fa fa-money"></i>
        <span class="label label-success"><i class="fa fa-usd"></i><?php echo Utils::walletMoney(Yii::$app->params['wallets']['CASH_WALLET']); ?></span>
    </a>
</li>
