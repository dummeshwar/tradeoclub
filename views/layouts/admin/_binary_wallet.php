<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Utils;
?>


<!-- Cash Wallet: style can be found in dropdown.less -->
<li class="messages-menu binary-wallet">
    <a href="#" title="Binary Wallet">
        <i class="fa fa-street-view"></i>
        <span class="label label-success"><i class="fa fa-usd"></i><?php echo Utils::walletMoney(Yii::$app->params['wallets']['BINARY_WALLET']); ?></span>
    </a>
</li>
