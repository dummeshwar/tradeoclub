<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Utils;
?>

<style>
    .logout_button {
        color: #a1a1a1;
        font-weight: 500;
        font-size: 14px;
        background: none;
        border: none;
        padding: 15px 25px;
    }
</style>

<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <ul class="nav side-menu">
            <li class="active"><a href="<?php echo Url::to(['admin/user/index']);?>"><i class="icon-home icons"></i> <span>Dashboard</span></a></li>
            <li><a href="<?php echo Url::to(['admin/manage-users']);?>"><i class="icon-people icons"></i> <span>User Management</span></a></li>
            <li><a href="<?php echo Url::to(['admin/user/add-fund']);?>"><i class="icon-refresh icons"></i> <span>Add Fund</span></a></li>
            <li><a href="<?php echo Url::to(['admin/user/deduct-fund']);?>"><i class="icon-docs icons"></i> <span>Deduct Fund</span></a></li>
            <li><a href="<?php echo Url::to(['admin/user/wallet-summary']);?>"><i class="icon-screen-desktop icons"></i> <span>Wallet Summary</span></a></li>
            <li><a href="<?php echo Url::to(['admin/withdrawal']);?>"><i class="icon-organization icons"></i> <span>Withdrawals</span></a></li>
            <li><a href="<?php echo Url::to(['admin/package/create']);?>"><i class="fa fa-users"></i> <span>Add Package</span></a></li>
            <li><a href="<?php echo Url::to(['admin/package']);?>"><i class="icon-note icons"></i> <span>Package List</span></a></li>
            <li><a href="<?php echo Url::to(['admin/admin-pop-up/create']);?>"><i class="icon-loop icons"></i> <span>Add Popup</span></a></li>
            <li><a href="<?php echo Url::to(['admin/admin-pop-up']);?>"><i class="icon-chart icons"></i> <span>Popup List</span></a></li>
            <li>
                <?= Html::beginForm(['/site/logout'], 'post', ['class' => '']); ?>
                <?=
                    Html::submitButton(
                        '<i class="icon-power icons" style="font-size: 16px;width: 16px;height: 16px;"></i> <span>Logout</span>', ['class' => 'logout_button']
                    );
                ?>
                <?= Html::endForm(); ?>
            </li>
        </ul>
    </div>
</div>
