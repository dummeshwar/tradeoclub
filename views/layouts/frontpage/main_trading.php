<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\FrontPageAsset;

use yii\helpers\Url;


AppAsset::register($this);
FrontPageAsset::register($this);
?>
<?php $this->beginPage() ?>





    <?=
        $this->render(
                'header_trading.php'
        )
    ?>


    <!--<div class="inner-content">-->
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    <!--</div>-->

    <?=
        $this->render(
                'footer_trading.php'
        )
    ?>

<!--</div>-->


<?php $this->endBody() ?>
</body>
</html>
<?php // $this->endPage() ?>
