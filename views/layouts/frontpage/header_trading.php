<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\FrontPageAsset;

use yii\helpers\Url;


AppAsset::register($this);
FrontPageAsset::register($this);
?>
<html lang="en">
<!-- <![endif]-->
<!-- head -->
<head>
<title>Tradeoclub - Bitcoin Crypto Currency</title>
<meta charset="utf-8" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta name="description" content="Bitcoin Crypto Currency Template" />
<meta name="keywords" content="Bitcoin Crypto Currency Template, Bitcoin, Crypto, Currency, Crypto Currency, Market, Html Template, bitcoin template">
<meta name="author" content="udayraj" />
<meta name="MobileOptimized" content="320" />
<link rel="icon" type="image/icon" href="/images/favicon/favicon.png"> <!-- favicon-icon -->
<!-- theme style -->    
<link href="/css/bootstrap.min.css" rel="stylesheet" /> <!-- bootstrap css -->
<link href="/css/font-awesome.min.css" rel="stylesheet" /> <!-- fontawesome css -->
<link rel="stylesheet" type="text/css" href="/css/pe-icon-7-stroke.css"><!-- peicons css -->
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,500i,700,900" rel="stylesheet"> <!-- google font -->
<link rel="stylesheet" href="/css/menumaker.css"> <!-- menu css -->
<link href="/css/themify-icons.css" rel="stylesheet" /> <!-- themify icons css -->
<link href="/css/owl.carousel.css" rel="stylesheet" /> <!-- owl carousel css -->
<link href="/css/magnific-popup.css" rel="stylesheet" /> <!-- magnify popup css -->
<!-- REVOLUTION STYLE SHEETS -->
<link rel="stylesheet" type="text/css" href="/css/settings.css">
<link rel="stylesheet" type="text/css" href="/css/layers.css">
<link rel="stylesheet" type="text/css" href="/css/navigation.css">
<link href="/css/style.css" rel="stylesheet" /> <!-- custom css -->
<!-- end theme style -->

<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

</head>
<!-- end head -->
<!--body start-->
<body>

<!-- preloader --> 
  <div class="preloader">
    <div class="status">
      <div class="status-message">
      </div>
    </div>
  </div>
<!-- end preloader --> 
<!--  top bar -->
  <div class="top-bar">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <div class="info-bar">
            <p>Building Wealth From The Trending Platform</p>
          </div>          
        </div>
        <div class="col-sm-4 hidden-xs">
          <ul class="top-social-icon">
            <li><a href="http://facebook.com" target="_blank"><i class="ti-facebook" aria-hidden="true"></i></a></li>
            <li><a href="http://twitter.com" target="_blank"><i class="ti-twitter-alt" aria-hidden="true"></i></a></li>
            <li><a href="http://linkedin.com" target="_blank"><i class="ti-linkedin" aria-hidden="true"></i></a></li>
            <li><a href="http://instagram.com" target="_blank"><i class="ti-instagram" aria-hidden="true"></i></a></li>
            <li><a href="http://youtybe.com" target="_blank"><i class="ti-youtube" aria-hidden="true"></i></a></li>
            <!--<li id="google_translate_element" ></li>-->
          </ul>
        </div>
      </div>
    </div>
  </div>
<!--  end top bar -->
<!--  navigation -->
  <div id="nav-bar" class="nav-bar">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="logo">
            <a href="/"><img src="/images/logo1.jpeg" alt="logo"></a>
          </div>
        </div>
        <div class="col-sm-8">
          <div class="row">
           	<div class="col-sm-3">
              <div class="info-nav">
    	       	  <div class="info-nav-heading">Mobile Number</div>
                <div class="info-nav-dtl"><a href="tel:#">+1(201)590-7038</a></div>
              </div>
    	      </div>
    	      <div class="col-sm-3">
              <div class="info-nav">
                <div class="info-nav-heading">Email Address:</div>
                <div class="info-nav-dtl"><a href="mailto:#">info@tradeoclub.com</a></div>
              </div>
            </div>
<!--    	      <div class="col-sm-3">
              <div class="info-nav">
                <div class="info-nav-heading">ISO Certificate</div>
                <div class="info-nav-dtl">9001:2016</div>
              </div>
            </div>-->
<div class="col-sm-3">
              <div class="info-nav">
                <div class="" id="google_translate_element"></div>
                <!--<div class="info-nav-dtl">9001:2016</div>-->
              </div>
            </div>
    	      <div class="col-sm-3">
                   
    	       	<!--<a href="get-quote.html" class="btn btn-default pull-right">Request A Quote</a>-->
                 <?php if (Yii::$app->user->isGuest) { ?>
    	       	<?= Html::a('Login', ['site/login'], ['class' => 'btn btn-default pull-right']) ?>
                <?php } else { ?>
               
                
                            <?= Html::beginForm(['/site/logout'], 'post', ['class' => '']); ?>
                                <?=
                                Html::submitButton(
                                        'Logout (' . Yii::$app->user->identity->username . ')', ['class' => 'logout_button btn btn-default pull-right']
                                );
                                ?>
                                <?= Html::endForm(); ?>
                            
                        <?php }?>
    	      </div>

 
          </div>
        </div>
        <div class="col-sm-10 pad-0">
          <div class="navigation">
            <div id="cssmenu">
              <ul>
                <li class="active"><a href="/">Home</a>
<!--                  <ul>
                    <li><a href="index.html">Home Style 1</a></li>
                    <li><a href="index2.html">Home Style 2</a></li>
                    <li><a href="index3.html">Home Style 3</a></li>
                  </ul>-->
                </li>
        
<!--                <li><a href="#">Pages</a>
                  <ul>
                    <li><a href="about-us.html">About</a></li>   
                    <li><a href="get-quote.html">Get Quote</a></li> 
                    <li><a href="faq.html">Faq</a></li>                       
                    <li><a href="team.html">Team</a></li>  
                    <li><a href="testimonials.html">Testimonials</a></li>   
                  </ul>
                </li>-->
<!--                <li><a href="#">Services</a>
                  <ul>
                    <li><a href="service.html">Services</a></li>
                    <li><a href="whychoose-details.html">Services Single</a></li>
                  </ul>
                </li>
                <li><a href="#">Gallery</a>
                  <ul>
                    <li><a href="gallery-3col.html">Galley 3 Column</a></li>
                    <li><a href="gallery-4col.html">Galley 4 Column</a></li>
                    <li><a href="gallery-3full.html">Galley 3 Full</a></li>
                    <li><a href="gallery-4full.html">Galley 4 Full</a></li>
                  </ul>
                </li>
                <li><a href="#">Blog</a>
                  <ul>
                    <li><a href="blog-grid.html">Blog Grid</a></li>
                    <li><a href="blog-list-left.html">Blog List</a></li>
                    <li><a href="blog-list-right.html">Blog List Right Sidebar</a></li>
                    <li><a href="single-post-left.html">Blog Single Left Sidebar</a></li>
                    <li><a href="single-post-right.html">Blog Single Right Sidebar</a></li>
                  </ul>
                </li>
                <li><a href="#">Shop</a>
                  <ul>
                    <li><a href="shop.html">Shop</a></li>
                    <li><a href="shop-right.html">Shop Right Sidebar</a></li>
                    <li><a href="single-product.html">Single Product</a></li>                    
                    <li><a href="single-product-right.html">Single Product Right Sidebar</a></li>
                    <li><a href="cart.html">Cart</a></li>
                    <li><a href="checkout.html">Checkout</a></li>
                    <li><a href="my-account.html">My Account</a></li>
                    <li><a href="login.html">Login Page</a></li>
                    <li><a href="registration.html">Registration Page</a></li>
                  </ul>
                </li>
                <li><a href="contact.html">Contact</a></li>-->
<!--<li class=""><?= Html::a('Business', ['inner-page/business'], ['class' => '']) ?></li>-->
<!--<li class=""><?= Html::a('Shop', ['site/shop'], ['class' => '']) ?></li>-->
                                <!--<li class=""><?= Html::a('Payment', ['site/payment'], ['class' => '']) ?></li>-->
                                <li class=""><?= Html::a('Blog', ['site/blog'], ['class' => '']) ?></li>
                                <li class=""><?= Html::a('About Us', ['site/about'], ['class' => '']) ?></li>
                                <li class=""><?= Html::a('Contact', ['site/contact'], ['class' => '']) ?></li>
                                
                                <?php if (!Yii::$app->user->isGuest) { ?>
                                <li class=""> <?= Html::a('Dashboard', ['user/index'], ['class' => '']) ?></li>
                                <?php } ?>
                                
                                
                <li class="vinlog">
                   
    	       	<!--<a href="get-quote.html" class="btn btn-default pull-right">Request A Quote</a>-->
                 <?php if (Yii::$app->user->isGuest) { ?>
    	       	<?= Html::a('Login', ['site/login'], ['class' => 'vinlog']) ?>
                <?php } else { ?>
               
                
                            <?= Html::beginForm(['/site/logout'], 'post', ['class' => '']); ?>
                                <?=
                                Html::submitButton(
                                        'Logout (' . Yii::$app->user->identity->username . ')', ['class' => 'logout_button btn']
                                );
                                ?>
                                <?= Html::endForm(); ?>
                            
                        <?php }?>
    	      </li>
                                
              </ul>
            </div>            
          </div>
        </div>
        <div class="col-sm-2 pad-0">
          <div class="nav-right">
            <ul>
              <li class="download-btn">
                <a href="#"><i class="ti-download"></i> Downloads</a>
              </li>
              <li class="search-btn search-icon text-center">
                <a href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
              </li>
            </ul>
          </div>
        </div>
     	</div>  
      <!-- search -->
      <div class="search">
        <div class="container">
          <input type="search" class="search-box" placeholder="Search"/>
          <a href="#" class="fa fa-times search-close"></a>
        </div>
      </div>
      <!-- end search -->      
    </div>
  </div> 
<!--  end navigation -->
