<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\FrontPageAsset;

use yii\helpers\Url;

?>


<footer id="footer" class="footer-main-block">
    <div class="footer-block">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <div class="footer-about footer-widget">
              <!--<img src="images/logo-white.png" class="img-responsive" alt="logo">-->
              <img src="/images/logo1.jpeg" class="img-responsive" alt="logo">
              <p>Bitcoin is the world's first advanced decentralized cash and installment organize. It causes us associate fiscally simply like the Internet has helped us interface socially. Bitcoin is the world's first advanced decentralized cash and installment organize.</p>   
              <div class="footer-social-icon">  
                <ul>
                  <li><a href="http://facebook.com" target="_blank"><i class="ti-facebook" aria-hidden="true"></i></a></li>
                  <li><a href="http://twitter.com" target="_blank"><i class="ti-twitter-alt" aria-hidden="true"></i></a></li>
                  <li><a href="http://linkedin.com" target="_blank"><i class="ti-linkedin" aria-hidden="true"></i></a></li>
                  <li><a href="http://instagram.com" target="_blank"><i class="ti-instagram" aria-hidden="true"></i></a></li>
                  <li><a href="http://youtybe.com" target="_blank"><i class="ti-youtube" aria-hidden="true"></i></a></li>
                </ul> 
              </div>
            </div>            
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="footer-solutions footer-widget">
              <h5 class="footer-heading">Our Services</h5>
              <ul>
                <li><a href="#">Bitcoin Exchange</a></li>
                <li><a href="#">Crypto Investments</a></li>
                <li><a href="#">Bitcoin Consulting</a></li>
                <!--<li><a href="#">Bitcoin Mining</a></li>-->
                <li><a href="#">Escrow Services</a></li>
                <li><a href="#">Bitcoin Shopping</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="footer-news footer-widget">
              <h5 class="footer-heading">Latest Post</h5>
              <ul>
                <li><a href="#">Five Bitcoin Must Reads of 2017</a> - January 01, 2018</li>
                <li><a href="#">Know The Risks Before You Buy</a> - January 05, 2018</li>
                <li><a href="#">Worried about tax on Bitcoin?</a> - January 06, 2018</li>
                <li><a href="#">Future of Bitcoin in 2018 Must Reads</a> - January 07, 2018</li>
              </ul>           
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="footer-contact footer-widget">
              <h5 class="footer-heading">Contact Us</h5>
              <p>If you want to contact us about any issue, our support available to help you 8am-7pm Monday to Saturday.</p>
              <ul class="footer-address">
                <li><span>Address:</span> 140 W 106th Street, New York, NY 10025 USA.</li>
                <li><span>Phone:</span> <a href="tel:#">+1(201)590-7038</a></li>
                <!--<li><span>Fax:</span> <a href="tel:#">+91-123-456-7890</a></li>-->
                <li><span>Email:</span> <a href="mailto:#">info@tradeoclub.com</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>      
    </div>
    <div class="copyright">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <div class="copyright-text">
              <p>Tradeoclub | &copy; 2019 <a href="/"></a>. | All Rights Reserved.</p>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="footer-nav">
              <ul>
                <li><a href="/">Home</a></li>
                <li><a href="#">Documentation</a></li>
                <li><?= Html::a('Contact', ['site/contact'], ['class' => '']) ?></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>




<script src="/js/jquery.min.js"></script> <!-- jquery library js -->
<script src="/js/bootstrap.min.js"></script> <!-- bootstrap js -->
<script src="/js/menumaker.js"></script> <!-- menumaker js -->
<script src="/js/owl.carousel.js"></script> <!-- owl carousel js -->
<script src="/js/jquery.magnific-popup.min.js"></script> <!-- magnify popup js -->
<script src="/js/jquery.elevatezoom.js"></script> <!-- product zoom js -->
<script src="/js/jquery.ajaxchimp.js"></script> <!-- mail chimp js -->
<script src="/js/smooth-scroll.js"></script> <!-- smooth scroll js -->
<script src="/js/waypoints.min.js"></script> <!-- facts count js required for jquery.counterup.js file -->
<script src="/js/jquery.counterup.js"></script> <!-- facts count js-->
<script src="/js/price-slider.js"></script> <!-- price slider / filter js-->
<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="/js/jquery.themepunch.revolution.min.js"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->  
<script type="text/javascript" src="/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="/js/extensions/revolution.extension.video.min.js"></script>
<script src="/js/theme.js"></script>  <!--custom js  -->  
<script>
    (function(b,i,t,C,O,I,N) {
    window.addEventListener('load',function() {
    if(b.getElementById(C))return;
    I=b.createElement(i),N=b.getElementsByTagName(i)[0];
    I.src=t;I.id=C;N.parentNode.insertBefore(I, N);
    },false)
    })(document,'script','https://widgets.bitcoin.com/widget.js','btcwdgt');
</script>   
<!-- end jquery -->

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 10689992;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<noscript>
<a href="https://www.livechatinc.com/chat-with/10689992/" rel="nofollow">Chat with us</a>,
powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a>
</noscript>
<!-- End of LiveChat code -->

</body>
<!--body end -->
</html>