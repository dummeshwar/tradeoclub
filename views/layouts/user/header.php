<style>
    li{
        /*font-family: "Times New Roman", Times, serif;*/
        font-size: 18px;
    }
</style>
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Utils;
use app\models\UserProfileForm;

/* @var $this \yii\web\View */
/* @var $content string */
?>

 <?php $UserProfile = UserProfileForm::findOne([
          'user_id' => Yii::$app->user->id
      ]); 
          ?>
<div class="top_nav">
    <div class="nav_menu">
        <ul class="nav navbar-nav navbar-left new-navbar-right">
            <li class="toggle-li">
                <div class="nav toggle burger-nav">
                    <a id="menu_toggle">
                        <div class="burger">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                </div>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-left">
            <li class="megamenu-li">  
                    <div class="megamenu-dropdown-wrapper">
                    <a class="megamenu-dropdown-trigger megamenu-trigger" href="#0">
                        <i class="icon-wallet icons"></i>
                        <span> The Wallet </span>
                        <span class="fa fa-angle-down"></span>
                    </a>
                    <nav class="megamenu-dropdown">
                        <a href="#0" class="megamenu-close">Close</a>
                        <ul class="megamenu-dropdown-content">
                            <li class="has-children">
                                <a href="#"> Bitcoin Wallet <span style="margin-left: 85px;"><i class="fa fa-btc" title="cash"></i> <?php echo Utils::walletMoney(Yii::$app->params['wallets']['CASH_WALLET']); ?> </span> </a>
<!--                                <ul class="megamenu-dropdown-icons is-hidden">
                                    <li>
                                        <a class="megamenu-dropdown-item item-1" href="search.html">
                                            <h3><i class="cc mmt-icon BTC" title="BTC"></i> All Transactions (BTC)</h3>
                                            <div>View Sent/Received Transactions</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="megamenu-dropdown-item item-2" href="search.html">
                                            <h3><i class="cc mmt-icon BTC" title="BTC"></i> Sent BTC (Transactions)</h3>
                                            <div>In-dept Check Sent Transactions</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="megamenu-dropdown-item item-3" href="search.html">
                                            <h3><i class="cc mmt-icon BTC" title="BTC"></i> Received BTC (Transactions)</h3>
                                            <div>In-dept Check Received Transactions</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="megamenu-dropdown-item item-4" href="search.html">
                                            <h3><i class="cc mmt-icon BTC" title="BTC"></i> Exchanged BTC (Transactions)</h3>
                                            <div>View Sent/Received Exchanges</div>
                                        </a>
                                    </li>
                                </ul>  -->
                            </li>  
                            <li class="has-children">
                                <a href="#">Profit Wallet <span style="margin-left: 58px;"><i class="fa fa-btc" title="cash"></i> <?php echo Utils::walletMoney(Yii::$app->params['wallets']['TRADEOCLUB_WALLET']); ?> </span></a>
<!--                                <ul class="megamenu-dropdown-icons is-hidden">
                                    <li>
                                        <a class="megamenu-dropdown-item item-1" href="search.html">
                                            <h3><i class="cc mmt-icon LTC" title="LTC"></i> All Transactions (LTC)</h3>
                                            <div>View Sent/Received Transactions</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="megamenu-dropdown-item item-2" href="search.html">
                                            <h3><i class="cc mmt-icon LTC" title="LTC"></i> Sent LTC (Transactions)</h3>
                                            <div>In-dept Check Sent Transactions</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="megamenu-dropdown-item item-3" href="search.html">
                                            <h3><i class="cc mmt-icon LTC" title="LTC"></i> Received LTC (Transactions)</h3>
                                            <div>In-dept Check Received Transactions</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="megamenu-dropdown-item item-4" href="search.html">
                                            <h3><i class="cc mmt-icon LTC" title="LTC"></i> Exchanged LTC (Transactions)</h3>
                                            <div>View Sent/Received Exchanges</div>
                                        </a>
                                    </li>
                                </ul>  -->
                            </li>  
                            <li class="has-children">
                                <a href="#">Referral Commission <span style="margin-left: 30px;"><i class="fa fa-btc" title="cash"></i> <?php echo Utils::walletMoney(Yii::$app->params['wallets']['REFERRAL_WALLET']); ?> </span></a>
<!--                                <ul class="megamenu-dropdown-icons is-hidden">
                                    <li>
                                        <a class="megamenu-dropdown-item item-1" href="search.html">
                                            <h3><i class="cc mmt-icon ETH" title="ETH"></i> All Transactions (ETH)</h3>
                                            <div>View Sent/Received Transactions</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="megamenu-dropdown-item item-2" href="search.html">
                                            <h3><i class="cc mmt-icon ETH" title="ETH"></i> Sent ETH (Transactions)</h3>
                                            <div>In-dept Check Sent Transactions</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="megamenu-dropdown-item item-3" href="search.html">
                                            <h3><i class="cc mmt-icon ETH" title="ETH"></i> Received ETH (Transactions)</h3>
                                            <div>In-dept Check Received Transactions</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="megamenu-dropdown-item item-4" href="search.html">
                                            <h3><i class="cc mmt-icon ETH" title="ETH"></i> Exchanged ETH (Transactions)</h3>
                                            <div>View Sent/Received Exchanges</div>
                                        </a>
                                    </li>
                                </ul> -->
                            </li>  
                            <li class="has-children">
                                <a href="#">Binary Commission <span style="margin-left: 36px;"><i class="fa fa-btc" title="cash"></i> <?php echo Utils::walletMoney(Yii::$app->params['wallets']['BINARY_WALLET']); ?> </span></a>
<!--                                <ul class="megamenu-dropdown-icons is-hidden">
                                    <li>
                                        <a class="megamenu-dropdown-item item-1" href="search.html">
                                            <h3><i class="cc mmt-icon DASH" title="DASH"></i> All Transactions (DASH)</h3>
                                            <div>View Sent/Received Transactions</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="megamenu-dropdown-item item-2" href="search.html">
                                            <h3><i class="cc mmt-icon DASH" title="DASH"></i> Sent DASH (Transactions)</h3>
                                            <div>In-dept Check Sent Transactions</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="megamenu-dropdown-item item-3" href="search.html">
                                            <h3><i class="cc mmt-icon DASH" title="DASH"></i> Received DASH (Transactions)</h3>
                                            <div>In-dept Check Received Transactions</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="megamenu-dropdown-item item-4" href="search.html">
                                            <h3><i class="cc mmt-icon DASH" title="DASH"></i> Exchanged DASH (Transactions)</h3>
                                            <div>View Sent/Received Exchanges</div>
                                        </a>
                                    </li>
                                </ul>  -->
                            </li>  
<!--                            <li class="has-children">
                                <a href="#">Promo Wallet <span style="margin-left: 71px;"><i class="fa fa-btc" title="cash"></i> <?php echo Utils::walletMoney(Yii::$app->params['secondary_wallets']['PROMO_WALLET']); ?> </span></a>
                            </li>-->
<!--                            <li>
                                <a href="#"><i class="icon-wallet icons cc"></i> Add New Wallet</a>
                            </li>-->
                        </ul>  
                    </nav>  
                </div>  
            </li>
            <li class="dropdown-menu-1-li">
                <div class="dropdown-menu-1 dropdown">
                    <a class="dropdown-toggle" href="<?php echo Url::toRoute(['transaction/wallet-summary']); ?>"><i class="icon-feed icons"></i> Reports
                        <!--<span class="fa fa-angle-down"></span>-->
                    </a>
<!--                    <ul class="dropdown-menu archive">
                        <li><a href="crypto_address.html"><span class="pull-left">...8b0133 (0.01 BTC)</span> <span class="label label-success pull-right">Success</span></a></li>
                        <li><a href="crypto_address.html"><span class="pull-left">...9c3286 (0.07 BTC)</span> <span class="label label-success pull-right">Success</span></a></li>
                        <li><a href="crypto_address.html"><span class="pull-left">...4a3432 (0.44 LTC)</span> <span class="label label-danger pull-right">Canceled</span></a></li>
                        <li><a href="crypto_address.html"><span class="pull-left">...1e8437 (0.91 BTC)</span> <span class="label label-warning pull-right">Pending</span></a></li>
                        <li><a href="crypto_address.html"><span class="pull-left">...8b0133 (3.51 LTC)</span> <span class="label label-success pull-right">Success</span></a></li>
                        <li><a href="crypto_address.html"><span class="pull-left">...9c3286 (0.07 ETH)</span> <span class="label label-warning pull-right">Pending</span></a></li>
                        <li><a href="crypto_address.html"><span class="pull-left">...8b0133 (0.01 BTC)</span> <span class="label label-success pull-right">Success</span></a></li>
                        <li><a href="crypto_address.html"><span class="pull-left">...9c3286 (0.07 BTC)</span> <span class="label label-success pull-right">Success</span></a></li>
                    </ul>-->
                </div>
            </li>
<!--            <li class="button-night-mode">Night Mode
                <button type="button" class="btn btn-sm btn-toggle" data-toggle="button" aria-pressed="true" autocomplete="off">
                    <div class="handle"></div>
                </button>
            </li>
            <li class="button-menu-right">Menu Right
                <button type="button" class="btn btn-sm btn-toggle" data-toggle="button" aria-pressed="true" autocomplete="off">
                    <div class="handle"></div>
                </button>
            </li>-->
        </ul>  <!-- top menu ul -->
        <ul class="nav navbar-nav navbar-right">
<!--            <li id="st-trigger-effects" class="">
                <a data-effect="st-effect" class="trigger-sidebar">
                    <i class="fa fa-th"></i>
                </a>
            </li>-->
            <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <!--<img src="/dashboard-assets/images/profile-pic.jpg" alt="">-->
                    <?php if(!empty($UserProfile->picture)){ ?>
                    <img src="/uploads/profile_pics/<?= $UserProfile->picture ?>" alt="profile">
                    <?php }else{ ?>
                     <img src="/dashboard-assets/images/profile-pic.jpg" alt="">
                     <?php }?>
                </a>
              
                
                <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo Url::toRoute(['user/profile']); ?>"> Profile</a></li>
<!--                    <li>
                        <a href="javascript:;">
                            <span class="badge bg-red pull-right">50%</span>
                            <span>Settings</span>
                        </a>
                    </li>-->
                    <!--<li><a href="<?php echo Url::toRoute(['inner-page/faq']); ?>">Help</a></li>-->
                    <li>
                        <?= Html::beginForm(['/site/logout'], 'post', ['class' => '']); ?>
                        <?=
                            Html::submitButton(
                                '<span style="color:#252525"> <i class="fa fa-sign-out pull-right" style="margin-top:4px"></i> Logout</span>', ['class' => 'logout_button']
                            );
                        ?>
                        <?= Html::endForm(); ?>
                        <!--<a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a>-->
                    </li>
                </ul>
            </li>
            <li role="presentation" class="dropdown">
<!--                <a href="javascript:;" class="dropdown-toggle info-number faa-horizontal" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope faa-horizontal animated"></i>
                    <span class="badge faa-horizontal animated">3</span>
                </a>-->
                <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                        <a>
                            <span class="image"><img src="/dashboard-assets/images/profile-pic.jpg" alt="Profile Image"/></span>
                            <span>
                                <span>John Smith</span>
                                <span class="time">3 mins ago</span>
                            </span>
                            <span class="message">
                                Film festivals used to be do-or-die moments for movie makers...
                            </span>
                        </a>
                    </li>
                    <li>
                        <a>
                            <span class="image"><img src="/dashboard-assets/images/profile-pic.jpg" alt="Profile Image"/></span>
                            <span>
                                <span>John Smith</span>
                                <span class="time">4 mins ago</span>
                            </span>
                            <span class="message">
                                Film festivals used to be do-or-die moments for movie makers...
                            </span>
                        </a>
                    </li>
                    <li>
                        <a>
                            <span class="image"><img src="/dashboard-assets/images/profile-pic.jpg" alt="Profile Image"/></span>
                            <span>
                                <span>John Smith</span>
                                <span class="time">6 mins ago</span>
                            </span>
                            <span class="message">
                                Film festivals used to be do-or-die moments for movie makers...
                            </span>
                        </a>
                    </li>
                    <li>
                        <div class="text-center">
                            <a>
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
<!--            <li class="search-wrap">
                <a href="javascript:;" id="btn-search" class="search-nav">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </a>
            </li>-->
            <li style="margin-top: 21px;" id="google_translate_element">
<!--                <a href="javascript:;" class="lang-swich dropdown-toggle" id="google_translate_element" data-toggle="dropdown" aria-expanded="false">
                    <img src="/dashboard-assets/images/united-kingdom.svg" alt="uk">
                </a>-->
<!--                <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"><img src="/dashboard-assets/images/france.svg" alt="fr"><span>France</span></a></li>
                    <li><a href="javascript:;"><img src="/dashboard-assets/images/germany.svg" alt="de"><span>Germany</span></a></li>
                    <li><a href="javascript:;"><img src="/dashboard-assets/images/italy.svg" alt="it"><span>Italy</span></a></li>
                    <li><a href="javascript:;"><img src="/dashboard-assets/images/spain.svg" alt="es"><span>Spain</span></a></li>
                </ul>-->
            </li>
<!--<li id="google_translate_element"></li>-->
        </ul>

    </div>
</div>

<!--<nav class="navbar navbar-inverse pl-header">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">
        <img class="logo" src="/images/logo.png" style="padding-top:0px" />
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo Url::toRoute(['user/index']); ?>">DASHBOARD</a></li>
        <li><a href="<?php echo Url::toRoute(['user/profile']); ?>">MEMBER'S AREA</a></li>
        <li><a href="<?php echo Url::toRoute(['package/purchase']); ?>">PACKAGE</a></li>
        <li><a href="<?php echo Url::toRoute(['user/profit-calculator']); ?>">CALCULATOR</a></li>
        <li><a href="<?php echo Url::toRoute(['site/payment']); ?>">PAYMENT</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li>
        <a class="dropdown-toggle" data-toggle="dropdown"> <?= Yii::$app->user->identity->username ?> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a tabindex="-1" href="<?php echo Url::toRoute(['user/profile']); ?>">Profile</a></li>
            <li><a tabindex="-1" href="<?php echo Url::toRoute(['order/my-orders']); ?>">Orders</a></li>
            <li><a tabindex="-1" href="<?php echo Url::toRoute(['user/my-tree']); ?>">Tree</a></li>
            <li><a tabindex="-1" href="<?php echo Url::toRoute(['user/my-downline-users']); ?>">Downline</a></li>
            <li><a tabindex="-1" href="<?php echo Url::toRoute(['money/transfer']); ?>">Transfer</a></li>
            <li><a tabindex="-1" href="<?php echo Url::toRoute(['withdrawal/request']); ?>">Withdraw</a></li>
            <li><a tabindex="-1" href="<?php echo Url::toRoute(['transaction/wallet-summary']); ?>">Wallet Summary</a></li>
            <li><?= Html::beginForm(['/site/logout'], 'post', ['class' => '']); ?>
<?=
Html::submitButton(
        'Logout (' . Yii::$app->user->identity->username . ')', ['class' => 'logout_button_user']
);
?>
<?= Html::endForm(); ?>
            </li>
             <li class="dropdown-submenu">
              <a class="test" tabindex="-1" href="#">New dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a tabindex="-1" href="#">2nd level dropdown</a></li>
                <li><a tabindex="-1" href="#">2nd level dropdown</a></li>                              
              </ul>
            </li> 
          </ul>
        </li>
      </ul>
       <ul class="nav navbar-nav navbar-right">
        <li class="mail-tab">
          <div class="icon-mail">
            <a class="dropdown-toggle" data-toggle="dropdown"><span class="members-name">Peter Anthony</span> <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a tabindex="-1" href="#">English</a></li>
              <li><a tabindex="-1" href="#">Plans</a></li>
              <li><a tabindex="-1" href="#">Statement</a></li>
              <li><a tabindex="-1" href="#">Deposit</a></li>
              <li><a tabindex="-1" href="#">Withdrow</a></li>
              <li><a tabindex="-1" href="#">Edit profile</a></li>
              <li><a tabindex="-1" href="#">Guidelines</a></li>
              <li><a tabindex="-1" href="#">Refer and Earn</a></li>
              <li><a tabindex="-1" href="#">Logout</a></li>
            <li class="dropdown-submenu">
                <a class="test" tabindex="-1" href="#">New dropdown <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a tabindex="-1" href="#">2nd level dropdown</a></li>
                  <li><a tabindex="-1" href="#">2nd level dropdown</a></li>                              
                </ul>
              </li> 
            </ul> 
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>-->
