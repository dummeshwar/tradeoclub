<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<div class="container">
    <div class="sub-container">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>