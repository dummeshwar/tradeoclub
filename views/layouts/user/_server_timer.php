<?php $today = getdate(); ?>
<script>
    var d = new Date(Date.UTC(<?php echo $today['year'].",".$today['mon'].",".$today['mday'].",".$today['hours'].",".$today['minutes'].",".$today['seconds']; ?>));
    setInterval(function() {
        d.setSeconds(d.getSeconds() + 1);
        var hours = ( d.getHours() < 10 ) ? "0" + d.getHours() : d.getHours();
        var minutes = ( d.getMinutes() < 10 ) ? "0" + d.getMinutes() : d.getMinutes();
        var seconds = ( d.getSeconds() < 10 ) ? "0" + d.getSeconds() : d.getSeconds();
        $('#timer').text((hours +':' + minutes + ':' + seconds ));
    }, 1000);
</script>


<!-- Cash Wallet: style can be found in dropdown.less -->
<li class="messages-menu cash-wallet hidden-xs hidden-sm">
    <a href="#">
        <strong>Server Time :  <?php echo date('M d, Y'); ?> - </strong><label id="timer"></label>
    </a>
</li>
