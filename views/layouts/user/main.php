<style>
.copyrights{
    padding: 15px 0;
    z-index: 1;
    display: block;
    position: relative;
    background-color: #fed602;
    color: #1f2839;
}

ul.dropdown-menu {
    font-family: "Times New Roman", Times, serif;
}
</style>
<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */


if (Yii::$app->controller->action->id === 'login') { 
/**
 * Do not use this code in your template. Remove it. 
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {

    if (class_exists('backend\assets\DashboardAsset')) {
        backend\assets\DashboardAsset::register($this);
    } else {
        app\assets\DashboardAsset::register($this);
    }

//    dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
   // echo '<pre>'; print_r($directoryAsset); exit;
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        
        <link rel="shortcut icon" href="/images/favicon/favicon.png" type="image/x-icon">
        <link rel="icon" href="/images/favicon/favicon.png" type="image/x-icon">
        
        
        <script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    </head>
    <body class="nav-md preloader-off">
    <div class="pace-cover"></div>
    <div id="st-container" class="st-container st-effect">
        <div class="container body">
            <div class="main_container">
                <?php $this->beginBody() ?>
                 <?= $this->render(
                    'header.php',
                    ['directoryAsset' => $directoryAsset]
                ) ?>
                <div class="col-md-3 left_col">
                    <div class="scroll-view">
                      <div class="navbar nav_title">
                        <h1 class="logo_wrapper">
                          <a href="/" class="site_logo">
                            <img class="logo" src="/images/logo1.jpeg" alt="cryptic logo">
                            <!--<span class="logo-text">Tradeoclub</span>-->
                          </a>
                        </h1>
                      </div>
                      <div class="clearfix"></div>
                      
                        <?= $this->render(
                            'left.php',
                            ['directoryAsset' => $directoryAsset]
                        ) ?>
                      
                    </div>
                </div>

                <?= $this->render(
                    'content.php',
                    ['content' => $content, 'directoryAsset' => $directoryAsset]
                ) ?>
                
<!--                <div class="copyrights text-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                Copyrights © 2018 <a href="#">Mining</a> All Rights Reserved.                            
                            </div> end col 
                        </div> end row 
                    </div> end container 
                </div>  -->
                
            </div>
        </div>
    </div>
    
    <?php $this->endBody() ?>
    
    <!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 10689992;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<noscript>
<a href="https://www.livechatinc.com/chat-with/10689992/" rel="nofollow">Chat with us</a>,
powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a>
</noscript>
<!-- End of LiveChat code -->
    
    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>