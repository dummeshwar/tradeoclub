<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Utils;
?>

<!-- User Account: style can be found in dropdown.less -->

<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
<?= Html::img(Url::to(Utils::getUserPicture()), ['class' => "user-image", 'alt' => "User Image"]); ?>

        <span class="hidden-xs"><?php echo Yii::$app->user->identity->username; ?></span>
    </a>
    <ul class="dropdown-menu">
        <!-- User image -->
        <li class="user-header">
                <?= Html::img(Url::to(Utils::getUserPicture()), ['class' => "img-circle", 'alt' => "User Image"]); ?>
            <p>
                <?php echo Yii::$app->user->identity->username; ?>
                <small>Member since <?php echo Yii::$app->formatter->format(Yii::$app->user->identity->created_at, 'date'); ?></small>
            </p>
        </li>

        <!-- Menu Footer-->
        <li class="user-footer">
            <div class="pull-left">
                <?=
                Html::a(
                        'Profile', ['/user/profile'], ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                )
                ?>
            </div>
            <div class="pull-right">
                <?=
                Html::a(
                        'Log out', ['/site/logout'], ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                )
                ?>
            </div>
        </li>
    </ul>
</li>