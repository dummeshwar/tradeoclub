<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Utils;
?>


<!-- Cash Wallet: style can be found in dropdown.less -->
<li class="messages-menu cash-wallet hidden-xs hidden-sm" style="width: 80px">
    <a href="#" title="Cash Wallet">
        <i class="fa fa-money dashboard_icon"></i>
        <span class="label label-success"><i class="fa fa-usd"></i> <?php echo number_format(Utils::walletMoney(Yii::$app->params['wallets']['CASH_WALLET']), 0); ?></span>
    </a>
</li>
