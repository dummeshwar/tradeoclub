<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Utils;
?>

<style>
    .logout_button {
        color: #a1a1a1;
        font-weight: 500;
        font-size: 14px;
        background: none;
        border: none;
        padding: 15px 25px;
    }
</style>

<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <ul class="nav side-menu">
            <li class="<?php echo (Yii::$app->controller->id == 'user' && Yii::$app->controller->action->id == 'index') ? 'active' : '' ?>"><a href="<?php echo Url::to(['user/index']);?>"><i class="icon-home icons"></i> <span>Dashboard</span></a></li>
            <li class="<?php echo (Yii::$app->controller->id == 'user' && Yii::$app->controller->action->id == 'profile') ? 'active' : '' ?>"><a href="<?php echo Url::to(['user/profile']);?>"><i class="icon-people icons"></i> <span>Profile</span></a></li>
            <!--<li class="<?php echo (Yii::$app->controller->id == 'package' && Yii::$app->controller->action->id == 'purchase') ? 'active' : '' ?>"><a href="<?php echo Url::to(['package/purchase']);?>"><i class="icon-refresh icons"></i> <span>Package</span></a></li>-->
            <li class="<?php echo (Yii::$app->controller->id == 'order' && Yii::$app->controller->action->id == 'my-orders') ? 'active' : '' ?>"><a href="<?php echo Url::to(['order/my-orders']);?>"><i class="icon-docs icons"></i> <span>Orders</span></a></li>
            <li class="<?php echo (Yii::$app->controller->id == 'user' && Yii::$app->controller->action->id == 'add-fund') ? 'active' : '' ?>"><a href="<?php echo Url::to(['user/add-fund']);?>"><i class="icon-screen-desktop icons"></i> <span>Add Fund</span></a></li>
            <li class="<?php echo (Yii::$app->controller->id == 'user' && Yii::$app->controller->action->id == 'my-tree') ? 'active' : '' ?>"><a href="<?php echo Url::to(['user/my-tree']);?>"><i class="icon-organization icons"></i> <span>Tree</span></a></li>
            <li class="<?php echo (Yii::$app->controller->id == 'user' && Yii::$app->controller->action->id == 'my-downline-users') ? 'active' : '' ?>"><a href="<?php echo Url::to(['user/my-downline-users']);?>"><i class="fa fa-users"></i> <span>Downline</span></a></li>
            <li class="<?php echo (Yii::$app->controller->id == 'withdrawal' && Yii::$app->controller->action->id == 'request') ? 'active' : '' ?>"><a href="<?php echo Url::to(['withdrawal/request']);?>"><i class="icon-loop icons"></i> <span>Withdraw</span></a></li>
            <li class="<?php echo (Yii::$app->controller->id == 'transaction' && Yii::$app->controller->action->id == 'wallet-summary') ? 'active' : '' ?>"><a href="<?php echo Url::to(['transaction/wallet-summary']);?>"><i class="icon-chart icons"></i> <span>Wallet Summary</span></a></li>
            <li>
                <?= Html::beginForm(['/site/logout'], 'post', ['class' => '']); ?>
                <?=
                    Html::submitButton(
                        '<i class="icon-power icons" style="font-size: 16px;width: 16px;height: 16px;"></i> <span>Logout</span>', ['class' => 'logout_button']
                    );
                ?>
                <?= Html::endForm(); ?>
            </li>
        </ul>
    </div>
</div>
