<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Utils;
?>


<!-- Cash Wallet: style can be found in dropdown.less -->
<li class="messages-menu tcg-wallet" style="width: 80px">
    <a href="#" title="PROMO Wallet">
        <i class="fa fa-inbox dashboard_icon"></i>
        <span class="label label-success"><i class="fa fa-hand-pointer-o"></i> <?php echo number_format(Utils::walletMoney(Yii::$app->params['secondary_wallets']['PROMO_WALLET']), 0); ?></span>
    </a>
</li>
