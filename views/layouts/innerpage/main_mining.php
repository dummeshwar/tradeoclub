<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\FrontPageAsset;

use yii\helpers\Url;


AppAsset::register($this);
FrontPageAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<!-- META -->

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="keywords" content="" />

    <meta name="author" content="" />

    <meta name="robots" content="" />    

    <meta name="description" content="" />

    

    <!-- FAVICONS ICON -->

    <link rel="icon" href="images/favicon.html" type="image/x-icon" />

    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />

    

    <!-- PAGE TITLE HERE -->

    <title>Bit-invest Template | Home Page 1</title>

    

    <!-- MOBILE SPECIFIC -->

    <meta name="viewport" content="width=device-width, initial-scale=1">

    

    <!-- [if lt IE 9]>

        <script src="js/html5shiv.min.js"></script>

        <script src="js/respond.min.js"></script>

	<![endif] -->

    

    <!-- BOOTSTRAP STYLE SHEET -->

    <link rel="stylesheet" type="text/css" href="css_mining/bootstrap.min.css">

    <!-- FONTAWESOME STYLE SHEET -->

    <link rel="stylesheet" type="text/css" href="css_mining/fontawesome/css/font-awesome.min.css" />

    <!-- FLATICON STYLE SHEET -->

    <link rel="stylesheet" type="text/css" href="css_mining/flaticon.min.css">

    <!-- ANIMATE STYLE SHEET --> 

    <link rel="stylesheet" type="text/css" href="css_mining/animate.min.css">

    <!-- OWL CAROUSEL STYLE SHEET -->

    <link rel="stylesheet" type="text/css" href="css_mining/owl.carousel.min.css">

    <!-- BOOTSTRAP SELECT BOX STYLE SHEET -->

    <link rel="stylesheet" type="text/css" href="css_mining/bootstrap-select.min.css">

    <!-- MAGNIFIC POPUP STYLE SHEET -->

    <link rel="stylesheet" type="text/css" href="css_mining/magnific-popup.min.css">

    <!-- LOADER STYLE SHEET -->

    <link rel="stylesheet" type="text/css" href="css_mining/loader.min.css">    

    <!-- MAIN STYLE SHEET -->

    <link rel="stylesheet" type="text/css" href="css_mining/style.css">

    <!-- THEME COLOR CHANGE STYLE SHEET -->

    <link rel="stylesheet" class="skin" type="text/css" href="css_mining/skin/skin-1.css">

    <!-- CUSTOM  STYLE SHEET -->

    <link rel="stylesheet" type="text/css" href="css_mining/custom.css">

    <!-- SIDE SWITCHER STYLE SHEET -->

    <link rel="stylesheet" type="text/css" href="css_mining/switcher.css">    



    

    <!-- REVOLUTION SLIDER CSS -->

    <link rel="stylesheet" type="text/css" href="plugins/revolution/revolution/css/settings.css">

    <!-- REVOLUTION NAVIGATION STYLE -->

    <link rel="stylesheet" type="text/css" href="plugins/revolution/revolution/css/navigation.css">

    

    <!-- GOOGLE FONTS -->

<!--	<link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">  

	<link href="https://fonts.googleapis.com/css?family=Crete+Round:400,400i&amp;subset=latin-ext" rel="stylesheet">-->
         <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,300italic,400italic,500,500italic,700,700italic,900italic,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,800italic,800,700italic' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Crete+Round:400,400i&amp;subset=latin-ext" rel="stylesheet">

  

 

</head>

<body>
<?php $this->beginBody() ?>

<!--  preloader start -->
<div id="tb-preloader">
    <div class="tb-preloader-wave"></div>
</div>
<!-- preloader end -->

<div class="wrapper" id="home">

    <?=
        $this->render(
                'header_mining.php'
        )
    ?>


    <div class="inner-content">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>

    <?=
        $this->render(
                'footer_mining.php'
        )
    ?>

</div>


<?php $this->endBody() ?>
</body>
</html>
<?php // $this->endPage() ?>
