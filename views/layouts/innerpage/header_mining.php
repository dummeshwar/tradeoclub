<?php 

use yii\helpers\Html;

?> 



<?php if (Yii::$app->session->hasFlash('registrationSuccessful')): ?>
        <div class="alert alert-success">
            Registration is <strong>Successful!</strong> . Please check your mail for further details.
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('registrationFailed')): ?>
        <div class="alert alert-danger">
            Registration is <strong>Failed!</strong> . Something went wrong.
        </div>
    <?php endif; ?>

<!-- HEADER START -->
        <header class="site-header header-style-6">
        
            <div class="top-bar bg-primary">
                <div class="container">
                    <div class="row">
                        <div class="clearfix">
                            <div class="wt-topbar-left">
                                <ul class="list-unstyled e-p-bx pull-left">
                                    <li><i class="fa fa-envelope"></i>mail@bitinvest.com</li>
                                    <li><i class="fa fa-phone"></i>(654) 321-7654</li>
                                </ul>
                            </div>
                            
                            <div class="wt-topbar-right">
<!--                                <div class=" language-select pull-right">
                                      <div class="dropdown">
                                            <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Language
                                            <span class="caret"></span></button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                              <li><a href="#"><img src="images/united-states.png" alt="">English</a></li>
                                              <li><a href="#"><img src="images/france.png" alt="">French</a></li>
                                              <li><a href="#"><img src="images/germany.png" alt="">German</a></li>
                                            </ul>
                                      </div>
                                </div>-->
                                
                                <ul class="list-unstyled e-p-bx pull-right">
<!--                                    <li><a href="#" data-toggle="modal" data-target="#Login-form"><i class="fa fa-user"></i>Login</a></li>
                                    <li><a href="#" data-toggle="modal" data-target="#Register-form"><i class="fa fa-sign-in"></i>Register</a></li>-->
                                    
                                    
                                        
                                <?php if (Yii::$app->user->isGuest) { ?>
                                      <li><a href="#" data-toggle="modal" data-target="#Login-form"><i class="fa fa-user"></i>Login</a></li>
                                   
                                    <li><a href="#" data-toggle="modal" data-target="#Register-form"><i class="fa fa-sign-in"></i>Register</a></li>
<!--                            <li class="li_home">
                                <?= Html::a('Login', ['site/login'], ['class' => '']) ?>
                            </li>-->
                            <?php } else { ?>
                           <li class="li_home " style="float:left;"><?= Html::a('Dashboard', ['user/index'], ['class' => '']) ?></li>
                            <li class="li_home" style="float:right;"><?= Html::beginForm(['/site/logout'], 'post', ['class' => '']); ?>
                                <?=
                                Html::submitButton(
                                        'Logout (' . Yii::$app->user->identity->username . ')', ['class' => 'logout_button','style' =>'background:#1bbce8; margin-right:28px']);
                                
                            
//                                 Html::a('Logout (' . Yii::$app->user->identity->username . ')',['class' => 'logout_button']);
                                ?>
                                <?= Html::endForm(); ?>
                            </li>
                        <?php }?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Search Link -->

            <!-- Search Form -->
            <div class="main-bar header-middle bg-white">
                <div class="container">
                    <div class="logo-header">
                        <a href="/">
                            <img src="images/logo-dark.png" width="216" height="37" alt="" />
                        </a>
                    </div>
                    <div class="header-info">
                        <ul>
                            <li>
                                <div>
                                    <div class="icon-sm">
                                        <span class="icon-cell  text-primary"><i class="iconmoon-travel"></i></span>
                                    </div>
                                    <div class="icon-content">
                                        <strong>Our Location </strong>
                                        <span>145 N Los Ave, NY</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                     <div class="icon-sm">
                                        <span class="icon-cell  text-primary"><i class="iconmoon-smartphone-1"></i></span>
                                    </div>
                                    <div class="icon-content">
                                        <strong>Phone Number</strong>
                                        <span>1500-2309-0202</span>
                                    </div>
                                </div>
                            </li>
                            <li class="btn-col-last">
                                <a class="site-button text-uppercase radius-sm font-weight-700">Requet Business Plan</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <!-- Search Form -->
            <div class="sticky-header main-bar-wraper">
                <div class="main-bar header-botton nav-bg-secondry">
                    <div class="container">
                        <!-- NAV Toggle Button -->
                        <button data-target=".header-nav" data-toggle="collapse" type="button" class="navbar-toggle collapsed">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        
                            <!-- MAIN Vav -->
                            <div class="header-nav navbar-collapse collapse ">
                                <ul class=" nav navbar-nav">
<!--                                    <li><a href="profitrex.html">Profit Rex</a></li>
                                <li><a href="business.html">Business</a></li>
                                <li class="active"><a href="opportunity.html">Opportunity</a></li>
                                <li><a href="news.html">News</a></li>
                                <li><a href="reachus.html">Reach Us</a></li>-->
                                
                                 <li class=""><?= Html::a('Profit Rex', ['inner-page/profitrex'], ['class' => '']) ?></li>
                                <li class=""><?= Html::a('Business', ['inner-page/business'], ['class' => '']) ?></li>
                                <li class=""><?= Html::a('Shop', ['site/shop'], ['class' => '']) ?></li>
                                <!--<li class=""><?= Html::a('Payment', ['site/payment'], ['class' => '']) ?></li>-->
                                <li class=""><?= Html::a('Blog', ['site/blog'], ['class' => '']) ?></li>
                                <li class=""><?= Html::a('Opportunity', ['inner-page/opportunity'], ['class' => '']) ?></li>
                                </ul>
                            </div>
                    </div>
                </div>
            </div>            
            
        </header>
        <!-- HEADER END -->
        
        
        
        
          <!--MODAL  LOGIN--> 
        <div id="Login-form" class="modal fade " role="dialog">
          <div class="modal-dialog modal-sm">
             Modal content
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-white">Login Your Account</h4>
              </div>
              <div class="modal-body p-a30">
                <!--<form id="log-form" action="/index.php?r=inner-page%2Flogin" method="post">-->
                     <form id="log-form" method="post" onsubmit="return validLogin();">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <!--<input class="form-control" placeholder="Enter Username" type="text">-->
                            <input type="text" id="loginform-username" class="form-control" name="LoginForm[username]" autofocus placeholder="User Name" aria-required="true" required>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <!--<input class="form-control" placeholder="Enter email" type="email">-->
                            <input type="password" id="loginform-password" class="form-control" name="LoginForm[password]" placeholder="Password" aria-required="true" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <!--<input class="form-control" placeholder="Enter email" type="email">-->
                            <input type="password" id="loginform-master_pin" class="form-control" name="LoginForm[master_pin]" placeholder="Key Pin" aria-required="true" required>  
                        </div>
                    </div>
                         <span id="logerror"> </span>
                       <button type="submit" class="site-button-secondry text-uppercase btn-block m-b10" name="login-button">Login</button>     <br/>

                     </form>
<!--                <a class="forgot-passwordwd-link" href="/index.php?r=site%2Frequest-password-reset">Forgot your password ?</a>-->
                <a href="#"class="forgot-passwordwd-link"  data-dismiss="modal" data-toggle="modal" data-target="#password-form">Forgot your password ?</a>

                    <!--<span class="font-12">Don't have an account? <a href="javascript:;" class="text-primary">Register Here</a></span>-->
                <!--</form>-->
              </div>
              <div class="modal-footer text-center">
                <div class="text-center"><img src="images/logo.png" alt=""></div>
              </div>
            </div>
          </div>
        </div>
        
        <!-- MODAL  REGISTER -->
        <div id="Register-form" class="modal fade " role="dialog">
          <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-white">Register here</h4>
              </div>
              <div class="modal-body p-a30">

                  <form id="reg-form" method="post" onsubmit="return validRegister();">
                 

                      
                    
                    <div class="form-group field-registrationform-parent_id required">

<div><input type="text" id="registrationform-parent_id" class="form-control" name="RegistrationForm[parent_id]" placeholder="Sponsor Id" wrapper="" aria-required="true" data-krajee-typeahead="typeahead_7864e59a"></div>

<p class="help-block help-block-error"></p>
</div>                    
                    
        <!--<div class="col-sm-offset-2 col-sm-10 col-sm-3">-->
            <button type="button" class="btn btn-primary btn-xs" style="margin-top:-64px; float:right" onclick=" $.ajax({
                        type     :&#039;POST&#039;,
                        cache    : false,
                        url  : &#039;/index.php?r=user%2Fgetd-default-sponsor-id&#039;,
                        success  : function(response) {
                                console.log(&#039;success: &#039;+response.sponsorId);
                                $(&#039;#registrationform-parent_id&#039;).val(response.sponsorId)
                        },
                        error: function(){
                          console.log(&#039;failure : something went wrong while fetching sponsor ID. Please try again later.&#039;);
                        }
                        });return false;">Get Sponsor Id</button>        <!--</div>-->
    
                        
                        
                    <!--<div class="form-group">-->

                        <div class="input-group">

                            <div class="form-group field-registrationform-position required">
<label class="control-label">Position</label>
<div>


<input type="hidden" name="RegistrationForm[position]" value="">
<div id="registrationform-position" aria-required="true">
<label class="radio-inline"><input type="radio" id="position" name="RegistrationForm[position]" value="L"  style="opacity: 1; right: 10px; position: relative;"> Left</label>
<label class="radio-inline"><input type="radio" name="RegistrationForm[position]" value="R" style="opacity: 1; right: 10px; position: relative;"> Right</label></div>


<p class="help-block help-block-error"></p>
</div>

</div>
                        </div>

                    <!--</div>-->
    
                        
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <!--<input class="form-control" placeholder="Enter Username" type="text">-->
                            <input type="text" id="registrationform-username" class="form-control" name="RegistrationForm[username]" autofocus placeholder="User Name" aria-required="true" value="" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <!--<input class="form-control" placeholder="Enter email" type="email">-->
                            <input type="text" id="registrationform-email" class="form-control" name="RegistrationForm[email]" autofocus placeholder="Email" value="" aria-required="true" required="required">
                        </div>
                    </div>
                    
                        
                        <div class="form-group">

                        <div class="input-group">

<!--                            <span class="input-group-addon"><i class="fa fa-key"></i></span>

                            <input class="form-control" placeholder="Enter Password" type="email">-->

                            <div class="form-group field-registrationform-verifycode">
<label class="control-label" for="registrationform-verifycode">Verify Code</label>
<img id="registrationform-verifycode-image" src="/index.php?r=site%2Fcaptcha&amp;v=5bf05fb84fbdc7.72715241" alt=""> <input type="text" id="registrationform-verifycode" name="RegistrationForm[verifyCode]" placeholder="To change, click on it.">

<p class="help-block help-block-error"></p>
</div>                        </div>

                    </div>


                        
                        <span id="regerror"></span>
                        
                    <button type="Register" class="site-button-secondry text-uppercase btn-block m-b10" name="register-button">Register</button> 
                     
                    


                    <!--<span class="font-12">Already Have an Account? <a href="javascript:;" class="text-primary">Login</a></span>-->
                </form>
              </div>
              <div class="modal-footer text-center">
                <div class="text-center"><img src="images/logo.png" alt=""></div>
              </div>
            </div>
          </div>
        </div> 
        
        
       

        
          <!--Forgot Possword  --> 
        <div id="password-form" class="modal fade " role="dialog">
          <div class="modal-dialog modal-sm">
     
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-white">Forgot Password</h4>
              </div>
              <div class="modal-body p-a30">
                <!--<form id="log-form" action="/index.php?r=inner-page%2Flogin" method="post">-->
                     <form id="password-form" method="post" onsubmit="return GetPassword();">
                    
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <!--<input class="form-control" placeholder="Enter email" type="email">-->
                            <input type="text" id="passwordresetrequestform-email" class="form-control" name="PasswordResetRequestForm[email]" placeholder="Email" aria-required="true" required>
                            
                        </div>
                    </div>
                 
                         <span id="resetPasserror"> </span>
                       <button type="submit" class="site-button-secondry text-uppercase btn-block m-b10" name="login-button">Submit</button>     <br/>

                     </form>
<!--                <a class="forgot-passwordwd-link" href="/index.php?r=site%2Frequest-password-reset">Forgot your password ?</a>-->
               

                    <!--<span class="font-12">Don't have an account? <a href="javascript:;" class="text-primary">Register Here</a></span>-->
                <!--</form>-->
              </div>
              <div class="modal-footer text-center">
                <div class="text-center"><img src="images/logo.png" alt=""></div>
              </div>
            </div>
          </div>
        </div>
<!--Forgot Possword  -->           
        
        
        
        <script type="text/javascript">


function validRegister(e){
//    e.preventDefault();
var parent_id=$('#registrationform-parent_id').val();
//var position=$('#position').val();
var position=$("input[name='RegistrationForm[position]']:checked").val();
var username=$('#registrationform-username').val();
//var username="vinay_sstest"
var email=$('#registrationform-email').val();
//var email="vinylkso@yopmail.com";
var verifyCode=$('#registrationform-verifycode').val();


$.ajax({
    type: "POST",

//    url: "index.php?r=inner-page%2Fregister",
    url: "index.php?r=inner-page/register",


    data: {"parent_id": parent_id, "position":position,"username":username,"email":email,"verifyCode":verifyCode},

    cache: false,
    success: function(result){

        if(result=='success'){
            window.location='/';
//            $('#loginForm').submit();
        }else if(result=='incorrectDetails'){
            $("#regerror").html("Please Check the details");
            $("#regerror").css("color","red");
            
            return false;
        }
    }
//    return false; 
});
return false;
}

function validLogin(e){
//    e.preventDefault();
var username=$('#loginform-username').val();
var password=$('#loginform-password').val();
var master_pin=$('#loginform-master_pin').val();


$.ajax({
    type: "POST",

//    url: "index.php?r=inner-page%2Fregister",
    url: "index.php?r=inner-page/login",


    data: {"username": username, "password":password,"master_pin":master_pin},
    cache: false,
    success: function(result){


        if(result=='success'){
            window.location='index.php?r=user/index';
//            $('#loginForm').submit();
        }else if(result=='error'){
            $("#logerror").html("Please Check the details");
            $("#logerror").css("color","red");
            
            return false;
        }
    }
});
return false;
}



function GetPassword(e){
//    e.preventDefault();
var email=$('#passwordresetrequestform-email').val();

$.ajax({
    type: "POST",

//    url: "index.php?r=inner-page%2Fregister",
    url: "index.php?r=inner-page/request-password-reset",


    data: {"email": email},
    cache: false,
    success: function(result){


        if(result=='success'){
            window.location='/';
//            $('#loginForm').submit();
        }else if(result=='syserror'){
            $("#resetPasserror").html("Sorry, we are unable to reset password for the provided email address");
            $("#resetPasserror").css("color","red");
            
            return false;
       
        }else if(result=='nouser'){
            $("#resetPasserror").html("There is no user with this email address.");
            $("#resetPasserror").css("color","red");
            
            return false;
        }
    }
});
return false;
}
</script>