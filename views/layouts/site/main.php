<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\SiteAsset;

use yii\helpers\Url;

AppAsset::register($this);
SiteAsset::register($this);

echo $this->context->action->id;

?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <!-- <title><?= Html::encode($this->title) ?></title> -->
<?php
    if(!empty($this->context->action->id)) { ?>
  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
<?php } ?>

    <?php $this->head() ?>

    <link href='http://fonts.googleapis.com/css?family=Abel|Source+Sans+Pro:400,300,300italic,400italic,600,600italic,700,700italic,900,900italic,200italic,200' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

</head>
<body>
    <?php $this->beginBody() ?>


        <div class="wrapper" id="home">
            <!--header start-->
            <?=
                $this->render(
                        'header'
                )
            ?>
            <!--header end-->


            <div class="inner-content">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= $content ?>
            </div>
        </div>

        <!--footer start-->
        <?=
            $this->render(
                    'footer'
            )
        ?>
        <!--footer end-->


    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
