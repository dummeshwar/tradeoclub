<?php
    use yii\helpers\Url;
?>
<style>
    .goog-te-gadget-simple .goog-te-menu-value span {
        color: #fff;
    }

    .goog-te-gadget-simple {
        background: transparent;
        color: #fff;
        border-top: none;
        border-bottom: none;
        border-right: none;
        border-left: none;
    }
</style>
    <!--header start-->
    <header id="header" class=" header-full-width">

        <div class="header-sticky dark-header">

            <div class="container">
                <div id="massive-menu" class="menuzord">

                    <!--logo start-->
                    <a href="/" class="logo-brand">
                        <img class="retina" src="/images/logo-dark.png" alt=""/>
                    </a>
                    <!--logo end-->

                     <ul class="menuzord-menu pull-right op-nav light">
                        <li>
                            <a id="google_translate_element" style="text-align: center;"></a>
                        </li>
                    </ul>


                    <style>
                           .goog-te-banner-frame.skiptranslate {
                               display: none !important;
                           }
                           .goog-te-menu-frame {
                           max-width:100% !important;
                           }
                           .goog-te-menu2 {
                           max-width: 100% !important;
                           overflow-x: scroll !important;
                           box-sizing:border-box !important;

                           }
                       </style>

                       <script type="text/javascript">
                           function googleTranslateElementInit() {
                               new google.translate.TranslateElement({
                                   pageLanguage: 'en',
                                   autoDisplay: false,
                                   layout: google.translate.TranslateElement.InlineLayout.SIMPLE
                               }, 'google_translate_element');
                               function changeGoogleStyles() {
                                   if($('.goog-te-menu-frame').contents().find('.goog-te-menu2').length) {
                                       $('.goog-te-menu-frame').contents().find('.goog-te-menu2').css({
                                           'max-width':'100%',
                                           'overflow-x':'auto',
                                           'box-sizing':'border-box',
                                           //'height':'auto'
                                       });
                                   } else {
                                       setTimeout(changeGoogleStyles, 50);
                                   }
                               }
                               changeGoogleStyles();
                           }
                       </script>
                       <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

                    <!--mega menu start-->
                    <ul class="menuzord-menu pull-right op-nav light">

                        <li  class="">
                            <a href="/">Go to Home</a>
                        </li>

                        <li  class="<?php echo ($this->context->route == 'site/register') ? 'active' : ''; ?>">
                            <a href="<?php echo Url::toRoute(['site/register']); ?>">Register</a>
                        </li>

                        <li  class="<?php echo ($this->context->route == 'site/login') ? 'active' : ''; ?>">
                            <a href="<?php echo Url::toRoute(['site/login']); ?>" >Login</a>
                        </li>

                    </ul>
                    <!--mega menu end-->

                </div>
            </div>
        </div>

    </header>
    <!--header end-->
