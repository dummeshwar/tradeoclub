
<body id="bg">

    <div class="page-wraper">
        	

            
        <!-- CONTENT START -->
        <div class="page-content">
            
            <!-- INNER PAGE BANNER -->
            <div class="wt-bnr-inr overlay-wraper" style="background-image:url(images/banner/profitrex.jpg);">
                <div class="overlay-main bg-black opacity-07"></div>
                <div class="container">
                    <div class="wt-bnr-inr-entry">
                        <h1 class="text-white">KNOW PROFITREX BETTER</h1>
                    </div>
                </div>
            </div>
            <!-- INNER PAGE BANNER END --> 
                
            <!-- BREADCRUMB ROW -->                            
            <div class="bg-gray-light p-tb20">
                <div class="container">
                    <ul class="wt-breadcrumb breadcrumb-style-2">
                        <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
                        <li>Profitrex</li>
                    </ul>
                </div>
            </div>
            <!-- BREADCRUMB  ROW END --> 
            
            <!-- ABOUT COMPANY SECTION START -->
            <div class="section-full p-tb100">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="section-head text-left">
                                <!--<span class="wt-title-subline text-gray-dark font-16 m-b15">What is bitcoin</span>-->
                                <h2 class="text-uppercase">Sketch to success </h2>
                                <div class="wt-separator-outer"><div class="wt-separator bg-primary"></div></div>
                                <p>We perform with extensive scooping of Bit coin & have since expanded to scooping additional Crypto-currencies. Our adventure has been marked with success because of the tireless effort of our organization, maintaining the highest goals  in performance & civility. PROFITREX has pegged its own decentralized Crypto-currency to be backed by gold, a solid & proven investment vehicle. Our trade model combines extensive crypto knowledge, skills & technology with the power of direct selling to truly harness “word of mouth” branding and create awareness globally about this compelling digital evolution.</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="wt-media">
                                <img src="images/gallery/pic3.jpg" alt="" class="img-responsive"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
            <!-- ABOUT COMPANY SECTION END --> 
            
            <!-- vision and mission value -->
            <div class="section-full  p-tb80 bg-full-height bg-repeat-x graph-slide-image" style="background-image:url(images/background/bg-1.jpg);">
            	
                <div class="container">
                        
                        <div class="row">
                        	<div class="col-md-6  clearfix">
                            	<div class="wt-box graph-tabing">
                                    <div class="wt-accordion acc-bg-primary acc-has-bg" id="accordion6">
                                            <div class="panel wt-panel">
                                            
                                                <div class="acod-head">
                                                     <h6 class="acod-title text-uppercase">
                                                        <a data-toggle="collapse" href="#collapseOne6" data-parent="#accordion6" aria-expanded="false" class="collapsed">
                                                            Vision
                                                            <span class="indicator"><i class="fa fa-plus"></i></span>
                                                        </a>
                                                     </h6>
                                                </div>
                                                
                                                <div id="collapseOne6" class="acod-body collapse" aria-expanded="false" style="height: 0px;">
                                                    <div class="acod-content p-tb15">To be the world’s leading Crypto-currency company meeting the highest standards of sincerity & excellence while applying a sound & brainy business acumen</div>
                                                </div>
                                                
                                            </div>
                                            <div class="panel wt-panel">
                                                <div class="acod-head acc-actives">
                                                     <h6 class="acod-title text-uppercase acc-actives">
                                                        <a data-toggle="collapse" href="#collapseTwo6" class="" data-parent="#accordion6" aria-expanded="true">
                                                        Mission 
                                                        <span class="indicator"><i class="fa fa-plus"></i></span>
                                                        </a>
                                                     </h6>
                                                </div>
                                                <div id="collapseTwo6" class="acod-body collapse in" aria-expanded="true" style="">
                                                    <div class="acod-content p-tb15">Our mission is to full fill our clients’ needs  by giving their requirement and Deciding what you want to achieve in life</div>
                                                </div>
                                            </div>
                                        </div>
                                </div>                            	                               
                            </div>
                            <div class="col-md-6">
                            	<div class="wt-box graph-part-right text-white">
                                	<strong class="text-uppercase title-first">ProfitRex Values</strong>
                                    <span class="text-uppercase text-primary display-block title-second">CORE VALUES</span>
                                    <ul class="list-check-circle primary">
                                        <li><strong>Confidence:</strong> Living up to the trust & time given to us by all our  clients.</li>
                                        <li><strong>Unity:</strong> Working together towards common objectives & shared success.</li>
                                        <li><strong>Inspiration:</strong> Moving forward in new areas of technological & business innovation with self confidence & passion.</li>
                                        <li><strong>Integrity:</strong> Measuring success not just in terms of financial gains but also in terms of the respect, goodwill & credibility we build among all our Stakeholders.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    
                </div>
            </div>
            <!-- vision and mission value  End -->
                 
            <!-- WHY CHOOSE US SECTION START  -->
            <div class="section-full  p-t80 p-b80 bg-gray">
            	<div class="container">
                    <!-- TITLE START-->
                    <div class="section-head text-center">
                        <h2 class="text-uppercase">Our approach</h2>
                        <div class="wt-separator-outer"><div class="wt-separator bg-primary"></div></div>
                    </div>
                    <!-- TITLE END-->
                    <div class="section-content no-col-gap">
                        <div class="row">
                        
                            <!-- COLUMNS 1 -->
                            <div class="col-md-4 col-sm-6">
                                <div class="wt-icon-box-wraper  p-a30 center m-a5">
                                    <div class="icon-lg text-primary m-b20">
                                        <a href="#" class="icon-cell"><img src="images/icon/pick-39.png" alt=""></a>
                                    </div>
                                    <div class="icon-content">
                                        <h4 class="wt-tilte text-uppercase font-weight-500">Analyse</h4>
                                        <p>Through PROFITREX’ thorough & exhaustive research methodology, we identify from over 378 Crypto-currencies, ones that are poised to gain & grow at a much faster pace than other competing currencies. Given the relative newness of the market, we are analysing  with hawkish accuracy Crypto-currencies with strong fundamentals that demonstrate a penchant to succeed.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 2 -->
                            <div class="col-md-4 col-sm-6">
                                <div class="wt-icon-box-wraper  p-a30 center m-a5">
                                    <div class="icon-lg text-primary m-b20">
                                        <a href="#" class="icon-cell"><img src="images/icon/pick-29.png" alt=""></a>
                                    </div>
                                    <div class="icon-content ">
                                        <h4 class="wt-tilte text-uppercase font-weight-500">Base Plan</h4>
                                        <p>Our investments are focused on building exchange reserves of Crypto-currencies & also adding resources to our trading teas. PROFITREX is also committed to building an natural system around Crypto-currencies that have a positive mind-set as you lead and motivate our team</p>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 3 -->
                            <div class="col-md-4 col-sm-60">
                                <div class="wt-icon-box-wraper  p-a30 center m-a5">
                                    <div class="icon-lg text-primary m-b20">
                                        <a href="#" class="icon-cell"><img src="images/icon/pick-40.png" alt=""></a>
                                    </div>
                                    <div class="icon-content">
                                        <h4 class="wt-tilte text-uppercase font-weight-500">MULTIPLY</h4>
                                        <p>Simply put, the ground plan  PROFITREX makes grow and multiply because of the rational decision making applied by our smart working team that peruses vast amounts of data prior to making any decision.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
            <!-- WHY CHOOSE US SECTION END --> 
            
            <!-- SECTION CONTENT -->
            <div class="section-full  p-t80 p-b80 bg-white">

            	<div class="container">

                    <!-- TITLE START-->

                    <div class="section-head text-center">

                    	<span class="wt-title-subline font-16 text-gray-dark m-b15">Buy and Sell Packages</span>

                        <h2 class="text-uppercase">We are changing the game</h2>

                        <div class="wt-separator-outer">

                            <div class="wt-separator bg-primary"></div>

                        </div>

                        <p>PROFITREX is a game changer trend  in a market that’s young and honesty  for success & broad based admire . We have applied an intuitive intelligence in all areas of Crypto-currency management to ensure we are always on the front foot. In our quest to ensure client success we have also introduced RENO switch each unit of REN being backed by 1 gram of 99.99% purity gold.</p>

                    </div>
					<img src="images/miningandtrading.png" alt="mining and trading"/>
                    <!-- TITLE END-->

               </div>
            </div>
            <!-- SECTION CONTENT END --> 
        </div>
        <!-- CONTENT END -->
        <!-- Network -->
        <div class="section-full no-col-gap bg-repeat">
                <div class="container-fluid">
                        
                        <div class="row">
                        	<div class="col-md-6 col-sm-6 bg-secondry">
                            	<div class="section-content p-tb60 p-r30 clearfix overflow-hide">
                                	<div class="wt-left-part any-query">
                                    	<img src="images/internationalnetwork.png" alt="">
                                    	<div class="text-center">
                                        	<h3 class="text-uppercase font-weight-500 text-white">Driven by passion</h3>
                                            <p class="text-white">Develop a passion for learning. If you do, you will never cease to grow. Never underestimate the power of passion. The greatest gift is a passion for reading. It is cheap, it consoles, it distracts, it excites, it gives you knowledge of the world and experience of a wide kind. It is a moral illumination Leadership is not about titles, positions or flowcharts. It is about one life influencing another.</p>
                                            
                                        </div>    
                                    </div>
                                </div>                               
                            </div>
                            <div class="col-md-6 col-sm-6 bg-primary">
                            	<div class="section-content p-tb60 p-l30 clearfix overflow-hide">
                                    <div class="wt-right-part any-query-contact">
                                    	<img src="images/ceo.png" alt="">
                                    	<div class="text-center">
                                        	<h3 class="text-uppercase font-weight-500 text-white">Defined Values</h3>
                                            <p class="text-white">For several years, Gold because of its timeless opulence, quickly and easily. was the backing system of currencies, printed by many governments. At PROFITREX, we have followed this sentiment in letter & spirit by ensuring our investments are of Gold standard, and also our partnerships in service, business & technology meet the highest standards of value & stability.</p>
                                        </div>                               
                                    </div>                                
                                </div>
                            </div>
                        </div>
                    
                </div>
            </div>
        <!-- Network End -->
        <!-- Gold Standard -->
        <div class="section-full overlay-wraper bg-center bg-cover bg-no-repeat  bg-gray" style="background-image:url(images/background/bg3.jpg)">

            	<div class="overlay-main bg-black opacity-07"></div>

                <div class="container">

                                	      

                        <div class="row conntact-home">

							<div class="col-md-7 col-sm-7">

                            	<div class="section-content p-tb70">

                                        <!-- TITLE -->

                                        <div class="section-head text-left text-white">

                                            <h2 class="text-uppercase">Powerful International Network</h2>

                                            <div class="wt-separator-outer">

                                                <div class="wt-separator bg-primary"></div>

                                            </div>

                                        </div>

                                        <!-- TITLE -->                                	

                                      <p>We work together in & serve a group and central network  that seeks to thrive in a universe  unrestricted by deed  lines. While we are headquartered in India, we have mining farms in Hungary, Poland & China and will be extending coverage to & development in Asia Pacific Our global value chain is indeed built on the strong support of a powerful international network that has placed their trust in us. the global value chain  and global production network  literature, one of the most vibrant areas of debate focuses on dynamics. and would be irreconcilable with global competitive pressures inherent in the fruit value chain.</p>                           

                                </div>

                            </div>                        

                            <div class="contact-home-right">

                                    <div>

                                        <img src="images/gold.png" class="st-1" alt="">

                                    </div>

                                </div>

                        </div>

                    

                </div>

            </div>
        <!-- Gold Standard End -->
        
      

       
            
    </div>
    




</body>
</html>
