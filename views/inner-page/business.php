

<body id="bg">

    <div class="page-wraper">
        	
      
            
        <!-- CONTENT START -->
        <div class="page-content">
            
            <!-- INNER PAGE BANNER -->
            <div class="wt-bnr-inr overlay-wraper" style="background-image:url(images/banner/business.jpg);">
                <div class="overlay-main bg-black opacity-07"></div>
                <div class="container">
                    <div class="wt-bnr-inr-entry">
                        <h1 class="text-white">THINK BIG. START NOW.</h1>
                    </div>
                </div>
            </div>
            <!-- INNER PAGE BANNER END --> 
                
            <!-- BREADCRUMB ROW -->                            
            <div class="bg-gray-light p-tb20">
                <div class="container">
                    <ul class="wt-breadcrumb breadcrumb-style-2">
                        <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
                        <li>Business</li>
                    </ul>
                </div>
            </div>
            <!-- BREADCRUMB  ROW END --> 
            
            <!-- Crypto Currency SECTION START -->
            <div class="section-full  p-t80 p-b80 bg-gray">
            	<div class="container">
                    <!-- TITLE START-->
                    <div class="section-head text-center">
                        <h2 class="text-uppercase">Crypto Currency – The New Buzzword</h2>
                        <div class="wt-separator-outer"><div class="wt-separator bg-primary"></div></div>
                    </div>
                    <!-- TITLE END-->
                    <div class="section-content no-col-gap">
                        <div class="row">
                        
                            <!-- COLUMNS 1 -->
                            <div class="col-md-3 col-sm-6">
                                <div class="wt-icon-box-wraper  p-a30 center m-a5">
                                    <div class="icon-lg text-primary m-b20">
                                        <a href="#" class="icon-cell"><img src="images/icon/science.png" alt=""></a>
                                    </div>
                                    <div class="icon-content">
                                        <h4 class="wt-tilte text-uppercase font-weight-500">Science and Reality</h4>
                                        <p>The art of cryptography is considered to be born along with the art of writing. As civilizations evolved, human beings got organized in tribes, groups, and kingdoms. This led to the emergence of ideas such as power, battles, supremacy, and politics. These ideas further fueled the natural need of people to communicate secretly with selective recipient which in turn ensured the continuous evolution of cryptography as well.</p>
                                        <p>Cryptography deals with the actual securing of digital data. It refers to the design of mechanisms based on mathematical algorithms that provide fundamental information security services. You can think of cryptography as the establishment of a large toolkit containing different techniques in security applications.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 2 -->
                            <div class="col-md-3 col-sm-6">
                                <div class="wt-icon-box-wraper  p-a30 center m-a5">
                                    <div class="icon-lg text-primary m-b20">
                                        <a href="#" class="icon-cell"><img src="images/icon/pick-21.png" alt=""></a>
                                    </div>
                                    <div class="icon-content ">
                                        <h4 class="wt-tilte text-uppercase font-weight-500">MIX</h4>
                                        <p>A crypto currency is a digital or virtual currency that uses cryptography for security. A crypto currency is difficult to counterfeit because of this security feature. Many crypto currencies are decentralized systems based on block chain technology, a distributed ledger enforced by a disparate network of computers.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 3 -->
                            <div class="col-md-3 col-sm-6">
                                <div class="wt-icon-box-wraper  p-a30 center m-a5">
                                    <div class="icon-lg text-primary m-b20">
                                        <a href="#" class="icon-cell"><img src="images/icon/internet.png" alt=""></a>
                                    </div>
                                    <div class="icon-content">
                                        <h4 class="wt-tilte text-uppercase font-weight-500">Global System</h4>
                                        <p>A crypto currency is a digital asset designed to work as a medium of exchange that uses strong cryptography to secure financial transactions, control the creation of additional units, and verify the transfer of assets. Crypto currencies are a kind of alternative currency and digital currency. Crypto currencies use decentralized control as opposed to centralized digital currency and central banking systems.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 4 -->
                            <div class="col-md-3 col-sm-6">
                                <div class="wt-icon-box-wraper  p-a30 center m-a5">
                                    <div class="icon-lg text-primary m-b20">
                                        <a href="#" class="icon-cell"><img src="images/icon/pick-19.png" alt=""></a>
                                    </div>
                                    <div class="icon-content">
                                        <h4 class="wt-tilte text-uppercase font-weight-500">Path for swap</h4>
                                        <p>A crypto currency is a digital or virtual currency that uses cryptography for security. A crypto currency is difficult to counterfeit because of this security feature. Many crypto currencies are decentralized systems based on block chain technology, a distributed ledger enforced by a disparate network of computers. A defining feature of a crypto currency, and arguably its biggest allure, is its organic nature; it is not issued by any central authority, rendering it theoretically immune to government interference or manipulation. </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
            <!-- Crypto Currency SECTION END --> 
            
           <!-- Feature of money -->
           <div class="section-full p-tb40">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="section-head text-left">
                                <h2 class="text-uppercase">Changing the future of money</h2>
                                <div class="wt-separator-outer"><div class="wt-separator bg-primary"></div></div>
                                <p>Crypto currencies are systems that allow for the secure payments of online transactions that are denominated in terms of a virtual "token," representing ledger entries internal to the system itself. "Crypto" refers to the fact that various encryption algorithms and cryptographic techniques, such as elliptical curve encryption, public-private key pairs, and hashing functions, are employed.</p>
                                <p>The first crypto currency to capture the public imagination was Bit coin, which was launched in 2009 by an individual or group known under the pseudonym, Satoshi Naka moto. As of October 2018, there were over 17.33 million bit coins in circulation with a total market value of around $115 billion (although the market price of bit coin can fluctuate quite a bit). Bit coin's success has spawned a number of competing crypto currencies, known as "al tcoins" such as Lite coin, Namecoin and Peer coin, as well as Ethereum, EOS, and Cardano. Today, there are literally thousands of crypto currencies in existence, with an aggregate market value of over $200 billion (Bit coin currently represents more than 50% of the total value).</p>
                                <p>Crypto-currency meets the monetary, investment & transaction needs of the modern, global citizen living in a fast-paced & demanding world. Crypto-currency was first created & designed to make a system that is superior to the fiat monetary system, which derives value from the relationship between supply & demand rather than the value of the material that the money is made of. Crypto-currency veers from other forms of digital currency (like Google wallet or Paypal) because there is no centralized bank or government body overseeing the transactions, making it decentralized, hence inflation proof.</p>
                                <p>It is easy & affordable to set-up a Crypto-currency account & having one enables users to cut out the middle man too & save on transactions fees.</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="wt-media">
                                <img src="images/gallery/pic9.jpg" alt="" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           <!-- Feature of money End -->
            
            <!-- SECTION CONTENT -->
            <div class="section-full  p-t40 p-b80 bg-gray">

            	<div class="container">

                    <!-- TITLE START-->

                    <div class="section-head text-center">
                        <h2 class="text-uppercase">A lot can be done</h2>

                        <div class="wt-separator-outer">

                            <div class="wt-separator bg-primary"></div>
                            </div>

                        <p>Crypto-currencies have come a long way since their early days in 2009 when the first form of acceptance came when 10,000 Bit coins were used to buy a pizza. Today the value of the same number of  Bit coins stands at USD 6 Million. From buying groceries to cars & houses a lot can be done today with Crypto-currencies that enjoy the support of hundreds of well-reputed brands such as Expedia, Dell, Microsoft, Subway, Bloomberg, Virgin Galactic, Word Press, Rakuten & Overstock.</p>

                    </div>
					<img src="images/miningandtrading.png" alt="mining and trading"/>
                    <!-- TITLE END-->

               </div>
            </div>
            <!-- SECTION CONTENT END --> 
        </div>
        <!-- CONTENT END -->
        
        <!-- Carfting Money -->
        <div class="section-full  p-tb80 bg-full-height bg-repeat-x graph-slide-image" style="background-image: url(&quot;images/background/bg-1.jpg&quot;); background-position: -1979px 0px;">
            	
                <div class="container">
                        
                        <div class="row">
                        	<div class="col-md-6">
                            	<div class="wt-box graph-part-right text-white">
                                	<strong class="text-uppercase title-first">The business of </strong>
                                    <span class="text-uppercase text-primary display-block title-second">crafting money</span>
                                    <p>Key among the governing principles of Crypto-currencies is that they should be free of the economic woes of any country & operate in a world without borders or brokers. The powerful operating backbone of Crypto-currencies, which includes mining, exchange & trading expertise permit them to live up to these governing principles.</p>
<p>Crypto currencies are virtual currencies that are exchanged online with no interference from anyone, not even the government. These currencies, through their language of cryptography, contain secured information and are exchanged through a recording system known as a block chain.</p>
<p>The impact of these crypto currencies take on a grand scale, especially from an economic context. People continually join the hype towards crypto currencies, so much so that it drives demand for them. Participating in online trading for crypto currencies is faster than those in the stock market, and is easily accessible by people since it is unregulated.</p>
<p>Crypto currency is the future. This is a truth that’s hard to deny. Although they have some disadvantages and risks associated with them, the benefits are great enough to consider purchasing them. But, if you do decide to invest in crypto currencies, it’s important to be educated about important things like where to buy, who to buy from, how to invest and how to save. We recommend you dig into crypto currency education before you take your first step into the world of digital currency.</p>
<p>Just like Forex, huge profits are made when Crypto-currencies are exchanged in big volumes. Profits are amplified & made possible by PROFITREX’ vast reserves of Crypto-currencies & years of successful investments experience that have created an attractive pool of Crypto-currencies, growing & exchanging at fast paces. Greater adoption of Crypto-currencies amongst people has lead to greater use & hence more exchange transactions.</p>
                                </div>
                            </div>
                           <div class="col-md-6">
                            	<div class="wt-box graph-part-right text-white">
                                    <strong class="text-uppercase title-first">Empowering Your </strong>
                                    <span class="text-uppercase text-primary display-block title-second">Transaction</span>
                                    <p>PROFITREX technology empowers over 60 business partners globally who in turn help hundreds of thousands of customers and clients on a monthly basis exchange their Crypto-currencies for several purposes within seconds. Carrying out an exchange is a simple, one-screen transaction that can be done in real-time.</p>
                                    <p>Through our services, both clients & businesses exchange their Crypto-Currency immediately without any hassles of creating account and waiting for hours before such exchanges happen.</p>
                                </div>
                            </div> 
                        </div>
                    
                </div>
            </div>
        
        <!-- Crafting Money End -->
        
        <!-- COLL-TO ACTION START -->
            <div class="call-to-action-wrap call-to-action-skew bg-primary bg-no-repeat" style="background-image:url(images/background/bg-4.html);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="call-to-action-left p-tb20 p-r50">
                                <h4 class="text-uppercase m-b10">Trading</h4>
                                <p>Crypto currency Trading is the Forex (Foreign Exchange) of crypto currencies. This means that you are able to trade different crypto currencies like Bit coin, Ether, Lite coin for USD. Most Alt coins (cryptos that are not Bit coin) are paired with Bit coin. The bigger ones are also paired with fiat currencies.</p>
                                <p>Crypto-currency trading is based on the same principles as trading Forex & is witnessing an exponential increase in adoption & circulation. PROFITREX has put in place a best-in-class platform to trade & has set-up a 24×7 team to provide support anytime. We trade in almost all available currencies & see substantial movement of over 500 currencies through our platform every day.</p>
                                <p>Now after you bought yourself some Bit coin, the time has come to choose your exchange platform. This is where you are able to instantly trade one crypto currency into another. Take note of the currency trading pairs – each exchange has a list of their own. There are exchanges, where you are able to only exchange Bit coin to Alt coin, but not Alt coin to any other Alt coin. This hinders the ability to trade fast and flexible. That is why we have made a list of the best crypto-exchanges. On this list you can find the most competitive bit coin brokers available, offering lowest transaction fees. </p>
                                <p>At the end of the day it is about the value we are able to deliver to our members. Majority of our profits are distributed in the form of weekly returns to members who have placed their trust & invested in us.</p>
                                <p>Our stellar mining, exchange & trading activities permit us to give back most of the profits generated from our investments while also keeping members free from risk as PROFITREX takes the exposure to unpredicted profits and losses. Our compensation plan gives members a sound investment vehicle that works for them, so they don’t have to.</p>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
        
        <!-- Gold Standard -->
        <div class="section-full overlay-wraper bg-center bg-cover bg-no-repeat  bg-gray" style="background-image:url(images/background/deliveringvalues.jpg)">

            	<div class="overlay-main bg-black opacity-07"></div>

                <div class="container">

                                	      

                        <div class="row conntact-home">

                            <div class="col-md-12 col-sm-12">

                            	<div class="section-content p-tb70">

                                        <!-- TITLE -->

                                        <div class="section-head text-left text-white">

                                            <h2 class="text-uppercase">Mining</h2>

                                            <div class="wt-separator-outer">

                                                <div class="wt-separator bg-primary"></div>

                                            </div>

                                        </div>

                                        <!-- TITLE -->                                	

                                      <p>With resources primarily dedicated to Bit coin mining, PROFITREX also mines several other Crypto-currencies at our state-of-the-art mining farms with hundreds of connected machines. We use best-in-class Application Specific Integrated Circuits (ASIC) for mining with hash rate of 14 Tera hash / second (TH/s). Our focus on continuous improvement ensures we are constantly upgrading to the best available hardware innovation, thus phasing out older machines. We currently have mining farms in Australia, England and Iceland.</p>                          

                                </div>

                            </div>                        

                                </div>

                        </div>

                    

                </div>

            </div>
        <!-- Gold Standard End -->
       
     
   
    </div>
    





</body>
</html>
