  <!-- CONTENT START -->
        <div class="page-content">
        
            <!-- BREADCRUMB ROW -->                            
            <div class="bg-gray-light p-tb20">
            	<div class="container">
                    <ul class="wt-breadcrumb breadcrumb-style-2">
                        <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
                        <li>Contact 1</li>
                    </ul>
                </div>
            </div>
            <!-- BREADCRUMB ROW END -->
             
            <!-- SECTION CONTENTG START -->
            <div class="section-full p-t80">
                <div class="container">
                    <!-- CONTACT DETAIL BLOCK -->
                    <div class="section-content ">
                        <div class="row">
                            <div class="wt-box text-center">
                                <h3 class="text-uppercase">Have any Questions?</h3>
                                <div class="wt-separator-outer m-b30">
                                    <div class="wt-separator bg-primary"></div>
                                </div>
                                <div class="row">
                                
                                    <div class="col-md-4 col-sm-12 m-b30">
                                        <div class="wt-icon-box-wraper center p-a30">
                                            <div class="wt-icon-box-md text-black"><span class="icon-cell"><i class="iconmoon-smartphone-1"></i></span></div>
                                            <div class="icon-content">
                                                <h5>Phone</h5>
                                                <p>+91 564 548 4854</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 m-b30">
                                        <div class="wt-icon-box-wraper center p-a30">
                                            <div class="wt-icon-box-md text-black"><span class="icon-cell"><i class="iconmoon-email"></i></span></div>
                                            <div class="icon-content">
                                                <h6>Email</h6>
                                                <p>demo@gmail.com</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 m-b30">
                                        <div class="wt-icon-box-wraper center p-a30">
                                            <div class="wt-icon-box-md  text-black"><span class="icon-cell"><i class="iconmoon-travel"></i></span></div>
                                            <div class="icon-content">
                                                <h5>Address</h5>
                                                <p>252 W 43rd St New York, NY</p>
                                            </div>
                                        </div>
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
            <!-- SECTION CONTENT END -->
            
            <!-- SECTION CONTENTG START -->
            <div class="section-full p-tb50 bg-gray ">
            	<div class="container">
                	<div class="section-content p-a15 ">
                        <div class="row">
                            <!-- LOCATION BLOCK-->
                            <div class="wt-box col-md-6">
                                <h3 class="text-uppercase">Location</h3>
                                <div class="wt-separator-outer m-b30">
                                   <div class="wt-separator bg-primary"></div>
                               </div>
                                <div class="gmap-outline m-b30">
                                    <div id="gmap_canvas" class="google-map"></div>
                                </div>
                            </div>                        
                        
                            <!-- CONTACT FORM-->
                            <div class="wt-box col-md-6">
                                <h3 class="text-uppercase">Contact Form </h3>
                                <div class="wt-separator-outer m-b30">
                                   <div class="wt-separator bg-primary"></div>
                               </div>
                                
                                <form class="cons-contact-form" method="post" action="http://thewebmax.com/bitinvest/form-handler.php">
                        
                                    <div class="row">
                                    
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <input name="username" type="text" required class="form-control" placeholder="Name">
                                                </div>
                                            </div>
                                        </div>
        
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                    <input name="email" type="text" class="form-control" required placeholder="Email">
                                                </div>
                                            </div>
        
                                        </div>
        
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon v-align-t"><i class="fa fa-pencil"></i></span>
                                                    <textarea name="message" rows="4" class="form-control " required placeholder="Message"></textarea>
                                                </div>
                                            </div>
                                        </div>
        
                                        <div class="col-md-12 text-right">
                                            <button name="submit" type="submit" value="Submit" class="site-button-secondry  m-r15">Submit </button>
                                            <button name="Resat" type="reset" value="Reset"  class="site-button " >Reset</button>
                                        </div>
        
                                    </div>
    
                                </form>
                        
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- SECTION CONTENT END -->            
            
        </div>
        <!-- CONTENT END -->
        
        
        
<!-- JAVASCRIPT  FILES ========================================= --> 
<script   src="js/jquery-1.12.4.min.js"></script><!-- JQUERY.MIN JS -->
<script   src="js/bootstrap.min.js"></script><!-- BOOTSTRAP.MIN JS -->

<script   src="js/bootstrap-select.min.js"></script><!-- FORM JS -->
<script   src="js/jquery.bootstrap-touchspin.min.js"></script><!-- FORM JS -->

<script   src="js/magnific-popup.min.js"></script><!-- MAGNIFIC-POPUP JS -->

<script   src="js/waypoints.min.js"></script><!-- WAYPOINTS JS -->
<script   src="js/counterup.min.js"></script><!-- COUNTERUP JS -->
<script   src="js/waypoints-sticky.min.js"></script><!-- COUNTERUP JS -->

<script  src="js/isotope.pkgd.min.js"></script><!-- MASONRY  -->

<script   src="js/owl.carousel.min.js"></script><!-- OWL  SLIDER  -->

<script   src="js/stellar.min.js"></script><!-- PARALLAX BG IMAGE   --> 
<script   src="js/scrolla.min.js"></script><!-- ON SCROLL CONTENT ANIMTE   --> 

<script  src="https://maps.google.com/maps/api/js?key=AIzaSyCz6xX2nI6cMkePba_DHQcs0MkR7m2IuvE&amp;callback=initMap"  ></script><!-- GOOGLE MAP -->
<script   src="js/map.script.js"></script><!-- MAP FUCTIONS [ this file use with google map]  -->

<script   src="js/custom.js"></script><!-- CUSTOM FUCTIONS  -->
<script   src="js/shortcode.js"></script><!-- SHORTCODE FUCTIONS  -->
<script   src="js/switcher.js"></script><!-- SWITCHER FUCTIONS  -->
<script  src="js/jquery.bgscroll.js"></script><!-- BACKGROUND SCROLL -->
<script  src="js/tickerNews.min.js"></script><!-- TICKERNEWS-->