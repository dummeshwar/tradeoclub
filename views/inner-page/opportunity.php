

<body id="bg">

    <div class="page-wraper">
        	
       
            
        <!-- CONTENT START -->
        <div class="page-content">
            
            <!-- INNER PAGE BANNER -->
            <div class="wt-bnr-inr overlay-wraper" style="background-image:url(images/banner/makewayforhappnies.jpg);">
                <div class="overlay-main bg-black opacity-07"></div>
                <div class="container">
                    <div class="wt-bnr-inr-entry">
                        <h1 class="text-white">MAKE WAY FOR HAPPINESS</h1>
                    </div>
                </div>
            </div>
            <!-- INNER PAGE BANNER END --> 
                
            <!-- BREADCRUMB ROW -->                            
            <div class="bg-gray-light p-tb20">
                <div class="container">
                    <ul class="wt-breadcrumb breadcrumb-style-2">
                        <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
                        <li>Opportunity</li>
                    </ul>
                </div>
            </div>
            <!-- BREADCRUMB  ROW END --> 
            
            <!-- Crypto Currency SECTION START -->
            <div class="section-full  p-t20 bg-gray">
            	<div class="container">
                    <!-- TITLE START-->
                    <div class="section-head text-center">
                        <h2 class="text-uppercase">Happy life mantra</h2>
                        <div class="wt-separator-outer"><div class="wt-separator bg-primary"></div></div>
                    </div>
                    <!-- TITLE END-->
                    <div class="section-content no-col-gap">
                        <div class="row">
                            <div class="col-md-12 col-sm-6">
                                <div class="wt-icon-box-wraper  p-a30 center m-a5">
                                    <div class="icon-lg text-primary m-b20">
                                        <a href="#" class="icon-cell"><img src="images/icon/science.png" alt=""></a>
                                    </div>
                                    <div class="icon-content">
                                        <p>The Gautama Buddha has said, “It doesn’t just end at being passionate about what you do. It’s about taking care of yourself (physically and mentally), striving for happiness in a relationship, and having a hobby or interests that stimulate your brain.</p>
<p>Over 90% of people are dissatisfied with their job, their boss, their income, or their current lifestyle. Don’t be one of them!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
            <!-- Crypto Currency SECTION END --> 
            
           <!-- The perfect time -->
           <div class="section-full overlay-wraper bg-center bg-cover bg-no-repeat  bg-gray" style="background-image:url(images/background/bg5.jpg)">

            	<div class="overlay-main bg-black opacity-07"></div>

                <div class="container">

                                	      

                        <div class="row conntact-home">

							<div class="col-md-7 col-sm-7">

                            	<div class="section-content p-tb70">

                                        <!-- TITLE -->

                                        <div class="section-head text-left text-white">

                                            <h2 class="text-uppercase">The Life is on correct track!</h2>

                                            <div class="wt-separator-outer">

                                                <div class="wt-separator bg-primary"></div>

                                            </div>

                                        </div>

                                        <!-- TITLE -->                                	

                                        <p>You cannot always wait for the perfect time, sometimes you must dare to jump. life is all about risks and it requires you to jump. don't be a person who has to look back and wonder what they would have or could have had. no one wait forever.</p>
                                    <p>Timing Is Every thing & the time is now to be part of our pioneering & growth focused Crypto-currency venture.</p>                           

                                </div>

                            </div>                        

                            <div class="contact-home-right">

                                    <div>

                                        <img src="images/time.png" class="st-1" alt="">

                                    </div>

                                </div>

                        </div>

                    

                </div>

            </div>
           
           <!-- The perfect time End -->
            
            <!-- Direct Selling and Rewarding -->
            <div class="section-full  p-tb80 bg-full-height bg-repeat-x graph-slide-image" style="background-image: url(&quot;images/background/bg-2.jpg&quot;); background-position: -1502px 0px;">
            	
                <div class="container">
                <div class="section-head text-left text-white">

                                            <h2 class="text-uppercase">Direct Selling is Rewarding</h2>
                        </div>
                        <div class="row">
                        	<div class="col-md-6 clearfix">
                            	<div class="wt-box graph-part-right text-white">
                                    <ul class="list-check-circle primary">
                                        <li>Approximately 20 Direct Selling companies are in the global billion-dollar league.</li>
                                        <li>Direct Selling is taught at the Harvard Business School and at Stanford University among other top institutes in the United States.</li>
                                        <li>Worldwide sales amount to USD 184 billion.</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                            	<div class="wt-box graph-part-right text-white">
                                    <ul class="list-check-circle primary">
                                        <li>To date Direct Selling has created more millionaires than any other industry.</li>
                                        <li>Direct Selling offers employment to over 103 million people worldwide.</li>
                                        <li>Avon the first Direct Selling Company was established in 1886.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    
                </div>
            </div>
            <!-- Direct Selling and Rewarding END --> 
        </div>
        <!-- CONTENT END -->
        
        <!-- COLL-TO ACTION START -->
<div class="section-full home-about-section p-t80 bg-no-repeat bg-bottom-right" style="background-image:url(images/background/bg-coin.png)">

                <div class="container-fluid ">

                    <div class="row">

                        <div class="col-md-6">

                            <div class="wt-box text-right">

                                <img src="images/background/businessplan.jpg" alt=""> 

                            </div>

                         </div>

                       <div class="col-md-6">

                            <div class="wt-right-part p-b80">

                                    <!-- TITLE START -->

                                    <div class="section-head text-left">

                                        <span class="wt-title-subline font-16 text-gray-dark m-b15">World class</span>

                                        <h2 class="text-uppercase">Reimbursement plan</h2>

                                        <div class="wt-separator-outer"><div class="wt-separator bg-primary"></div></div>

                                    </div>

                                    <!-- TITLE END -->

                                    <div class="section-content">

                                        <div class="wt-box">

                                            <p>Compensation is the total cash and non-cash payments that you give to an employee in exchange for the work they do for your business. It is typically one of the biggest expenses for businesses with employees.</p>
                                            <p>Our compensation plan is best-in-class, created for both long term & fast paced growth. A right balance of success and longevity.</p>
                                            
                                            <h4>Download Opportunity Presentation</h4>

                                        <div class="wt-separator-outer"><div class="wt-separator bg-primary"></div></div>

                                            <a href="#" class="site-button text-uppercase m-r15 m-b15">English</a>
                                            <a href="#" class="site-button text-uppercase m-r15 m-b15">Germany</a>
                                            <a href="#" class="site-button text-uppercase m-r15 m-b15">Turkey</a>
                                            <a href="#" class="site-button text-uppercase m-r15 m-b15">China</a>

                                        </div>

                                    </div>                                	

                                </div>

                        </div>

                </div>

             </div>
             <div class="container-fluid ">

                    <div class="row">

                       <div class="col-md-6">

                            <div class="wt-left-part p-tb20">

                                    <!-- TITLE START -->

                                    <div class="section-head text-left">

                                        <span class="wt-title-subline font-16 text-gray-dark m-b15">Four brilliant ways</span>

                                        <h2 class="text-uppercase">to get paid</h2>

                                        <div class="wt-separator-outer"><div class="wt-separator bg-primary"></div></div>

                                    </div>

                                    <!-- TITLE END -->

                                    <div class="section-content">

                                        <div class="wt-box text-center">
                                        <h3>ROI</h3>
                                        Our investment allocations are based on our Global Portfolio of Crypto-currencies & related investments. The returns range from 2.5 to 5% each week.
                                        <h3>FAST TRACK BONUS</h3>
                                        Members are rewarded with a Fast Track Bonus ranging from 5 to 10% for introducing new members to PROFITREX who believe in our vision.
                                        <h3>DIFFERENTIAL FAST TRACK BONUS</h3>
                                        Members are rewarded with Differential FTB on new memberships in the direct line of sponsorship. It’s based on differential percentage levels and is paid out instantly till infinite levels.
                                        <h3>DUAL TEAM COMMISSION</h3>
                                        Our revolutionary Dual Team Commissions design rewards members a staggering 10% matching commission, for the growth in right and left leg organisations.
                                        <h3>For A Brighter Tomorrow</h3>
                                        We train & enrich our subscribers in a world class methodology for which we have an Ace team and a system to help passionate people succeed in our business.
                                        </div>

                                    </div>                                	

                                </div>

                        </div>
                        <div class="col-md-6">

                            <div class="wt-box text-right p-tb150">

                                <img src="images/background/earnings.jpg" alt=""> 

                            </div>

                         </div>

                </div>

             </div>
        <!-- For a bright -->
        <div class="section-full p-t80 p-b50 bg-gray">
                <div class="container">
                    <div class="section-content">
                        <div class="row">
                            <div class="col-md-6">
                                <h3>World Class Training</h3>
                                <div class="wt-separator-outer  m-b30">
                           			 <div class="wt-separator bg-primary"></div>
                                </div>
                        		<!-- TAB DEFAULT NAV LEFT -->
                                <div class="wt-tabs border">
                                    <div class="tab-content">
                                    
                                        <div class="tab-pane active">
                                            <p class="m-b0"><strong><em>We teach the brain-science behind positive behavioural change that leads to effective results and the tools and processes teams can apply to work collaboratively and creatively.</em></strong> Organizations indicate low effectiveness of existing leadership development programs at all leader levels, but the problem is most acute with respect to high-potential leader development programs. While the vast majority (90 percent) of organizations have programs aimed at high-potential and emerging leaders, leadership development efforts are simply not succeeding.</p>
                                        </div>
                                        </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h3>Governorship</h3>
                                <div class="wt-separator-outer m-b30">
                            		<div class="wt-separator bg-primary"></div>
                                </div>                            
                            
                            	<!-- TAB DEFAULT NAV RIGHT -->
                                <div class="wt-tabs right border">

                                    <div class="tab-content">
                                    
                                        <div id="web-design-10" class="tab-pane active">
                                            <p class="m-b0">We build leaders because we believe each human has the latent qualities & a strong foundation of any business is dependent upon the people who lead the organization, and in the case of PROFITREX, our members lead the organization. We believe in creating a system to qualify leadership & enhance overall personality for better results.</p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
        <!-- For a bright End -->
        <div class="section-full p-t80 p-b50 overlay-wraper bg-parallax" data-stellar-background-ratio="0.5" style="background-image: url(&quot;images/background/bg2.jpg&quot;); background-position: 50% -422.055px;">
                <div class="overlay-main bg-black opacity-07"></div>
                <div class="container">
                    <div class="section-head">
                        <h3 class="text-uppercase text-white">Events & celebrations</h3>
                        <div class="wt-separator-outer">
                            <div class="wt-separator bg-primary"></div>
                        </div>
                    </div>
                	<!-- TAB DEFAULT WITH NAV BG ON BACKGROUND -->
                    <div class="section-content">
                        <div class="wt-tabs bg-tabs has-bg">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#annual-convention"><i class="fa fa-globe"></i> Annual convention</a></li>
                                <li><a data-toggle="tab" href="#business-opportunity-meetings"><i class="fa fa-photo"></i> Business Occasion meetings</a></li>
                                <li><a data-toggle="tab" href="#super-saturdays"><i class="fa fa-cog"></i>Fun and Feel weekends </a></li>			
                                <li><a data-toggle="tab" href="#black-tie-events"><i class="fa fa-cog"></i>Black tie events</a></li>
                            </ul>
                            <div class="tab-content">
                            
                                <div id="annual-convention" class="tab-pane active">
                                    <p class="m-b0">Considered a landmark event convent to our employees and stakeholders, each year we celebrate success, friendship, commitment & teamwork in an exotic location where our leaders from all over the world gather with the corporate leadership to make new announcements & celebrate achievements along with cultural events.</p>
                                </div>
                                
                                <div id="business-opportunity-meetings" class="tab-pane">
                                    <p class="m-b0">BOM’s as they are generally referred to are a regular affair at PROFITREX. It’s the core  of our business & each successful leader of PROFITREX understands the importance of conducting BOMs. Simply put, more BOMs means more success! Our leaders conduct BOMs across regions in small groups & through road shows, seminars and business events to larger audiences.</p>
                                </div>
                                
                                <div id="super-saturdays" class="tab-pane">
                                    <p class="m-b0">As the name suggests, Super Saturdays are conducted on Saturdays & address bigger audiences. Super Saturdays are a way of promoting business to prospective customers & celebrating different milestones of our achievers.</p>
                                </div>
                                
                                <div id="black-tie-events" class="tab-pane">
                                    <p class="m-b0">Our Black Tie Events are held thrice a year to felicitate elite members who have achieved the rank of Winner and Runner. PROFITREX Winner and Runner are in the top quartile of performance & have demonstrated extraordinary drive & commitment in growing the business.</p>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
    
       

            
    </div>
    






</body>
</html>
