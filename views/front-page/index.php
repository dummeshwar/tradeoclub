
<!--  slider -->
<section id="home-rev-slider" class="home-rev-slider">
  <div id="rev_slider_1068_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="levano4export" data-source="gallery" style="background-color:transparent;padding:0px;">
    <div id="rev_slider_1068_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.3.0.2">
      <ul>  
      <!-- SLIDE  -->
        <li data-index="rs-3010" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-thumb="images/slider/slider-1.jpg"  data-delay="8000" data-rotate="0"  data-saveperformance="off"  data-title="The Menu" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
          <!-- MAIN IMAGE -->
          <img src="images/slider/slider-1.jpg"  alt=""  data-lazyload="images/slider/slider-1.jpg" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Power1.easeOut" data-scalestart="110" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
          <!-- LAYERS -->    

          <!-- LAYER NR. 1 -->
          <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme" 
            id="slide-3010-layer-1" 
            data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']" 
            data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
            data-width="full"
            data-height="full"
            data-whitespace="nowrap"       
            data-type="shape" 
            data-basealign="slide" 
            data-responsive_offset="on" 
            data-frames='[{"from":"z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","ease":"nothing"}]'
            data-textAlign="['left','left','left','left']"

            style="z-index: 5;text-transform:left;background-color:rgba(0, 0, 0, 0.4);border-color:rgba(0, 0, 0, 1.00);border-width:0px;"> </div>

          <!-- LAYER NR. 2 -->
          <div class="tp-caption Restaurant-Display tp-resizeme" 
            id="slide-3010-layer-2" 
            data-x="left" 
            data-y="center" 
            data-hoffset="['0', '0', '10', '10']" 
            data-voffset="['-60', '-60', '-60', '-60']"
            data-textAlign="left"
            data-fontsize="['60', '40', '32', '22']"
            data-responsive_offset="on" 
            data-responsive="off"
            data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1200,"to":"o:1;","delay":500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"nothing"}]'
            
            style="z-index: 5; white-space: nowrap; text-transform:left;">Thinking Of Travelling</div>

          <!-- LAYER NR. 3 -->
          <div class="tp-caption Restaurant-Display tp-resizeme" 
             id="slide-3010-layer-3" 
            data-x="left" 
            data-y="center" 
            data-hoffset="['0', '0', '15', '15']" 
            data-voffset="['8', '8', '10', '10']"
            data-textAlign="left"
            data-fontsize="['60', '40', '32', '22']"
            data-responsive_offset="on" 
            data-responsive="off"
            data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":700,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"nothing"}]'
            style="z-index: 5; white-space: nowrap;color: #FF8E31;text-transform:left;">With Bitcoin
          </div>

          <!-- LAYER NR. 4 -->
          <div class="tp-caption Restaurant-Cursive   tp-resizeme" 
             id="slide-3010-layer-4" 
            data-x="left" 
            data-y="center" 
            data-hoffset="['0', '0', '15', '15']" 
            data-voffset="['80', '80', '100', '140']"
            data-textAlign="left"
            data-fontsize="['18', '16', '14', '12']"
            data-lineheight="['22', '20', '18', '16']"
            data-responsive_offset="on" 
            data-responsive="off"
            data-width="['500', '400', '350', '300']"
            data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1200,"to":"o:1;","delay":900,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"nothing"}]'
            style="z-index: 6; white-space: normal;line-height: 1;text-transform:left;">Join millions who have officially found more astute methodologies for putting resources into Bitcoin. </div>

          <!-- LAYER NR. 5 -->
          <div class="tp-caption rev-btn rev-bordered  " 
             id="slide-3010-layer-5" 
             data-x="left" data-hoffset="['0', '0', '15', '15']" 
             data-y="center" data-voffset="['150','150','200','290']" 
            data-type="text" 
            data-actions='[{"event":"click","action":"jumptoslide","slide":"rs-3012","delay":""}]'
            data-responsive_offset="on" 
            data-responsive="off"
            data-frames='[{"from":"z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1800,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power3.easeOut"},{"frame":"hover","speed":"300","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(0, 0, 0, 0);bc:rgba(255, 224, 129, 1.00);bw:2px 2px 2px 2px;"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[12,12,10,10]"
            data-paddingright="[35,35,30,25]"
            data-paddingbottom="[12,12,10,10]"
            data-paddingleft="[35,35,30,25]"

            style="z-index: 19; white-space: nowrap; line-height: 17px; font-weight: 500; color: rgba(255, 255, 255, 1.00);font-family:Roboto;text-transform:left;background-color:rgba(10, 10, 10, 0);border-color:rgba(255, 255, 255, 0.50);border-style:solid;border-width:2px;letter-spacing:3px;">Contact Us </div>
           
        </li>
        <!-- SLIDE  -->
        <li data-index="rs-3011" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-thumb="images/slider/slider-2.jpg"  data-delay="8000" data-rotate="0"  data-saveperformance="off"  data-title="The Menu" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
          <!-- MAIN IMAGE -->
          <img src="images/slider/slider-2.jpg"  alt=""  data-lazyload="images/slider/slider-2.jpg" data-bgposition="right center" data-kenburns="on" data-duration="10000" data-ease="Power1.easeOut" data-scalestart="110" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
          <!-- LAYERS -->    

          <!-- LAYER NR. 1 -->
          <div class="tp-caption tp-shape tp-shapewrapper   tp-resizeme" 
             id="slide-3011-layer-1" 
             data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']" 
             data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                  data-width="full"
            data-height="full"
            data-whitespace="nowrap"
       
            data-type="shape" 
            data-basealign="slide" 
            data-responsive_offset="on" 

            data-frames='[{"from":"z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","ease":"nothing"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 5;text-transform:left;background-color:rgba(0, 0, 0, 0.4);border-color:rgba(0, 0, 0, 1.00);border-width:0px;"> </div>

          <!-- LAYER NR. 2 -->
          <div class="tp-caption Restaurant-Display tp-resizeme" 
             id="slide-3011-layer-2" 
            data-x="left" 
            data-y="center" 
            data-hoffset="['0', '0', '10', '10']" 
            data-voffset="['-60', '-60', '-60', '-60']"
            data-textAlign="left"
            data-fontsize="['60', '40', '32', '22']"
            data-responsive_offset="on" 
            data-responsive="off"
            data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1200,"to":"o:1;","delay":500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"nothing"}]'
            style="z-index: 5; white-space: nowrap;text-transform:left;">Simple Way to</div>

          <!-- LAYER NR. 3 -->
          <div class="tp-caption Restaurant-Display tp-resizeme" 
             id="slide-3011-layer-3" 
            data-x="left" 
            data-y="center" 
            data-hoffset="['0', '0', '15', '15']" 
            data-voffset="['8', '8', '10', '10']"
            data-textAlign="left"
            data-fontsize="['60', '40', '32', '22']"
            data-responsive_offset="on" 
            data-responsive="off"
            data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":700,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"nothing"}]'
            style="z-index: 5; white-space: nowrap;color: #FF8E31; text-transform:left;">Trade Bitcoin</div>

          <!-- LAYER NR. 4 -->
          <div class="tp-caption Restaurant-Cursive   tp-resizeme" 
             id="slide-3011-layer-4" 
            data-x="left" 
            data-y="center" 
            data-hoffset="['0', '0', '15', '15']" 
            data-voffset="['80', '80', '100', '130']"
            data-textAlign="left"
            data-fontsize="['18', '16', '14', '12']"
            data-lineheight="['22', '20', '18', '16']"
            data-responsive_offset="on" 
            data-responsive="off"
            data-width="['500', '400', '350', '300']"
            data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1200,"to":"o:1;","delay":900,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"nothing"}]'
            style="z-index: 6; white-space: normal; line-height: 1;text-transform:left;">Bitcoin is the world's first advanced decentralized cash and installment organize. </div>

          <!-- LAYER NR. 5 -->
          <div class="tp-caption rev-btn rev-bordered  " 
            id="slide-3011-layer-5" 
             data-x="left" data-hoffset="['0', '0', '15', '15']" 
             data-y="center" data-voffset="['150','150','200','280']" 
            data-type="text"    
            data-responsive_offset="on" 
            data-responsive="off"
            data-actions='[{"event":"click","action":"jumptoslide","slide":"rs-3012","delay":""}]'
            data-frames='[{"from":"z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1800,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power3.easeOut"},{"frame":"hover","speed":"300","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(0, 0, 0, 0);bc:rgba(255, 224, 129, 1.00);bw:2px 2px 2px 2px;"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[12,12,10,10]"
            data-paddingright="[35,35,30,25]"
            data-paddingbottom="[12,12,10,10]"
            data-paddingleft="[35,35,30,25]"

            style="z-index: 19; white-space: nowrap; line-height: 17px; font-weight: 500; color: rgba(255, 255, 255, 1.00);font-family:Roboto;text-transform:left;background-color:rgba(10, 10, 10, 0);border-color:rgba(255, 255, 255, 0.50);border-style:solid;border-width:2px;letter-spacing:3px;">Contact Us </div>
           
        </li>
        <!-- SLIDE  -->
        <li data-index="rs-3013" data-transition="slideoverup" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-thumb="images/slider/slider-3.jpg"  data-delay="8000" data-rotate="0"  data-saveperformance="off"  data-title="Opening TImes" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
          <!-- MAIN IMAGE -->
          <img src="images/slider/slider-3.jpg"  alt=""  data-lazyload="images/slider/slider-3.jpg" data-bgposition="left center" data-kenburns="on" data-duration="10000" data-ease="Power1.easeOut" data-scalestart="110" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
          <!-- LAYERS -->
          
          <!-- LAYER NR. 1 -->
          <div class="tp-caption tp-shape tp-shapewrapper   tp-resizeme" 
             id="slide-3013-layer-1" 
             data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']" 
             data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                  data-width="full"
            data-whitespace="nowrap"
       
            data-type="shape" 
            data-basealign="slide" 
            data-responsive_offset="on" 

            data-frames='[{"from":"z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","ease":"nothing"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 5;text-transform:left;background-color:rgba(0, 0, 0, 0.4);border-color:rgba(0, 0, 0, 1.00);border-width:0px;"> </div>

          <!-- LAYER NR. 2 -->
          <div class="tp-caption Restaurant-Display tp-resizeme" 
             id="slide-3013-layer-2" 
            data-x="left" 
            data-y="center" 
            data-hoffset="['0', '0', '10', '10']" 
            data-voffset="['-60', '-60', '-60', '-60']"
            data-textAlign="left"
            data-fontsize="['60', '40', '32', '22']"
            data-responsive_offset="on" 
            data-responsive="off"
            data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1200,"to":"o:1;","delay":500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"nothing"}]'
            style="z-index: 5; white-space: nowrap;text-transform:left;">Invest In Current</div>

          <!-- LAYER NR. 3 -->
          <div class="tp-caption Restaurant-Display tp-resizeme" 
             id="slide-3013-layer-3" 
            data-x="left" 
            data-y="center" 
            data-hoffset="['0', '0', '15', '15']" 
            data-voffset="['8', '8', '10', '10']"
            data-textAlign="left"
            data-fontsize="['60', '40', '32', '22']"
            data-responsive_offset="on" 
            data-responsive="off"
            data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":700,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"nothing"}]'
            style="z-index: 5; white-space: nowrap;color: #FF8E31; text-transform:left;">Trending Market</div>

          <!-- LAYER NR. 4 -->
          <div class="tp-caption Restaurant-Cursive   tp-resizeme" 
             id="slide-3013-layer-4" 
            data-x="left" 
            data-y="center" 
            data-hoffset="['0', '0', '15', '15']" 
            data-voffset="['80', '80', '100', '140']"
            data-textAlign="left"
            data-fontsize="['18', '16', '14', '12']"
            data-lineheight="['22', '20', '18', '16']"
            data-responsive_offset="on" 
            data-responsive="off"  
            data-width="['500', '400', '350', '300']"
            data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1200,"to":"o:1;","delay":900,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"nothing"}]'
            style="z-index: 6; line-height: 1; white-space: normal; text-transform:left;">Bitcoin causes usassociate fiscally simply like the Internet has helped us interface socially. </div>

          <!-- LAYER NR. 6 -->
        <div class="tp-caption rev-btn rev-bordered" 
          id="slide-3013-layer-6" 
          data-x="left" data-hoffset="['0', '0', '15', '15']" 
          data-y="center" data-voffset="['150','150','200','290']" 
          data-type="text"    
          data-responsive_offset="on" 
          data-responsive="off"
          data-actions='[{"event":"click","action":"jumptoslide","slide":"rs-3012","delay":""}]'
          data-frames='[{"from":"z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1800,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power3.easeOut"},{"frame":"hover","speed":"300","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(0, 0, 0, 0);bc:rgba(255, 224, 129, 1.00);bw:2px 2px 2px 2px;"}]'
          data-textAlign="['left','left','left','left']"
          data-paddingtop="[12,12,10,10]"
          data-paddingright="[35,35,30,25]"
          data-paddingbottom="[12,12,10,10]"
          data-paddingleft="[35,35,30,25]"

          style="z-index: 19; white-space: nowrap; line-height: 17px; font-weight: 500; color: rgba(255, 255, 255, 1.00);font-family:Roboto;text-transform:left;background-color:rgba(10, 10, 10, 0);border-color:rgba(255, 255, 255, 0.50);border-style:solid;border-width:2px;letter-spacing:3px;">Contact Us </div>
        </li>
      </ul>
    </div>
  </div>
</section>
<!--  end slider -->
<!--  chart -->
  <div class="currency-chart">
    <div class="container-fluid">
      <div class="row">
        <script>
          baseUrl = "https://widgets.cryptocompare.com/";
          var scripts = document.getElementsByTagName("script");
          var embedder = scripts[scripts.length - 1];
          var cccTheme = { "General": { "enableMarquee": true } };
          (function() {
              var appName = encodeURIComponent(window.location.hostname);
              if (appName == "") { appName = "local"; }
              var s = document.createElement("script");
              s.type = "text/javascript";
              s.async = true;
              var theUrl = baseUrl + 'serve/v3/coin/header?fsyms=BTC,ETH,XMR,LTC,DASH&tsyms=BTC,USD,CNY,EUR';
              s.src = theUrl + (theUrl.indexOf("?") >= 0 ? "&" : "?") + "app=" + appName;
              embedder.parentNode.appendChild(s);
          })();
        </script>
      </div>
    </div>
  </div>
<!-- end chart -->


<!-- we are  -->
  <section id="we-are" class="we-are-two-main-block" style="margin-top: 50px;">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="we-are-img">
            <img src="images/about/about-img-4.jpg" class="img-responsive" alt="about">
          </div>
        </div>
        <div class="col-md-6">
          <div class="we-are-dtl">
            <h3 class="we-are-heading">We are Crypto Group</h3>
            <p>Cryptocurrency has tremendous influences over the world from the last 10 years, we build wealth in this multi-billion dollar market.</p>
            <p>Beyond accepting Bitcoin as a form of payment, there is a multitude of different ways to engage and use digital currency.</p>
            <p>Here in this club we constantly try to grow money in your pocket by using multiple technologies to trade in cryptocurrencies.</p>
            <!--<a href="about-us.html" class="btn btn-default">See More About</a>-->
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- end we are  -->
<!-- about -->
  <section id="about" class="about-main-block">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-6">
          <div class="about-block">
            <h3 class="about-heading">About Our Bitcoin</h3>            
            <p></p>
            <p>Bitcoin is the mother of all Cryptocurrencies. Bitcoin is digital money and overall instalment framework. It is the principal decentralised advanced money, as the framework works without a national bank or single chairman. The system is distributed and exchanges occur between clients specifically using cryptography, without a delegate.</p>
            <p>Bitcoins are made as a reward for a procedure known as mining. They can be traded for different monetary standards, items, and administrations. Value of all the cryptocurrencies are directly influenced according to the value of Bitcoin.
</p>
            <!--<a href="about-us.html" class="plain-btn">Read More <i class="ti-arrow-right" aria-hidden="true"></i></a>-->
          </div>        
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="about-features-block">
            <div class="row">
              <div class="col-xs-3">
                <div class="about-features-icon">
                  <span class="ti-world"></span>
                </div>
              </div>
               <div class="col-xs-9">
                <div class="about-features-dtl">
                  <h6 class="about-features-heading">Worldwide Accepted</h6>
                  <p>Cryptocurrency is the modern trend all over the world, currently, over 100,000 merchants accepting crypto and more every day.</p>
                </div>
              </div>
            </div>
          </div>
           <div class="about-features-block">
            <div class="row">
              <div class="col-xs-3">
                <div class="about-features-icon">
                  <span class="ti-settings"></span>
                </div>
              </div>
               <div class="col-xs-9">
                <div class="about-features-dtl">
                  <h6 class="about-features-heading">Easy to Set up</h6>
                  <p>Just create your wallet online and you are all set to use digital money.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="about-features-block">
            <div class="row">
              <div class="col-xs-3">
                <div class="about-features-icon">
                  <span class="ti-view-list"></span>
                </div>
              </div>
               <div class="col-xs-9">
                <div class="about-features-dtl">
                  <h6 class="about-features-heading">Completely Transparent</h6>
                  <p>All transactions are made with Blockchain technology and every single movement of your transaction is trackable.</p>
                </div>
              </div>
            </div>
          </div>  
          <div class="about-features-block">
            <div class="row">
              <div class="col-xs-3">
                <div class="about-features-icon">
                  <span class="ti-bolt"></span>
                </div>
              </div>
               <div class="col-xs-9">
                <div class="about-features-dtl">
                  <h6 class="about-features-heading">Super Fast</h6>
                  <p>Transactions are secured with Blockchains new lighting network, so everything is so fast and secure here.</p>
                </div>
              </div>
            </div>
          </div>         
        </div>        
        <div class="col-md-4 hidden-sm">
          <div class="about-img">
            <img src="images/about/about-img.jpg" class="img-responsive" alt="about">
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- end about  -->




<!-- counter  -->
  <section id="counter" class="counter-main-block marginbtm100">
    <div class="parallax" style="background-image: url('images/bg/counter-bg.jpg')">
      <div class="overlay-bg" style="background-image: url('images/bg/facts-bg.jpg')"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="counter-section">
              <h2 class="counter-heading"><span>Doing the right thing,</span> at the ideal time.</h2>
            </div>
          </div>
            <div class="col-md-3 col-sm-4">
              <div class="counter-block">
                <h1 class="counter-number counter">900</h1>
                <h6 class="counter-text">Countries Accepted</h6>
              </div>
            </div>
            <div class="col-md-2 col-sm-4">
              <div class="counter-block">
                <h1 class="counter-number counter">16</h1>
                <h6 class="counter-text">Million Wallets</h6>
              </div>
            </div>
            <div class="col-md-3 col-sm-4">
              <div class="counter-block">
                <h1 class="counter-number counter">100</h1>
                <h1 class="counter-number-spec">%</h1>
                <h6 class="counter-text">Satisfied customers</h6>
              </div>
            </div>
        </div>
      </div>
    </div>
  </section>
<!-- end counter  -->
<!-- why choose  -->
<!--  <section id="whychoose" class="whychoose-main-block-2">
    <div class="container">
      <div class="section">
        <h3 class="section-heading">Why Choose Bitcoin</h3>     
      </div>
      <div class="row">
        <div class="col-md-4 col-sm-6">
          <div class="whychoose-block">
            <div class="whychoose-img">
             <img src="images/whychoose/full-control.png" class="img-responsive" alt="whychoose">            
            </div>
            <div class="whychoose-dtl text-center">
             <h5 class="whychoose-heading">Full Control</h5>
             <p>Nullam non magna in diam ultricies hendrerit at nec ligula. Aenean neque nisl, tincidunt sit amet neque ut.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="whychoose-block">
            <div class="whychoose-img">
             <img src="images/whychoose/payment.png" class="img-responsive" alt="whychoose">   
            </div>
            <div class="whychoose-dtl text-center">
             <h5 class="whychoose-heading">Payment Freedom</h5>
             <p>Nullam non magna in diam ultricies hendrerit at nec ligula. Aenean neque nisl, tincidunt sit amet neque ut.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="whychoose-block">
            <div class="whychoose-img">
             <img src="images/whychoose/app.png" class="img-responsive" alt="whychoose">   
            </div>
            <div class="whychoose-dtl text-center">
             <h5 class="whychoose-heading">Mobile App</h5>
             <p>Nullam non magna in diam ultricies hendrerit at nec ligula. Aenean neque nisl, tincidunt sit amet neque ut.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="whychoose-block">
            <div class="whychoose-img">
             <img src="images/whychoose/secure.png" class="img-responsive" alt="whychoose">   
            </div>
            <div class="whychoose-dtl text-center">
             <h5 class="whychoose-heading">Secure And Safe</h5>
             <p>Nullam non magna in diam ultricies hendrerit at nec ligula. Aenean neque nisl, tincidunt sit amet neque ut.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="whychoose-block">
            <div class="whychoose-img">
             <img src="images/whychoose/exchange.png" class="img-responsive" alt="whychoose">   
            </div>
            <div class="whychoose-dtl text-center">
             <h5 class="whychoose-heading">Instant Exchange</h5>
             <p>Nullam non magna in diam ultricies hendrerit at nec ligula. Aenean neque nisl, tincidunt sit amet neque ut.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="whychoose-block">
            <div class="whychoose-img">
             <img src="images/whychoose/network.png" class="img-responsive" alt="whychoose">   
            </div>
            <div class="whychoose-dtl text-center">
             <h5 class="whychoose-heading">Support The Network</h5>
             <p>Nullam non magna in diam ultricies hendrerit at nec ligula. Aenean neque nisl, tincidunt sit amet neque ut.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>-->
<!-- end why choose  -->
<!-- quote -->
<!--  <div id="quote" class="quote-main-block parallax" style="background-image: url('images/bg/quote-bg.jpg')">
  <div class="overlay-bg"></div>
    <div class="quote-text text-center">
      <p>“Bitcoin is a type of computerized money, made and held electronically. Nobody controls it.”</p>
    </div>       
  </div>-->
<!-- end quote  -->
<!-- process  -->
<!--  <section id="process" class="process-main-block">
    <div class="container">
      <div class="section">
        <h3 class="section-heading">How It Process</h3>     
      </div>
      <div class="design-process-section" id="process-tab">
         Nav tabs 
        <ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
          <li role="presentation" class="active"><a href="#login" aria-controls="login" role="tab" data-toggle="tab"><img src="images/icons/login.png" class="process-img img-responsive" alt="login">
            <p>Login &amp; Signup</p>
            </a>
          </li>
          <li role="presentation"><a href="#quantity" aria-controls="quantity" role="tab" data-toggle="tab"><img src="images/icons/quantity.png" class="process-img img-responsive" alt="Quantity">
            <p>Add Quantity</p>
            </a>
          </li>
          <li role="presentation"><a href="#payment" aria-controls="payment" role="tab" data-toggle="tab"><img src="images/icons/payment.png" class="process-img img-responsive" alt="Payment">
            <p>Payment</p>
            </a>
          </li>
          <li role="presentation"><a href="#buy" aria-controls="buy" role="tab" data-toggle="tab"><img src="images/icons/cart.png" class="process-img img-responsive" alt="cart">
            <p>Buy And Sell</p>
            </a>
          </li>          
        </ul>        
         Tab panes 
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="login">
            <div class="design-process-content">
              <h3 class="semi-bold">Login &amp; Signup</h3>
             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid unt ut laoreet dolore magna aliquam erat volutpat. Objectively embrace next-generation testing procedures vis-a-vis installed base bandwidth. Collaboratively network interdependent e-services whereas pandemic interfaces.</p>
             </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="quantity">
            <div class="design-process-content">
              <h3 class="semi-bold">Add Quantity</h3>
              <p>Synergistically extend goal-oriented strategic theme areas after B2B communities. Assertively reinvent orthogonal information for innovative models. Objectively embrace next-generation testing procedures vis-a-vis installed base bandwidth. Collaboratively network interdependent e-services whereas pandemic interfaces.</p>
              </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="payment">
            <div class="design-process-content">
              <h3 class="semi-bold">Payment</h3>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid unt ut laoreet dolore magna aliquam erat volutpat. Objectively embrace next-generation testing procedures vis-a-vis installed base bandwidth. Collaboratively network interdependent e-services whereas pandemic interfaces.</p>
               </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="buy">
            <div class="design-process-content">
              <h3 class="semi-bold">Buy And Sell</h3>
              <p>Synergistically extend goal-oriented strategic theme areas after B2B communities. Assertively reinvent orthogonal information for innovative models. Objectively embrace next-generation testing procedures vis-a-vis installed base bandwidth. Collaboratively network interdependent e-services whereas pandemic interfaces.</p>              
              </div>
          </div>          
        </div>    
      </div>
    </div>
  </section>-->
<!-- end process  -->
<!-- calculator  -->
<!--  <div id="converter" class="converter-main-block">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-sm-6 "> 
          <div class="converter-block">
            <h2 class="converter-heading"><span>Know Bitcoin</span><br>prices in your currency</h2>  
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed hendrerit erat. Donec quis nisl id diam finibus pulvinar. Nullam eleifend sit amet tellus et scelerisque. Cras in velit a tortor congue rutrum. Curabitur libero justo, auctor quis laoreet dictum, facilisis sit amet arcu. Ut nec pulvinar augue, in condimentum odio. Duis gravida risus ligula, ut vehicula ante convallis at. Quisque interdum finibus neque viverra molestie. Morbi non eros blandit felis interdum varius at eu est. Donec ullamcorper euismod libero hendrerit bibendum. Praesent vehicula quis sem vel dignissim. Morbi euismod nunc et ultrices maximus. Mauris pretium sapien et enim laoreet sollicitudin.Donec ullamcorper euismod libero hendrerit bibendum. Praesent vehicula quis sem vel dignissim.</p>  
            <p>Praesent vehicula quis sem vel dignissim. Morbi euismod nunc et ultrices maximus. Mauris pretium sapien et enim laoreet sollicitudin.Donec ullamcorper euismod libero hendrerit bibendum. Praesent vehicula quis sem vel dignissim. Consectetur adipiscing elit. Aliquam sed hendrerit erat. Donec quis nisl id diam finibus pulvinar. Nullam eleifend sit amet tellus et scelerisque. Cras in velit a tortor congue rutrum. Curabitur libero justo, auctor quis laoreet dictum, facilisis sit amet arcu. Ut nec pulvinar augue.</p>        
            <a href="#" class="btn btn-default">Read More</a>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">           
          <div class="calculator-block">
            <div class="calculator-top">
              <h5 class="calculator-heading"><span>Bitcoin</span><br> Currency Converter</h5>  
            </div>
            <div class="calculator-body">
              <div class="currency-icon"><img src="images/icons/bitcoin-icon.png" alt="bitcoin-icon"></div>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed consectetur adipiscing elit hendrerit erat. Donec quis nisl id diam finibus pulvinar. </p>
              <script src="https://www.cryptonator.com/ui/js/widget/calc_widget.js"></script>         
              <a href="#" class="btn btn-default">Buy Now</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>-->
<!-- end calculator  -->



<!-- prices  -->
<!--  <section id="prices" class="prices-main-block">
    <div class="container">
      <div class="section">
        <h3 class="section-heading">Crypto Currency Prices</h3>     
      </div>
      <div class="prices-block">
        <table class="table table-striped">
          <thead>
          <tr>
              <th>Crypto Currency</th>
              <th>Price</th>
              <th>24H % Change</th>
              <th>24H Volume</th>
              <th>Supply</th>
          </tr>
          </thead>
          <tbody>
          <tr>
              <td><img src="images/icons/bitcoin.png" class="prices-icon" alt="Bitcoin">Bitcoin</td>
              <td>10,520.00 $</td>
              <td class="up">18.88%</td>
              <td>118,045 BTC</td>
              <td>825,045</td>
          </tr>
          <tr>
              <td><img src="images/icons/ethereum.png" class="prices-icon" alt="Ethereum">Ethereum</td>
              <td>850.00 $</td>
              <td class="up">14.33%</td>
              <td>2,23,255 ETH</td>
              <td>568,896</td>
          </tr>       
          <tr>
              <td><img src="images/icons/ripple.png" class="prices-icon" alt="Ripple">Ripple</td>
              <td>450.00 $</td>
              <td class="up">22.22%</td>
              <td>3,50,000 XRP</td>
              <td>620,620</td>
          </tr> 
          <tr>
              <td><img src="images/icons/litecoin.png" class="prices-icon" alt="Litecoin">Litecoin</td>
              <td>555.55 $</td>
              <td class="up">25.00%</td>
              <td>387,565 XMR</td>
              <td>487,225</td>
          </tr>
          </tbody>
        </table>
      </div>
    </div>
  </section>-->
<!-- end prices  -->
<!-- price plan -->
<!--  <div id="price-plan" class="price-plan-main-block">
    <div class="container">
      <div class="row">
        <div class="section">
          <h3 class="section-heading text-center">Pricing Plan</h3>
          <p>Nullam vulputate lorem ut leo. Sed volutpat. Etiam non pede. Nullam et mauris. Praesent sed elit. Nulla posuere. Etiam sit amet turpis. Nullam mattis libero non</p>
        </div> 
        <div class="col-sm-3"> 
          <div class="price-plan-block">
            <div class="price-icon"><img src="images/icons/bitcoin-icon.png" alt="bitcoin-icon"></div>
            <h5 class="price-subheading">1 Bitcoin</h5>
            <h2 class="price-heading">$1350</h2>
            <div class="price-text">You Get 1 Bitcoin</div>   
            <a href="#" class="btn btn-default">Buy Now</a>
          </div>
        </div>
        <div class="col-sm-3"> 
          <div class="price-plan-block">
            <div class="price-icon"><img src="images/icons/bitcoin-icon.png" alt="bitcoin-icon"></div>
            <h5 class="price-subheading">5 Bitcoin</h5>
            <h2 class="price-heading">$3750</h2>
            <div class="price-text">You Get 5 Bitcoin</div>   
            <a href="#" class="btn btn-default">Buy Now</a>
          </div>
        </div>
        <div class="col-sm-3"> 
          <div class="price-plan-block">
            <div class="price-icon"><img src="images/icons/bitcoin-icon.png" alt="bitcoin-icon"></div>
            <h5 class="price-subheading">10 Bitcoin</h5>
            <h2 class="price-heading">$5500</h2>
            <div class="price-text">You Get 10 Bitcoin</div>   
            <a href="#" class="btn btn-default">Buy Now</a>
          </div>
        </div>
        <div class="col-sm-3"> 
          <div class="price-plan-block">
            <div class="price-icon"><img src="images/icons/bitcoin-icon.png" alt="bitcoin-icon"></div>
            <h5 class="price-subheading">12 Bitcoin</h5>
            <h2 class="price-heading">$9500</h2>
            <div class="price-text">You Get 12 Bitcoin</div>   
            <a href="#" class="btn btn-default">Buy Now</a>
          </div>
        </div>
      </div>
    </div>
  </div>-->
<!-- end price plan -->
<!-- market  -->
<!--  <div id="market" class="market-main-block" style="background-image: url('images/bg/market-bg.jpg')">
    <div class="overlay-bg"></div>
    <div class="container">
      <div class="row">
        <div class="col-sm-6"> 
          <div class="market-block">
            <h3 class="market-heading">Current Market</h3>  
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed hendrerit erat. Donec quis nisl id diam finibus pulvinar. Nullam eleifend sit amet tellus et scelerisque. Cras in velit a tortor congue rutrum. Curabitur libero justo, auctor quis laoreet dictum, facilisis sit amet arcu. Ut nec pulvinar augue, in condimentum odio. Duis gravida risus ligula, ut vehicula ante convallis at. Quisque interdum finibus neque viverra molestie. Morbi non eros blandit felis interdum varius at eu est.<p>
            <p>Donec ullamcorper euismod libero hendrerit bibendum. Praesent vehicula quis sem vel dignissim. Morbi euismod nunc et ultrices maximus. Mauris pretium sapien et enim laoreet sollicitudin.Donec ullamcorper euismod libero hendrerit bibendum. Praesent vehicula quis sem vel dignissim. Ut nec pulvinar augue, in condimentum odio. Duis gravida risus ligula, ut vehicula ante convallis at. Donec ullamcorper euismod libero hendrerit bibendum.</p>
            <a href="#" class="btn btn-default">Read More</a>
          </div>   
        </div>
        <div class="col-sm-6">
          <div class="market-chart">
            <div class="btcwdgt-chart">
            </div>
          </div> 
        </div>  
      </div>
    </div>
  </div>-->
<!-- end market  -->


<!-- testimonials  -->
<!--  <section id="testimonials" class="testimonials-main-block">
    <div class="container">
      <div class="section">
        <h3 class="section-heading">Testimonials</h3>
      </div>
      <div class="row">
        <div id="testimonials-slider" class="testimonials-slider">
          <div class="item">
            <div class="testimonial-block">
              <div class="testimonial-detail">
                <p>“My family and me need to thank you for helping us locate an incredible chance to profit on the web. Extremely content with how things are going!.”
                </p> 
                <div class="testimonial-arrow">
                  <img src="images/icons/testimonial-arrow-2.png" alt="arrow">
                </div>                              
              </div>               
              <div class="testimonial-img">
                <img src="images/testimonial/testimonial-1.jpg" class="img-responsive" alt="testimonial">
              </div>
              <div class="testimonial-client-dtl">
                <div class="testimonial-client">Neha Chechani <span>- Media City</span></div>
                <div class="rating">     
                  <ul>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  </ul>
                </div> 
              </div>
            </div>            
          </div>    
          <div class="item">
            <div class="testimonial-block">
              <div class="testimonial-detail">
                <p>“Mycoin is a reasonable program for anybody searching for site to contribute. Paid to me routinely, keep up great work. Thank you for helping us.”
                </p>
                <div class="testimonial-arrow">
                  <img src="images/icons/testimonial-arrow-2.png" alt="arrow">
                </div>
              </div>
              <div class="testimonial-img">
                <img src="images/testimonial/testimonial-2.jpg" class="img-responsive" alt="testimonial">
              </div>
              <div class="testimonial-client-dtl">
                <div class="testimonial-client">Udayraj Khatri <span>- Charted Accountant</span></div>
                <div class="rating">     
                  <ul>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                  </ul>
                </div> 
              </div>
            </div>            
          </div>     
        </div>
      </div>
    </div>
  </section>-->
<!-- end testimonials  -->
<!-- team  -->
<!--  <section id="team" class="team-main-block">
    <div class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="section">
            <h3 class="section-heading">Our Team</h3>
          </div>
          <div class="team-block">
            <p>If you want to contact us about any issue please call +91-123-456-7890 or send us an e-mail. If you would like to submit a proposal for consideration simply <a href="#">submit a quote.</a></p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quam lectus, dapibus sit amet accumsan a, porttitor at lectus. Nam euismod velit eu sagittis sollicitudin.</p>
            <a href="team.html" class="btn btn-default">Meet Team</a>
          </div>
        </div>
        <div class="col-sm-9">
          <div class="row">
            <div class="col-md-4 col-sm-6">
              <div class="team-member">
                <div class="team-img">
                  <img src="images/team/team-1.jpg" class="img-responsive" alt="team-member">
                  <div class="team-social">
                    <ul>
                      <li><a href="http://facebook.com" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                      <li><a href="http://twitter.com" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                      <li><a href="http://instagram.com" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                       <li><a href="http://instagram.com" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    </ul>
                  </div>
                </div>
                <h6 class="team-name"><a href="#">Sudhir Chechani</a></h6>
                <div class="team-post">Co-Founder</div>
                <div class="team-contact"><a href="tel:#">+91 123-456-7890</a></div>
                <div class="team-mail"><a href="mailto:#">info@mycoin.com</a></div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="team-member">
                <div class="team-img">
                  <img src="images/team/team-2.jpg" class="img-responsive" alt="team-member">
                  <div class="team-social">
                    <ul>
                      <li><a href="http://facebook.com" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                      <li><a href="http://twitter.com" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                      <li><a href="http://instagram.com" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                      <li><a href="http://instagram.com" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    </ul>
                  </div>
                </div>
                <h6 class="team-name"><a href="#">Riya Fadge</a></h6>
                <div class="team-post">Managing Director</div>
                <div class="team-contact"><a href="tel:#">+91 123-456-7891</a></div>
                <div class="team-mail"><a href="mailto:#">info@mycoin.com</a></div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="team-member">
                <div class="team-img">
                  <img src="images/team/team-3.jpg" class="img-responsive" alt="team-member">
                  <div class="team-social">
                    <ul>
                      <li><a href="http://facebook.com" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                      <li><a href="http://twitter.com" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                      <li><a href="http://instagram.com" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                      <li><a href="http://instagram.com" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    </ul>
                  </div>
                </div>
                <h6 class="team-name"><a href="#">Udhay Raj</a></h6>
                <div class="team-post">Consultant</div>
                <div class="team-contact"><a href="tel:#">+91 123-456-7891</a></div>
                <div class="team-mail"><a href="mailto:#">info@mycoin.com</a></div>
              </div>
            </div>            
          </div>
        </div>       
      </div>
    </div>
  </section>-->
<!-- end team  -->
<!-- contact us  -->
<!--  <section id="home-contact" class="home-contact-main-block">
    <div class="parallax" style="background-image: url('images/bg/home-contact.jpg')">
      <div class="overlay-bg"></div>
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <div class="section">
              <h3 class="section-heading">Quick Contact</h3>
              <p>Nullam vulputate lorem ut leo. Sed volutpat. Etiam non pede. Nullam et mauris. Praesent sed elit. Nulla posuere. Etiam sit amet turpis. Nullam mattis libero non</p>
            </div> 
            <div class="home-contact-block">
              <ul>
                <li><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:#"> +91-123-456-7890</a></li>
                <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:#"> info@mycoin.com</a></li>
                <li><i class="fa fa-globe" aria-hidden="true"></i><a href="#"> mycoin.com </a></li>
                <li><i class="fa fa-map-o" aria-hidden="true"></i> 189, A Park, Newyork, USA</li>
              </ul>
            </div>
            <div class="contact-social-icon">
              <ul>
                <li><a href="http://facebook.com" target="_blank"><i class="ti-facebook" aria-hidden="true"></i></a></li>
                <li><a href="http://twitter.com" target="_blank"><i class="ti-twitter-alt" aria-hidden="true"></i></a></li>
                <li><a href="http://linkedin.com" target="_blank"><i class="ti-linkedin" aria-hidden="true"></i></a></li>
                <li><a href="http://instagram.com" target="_blank"><i class="ti-instagram" aria-hidden="true"></i></a></li>
                <li><a href="http://youtybe.com" target="_blank"><i class="ti-youtube" aria-hidden="true"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="col-sm-6">
            <div id="home-contact-form" class="home-contact-form">          
              <form id="contactform" name="contactformhome" method="post">
                <input type="text" name="name" id="name" class="form-control" placeholder="Name *"> 
                <input type="text" name="email" id="email" class="form-control" placeholder="Email *"> 
                <textarea class="form-control" name="comments" id="comments" rows="6" placeholder="Message"></textarea>
                <button type="submit" value="send" id="submit" class="btn btn-default">Send</button>
              </form>    
            </div>
          </div>
        </div>
      </div>  
    </div> 
  </section>-->
<!-- end contact us  -->
<!--  call out -->
<!--  <section id="call-out" class="call-out-main-block">
    <div class="container">
      <div class="row">
        <div class="col-sm-10">
          <h4 class="call-block">Need to fare thee well of your Currency Investments.</h4>
        </div>
        <div class="col-sm-2">
          <div class="call-btn">
            <a href="get-quote.html" class="btn btn-default">Get A Quote</a>
          </div>
        </div>
      </div>
    </div>
  </section>-->
<!--  end call out -->
<!-- latest news  -->
<!--  <section id="news-grid" class="news-grid-main-block news-two">
    <div class="container">
      <div class="section">
        <h3 class="section-heading">Latest Post</h3>     
      </div>
      <div id="news-two-slider" class="news-two-slider"> 
        <div class="item">
          <div class="news-grid-block">
            <div class="news-grid-img">
              <a href="single-post.html"><img src="images/blog/news-img-2.jpg" class="img-responsive" alt="news"></a>
            </div>
            <div class="news-grid-dtl">
              <h5 class="news-grid-heading"><a href="single-post.html">Five Bitcoin Must Reads of 2017</a></h5>
              <div class="meta-tag">
                <ul>
                  <li><i class="ti-timer" aria-hidden="true"></i> <a href="#">January 01, 2018</a></li>
                  <li><i class="ti-user" aria-hidden="true"></i> <a href="#">Admin</a></li>
                  <li><i class="ti-comment" aria-hidden="true"></i> <a href="#">25</a></li>
                </ul>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quam lectus, dapibus sit amet accumsan a, porttitor at lectus. Nam euismod velit eu sagittis sollicitudin. In sed lorem sollicitudin.</p> 
              <a href="single-post.html" class="plain-btn">Read More <i class="ti-arrow-right" aria-hidden="true"></i></a> 
            </div>
          </div>
        </div>
        <div class="item">
          <div class="news-grid-block">
            <div class="news-grid-img">
              <a href="single-post.html"><img src="images/blog/news-img-3.jpg" class="img-responsive" alt="news"></a>
            </div>
            <div class="news-grid-dtl">
              <h5 class="news-grid-heading"><a href="single-post.html">Know The Risks Before You Buy</a></h5>
              <div class="meta-tag">
                <ul>
                  <li><i class="ti-timer" aria-hidden="true"></i> <a href="#">January 05, 2018</a></li>
                  <li><i class="ti-user" aria-hidden="true"></i> <a href="#">Admin</a></li>
                  <li><i class="ti-comment" aria-hidden="true"></i> <a href="#">14</a></li>
                </ul>
              </div>
              <p>Vestibulum sit amet vulputate nunc, sit amet malesuada metus. Aenean tellus mauris, pharetra et dictum id, feugiat finibus dui. Suspendisse vestibulum tincidunt erat ut lobortis.</p> 
              <a href="single-post.html" class="plain-btn">Read More <i class="ti-arrow-right" aria-hidden="true"></i></a> 
            </div>
          </div>
        </div>
        <div class="item">
          <div class="news-grid-block">
            <div class="news-grid-img">
              <a href="single-post.html"><img src="images/blog/news-img-5.jpg" class="img-responsive" alt="news"></a>
            </div>
            <div class="news-grid-dtl">
              <h5 class="news-grid-heading"><a href="single-post.html">Worried about tax on Bitcoin?</a></h5>
              <div class="meta-tag">
                <ul>
                  <li><i class="ti-timer" aria-hidden="true"></i> <a href="#">January 06, 2018</a></li>
                  <li><i class="ti-user" aria-hidden="true"></i> <a href="#">Admin</a></li>
                  <li><i class="ti-comment" aria-hidden="true"></i> <a href="#">20</a></li>
                </ul>
              </div>
              <p>Aenean tellus mauris, pharetra et dictum id, feugiat finibus dui. Suspendisse vestibulum tincidunt erat ut lobortis. Cras dapibus eleifend metus, eget tempus neque tincidunt vel. Nunc fringilla.</p> 
              <a href="single-post.html" class="plain-btn">Read More <i class="ti-arrow-right" aria-hidden="true"></i></a> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>-->
<!-- end latest news  -->
<!--  clients -->
 <!-- <div id="clients" class="clients-main-block">    
    <div class="container">      
      <div id="client-slider" class="client-slider"> 
        <div class="item client-img">
          <img src="images/client-1.jpg" class="img-responsive" alt="client-1">
        </div>
        <div class="item client-img">
          <img src="images/client-2.jpg" class="img-responsive" alt="client-2">
        </div>
        <div class="item client-img">
          <img src="images/client-3.jpg" class="img-responsive" alt="client-3">
        </div>
        <div class="item client-img">
          <img src="images/client-4.jpg" class="img-responsive" alt="client-4">
        </div>
        <div class="item client-img">
          <img src="images/client-5.jpg" class="img-responsive" alt="client-5">
        </div>
        <div class="item client-img">
          <img src="images/client-6.jpg" class="img-responsive" alt="client-6">
        </div>
        <div class="item client-img">
          <img src="images/client-6.jpg" class="img-responsive" alt="client-7">
        </div>
        <div class="item client-img">
          <img src="images/client-6.jpg" class="img-responsive" alt="client-8">
        </div>
      </div>         
    </div>   
  </div> -->
<!--  end clients -->
<!--  footer -->

<!--  end footer -->
<!-- jquery -->  
