<style>
    .custom-bullet li {
        display: block;
    }

    .custom-bullet li:before
    {
        /*Using a Bootstrap glyphicon as the bullet point*/
        content: "\e080";
        font-family: 'Glyphicons Halflings';
        font-size: 9px;
        float: left;
        margin-top: 4px;
        margin-left: -17px;
        color: #3500ff;

    }
    li{
        /*font-family: "Times New Roman", Times, serif;*/
        font-size: 18px;
    }
    
    
    .price-plan-block {
    position: relative;
    color: #FFF;
    box-shadow: 2px 2px 5px #5B5B5B;
    padding: 30px 10px;
    background: linear-gradient(rgba(48, 48, 48, 0.8), rgba(48, 48, 48, 0.8)), rgba(48, 48, 48, 0.8) url(../images/price-01.jpg) center;
    background-size: auto 100%;
    background-position: center;
    text-align: center;
    height: 284px;
    /*height: 100%;*/
    margin: 30px 0 auto;
    border-bottom: 2px solid transparent;
    -webkit-transition: all 0.5s ease-in-out;
}
</style>
<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Utils;
use yii\helpers\Url;

$this->title = 'Package Purchase';
$this->params['breadcrumbs'][] = $this->title;

//        echo '<pre>'; print_r($packageData); exit;

?>

<div class="right_col" role="main" style="min-height: 202px;">
    <div class="clearfix"></div>
    <div class="header-title-breadcrumb element-box-shadow">
        <div class="container" style="height: 36px;">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12 text-left">
                    <h3><?php echo !empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></h3>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12 hide-on-tablet">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo Url::to(['user/dashboard']); ?>">Dashboard</a></li> 
                        <li class="active"><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>

    <div class="row chart-section1">
        <div class="col-md-12">
            <div class="panel panel-default table-transactions">
                <div class="panel-body padding_30">
                    <?php if (Yii::$app->session->hasFlash('packagePurchaseSuccess')): ?>
                        <div class="alert alert-success">
                            Package purchase is <strong>Successful!</strong>.
                        </div>
                    <?php endif; ?>

                    <?php if (Yii::$app->session->hasFlash('packagePurchaseSuccessFailed')): ?>
                        <div class="alert alert-danger">
                            Package purchase is <strong>Failed!</strong> . Something went wrong. Please try later.
                        </div>
                    <?php endif; ?>

                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <?php $form = ActiveForm::begin(['id' => 'package-purchase-form']); ?>

                        <?php
                        echo $form->field($model, 'package')->label('Package')->dropDownList(array_combine($packagePrices, $packagePrices), [
                            'prompt' => '-- Select --',
                            'options' => ["$defaultPackagePrice" => ['Selected' => true]],
                                /* 'onchange'=> '
                                  console.log($model);
                                  ', */
                                /* 'onchange'=> '
                                  $.get( "https://blockchain.info/tobtc?currency=USD&value="+$(this).val(), function( value ) {
                                  $( ".place-bitcoin-value" ).html( ` <p> <strong> BTC : ${value} $ </strong> </p> ` ); console.log(value);
                                  });
                                  ' */                                ]
                        );
                        ?>

                        <div class ="place-bitcoin-value">
                            <?php //echo !empty($packageName) ? "<p> <strong> Name :  $packageName </strong></p>" : '';?>      
                            <?php echo!empty($defaultPackageUSDPrice) ? "<p> <strong> USD :  $defaultPackageUSDPrice $</strong></p>" : ''; ?>   
                        </div>    

                        <div class="form-group">
                            <?= Html::submitButton('Buy Now', ['class' => 'btn btn-primary']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                    
                    <div class="col-lg-8 col-md-8">
                        <h1>How to Buy Package</h1>
                        <ul class="custom-bullet">
                            <li>Recharge/Add Cash to your cash wallet from available <a href="<?php echo Url::toRoute(['site/payment']); ?>" target="_blank">E-Currencies</a>.</li>
                            <li>Select a Package from above dropdown list.</li>
                            <li>After selecting package, just click on "Buy Now".</li>
                            <li>That's all. You bought a package and that will be listed on <a href="<?php echo Url::toRoute(['order/my-orders']); ?>" target="_blank">Orders</a>.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
  
</div>

