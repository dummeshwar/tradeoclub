<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Withdrawal Summary';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="withdrwal-request-summary">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
        <?= Html::a('Withdrawal Request', ['request'], ['class' => 'btn btn-success']) ?>
    </p>
<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'user.username',

            [
                'header' => 'Amount (<i class="fa fa-btc">)',
                //'label' => 'Amount',
                'attribute' => 'paid_amount',
                'format'=>'raw',
                'filter'=>false,
                'value' => 'paid_amount',
            ],

            [
                'label' => 'From wallet',
                'attribute' => 'fromWallet',
                'format'=>'raw',
                'filter'=>false,
                'value' => 'fromWallet.name',
            ],

            // used for other purpose.
            [
                'label' => 'Admin charges',
                'attribute' => 'admin_comment',
                'format'=>'raw',
                'filter'=>false,
                'value' => function($model, $key, $index) {
                    return Yii::$app->params['transaction_charges']['withdrawal'] . "%";
                },
            ],
            // [
            //     'label' => 'E-currency',
            //     'attribute' => 'paymentGateway',
            //     'format' => 'raw',
            //     'filter'=> false,
            //     'value' => 'paymentGateway.name',
            // ],
            [
                'attribute' => 'status',
                'value' => 'transactionStatus',
                'format' => 'html',
                'filter' => ['1' => 'Success', '0' => 'Failed', '2' => 'Pending'],
            ],
            [
                'attribute' => 'admin_marked_status',
                'value' => 'transactionStatusGivenByAdmin',
                'format' => 'html',
                'filter' => ['1' => 'Approved', '0' => 'Rejected', '2' => 'Pending'],
            ],
            [
                'label' => 'Admin Remarks',
                'attribute' => 'admin_comment',
                'format'=>'raw',
                'filter'=>false,
                'value' => function($model, $key, $index) {
                    return !empty($model->admin_comment) ? $model->admin_comment : "No comments";
                },
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Created on/before',
                'filter' => '<div class="drp-container input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>' .
                DateRangePicker::widget([
                    'name' => 'createdAt',
                    'model' => $searchModel,
                    'convertFormat' => true,
                    'attribute' => 'created_at',
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'Y-m-d',
                            'separator' => ' to ',
                        ],
                        'opens' => 'left'
                    ]
                ]) . '</div>'
            ],
            // 'updated_at',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>
