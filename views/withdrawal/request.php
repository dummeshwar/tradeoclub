<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $user app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Url;

$this->title = 'Withdrawal Request';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="right_col" role="main" style="min-height: 202px;">
    <div class="clearfix"></div>
    <div class="header-title-breadcrumb element-box-shadow">
        <div class="container" style="height: 36px;">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12 text-left">
                    <h3><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></h3>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12 hide-on-tablet">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo Url::to(['user/dashboard']); ?>">Dashboard</a></li> 
                        <li class="active"><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>

    <div class="row chart-section1">
        <div class="col-md-12">
            <div class="panel panel-default table-transactions">
                <div class="panel-body padding_30">
                    <?php if (Yii::$app->session->hasFlash('withdrawalRequestSuccess')): ?>
                        <div class="alert alert-success">
                            You have submitted Withdrawal Request <strong>Successfully!</strong>.
                        </div>
                    <?php endif; ?>

                    <?php if (Yii::$app->session->hasFlash('withdrawalRequestFailed')): ?>
                        <div class="alert alert-danger">
                            Withdrawal request is <strong>Failed!</strong> . Something went wrong. Please try later.
                        </div>
                    <?php endif; ?>
                    <?php
                    $form = ActiveForm::begin([
                                'id' => 'withdrawal-request-form',
                                'layout' => 'horizontal',
                                'fieldConfig' => [
                                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                    'horizontalCssClasses' => [
                                        'label' => 'col-sm-2',
                                        'offset' => 'col-sm-offset-4',
                                    ],
                                ],
                    ]);
                    ?>

                    <?php
                    echo $form->field($model, 'from_wallet')
                            ->dropDownList(
                                    $activeWallets, // Flat array ('id'=>'label')
                                    ['prompt' => '-- Select wallet -- ']    // options
                    );

                    echo $form->field($model, 'amount')->label('Amount (<i class="fa fa-btc"> </i>)');
                    ;

                    //echo $form->field($model,'account_number');
                    //
            echo $form->field($model, 'e_currency')
                            ->dropDownList(
                                    $e_currencies, // Flat array ('id'=>'label')
                                    ['prompt' => '-- Select wallet -- ']    // options
                    );

                    echo $form->field($model, 'comment')->textarea();
                    ?>


                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'register-button', 'onclick' => "return confirm('Are you sure, you want to submit a withdrawal request ?')"]) ?>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>