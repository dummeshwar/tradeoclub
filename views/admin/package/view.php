<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Package */

$this->title = "Package name: " . $model->name;
// $this->params['breadcrumbs'][] = ['label' => 'Packages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="right_col" role="main" style="min-height: 202px;">
    <div class="clearfix"></div>
    <div class="header-title-breadcrumb element-box-shadow">
        <div class="container" style="height: 36px;">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12 text-left">
                    <h3><?php echo !empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></h3>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12 hide-on-tablet">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo Url::to(['user/dashboard']); ?>">Dashboard</a></li> 
                        <li class="active"><?php echo !empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>

    <div class="row chart-section1">
        <div class="col-md-12">
            <div class="panel panel-default table-transactions">
                <div class="panel-body padding_30">
                    <div class="package-view">

                        <p>
                            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                            <?=
                            Html::a('Delete', ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ])
                            ?>
                        </p>

                        <?=
                        DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                //  'id',
                                //  'name',
                                'price',
                                'points',
                                'description:ntext',
                                [
                                    'attribute' => 'status',
                                    'value' => !empty($model->status) ? "Active" : "Blocked",
                                ],
                                'no_of_days',
                                [
                                    'attribute' => 'created_at',
                                    'label' => 'Created On',
                                    'value' => Yii::$app->formatter->asDate($model->created_at, 'php:Y-m-d'),
                                ],
                                [
                                    'attribute' => 'image',
                                    'value' => function($model) {
                                        if (!empty($model->image)) {
                                            return Html::img(Yii::$app->params['packageImagesUpload'] . $model->image);
                                        } else {
                                            return "No Image found";
                                        }
                                    },
                                    'format' => 'raw',
                                ],
                            ],
                        ])
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
