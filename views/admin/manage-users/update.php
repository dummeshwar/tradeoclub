<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Update User: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-update">

    <?php if (Yii::$app->session->hasFlash('userUpdationFailed')): ?>
        <div class="alert alert-danger">
            User updation is <strong>Failed!</strong>. Please try again later.
        </div>
    <?php endif; ?>

    <?= $this->render('_form', [
        'model' => $model,
        'userProfile' => $userProfile,
        'activeUsers' => $activeUsers,
    ]) ?>

</div>
