<?php
use borales\extensions\phoneInput\PhoneInput;
use yii\helpers\Html;
use yii\jui\AutoComplete;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="right_col" role="main" style="min-height: 202px;">
<div class="user-prfile">

    <?php $form = ActiveForm::begin([
                'id' => 'user-prfile-form',
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-2',
                        'offset' => 'col-sm-offset-4',
                    ],
                ],
                'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>


    <?php if($model->isNewRecord): ?>

            <?php
        echo $form->field($model, 'parent_id', [
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-4',
            ],
            'inputTemplate' => '<div class="input-group"><span class="input-group-btn">' .
            '</span>{input}&nbsp;&nbsp;&nbsp;&nbsp;</div>',
        ])->widget(AutoComplete::className(), ['clientOptions' =>
            ['source' => $activeUsers]]);
        ?>

            <?= Html::button('Get Sponsor Id', ['class' => 'btn btn-primary btn-xs', 'style' => 'margin:0;bottom: 10px;position: relative;', 'onclick' => "
                         $.ajax({
                        type     :'POST',
                        cache    : false,
                        url  : '" . Url::to(['user/getd-default-sponsor-id']) . "',
                        success  : function(response) {
                                console.log('success: '+response.sponsorId);
                                $('#manageusers-parent_id').val(response.sponsorId)
                        },
                        error: function(){
                          console.log('failure : something went wrong while fetching sponsor ID. Please try again later.');
                        }
                        });return false;",]) ?>

    <?php
    echo $form->field($model, 'position', [
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-4',
        ]
    ])->inline()->radioList(array('L' => 'Left', 'R' => 'Right'));
    ?>

    <?php endif; ?>


    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'value'=>'']) ?>

    <?= $form->field($model, 'master_pin')->passwordInput(['maxlength' => true, 'value'=>'']) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?=
        $form->field($model, 'is_admin')->dropDownList(
                [1 => 'Admin', 0 => "Normal User"], // Flat array ('id'=>'label')
                ['prompt' => '-- Select Option -- ']    // option.
        );
    ?>

    <?=
        $form->field($model, 'status')->dropDownList(
                [1 => 'Active', 0 => "Blocked"], // Flat array ('id'=>'label')
                ['prompt' => '-- Select Option -- ']    // option.
        );
    ?>

    <?= $form->field($userProfile, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($userProfile, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($userProfile, 'picture')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
    ]); ?>

    <?php
    echo $form->field($userProfile, 'contact_number', [
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-3',
        ]
    ])->widget(PhoneInput::className(), [
        'jsOptions' => [
            'allowExtensions' => true,
            'nationalMode' => false,
            'allowDropdown' => true,
        ]
    ]);
    ?>

    <?= $form->field($userProfile, 'date_of_birth')->widget(yii\jui\DatePicker::className(), ['clientOptions' => ['dateFormat' => 'yyyy-MM-dd']]) ?>
    <?= $form->field($userProfile, 'address')->textarea(['rows' => '6']) ?>
    <?= $form->field($userProfile, 'skype_id') ?>
    <?= $form->field($userProfile, 'facebook_id') ?>
    <?= $form->field($userProfile, 'twitter_id') ?>
    <?= $form->field($userProfile, 'street') ?>
    <?= $form->field($userProfile, 'state') ?>
    <?= $form->field($userProfile, 'city') ?>
    <?= $form->field($userProfile, 'zip_code') ?>
    <?= $form->field($userProfile, 'national_id') ?>
    <?= $form->field($userProfile, 'passport_id') ?>
    <?= $form->field($userProfile, 'bank_statement')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
    ]); ?>


    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
