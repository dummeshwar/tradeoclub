<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\daterange\DateRangePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Users';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="right_col" role="main" style="min-height: 202px;">
    <div class="clearfix"></div>
    <div class="header-title-breadcrumb element-box-shadow">
        <div class="container" style="height: 36px;">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12 text-left">
                    <h3><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></h3>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12 hide-on-tablet">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo Url::to(['user/dashboard']); ?>">Dashboard</a></li> 
                        <li class="active"><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>

    <div class="row chart-section1">
        <div class="col-md-12">
            <div class="panel panel-default table-transactions">
                <div class="panel-body padding_30">
                    <div class="user-index">

                        <?php // echo $this->render('_search', ['model' => $searchModel]);   ?>

                        <p>
                            <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
                        </p>

                        <br />

                        <?= Html::beginForm(['admin/manage-users/status-change'], 'post'); ?>
                        <?= Html::dropDownList('action', '', [1 => 'Active', 0 => 'Blocked'], ['class' => 'dropdown',]) ?>
                        <?= Html::submitButton('Perform', ['class' => 'btn btn-xs btn-primary',]); ?>

                        <br />

                        <?php Pjax::begin(); ?>    <?=
                        GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                ['class' => 'yii\grid\CheckboxColumn'],
                                'username',
//            'userProfiles.first_name',
                                [
                                    'label' => "Sponsor ID",
                                    'attribute' => 'parent',
                                    'value' => 'parent.username',
                                    'filter' => false,
                                ],
                                'email:email',
                                [
                                    'attribute' => 'original_password',
                                    'label' => 'Password',
                                    'format' => 'html',
                                    'filter' => false,
                                ],
                                [
                                    'attribute' => 'original_master_pin',
                                    'label' => 'Key Pin',
                                    'format' => 'html',
                                    'filter' => false,
                                ],
                                // [
                                //     'attribute' => 'is_admin',
                                //     'value' => 'showUserType',
                                //     'format' => 'html',
                                //     'filter' => ['1' => 'Admin', '0' => 'Blocked'],
                                // ],
                                [
                                    'attribute' => 'status',
                                    'value' => 'userStatus',
                                    'format' => 'html',
                                    'filter' => ['1' => 'Active', '0' => 'Blocked'],
                                ],
                                [
                                    'attribute' => 'created_at',
                                    'label' => 'Created on/before',
                                    'filter' => '<div class="drp-container input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>' .
                                    DateRangePicker::widget([
                                        'name' => 'createdAt',
                                        'model' => $searchModel,
                                        'convertFormat' => true,
                                        'attribute' => 'created_at',
                                        'pluginOptions' => [
                                            'locale' => [
                                                'format' => 'Y-m-d',
                                                'separator' => ' to ',
                                            ],
                                            'opens' => 'left'
                                        ]
                                    ]) . '</div>'
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{view} {update}'
                                ],
                            ],
                        ]);
                        ?>

                        <?= Html::endForm(); ?>

                        <?php Pjax::end(); ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
