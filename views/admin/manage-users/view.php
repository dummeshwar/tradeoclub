<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = "User Name : " . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="right_col" role="main" style="min-height: 202px;">
<div class="user-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'username',
            'email:email',
            [
                'attribute' => 'is_admin',
                'value' => ($model->is_admin === 1) ? "Admin" : "Normal User",
            ],
            'userStatus',
            'userProfiles.first_name',
            'userProfiles.last_name',
            [
                'attribute'=>'userProfiles.picture',
                'value' => function($model){
                    if (!empty($model->userProfiles->picture)) {
                        return Html::img(Yii::$app->params['userImagesUpload'] . $model->userProfiles->picture);
                    } else {
                        return "No Image found";
                    }
                },
                'format' => 'raw',
            ],
            'userProfiles.skype_id',
            'userProfiles.facebook_id',
            'userProfiles.twitter_id',
            'userProfiles.date_of_birth',
            'userProfiles.country_code',
            'userProfiles.contact_number',
            'userProfiles.address',
            'userProfiles.state',
            'userProfiles.city',
            'userProfiles.street',
            'userProfiles.zip_code',
            'userProfiles.national_id',
            [
                'attribute'=>'bank_statement',
                'value' => function($model){
                    if (!empty($model->userProfiles->bank_statement)) {
                        return Html::img(Yii::$app->params['userBankStatements'] . $model->userProfiles->bank_statement);
                    } else {
                        return "No Image found";
                    }
                },
                'format' => 'raw',
            ],
            'createdDate',
            'updatedDate',
            //'lastLogin',
        ],
    ])
    ?>

</div>
</div>
