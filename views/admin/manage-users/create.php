<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Create User';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <?php if (Yii::$app->session->hasFlash('userCreationFailed')): ?>
        <div class="alert alert-danger">
            User creation is <strong>Failed!</strong>. Please try again later.
        </div>
    <?php endif; ?>

    <?=
    $this->render('_form', [
        'model' => $model,
        'userProfile' => $userProfile,
        'activeUsers' => $activeUsers,
    ])
    ?>

</div>
