
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ManageUsersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="manage-users-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'parent_id') ?>

    <?= $form->field($model, 'current_parent_id') ?>

    <?= $form->field($model, 'position') ?>

    <?php // echo $form->field($model, 'password') ?>

    <?php // echo $form->field($model, 'password_reset_token') ?>

    <?php // echo $form->field($model, 'master_pin') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'auth_key') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'is_deleted') ?>

    <?php // echo $form->field($model, 'is_admin') ?>

    <?php // echo $form->field($model, 'left_user') ?>

    <?php // echo $form->field($model, 'right_user') ?>

    <?php // echo $form->field($model, 'left_user_id') ?>

    <?php // echo $form->field($model, 'right_user_id') ?>

    <?php // echo $form->field($model, 'lc_fund') ?>

    <?php // echo $form->field($model, 'rc_fund') ?>

    <?php // echo $form->field($model, 'binary_calculated_order_ids') ?>

    <?php // echo $form->field($model, 'level') ?>

    <?php // echo $form->field($model, 'original_password') ?>

    <?php // echo $form->field($model, 'original_master_pin') ?>

    <?php // echo $form->field($model, 'last_login') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
