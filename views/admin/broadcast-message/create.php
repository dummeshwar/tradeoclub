<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BroadcastMessage */

$this->title = 'Create Broadcast Message';
$this->params['breadcrumbs'][] = ['label' => 'Broadcast Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="broadcast-message-create">
  
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
