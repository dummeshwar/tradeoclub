<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\BroadcastMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="broadcast-message">

  <?php $form = ActiveForm::begin([
              'id' => 'broadcast-message-form',
              'layout' => 'horizontal',
              'fieldConfig' => [
                  'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                  'horizontalCssClasses' => [
                      'label' => 'col-sm-2',
                      'offset' => 'col-sm-offset-4',
                  ],
              ],
              'options' => ['enctype' => 'multipart/form-data'],
  ]); ?>

  <?php if (Yii::$app->session->hasFlash('messageCreated')): ?>
    <div class="alert alert-success alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <p><i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('messageCreated') ?></p>
    </div>
  <?php endif; ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
      </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
