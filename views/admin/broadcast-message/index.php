<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BroadcastMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Broadcast Messages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="broadcast-message-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Broadcast Message', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <br />

    <?=Html::beginForm(['admin/broadcast-message/status-change'],'post');?>
    <?=Html::dropDownList('action','',[1=>'Active',0=>'Blocked'],['class'=>'dropdown',])?>
    <?=Html::submitButton('Perform', ['class' => 'btn btn-xs btn-primary',]);?>

    <br />

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'],
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Description',
                'attribute' => 'description',
                'format'=>'raw',
                'filter'=>false,
            ],
            [
                'attribute' => 'status',
                'value' => 'messageStatus',
                'format' => 'html',
                'filter' => ['1' => 'Active', '0' => 'Blocked'],
            ],
            //'created_at',
            [
                'attribute' => 'created_at',
                'label' => 'Created on/before',
                'filter' => '<div class="drp-container input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>' .
                DateRangePicker::widget([
                    'name' => 'createdAt',
                    'model' => $searchModel,
                    'convertFormat' => true,
                    'attribute' => 'created_at',
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'Y-m-d',
                            'separator' => ' to ',
                        ],
                        'opens' => 'left'
                    ]
                ]) . '</div>'
            ],
            // 'no_of_ads_to_show',
            // 'no_of_days',
            // 'update_at',
            // 'image',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
