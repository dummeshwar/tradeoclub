<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BroadcastMessage */

$this->title = '';
$this->params['breadcrumbs'][] = ['label' => 'Broadcast Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="broadcast-message-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'description:ntext',
            [
                'attribute' => 'status',
                'value' => !empty($model->status) ? "Active" : "Blocked",
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Created On',
                'value' => Yii::$app->formatter->asDate($model->created_at, 'php:Y-m-d'),
            ],
        ],
    ]) ?>

</div>
