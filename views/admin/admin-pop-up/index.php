<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pop Ups';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="right_col" role="main" style="min-height: 202px;">
    <div class="clearfix"></div>
    <div class="header-title-breadcrumb element-box-shadow">
        <div class="container" style="height: 36px;">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12 text-left">
                    <h3><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></h3>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12 hide-on-tablet">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo Url::to(['user/dashboard']); ?>">Dashboard</a></li> 
                        <li class="active"><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>

    <div class="row chart-section1">
        <div class="col-md-12">
            <div class="panel panel-default table-transactions">
                <div class="panel-body padding_30">
                    <div class="admin-pop-ups-index">

                        <p>
                            <?= Html::a('Create Pop Ups', ['create'], ['class' => 'btn btn-success']) ?>
                        </p>

                        <br />

                        <?= Html::beginForm(['admin/admin-pop-up/status-change'], 'post'); ?>
                        <?= Html::dropDownList('action', '', [1 => 'Active', 0 => 'Blocked'], ['class' => 'dropdown',]) ?>
                        <?= Html::submitButton('Perform', ['class' => 'btn btn-xs btn-primary',]); ?>

                        <br />

                        <?=
                        GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\CheckboxColumn'],
                                ['class' => 'yii\grid\SerialColumn'],
                                'name',
                                [
                                    'attribute' => 'status',
                                    'value' => 'popUpStatus',
                                    'format' => 'html',
                                    'filter' => ['1' => 'Active', '0' => 'Blocked'],
                                ],
                                //'created_at',
                                [
                                    'attribute' => 'created_at',
                                    'label' => 'Created on/before',
                                    'filter' => '<div class="drp-container input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>' .
                                    DateRangePicker::widget([
                                        'name' => 'createdAt',
                                        'model' => $searchModel,
                                        'convertFormat' => true,
                                        'attribute' => 'created_at',
                                        'pluginOptions' => [
                                            'locale' => [
                                                'format' => 'Y-m-d',
                                                'separator' => ' to ',
                                            ],
                                            'opens' => 'left'
                                        ]
                                    ]) . '</div>'
                                ],
                                // 'no_of_ads_to_show',
                                // 'no_of_days',
                                // 'update_at',
                                // 'image',
                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]);
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>