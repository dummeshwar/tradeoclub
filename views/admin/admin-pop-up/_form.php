<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\AdminPopUps */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-pop-ups-form">

  <?php $form = ActiveForm::begin([
              'id' => 'admin-pop-ups-form',
              'layout' => 'horizontal',
              'fieldConfig' => [
                  'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                  'horizontalCssClasses' => [
                      'label' => 'col-sm-2',
                      'offset' => 'col-sm-offset-4',
                  ],
              ],
              'options' => ['enctype' => 'multipart/form-data'],
  ]); ?>

  <?php if (Yii::$app->session->hasFlash('popUpCreated')): ?>
    <div class="alert alert-success alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <p><i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('popUpCreated') ?></p>
    </div>
  <?php endif; ?>


    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'image_desktop')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
    ]); ?>

    <?php echo $form->field($model, 'image_mobile')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
    ]); ?>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
      </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
