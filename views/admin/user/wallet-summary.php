<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BalanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Wallets Summary';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="right_col" role="main" style="min-height: 202px;">
    <div class="clearfix"></div>
    <div class="header-title-breadcrumb element-box-shadow">
        <div class="container" style="height: 36px;">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12 text-left">
                    <h3><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></h3>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12 hide-on-tablet">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo Url::to(['user/dashboard']); ?>">Dashboard</a></li> 
                        <li class="active"><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>

    <div class="row chart-section1">
        <div class="col-md-12">
            <div class="panel panel-default table-transactions">
                <div class="panel-body padding_30">
                    <div class="balance-index">

                        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                        <p>
                            <?= Html::a('Add Cash', ['add-fund'], ['class' => 'btn btn-success']) ?>
                            <?= Html::a('Deduct Fund', ['deduct-fund'], ['class' => 'btn btn-success']) ?>
                        </p>
                        <?=
                        GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                'user.username',
                                'wallet.name',
                                [
                                    'header' => 'Amount (<i class="fa fa-usd"> </i>)',
                                    //'label' => 'Amount',
                                    'attribute' => 'amount',
                                    'format' => 'raw',
                                    'filter' => false,
                                    'value' => 'amount',
                                ],
                                [
                                    'attribute' => 'created_at',
                                    'label' => 'Created on/before',
                                    'filter' => '<div class="drp-container input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>' .
                                    DateRangePicker::widget([
                                        'name' => 'createdAt',
                                        'model' => $searchModel,
                                        'convertFormat' => true,
                                        'attribute' => 'created_at',
                                        'pluginOptions' => [
                                            'locale' => [
                                                'format' => 'Y-m-d',
                                                'separator' => ' to ',
                                            ],
                                            'opens' => 'left'
                                        ]
                                    ]) . '</div>'
                                ],
                            // 'updated_at',
                            //['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


