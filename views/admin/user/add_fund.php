<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $user app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Url;

$this->title = 'Wallet Recharge';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="right_col" role="main" style="min-height: 202px;">
    <div class="clearfix"></div>
    <div class="header-title-breadcrumb element-box-shadow">
        <div class="container" style="height: 36px;">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12 text-left">
                    <h3><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></h3>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12 hide-on-tablet">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo Url::to(['user/dashboard']); ?>">Dashboard</a></li> 
                        <li class="active"><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>

    <div class="row chart-section1">
        <div class="col-md-12">
            <div class="panel panel-default table-transactions">
                <div class="panel-body padding_30">
                    <div class="credit-wallet container">

                        <?php if (Yii::$app->session->hasFlash('creditSuccessful')): ?>
                            <div class="alert alert-success">
                                Amount is credited <strong>Successfully!</strong>.
                            </div>
                        <?php endif; ?>

                        <?php if (Yii::$app->session->hasFlash('creditFailed')): ?>
                            <div class="alert alert-danger">
                                Amount is not <strong>Credited!</strong> . Something went wrong.
                            </div>
                        <?php endif; ?>


                        <?php
                        $form = ActiveForm::begin([
                                    'id' => 'credit-wallet-form',
                                    'layout' => 'horizontal',
                                    'fieldConfig' => [
                                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                        'horizontalCssClasses' => [
                                            'label' => 'col-sm-2',
                                            'offset' => 'col-sm-offset-4',
                                        ],
                                    ],
                        ]);
                        ?>

                        <?php
                        echo $form->field($model, 'to_user', [
                            'horizontalCssClasses' => [
                                'wrapper' => 'col-sm-6',
                            ],
                            'inputTemplate' => '<div class="input-group"><span class="input-group-btn">' .
                            '</span>{input}&nbsp;&nbsp;&nbsp;&nbsp;</div>',
                        ])->widget(AutoComplete::className(), ['clientOptions' =>
                            ['source' => $allUsers]]);
                        ?>

                        <?php
                        echo $form->field($model, 'to_wallet')
                                ->dropDownList(
                                        $activeWallets, // Flat array ('id'=>'label')
                                        ['prompt' => '-- Select wallet -- ']    // options
                        );

                        echo $form->field($model, 'amount')->label('Amount (<i class="fa fa-usd"> </i>)');

                        echo $form->field($model, 'comment')->textarea();
                        ?>


                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'register-button']) ?>
                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


