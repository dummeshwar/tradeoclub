<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Withdrawal Summary';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="right_col" role="main" style="min-height: 202px;">
    <div class="clearfix"></div>
    <div class="header-title-breadcrumb element-box-shadow">
        <div class="container" style="height: 36px;">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12 text-left">
                    <h3><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></h3>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12 hide-on-tablet">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo Url::to(['user/dashboard']); ?>">Dashboard</a></li> 
                        <li class="active"><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>

    <div class="row chart-section1">
        <div class="col-md-12">
            <div class="panel panel-default table-transactions">
                <div class="panel-body padding_30">
                    <div class="withdrwal-request-summary">
                        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                        <?php if (Yii::$app->session->hasFlash('withdrawalRequestSuccess')): ?>
                            <div class="alert alert-success">
                                Withdrawal request is <strong>Successful!</strong>.
                            </div>
                        <?php endif; ?>

                        <?php if (Yii::$app->session->hasFlash('withdrawalRequestDeclined')): ?>
                            <div class="alert alert-danger">
                                Withdrawal request <strong>Declined!</strong>.
                            </div>
                        <?php endif; ?>

                        <?=
                        GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                'user.username',
                                [
                                    'header' => 'Paid Amount (<i class="fa fa-usd">)',
                                    'label' => 'Amount',
                                    'attribute' => 'actual_amount',
                                    'format' => 'raw',
                                    'filter' => false,
                                    'value' => 'actual_amount',
                                ],
                                [
                                    'header' => 'Final Amount (<i class="fa fa-usd">)',
                                    'label' => 'Amount',
                                    'attribute' => 'paid_amount',
                                    'format' => 'raw',
                                    'filter' => false,
                                    'value' => 'paid_amount',
                                ],
                                // [
                                //     'label' => 'From wallet',
                                //     'attribute' => 'fromWallet',
                                //     'format'=>'raw',
                                //     'filter'=>false,
                                //     'value' => 'fromWallet.name',
                                // ],
                                // used for other purpose.
                                // [
                                //     'label' => 'Admin charges',
                                //     'attribute' => 'admin_comment',
                                //     'format'=>'raw',
                                //     'filter'=>false,
                                //     'value' => function($model, $key, $index) {
                                //         return Yii::$app->params['transaction_charges']['withdrawal'] . "%";
                                //     },
                                // ],
                                # [
                                #     'label' => 'E-currency',
                                #     'attribute' => 'paymentGateway',
                                #     'format' => 'raw',
                                #     'filter'=> false,
                                #     'value' => 'paymentGateway.name',
                                # ],
                                // [
                                //     'attribute' => '',
                                //     'value' => 'transactionStatus',
                                //     'format' => 'html',
                                //     'filter' => ['1' => 'Success', '0' => 'Rejected', '2' => 'Pending'],
                                // ],
                                // [
                                //     'attribute' => 'admin_marked_status',
                                //     'value' => 'transactionStatusGivenByAdmin',
                                //     'format' => 'html',
                                //     'filter' => ['1' => 'Approved', '0' => 'Rejected', '2' => 'Pending'],
                                // ],
                                [
                                    'label' => 'User Address',
                                    'attribute' => 'user_comment',
                                    'format' => 'raw',
                                    'filter' => false,
                                    'value' => function($model, $key, $index) {
                                        return !empty($model->user_comment) ? $model->user_comment : "No comments";
                                    },
                                ],
                                [
                                    'label' => 'Admin Remarks',
                                    'attribute' => 'admin_comment',
                                    'format' => 'raw',
                                    'filter' => false,
                                    'value' => function($model, $key, $index) {
                                        return !empty($model->admin_comment) ? $model->admin_comment : "No comments";
                                    },
                                ],
                                // [
                                //     'attribute' => 'created_at',
                                //     'label' => 'Created on/before',
                                //     'filter' => '<div class="drp-container input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>' .
                                //     DateRangePicker::widget([
                                //         'name' => 'createdAt',
                                //         'model' => $searchModel,
                                //         'convertFormat' => true,
                                //         'attribute' => 'created_at',
                                //         'pluginOptions' => [
                                //             'locale' => [
                                //                 'format' => 'Y-m-d',
                                //                 'separator' => ' to ',
                                //             ],
                                //             'opens' => 'left'
                                //         ]
                                //     ]) . '</div>'
                                // ],
                                // 'updated_at',
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{process}',
                                    'buttons' => [
                                        'process' => function ($url, $model) {
                                            if ($model->status == 2) {
                                                return Html::a(
                                                                '<span class="fa fa-sign-out"></span>', $url, [
                                                            'title' => 'Process Withdrawal',
                                                            'data-pjax' => '0',
                                                                ]
                                                );
                                            } else {
                                                return "<span class='label label-success'>Processed</span>";
                                            }
                                        },
                                    ],
                                ],
                            ],
                        ]);
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
