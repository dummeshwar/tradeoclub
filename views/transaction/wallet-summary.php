<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MoneyTransferSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Wallet Summary';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="right_col" role="main" style="min-height: 202px;">
    <div class="clearfix"></div>
    <div class="header-title-breadcrumb element-box-shadow">
        <div class="container" style="height: 36px;">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12 text-left">
                    <h3><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></h3>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12 hide-on-tablet">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo Url::to(['user/dashboard']); ?>">Dashboard</a></li> 
                        <li class="active"><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>

    <div class="row chart-section1">
        <div class="col-md-12">
            <div class="panel panel-default table-transactions">
                <div class="panel-body padding_30">
                    <?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'label' => 'Transaction Id',
                                'attribute' => 'transaction',
                                'value' => 'transaction_id',
                            ],
                            [
                                'label' => 'From wallet',
                                'attribute' => 'from_wallet_id',
                                'format' => 'html',
                                'filter' => Yii::$app->params['wallets_by_keys'],
                                //'filter'=>false,
                                //'value' => 'fromWallet.name',
                                // 'value' => function($model, $key, $index) {
                                //     return !empty(Yii::$app->params['wallets_by_keys'][$model->from_wallet_id]) ? Yii::$app->params['wallets_by_keys'][$model->from_wallet_id] : "None";
                                // },
                                'value' => 'fromWalletNames',
                            ],
                            [
                                'label' => 'To Wallet',
                                'attribute' => 'to_wallet_id',
                                'format' => 'html',
                                'filter' => Yii::$app->params['wallets_by_keys'],
                                //'filter'=>false,
                                // 'value' => function($model, $key, $index) {
                                //     return !empty(Yii::$app->params['wallets_by_keys'][$model->to_wallet_id]) ? Yii::$app->params['wallets_by_keys'][$model->to_wallet_id] : "None";
                                // },
                                'value' => 'toWalletNames',
                            ],
                            # [
                            #     'label' => 'From User',
                            #     'attribute' => 'fromUser',
                            #     'value' => 'fromUser.username',
                            # ],
                            # [
                            #     'label' => 'To User',
                            #     'attribute' => 'toUser',
                            #     'value' => 'toUser.username',
                            # ],
                            [
                                'header' => 'Amount (<i class="fa fa-btc">)',
                                //'label' => 'Amount',
                                'attribute' => 'paid_amount',
                                'format' => 'raw',
                                'filter' => false,
                            ],
                            [
                                'label' => 'Remarks',
                                'filter' => false,
                                'attribute' => 'transaction_mode',
                                'value' => 'transaction_mode',
                            ],
                            [
                                'attribute' => 'status',
                                'format' => 'html',
                                'filter' => false,
                                'value' => function($model, $key, $index) {
                                    return ($model->status == 1) ? "Success" : "Failed";
                                },
                            ],
                            [
                                'attribute' => 'created_at',
                                'label' => 'Date on/before',
                                'filter' => '<div class="drp-container input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>' .
                                DateRangePicker::widget([
                                    'name' => 'createdAt',
                                    'model' => $searchModel,
                                    'convertFormat' => true,
                                    'attribute' => 'created_at',
                                    'pluginOptions' => [
                                        'locale' => [
                                            'format' => 'Y-m-d',
                                            'separator' => ' to ',
                                        ],
                                        'opens' => 'left'
                                    ]
                                ]) . '</div>'
                            ],
                        ],
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
