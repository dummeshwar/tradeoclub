<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;


/* @var $this yii\web\View */
/* @var $searchModel app\models\MoneyTransferSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Binary Referral';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="direct-referral-index table-responsive">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Transaction Id',
                'attribute' => 'transaction',
                'value' => 'transaction_id',
            ],

            # [
            #     'label' => 'From wallet',
            #     'attribute' => 'fromWallet',
            #     'format'=>'raw',
            #     'filter'=>false,
            #     'value' => 'fromWallet.name',
            # ],

            [
                'label' => 'Wallet',
                'attribute' => 'toWallet',
                'format'=>'raw',
                'filter'=>false,
                'value' => 'toWallet.name',
            ],

            # [
            #     'label' => 'From User',
            #     'attribute' => 'fromUser',
            #     'value' => 'fromUser.username',
            # ],

            # [
            #     'label' => 'To User',
            #     'attribute' => 'toUser',
            #     'value' => 'toUser.username',
            # ],
            [
                'header' => 'Amount (<i class="fa fa-usd">)',
                //'label' => 'Amount',
                'attribute' => 'paid_amount',
                'format'=>'raw',
                'filter'=>false,
            ],

            [
                'label' => 'Mode',
                'filter'=>false,
                'attribute' => 'transaction_mode',
                'value' => 'transaction_mode',
            ],


            [
                'attribute' => 'status',
                'format' => 'html',
                'filter'=> false,
                'value' => function($model, $key, $index) {
                    return ($model->status == 1) ? "Success" : "Failed";
                },
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Date on/before',
                'filter' => '<div class="drp-container input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>' .
                DateRangePicker::widget([
                    'name' => 'createdAt',
                    'model' => $searchModel,
                    'convertFormat' => true,
                    'attribute' => 'created_at',
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'Y-m-d',
                            'separator' => ' to ',
                        ],
                        'opens' => 'left'
                    ]
                ]) . '</div>'
            ],

        ],
    ]); ?>
</div>
