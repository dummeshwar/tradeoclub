<?php

use yii\helpers\Url;

	require_once( "lib/cryptobox.class.php" );
	$this->title = 'Add Fund Request';
	$this->params['breadcrumbs'][] = $this->title;

	$def_language	= "en";	// default payment box language; en - English, es - Spanish, fr - French, etc

	// Remove all the characters from the string other than a..Z0..9_-@.
	//$orderID = preg_replace('/[^A-Za-z0-9\.\_\-\@]/', '', $orderID);
	//$userID = preg_replace('/[^A-Za-z0-9\.\_\-\@]/', '', $userID);

	$options = array(
		"public_key"  => "38066AAEUv76Bitcoin77BTCPUBhPETGvI7fbMOeGuFyOxXS3T",         // place your public key from gourl.io
		"private_key" => "38066AAEUv76Bitcoin77BTCPRVWHhj6YbtSvH3RZ6CqGlSiMx",         // place your private key from gourl.io
		"webdev_key"  => "", 		// optional, gourl affiliate program key
		"orderID"     => $order_id,   // few your users can have the same orderID but combination 'orderID'+'userID' should be unique
		"userID"      => $user_id, 	// optional; place your registered user id here (user1, user2, etc)
				// for example, on premium page you can use for all visitors: orderID="premium" and userID="" (empty)
				// when userID value is empty - system will autogenerate unique identifier for every user and save it in cookies
		"userFormat"  => "COOKIE",   // save your user identifier userID in cookies. Available: COOKIE, SESSION, IPADDRESS, MANUAL
		"amount"   	  => $amount,			// amount in cryptocurrency or in USD below
		//"amountUSD"   => 2,			// price is 2 USD; it will convert to cryptocoins amount, using Live Exchange Rates
									// For convert fiat currencies Euro/GBP/etc. to USD, use function convert_currency_live()
		"period"      => "24 HOUR",  // payment valid period, after 1 day user need to pay again
		"iframeID"    => "",         // optional; when iframeID value is empty - system will autogenerate iframe html payment box id
		"language"	  => $def_language // text on EN - english, FR - french, please contact us and we can add your language
		);

	// Initialise Bitcoin Payment Class
	$box = new Cryptobox ($options);

// echo '<pre>'; print_r($box); exit;

	// Display payment box with custom width = 560 px and big qr code / or successful result
	$payment_box = $box->display_cryptobox(true, 560, 230, "border-radius:15px;border:1px solid #eee;padding:3px 6px;margin:10px;",
					"display:inline-block;max-width:580px;padding:15px 20px;border:1px solid #eee;margin:7px;line-height:25px;");

//echo '<pre>'; print_r($payment_box); exit;

	// Language selection list for payment box (html code)
	$languages_list = display_language_box($def_language);

    // Log
    $message = "";

    // A. Process Received Payment
    if ($box->is_paid())
	{
		$message .= "A. User will see this message during 24 hours after payment has been made!";

		$message .= "<br>".$box1->amount_paid()." ".$box1->coin_label()."  received<br>";

		// Your code here to handle a successful cryptocoin payment/captcha verification
		// For example, give user 24 hour access to your member pages
		// ...

		// Please use IPN (instant payment notification) function cryptobox_new_payment() for update db records, etc
		// Function cryptobox_new_payment($paymentID = 0, $payment_details = array(), $box_status = "") called every time
		// when a new payment from any user is received.
		// IPN description: https://gourl.io/api-php.html#ipn
	}
	else $message .= "";

	?>
	<?php $this->registerJsFile('/js/cryptobox.min.js', ['position' => 1]); ?>
	
	<div class="right_col" role="main" style="min-height: 596px;">
    <div class="clearfix"></div>
    <div class="header-title-breadcrumb element-box-shadow">
        <div class="container" style="height: 36px;">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12 text-left">
                    <h3><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></h3>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12 hide-on-tablet">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo Url::to(['user/dashboard']); ?>">Dashboard</a></li> 
                        <li class="active"><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>
    <div class="spacer_30"></div>
    <div class="clearfix"></div>
	<div style='margin:30px 0 5px 480px'>Language: &#160; <?= $languages_list ?></div>
	<?= $payment_box ?>
	<?= $message ?>
	</div>
