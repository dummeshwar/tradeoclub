<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Utils;


$this->title = 'Add TGC Tokens';
$this->params['breadcrumbs'][] = $this->title;

?>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">-->
<link rel="stylesheet" href="../css/timeline_style.css">
<style>
.blinking{
	animation:blinkingText 0.8s infinite;
}
@keyframes blinkingText{
	0%{color: #ffffff;	}
	/*49%{color: transparent;}*/
	50%{color: transparent;}
	/*99%{color:transparent;}*/
	100%{color: #ffffff;}
}
</style>
<div class="tcg-points container">
    <div class="row">
    <?php if (Yii::$app->session->hasFlash('tcgPointsPurchaseSuccess')): ?>
        <div class="alert alert-success">
            TGC Tokens added <strong>Successfully!</strong>.
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('tcgPointsPurchaseSuccessFailed')): ?>
        <div class="alert alert-danger">
            <strong>Failed!</strong> . Something went wrong. Please try later.
        </div>
    <?php endif; ?>

        <div class="col-lg-4 col-md-4">
            <?php $form = ActiveForm::begin(['id' => 'tcg-points-form']); ?>

                <?php echo $form->field($model,'points'); ?>
                <input type="hidden" id="btc-value" name="btc-value" value="<?php echo rtrim(sprintf('%.15F', Yii::$app->params['points_to_BTC']), '0'); ?>">
                <p class="points-to-btc"></p>
                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-3" style="font-weight:bold;">
            <p>AVAILABLE TOKENS – 50,000,000</p>
            <ul style="line-height: 2; padding: 0;list-style-type: none;">
                <li>1 TGC => 0.0001 BTC</li>
                <li>1000 TGC => 0.1 BTC </li>
                <li>10,000 TGC => 1 BTC</li>
                <li>100,000 TGC => 10 BTC</li>
            </ul>
        </div>
        <div class="col-md-3" style="font-weight:bold;">
            <p>TGC TOKENS DISTRIBUTION</p>
            <ul style="line-height: 2; padding: 0;list-style-type: none;">
                <li>Basic (0.1BTC) => 100 TGC tokens </li>
                <li>Starter (0.3BTC) => 300 TGC tokens </li>
                <li>Advance (0.5BTC) => 500 TGC tokens</li>
                <li>Elite (1BTC) => 1000 TGC tokens</li>
                <li>Ultra (2BTC) => 3000 TGC tokens</li>
                <li>Premium (3BTC) => 5000 TGC tokens</li>
                <li>Mega (5BTC) => 10,000 TGC tokens</li>
            </ul>
        </div>
    </div>
</div>
<!--    <div class="row">
        <div class="col-md-6">
            <h2></h2>
            <ul style="list-style-type: none">
                <li>dfgdh</li>
                <li>dfghfdh</li>
                <li>dfghdfh</li>
                <li>dfghgfdh</li>
                <li>dghfgdh</li>
            </ul>
        </div>
        <div class="col-md-6">
            <ul style="list-style-type: none">
                <li>dfgdh</li>
                <li>dfghfdh</li>
                <li>dfghdfh</li>
                <li>dfghgfdh</li>
                <li>dghfgdh</li>
            </ul>
        </div>
    </div>-->
<!--    <div class="row">
        <div class="col-md-12">
              <section id=timeline>
                <h1>Timeline</h1>
                <p class="leader"></p>
                <div class="demo-card-wrapper">
                        <div class="demo-card demo-card--step1">
                                <div class="head">
                                        <div class="number-box">
                                                <span>2018</span>
                                        </div>
                                        <h2>JUNE</h2>
                                </div>
                                <div class="body">
                                        <p>Start Token Distribution to Members.</p>
                                        <img src="../images/timeline/TOKEN_DISTRIBUTION_TO_MEMBERS.jpg" alt="Graphic">
                                </div>
                        </div>

                        <div class="demo-card demo-card--step2">
                                <div class="head">
                                        <div class="number-box">
                                                <span>2018</span>
                                        </div>
                                        <h2>OCTOBER</h2>
                                </div>
                                <div class="body">
                                        <p>Internal Exchange Launch.</p>
                                        <img src="../images/timeline/internal_exchange_launch.jpg" alt="Graphic">
                                </div>
                        </div>

                        <div class="demo-card demo-card--step3">
                                <div class="head">
                                        <div class="number-box">
                                                <span>2018</span>
                                        </div>
                                        <h2>DECEMBER</h2>
                                </div>
                                <div class="body">
                                        <p>ICO Starts.</p>
                                        <img src="../images/timeline/ico_starts.jpg" alt="Graphic">
                                </div>
                        </div>

                        <div class="demo-card demo-card--step4">
                                <div class="head">
                                        <div class="number-box">
                                                <span>2019</span>
                                        </div>
                                         <h2><span class="small">Subtitle</span> Consistency</h2> 
                <h2>JANUARY</h2>
                                </div>
                                <div class="body">
                                        <p>ICO Ends.</p>
                                        <img src="../images/timeline/ICO_ENDS.jpg" alt="Graphic">
                                </div>
                        </div>

                        <div class="demo-card demo-card--step5">
                                <div class="head">
                                        <div class="number-box">
                                                <span>2019</span>
                                        </div>
                                        <h2>FEBRUARY</h2>
                                </div>
                                <div class="body">
                                        <p>Wallet Launch.</p>
                                        <img src="../images/timeline/WALLET_LAUNCH.jpg" alt="Graphic">
                                </div>
                        </div>
                    
                        <div class="demo-card demo-card--step6">
                                <div class="head">
                                        <div class="number-box">
                                                <span>2019</span>
                                        </div>
                                         <h2><span class="small">Subtitle</span> Consistency</h2> 
                <h2>MARCH</h2>
                                </div>
                                <div class="body">
                                        <p>Available in Exchanges.</p>
                                        <img src="../images/timeline/AVAILABLE_IN_EXCHANGES.jpg" alt="Graphic">
                                </div>
                        </div>

                </div>
            </section>
        </div>
    </div>-->
<section class="content-header">
    <h1 style="font-weight: bold;font-size: 35px;">
      TIMELINE
    </h1>
  </section>
<section class="content">

      <!-- row -->
      <div class="row">
        <div class="col-md-10">
          <!-- The time line -->
          <ul class="timeline">
            <!-- timeline time label -->
            <li class="time-label">
                  <span class="bg-red">
                    JUNE 2018
                  </span>
            </li>
            <!-- /.timeline-label -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-envelope bg-blue"></i>

              <div class="timeline-item">
                <!--<span class="time"><i class="fa fa-clock-o"></i> 12:05</span>-->

                <h3 class="timeline-header">Start Token Distribution to Members.</h3>

                <div class="timeline-body">
                    <img src="../images/timeline/TOKEN_DISTRIBUTION_TO_MEMBERS.jpg" alt="Graphic" style="width:100%">
                </div>
              <!--  <div class="timeline-footer">
                  <a class="btn btn-primary btn-xs">Read more</a>
                  <a class="btn btn-danger btn-xs">Delete</a>
                </div>-->
              </div>
            </li>
            <!-- END timeline item -->
            
            <!-- END timeline item -->
            <!-- timeline time label -->
            <li class="time-label">
                  <span class="bg-green">
                    DECEMBER 2018
                  </span>
            </li>
            <!-- /.timeline-label -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-envelope bg-blue"></i>

              <div class="timeline-item">
                <!--<span class="time"><i class="fa fa-clock-o"></i> 12:05</span>-->
                                        
                <h3 class="timeline-header">Internal Exchange Launch.</h3>

                <div class="timeline-body">
                    <img src="../images/timeline/internal_exchange_launch.jpg" alt="Graphic" style="width:100%">
                </div>
              <!--  <div class="timeline-footer">
                  <a class="btn btn-primary btn-xs">Read more</a>
                  <a class="btn btn-danger btn-xs">Delete</a>
                </div>-->
              </div>
            </li>
            <!-- END timeline item -->
            <!-- timeline time label -->
            <li class="time-label">
                  <span class="bg-yellow">
                    JANUARY 2019
                  </span>
            </li>
            <!-- /.timeline-label -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-envelope bg-blue"></i>

              <div class="timeline-item">
                <!--<span class="time"><i class="fa fa-clock-o"></i> 12:05</span>-->

                <h3 class="timeline-header">ICO Starts.</h3>

                <div class="timeline-body">
                    <img src="../images/timeline/ico_starts.jpg" alt="Graphic" style="width:100%">
                </div>
              <!--  <div class="timeline-footer">
                  <a class="btn btn-primary btn-xs">Read more</a>
                  <a class="btn btn-danger btn-xs">Delete</a>
                </div>-->
              </div>
            </li>
            <!-- END timeline item -->
            <!-- timeline time label -->
            <li class="time-label">
                  <span class="bg-blue">
                    JANUARY 2019
                  </span>
            </li>
            <!-- /.timeline-label -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-envelope bg-blue"></i>

              <div class="timeline-item">
                <!--<span class="time"><i class="fa fa-clock-o"></i> 12:05</span>-->

                <h3 class="timeline-header">ICO Ends.</h3>

                <div class="timeline-body">
                    <img src="../images/timeline/ICO_ENDS.jpg" alt="Graphic" style="width:100%">
                </div>
              <!--  <div class="timeline-footer">
                  <a class="btn btn-primary btn-xs">Read more</a>
                  <a class="btn btn-danger btn-xs">Delete</a>
                </div>-->
              </div>
            </li>
            <!-- END timeline item -->
            <!-- timeline time label -->
            <li class="time-label">
                  <span class="bg-orange">
                    FEBRUARY 2019
                  </span>
            </li>
            <!-- /.timeline-label -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-envelope bg-blue"></i>

              <div class="timeline-item">
                <!--<span class="time"><i class="fa fa-clock-o"></i> 12:05</span>-->

                <h3 class="timeline-header">Wallet Launch.</h3>

                <div class="timeline-body">
                    <img src="../images/timeline/WALLET_LAUNCH.jpg" alt="Graphic" style="width:100%">
                </div>
              <!--  <div class="timeline-footer">
                  <a class="btn btn-primary btn-xs">Read more</a>
                  <a class="btn btn-danger btn-xs">Delete</a>
                </div>-->
              </div>
            </li>
            <!-- END timeline item -->
            <!-- timeline time label -->
            <li class="time-label">
                  <span class="bg-maroon">
                    MARCH 2019
                  </span>
            </li>
            <!-- /.timeline-label -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-envelope bg-blue"></i>

              <div class="timeline-item">
                <!--<span class="time"><i class="fa fa-clock-o"></i> 12:05</span>-->

                <h3 class="timeline-header">Available In Exchanges.</h3>

                <div class="timeline-body">
                    <img src="../images/timeline/AVAILABLE_IN_EXCHANGES.jpg" alt="Graphic" style="width:100%">
                </div>
              <!--  <div class="timeline-footer">
                  <a class="btn btn-primary btn-xs">Read more</a>
                  <a class="btn btn-danger btn-xs">Delete</a>
                </div>-->
              </div>
            </li>
            <!-- END timeline item -->
            <li>
              <i class="fa fa-clock-o bg-gray"></i>
            </li>
          </ul>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
</section>
<div class="tcg-points container">
<!--    <div class="row">
        <div class="col-md-8">
            <img src="../images/abc_tgc_banner.jpg" alt="coming soon.">
            <div style="background-image: url(../images/abc_tgc_banner.jpg); width:960px; height:400px; background-size: cover; background-repeat: no-repeat;">
                <div style="color:#ffffff;text-align: center;font-size: 50px;margin-top: 15px;font-weight: bold;padding-top: 125px;"><span class="blinking" style="top: 8px;position: relative;">*** TGC ROADMAP LAUNCHING ON THE DATE 26-06-2018 ***</span></div>
            </div>
        </div>
    </div>-->
    <div class="row">
        <div class="col-md-5" style="margin-top: 39px;">
            <h4 style="font-size:14px; font-weight:bold;">SOME OF OUR EXCHANGES WE ARE PLANING TO LISTED ON</h4>
            <img src="../images/timeline/SOME_OF_OUR_EXCHANGES_WE_ARE_PLANING_TO_LISTED_ON.png" alt="Graphic" class="img-responsive">
        </div>
        <div class="col-md-4 col-md-offset-1" style="text-align: justify; margin-top: 45px; font-size: 20px;">
            <p style="font-weight: bold; font-size: 14px; text-transform: uppercase;">TGC Price Road Map</p>
            <ul style="list-style-type: none;padding:0px;font-size: 14px;font-weight:bold;line-height: 1.5;">
                <li>3RD QUARTER 2018 – $0.6 USD</li>
                <li>4TH QUARTER 2018 – $1 USD</li>
                <li>1ST QUARTER 2019 – $10 USD</li>
                <li>2ND QUARTER 2019 - $10 USD</li>
                <li>3TH QUARTER 2019 - $50 USD</li>
                <li>4TH QUARTER 2019 - $100 USD</li>
            </ul>
        </div>
    </div>
<div class="row">
    <div class="col-md-10">
        <p style="text-align:center;width:100%;margin: 45px 45px 45px 0px; font-size: 16px;font-weight:bold;">Profitrex has started its journey to become the no.1 Trading and Networking company in the world. TGC coin making the target stronger. TGC is our own crypto which will be help to all our members to become financial independence and making their life beautiful.    </p>
    </div>
</div>
</div>
