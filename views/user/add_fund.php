<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $user app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Url;

$this->title = 'Add Fund';
$this->params['breadcrumbs'][] = $this->title;
?>


<!-- <div class="right_col" role="main" style="min-height: 100%; background-image: url(../images/bg/addFund_bg.jpg);"> -->
<div class="right_col" role="main" style="min-height: 767px;">
    <div class="clearfix"></div>
    <div class="header-title-breadcrumb element-box-shadow">
        <div class="container" style="height: 36px;">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12 text-left">
                    <h3><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></h3>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12 hide-on-tablet">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo Url::to(['user/dashboard']); ?>">Dashboard</a></li> 
                        <li class="active"><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>
    <div class="spacer_30"></div>
    <div class="clearfix"></div>
<div class="add-fund container">

    <?php
        $form = ActiveForm::begin([
                    'id' => 'credit-wallet-form',
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2',
                            'offset' => 'col-sm-offset-4',
                        ],
                    ],
        ]);
    ?>

    <?php echo $form->field($model,'amount')->label('Amount (<i class="fa fa-btc"> </i>)'); ?>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-warning', 'name' => 'register-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<img src="/images/add_fund1.png" class="img-responsive" alt="about add fund"> 

<br/>

<div class="col-md-12">
            <div class="panel panel-default table-transactions">
                <div class="panel-body padding_30">

                    <div class="col-lg-8 col-md-8">
                        <h3>We accept only Bitcoin </h3>
                        <ul>
                            <li>-> Select the package </li>
                            <li>-> Enter the package value in Bitcoin.</li>
                            <li>-> Send same amount of Bitcoin to the Bitcoin address given.</li>
                            
                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="market-chart">
                        <div class="btcwdgt-chart">
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
</div>
