<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Utils;
use borales\extensions\phoneInput\PhoneInput;
use kartik\file\FileInput;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\UserProfile */
/* @var $form ActiveForm */

$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="right_col" role="main" style="min-height: 596px;">
    <div class="clearfix"></div>
    <div class="header-title-breadcrumb element-box-shadow">
        <div class="container" style="height: 36px;">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12 text-left">
                    <h3><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></h3>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12 hide-on-tablet">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo Url::to(['user/dashboard']); ?>">Dashboard</a></li> 
                        <li class="active"><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>
    <div class="spacer_30"></div>
    <div class="clearfix"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-4">
                <div class="panel element-box-shadow">
<!--                    <div class="section1 text-center">
                        <div class="top animated bounceIn">
                            <div id="popup">
                                <span class="message">
                                    Profile Image Uploader<br>
                                    <small>Drop your image into the space below</small>
                                </span>
                            </div>
                        </div>

                        <div id="user-image-v1">
                            <div id="container">
                                <div class="box">
                                    <div class="progress"></div>
                                </div>
                                <div class="v-align">
                                    <img src="dashboard-assets/images/profile-pic.jpg" alt="user-image">
                                    <svg>
                                      <path d="M25,75a50,50 0 1,0 100,0a50,50 0 1,0 -100,0"></path>
                                    </svg>
                                    <div class="arrow"></div>
                                </div>
                                <img id="image-holder">
                            </div>
                             <img src="assets/images/profile-pic.jpg" alt="user-image"> 
                            <i class="fa fa-camera" aria-hidden="true"> Change</i>
                        </div>
                        <div class="clearfix"></div>
                        <div class="spacer_80"></div>
                        <div class="spacer_70"></div>
                        <div class="clearfix"></div>
                        <h3 class="text-bold">James Stones</h3>
                        <h4 class="text-bold"><i class="fa fa-envelope-o"></i> james.stones@gmail.com</h4>
                        <h4 class="text-bold"><i class="fa fa-phone"></i> (0088) 4455 1189</h4>
                    </div>-->
                    <div class="section2">
                        <ul>
                            <li><a href="<?php echo Url::to(['user/profile']); ?>" style="color:#000000; font-weight: bold;"><span class="icon-arrow-right-circle" style="font-weight: bold;"></span> my profile </a></li>
                            <li><a href="<?php echo Url::to(['package/purchase']); ?>" style="color:#000000; font-weight: bold;"><span class="icon-arrow-right-circle" style="font-weight: bold;"></span> invests </a></li>
                            <li><a href="<?php echo Url::to(['transaction/wallet-summary']); ?>" style="color:#000000; font-weight: bold;"><span class="icon-arrow-right-circle" style="font-weight: bold;"></span> the wallet </a></li>
                            <!--<li><a href="<?php echo Url::to(['site/shop']); ?>" style="color:#000000; font-weight: bold;"><span class="icon-arrow-right-circle" style="font-weight: bold;"></span> deposit </a></li>-->
                            <li><a href="<?php echo Url::to(['order/my-orders']); ?>" style="color:#000000; font-weight: bold;"><span class="icon-arrow-right-circle" style="font-weight: bold;"></span> reports </a></li>
                            <!--<li><a href="<?php echo Url::to(['inner-page/business']); ?>" style="color:#000000; font-weight: bold;"><span class="icon-arrow-right-circle" style="font-weight: bold;"></span> services </a></li>-->
                            <!--<li><a href="<?php echo Url::to(['inner-page/contactus']); ?>" style="color:#000000; font-weight: bold;"><span class="icon-arrow-right-circle" style="font-weight: bold;"></span> support </a></li>-->
                        </ul>
                    </div>

                </div>
            </div>
            <div class="col-md-12 col-lg-8">
                <!--Personal details-->
                <div class="panel panel-cryptic element-box-shadow">
                    <div class="panel-heading padding_30">
                        <h3 class="no-margin">Profile Details</h3>
                    </div>
                    <div class="panel-body padding_30">
                    <?php if (Yii::$app->session->hasFlash('profileUpdate')): ?>
                        <div class="alert alert-success">
                            Profile Update successful.
                        </div>
                    <?php endif; ?>

                    <?php
                    $form = ActiveForm::begin([
                                'id' => 'user-prfile-form',
                                'layout' => 'horizontal',
                                'fieldConfig' => [
                                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                    'horizontalCssClasses' => [
                                        'label' => 'col-sm-3',
                                        'offset' => 'col-sm-offset-4',
                                    ],
                                ],
                                'options' => ['enctype' => 'multipart/form-data'],
                    ]);
                    ?>

                    <?= $form->field($profile, 'first_name') ?>
                    <?= $form->field($profile, 'last_name') ?>
                    <?= $form->field($user, 'email')->textInput(['id' => 'user-email', 'readonly' => true]) ?>
                    <?=
                    $form->field($profile, 'picture')->widget(FileInput::classname(), [
                        'options' => ['accept' => 'image/*'],
                        'pluginOptions' => [
                            'showPreview' => false,
                            'showCaption' => true,
                            'showRemove' => true,
                            'showUpload' => false
                        ],
                    ]);
                    ?>
                    <?=
                    $form->field($profile, 'contact_number')->widget(PhoneInput::className(), [
                        'jsOptions' => [
                            'allowExtensions' => true,
                            'nationalMode' => false,
                        ]
                    ]);
                    ?>
                    <?php //$form->field($profile, 'date_of_birth')->widget(yii\jui\DatePicker::className(), ['clientOptions' => ['dateFormat' => 'yyyy-MM-dd']]) ?>
                    <?php // $form->field($profile, 'address')->textarea(['rows' => '6']) ?>
                    <?= $form->field($profile, 'street') ?>
                    <?= $form->field($profile, 'state') ?>
                    <?= $form->field($profile, 'city') ?>
                    <?= $form->field($profile, 'zip_code') ?>
                    <?= $form->field($profile, 'skype_id') ?>
                    <?= $form->field($profile, 'facebook_id') ?>
                    <?= $form->field($profile, 'twitter_id') ?>

                    <?=
                    $form->field($profile, 'national_id')->widget(FileInput::classname(), [
                        'options' => ['accept' => 'image/*'],
                        'pluginOptions' => [
                            'showPreview' => false,
                            'showCaption' => true,
                            'showRemove' => true,
                            'showUpload' => false
                        ],
                    ]);
                    ?>
                    <?=
                    $form->field($profile, 'passport_id')->widget(FileInput::classname(), [
                        'options' => ['accept' => 'image/*'],
                        'pluginOptions' => [
                            'showPreview' => false,
                            'showCaption' => true,
                            'showRemove' => true,
                            'showUpload' => false
                        ],
                    ]);
                    ?>
                    <?=
                    $form->field($profile, 'bank_statement')->widget(FileInput::classname(), [
                        'options' => ['accept' => 'image/*'],
                        'pluginOptions' => [
                            'showPreview' => false,
                            'showCaption' => true,
                            'showRemove' => true,
                            'showUpload' => false
                        ],
                    ]);
                    ?>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-10">
                            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                </div>
            </div>
        </div>
    </div>
    <a href="#" class="scrollToTop" style="display: none;"><i class="fa fa-chevron-up text-white" aria-hidden="true"></i></a>
</div>
