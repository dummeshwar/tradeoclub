<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Utils;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;
use yii\bootstrap\ActiveForm;


$this->title = 'User Dashboard';
$this->params['breadcrumbs'][] = $this->title;

?>
<script>
    (function(b,i,t,C,O,I,N) {
    window.addEventListener('load',function() {
    if(b.getElementById(C))return;
    I=b.createElement(i),N=b.getElementsByTagName(i)[0];
    I.src=t;I.id=C;N.parentNode.insertBefore(I, N);
    },false)
    })(document,'script','https://widgets.bitcoin.com/widget.js','btcwdgt');
</script>
<style>
    .cashwallet{
        background: linear-gradient(rgba(48, 48, 48, 0.8), rgba(48, 48, 48, 0.8)), rgba(48, 48, 48, 0.8) url(../images/wallets/cash_wallet.png) center;
    }
    .clubwallet{
        background: linear-gradient(rgba(48, 48, 48, 0.8), rgba(48, 48, 48, 0.8)), rgba(48, 48, 48, 0.8) url(../images/wallets/tradeoclub_wallet.png) center;
    }
    .referralwallet{
        background: linear-gradient(rgba(48, 48, 48, 0.8), rgba(48, 48, 48, 0.8)), rgba(48, 48, 48, 0.8) url(../images/wallets/referral_wallet.png) center;
    }
    .binarywallet{
        background: linear-gradient(rgba(48, 48, 48, 0.8), rgba(48, 48, 48, 0.8)), rgba(48, 48, 48, 0.8) url(../images/wallets/binary_wallet.png) center;
    }
 .price-plan-block {
    position: relative;
    color: #FFF;
    box-shadow: 2px 2px 5px #5B5B5B;
    padding: 30px 10px;
     background: linear-gradient(rgba(48, 48, 48, 0.8), rgba(48, 48, 48, 0.8)), rgba(48, 48, 48, 0.8) url(../images/price-01.jpg) center;
    background-size: auto 100%;
    background-position: center;
    text-align: center;
    height: 284px;
    /*height: 100%;*/
    margin: 30px 0 auto;
    border-bottom: 2px solid transparent;
    -webkit-transition: all 0.5s ease-in-out;
    margin-bottom:30px;
}
 .price-plan-block1 {
    position: relative;
    color: #FFF;
    box-shadow: 2px 2px 5px #5B5B5B;
    padding: 30px 10px;
    
    background-size: auto 100%;
    background-position: center;
    text-align: center;
    height: 284px;
    /*height: 100%;*/
    margin: 30px 0 auto;
    border-bottom: 2px solid transparent;
    -webkit-transition: all 0.5s ease-in-out;
    margin-bottom:30px;
}

.price-plan-block:hover {
    background-size: auto 110%;
    -webkit-transition: all 0.5s ease-in-out;
    border-bottom: 2px solid #FF8E31;
    box-shadow: 4px 4px 8px #5B5B5B;
}
.price-plan-block1:hover {
    background-size: auto 110%;
    -webkit-transition: all 0.5s ease-in-out;
    border-bottom: 2px solid #FF8E31;
    box-shadow: 4px 4px 8px #5B5B5B;
}



.price-icon {
    position: absolute;
    top: -30px;
    left: 0;
    right: 0;
    margin: 0 auto;
    background-color: #333;
    border-radius: 50%;
    width: 64px;
    height: 64px;
}
.price-subheading {
    color: #FFF;
    margin: 20px 0;
}
h5 {
    font-size: 18px;
    font-family: 'Roboto', sans-serif;
    font-weight: 900;
}

.price-heading {
    color: #FF8E31;
    margin: 10px 0;
}

h2 {
    font-size: 25px;
        font-family: 'Roboto', sans-serif;
    font-weight: 900;
}
h1 {
    font-size:25px;
        font-family: 'Roboto', sans-serif;
    font-weight: 900;
}

.btn-default {
    border-radius: 0;
    font-family: 'Roboto', sans-serif;
    font-size: 13px;
    font-weight: 900;
    line-height: 1.69;
    letter-spacing: 0.3px;
    text-align: center;
    color: #FFF;
    border: none;
    padding: 12px;
    text-transform: uppercase;
    background-color: #FF8E31;
    -webkit-transition: all 0.5s ease;
    -ms-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    transition: all 0.5s ease;
        margin-top: 27px;
}

.bottom_margin{
    margin-bottom: 30px;
}
@media only screen and (max-width: 767px){

.margin_mobile{
    margin-top: 81px;
}
}

#marquee-inner {
  background-color: #222;
  padding: 10px 0;
}
#marquee-inner span {
  color: #FFF !important;
}
.ccc-header-v3-price-logo {
  margin-right: 10px;
}
</style>
<link rel="icon" type="image/icon" href="/images/favicon/favicon.png">
<div class="right_col right_col_exchange" id="dashboard-v2" role="main" style="min-height: 202px;">
    <div class="margin_left_right_30">
        <div class="row">
<!--            <div class="col-xs-12 col-sm-6 col-lg-3">
                <div class="panel panel-danger element-box-shadow market-place">
                    <div class="panel-heading no-padding">
                        <div class="div-market-place">
                            <h3> Cash <br/>Wallet</h3>
                            <h1>$ <?php echo Utils::walletMoney(Yii::$app->params['wallets']['CASH_WALLET']); ?></h1>
                            <h1>USD</h1>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-3">
                <div class="panel panel-success element-box-shadow market-place">
                    <div class="panel-heading no-padding">
                        <div class="div-market-place">
                            <h3> Tradeoclub Wallet </h3>
                            <h1>$ <?php echo Utils::walletMoney(Yii::$app->params['wallets']['TRADEOCLUB_WALLET']); ?></h1>
                            <h1>USD</h1>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-3">
                <div class="panel panel-info element-box-shadow market-place">
                    <div class="panel-heading no-padding">
                        <div class="div-market-place">
                            <h3> Referral Commission </h3>
                            <h1>$ <?php echo Utils::walletMoney(Yii::$app->params['wallets']['REFERRAL_WALLET']); ?></h1>
                            <h1>USD</h1>
                            
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-6 col-lg-3">
                <div class="panel panel-primary element-box-shadow market-place">
                    <div class="panel-heading no-padding">
                        <div class="div-market-place">
                            <h3> Binary Commission </h3>
                            <h1>$ <?php echo Utils::walletMoney(Yii::$app->params['wallets']['BINARY_WALLET']); ?></h1>
                            <h1>USD</h1>
                           
                        </div>
                    </div>
                </div>
            </div>-->
        
        
        
        <!--modified wallet format-->
        
        <div id="price-plan" class="price-plan-main-block margin_left_right_30 bottom_margin">
    <div class="container">
      <div class="row">
        <div class="section">
<!--          <h1 class="section-heading text-center">Packages</h1>-->
          
        </div> 
          
       
                                                   
        <div class="col-sm-3 margin_mobile"> 
          <div class="price-plan-block1 cashwallet">
            <div class="price-icon"><img src="/images/icons/bitcoin-icon.png" alt="bitcoin-icon"></div>
            <h1 class="price-subheading">Bitcoin <br/>Wallet</h1>
            <h2 class="price-heading"><i class="fa fa-btc" aria-hidden="true"></i> <?php echo number_format(Utils::walletMoney(Yii::$app->params['wallets']['CASH_WALLET']), 4); ?></h2>
            <!--<div class="price-text">You Get 5 Bitcoin</div>-->   
            <!--<a href="#" class="btn btn-default">Buy Now</a>-->
          </div>
        </div>
                                                     
                                                        
        <div class="col-sm-3"> 
          <div class="price-plan-block1 clubwallet">
            <div class="price-icon"><img src="/images/icons/bitcoin-icon.png" alt="bitcoin-icon"></div>
            <h1 class="price-subheading">Profit <br/> Wallet</h1>
            <h2 class="price-heading"><i class="fa fa-btc" aria-hidden="true"></i> <?php echo number_format(Utils::walletMoney(Yii::$app->params['wallets']['TRADEOCLUB_WALLET']), 4); ?></h2>
            <!--<div class="price-text">You Get 5 Bitcoin</div>-->   
            <!--<a href="#" class="btn btn-default">Buy Now</a>-->
          </div>
        </div>
        <div class="col-sm-3"> 
          <div class="price-plan-block1 referralwallet">
            <div class="price-icon"><img src="/images/icons/bitcoin-icon.png" alt="bitcoin-icon"></div>
            <h1 class="price-subheading">Referral <br/> Commission</h1>
            <h2 class="price-heading"><i class="fa fa-btc" aria-hidden="true"></i> <?php echo number_format(Utils::walletMoney(Yii::$app->params['wallets']['REFERRAL_WALLET']), 4); ?></h2>
            <!--<div class="price-text">You Get 10 Bitcoin</div>-->   
            <!--<a href="#" class="btn btn-default">Buy Now</a>-->
          </div>
        </div>
        <div class="col-sm-3"> 
          <div class="price-plan-block1 binarywallet">
            <div class="price-icon"><img src="/images/icons/bitcoin-icon.png" alt="bitcoin-icon"></div>
            <h1 class="price-subheading">Binary <br/> Commission</h1>
            <h2 class="price-heading"><i class="fa fa-btc" aria-hidden="true"></i> <?php echo number_format(Utils::walletMoney(Yii::$app->params['wallets']['BINARY_WALLET']), 4); ?></h2>
            <!--<div class="price-text">You Get 12 Bitcoin</div>-->   
            <!--<a href="#" class="btn btn-default">Buy Now</a>-->
          </div>
        </div>
      </div>
    </div>
  </div>
        
        </div>
        
        <!--modified wallet format-->
        
        
        
        
        

        <div class="row">
<!--            <div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="panel panel-primary element-box-shadow market-place">
                    <div class="panel-heading no-padding">
                        <div class="div-market-place">
                            <h3> Binary Commission </h3>
                            <h1>$ <?php echo Utils::walletMoney(Yii::$app->params['wallets']['BINARY_WALLET']); ?></h1>
                            <h1>USD</h1>
                            <p class="text-bold text-white">(Wallet description here).</p>
                        </div>
                    </div>
                </div>
            </div>-->

<!--            <div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="panel panel-warning element-box-shadow market-place">
                    <div class="panel-heading no-padding">
                        <div class="div-market-place">
                            <h3> Promo Wallet </h3>
                            <h1>$ <?php echo Utils::walletMoney(Yii::$app->params['secondary_wallets']['PROMO_WALLET']); ?></h1>
                            <h1>USD</h1>
                            <p class="text-bold text-white">(Wallet description here).</p>
                        </div>
                    </div>
                </div>
            </div>-->
            
            
            
              <!--  chart -->
  <div class="currency-chart">
    <div class="container-fluid">
      <div class="row">
        <script>
          baseUrl = "https://widgets.cryptocompare.com/";
          var scripts = document.getElementsByTagName("script");
          var embedder = scripts[scripts.length - 1];
          var cccTheme = { "General": { "enableMarquee": true } };
          (function() {
              var appName = encodeURIComponent(window.location.hostname);
              if (appName == "") { appName = "local"; }
              var s = document.createElement("script");
              s.type = "text/javascript";
              s.async = true;
              var theUrl = baseUrl + 'serve/v3/coin/header?fsyms=BTC,ETH,XMR,LTC,DASH&tsyms=BTC,USD,CNY,EUR';
              s.src = theUrl + (theUrl.indexOf("?") >= 0 ? "&" : "?") + "app=" + appName;
              embedder.parentNode.appendChild(s);
          })();
        </script>
      </div>
    </div>
  </div>
<!-- end chart -->
        </div>
    </div>
    

    
    
    
    <div class="clearfix"></div>
    <div class="spacer_30"></div>

    
  
<!--<div class="right_col" role="main" style="min-height: 202px;">-->
    

    <!--<div class="row chart-section1">-->
        <div class="col-md-12">
            <div class="panel panel-default table-transactions">
                <div class="panel-body padding_30">
                   

                 
                    
                    <div class="col-lg-6 col-md-6">
                        <h1>How to Buy Package</h1>
                        <ul class="custom-bullet">
                            <li>Recharge/Add Cash to your cash wallet from available <a href="<?php echo Url::toRoute(['site/payment']); ?>" target="_blank">&nbsp;E-Currencies</a>.</li>
                            <li>Select a Package below.</li>
                            <li>After selecting package, just click on "Buy Now" and confirm the package</li>
                            <li>That's all. You bought a package and that will be listed on <a href="<?php echo Url::toRoute(['order/my-orders']); ?>" target="_blank">Orders</a>.</li>
                        </ul>
                    </div>
                    
                    <div class="col-lg-6 col-md-6">
                        <div class="market-chart">
                        <div class="btcwdgt-chart">
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    <!--</div>-->
    
  
<!--</div>-->
    
    
    
 <!-- price plan -->
  <div id="price-plan" class="price-plan-main-block margin_left_right_30 bottom_margin">
    <div class="container">
      <div class="row">
        <div class="section">
          <h1 class="section-heading text-center">Packages</h1>
          
        </div> 
          
          <?php if (!empty($packages)) {
              
          $i=3;    ?>
          
							<!-- Starts here -->
								<?php foreach($packages as $package): ?>
                                                       
    
                                                        
                                                        <?php $link_to_package = Url::to(['user/package-detail', 'id' => base64_encode($package->id)]); ?>
                                                        <?php $form = ActiveForm::begin(['id' => 'package-purchase-form']); ?>
         <?php if($i % 3 ==0) { ?>
                                                        <div><h3>Level- <?php echo $i/3; ?></h3></div>
                                                        <?php } ?>
        <div class="col-sm-3 col-md-4"> 
          <div class="price-plan-block">
            <div class="price-icon"><img src="/images/icons/bitcoin-icon.png" alt="bitcoin-icon"></div>
            <h5 class="price-subheading"><?= $package->name ?></h5>
            <h2 class="price-heading"> <i class="fa fa-btc" aria-hidden="true"></i> <?= number_format($package->price, 1); ?></h2>
            <!--<div class="price-text">You Get 1 Bitcoin</div>-->   
            <a href="<?= $link_to_package; ?>" class="btn btn-default">Buy Now </a>
           
           
          </div>
             <?php ActiveForm::end(); ?>
                       
        </div>
                                                       
                                                     <?php $i++; ?>
                                                         <?php endforeach; ?>
                                                        <?php } ?>
<!--        <div class="col-sm-3"> 
          <div class="price-plan-block">
            <div class="price-icon"><img src="/images/icons/bitcoin-icon.png" alt="bitcoin-icon"></div>
            <h5 class="price-subheading">5 Bitcoin</h5>
            <h2 class="price-heading">$3750</h2>
            <div class="price-text">You Get 5 Bitcoin</div>   
            <a href="#" class="btn btn-default">Buy Now</a>
          </div>
        </div>-->
<!--        <div class="col-sm-3"> 
          <div class="price-plan-block">
            <div class="price-icon"><img src="/images/icons/bitcoin-icon.png" alt="bitcoin-icon"></div>
            <h5 class="price-subheading">10 Bitcoin</h5>
            <h2 class="price-heading">$5500</h2>
            <div class="price-text">You Get 10 Bitcoin</div>   
            <a href="#" class="btn btn-default">Buy Now</a>
          </div>
        </div>
        <div class="col-sm-3"> 
          <div class="price-plan-block">
            <div class="price-icon"><img src="/images/icons/bitcoin-icon.png" alt="bitcoin-icon"></div>
            <h5 class="price-subheading">12 Bitcoin</h5>
            <h2 class="price-heading">$9500</h2>
            <div class="price-text">You Get 12 Bitcoin</div>   
            <a href="#" class="btn btn-default">Buy Now</a>
          </div>
        </div>-->
      </div>
    </div>
  </div>
<!-- end price plan -->

</div>


<script>
function leftFunction() {
  var copyText = document.getElementById("leftLink");
  copyText.select();
  document.execCommand("copy");
  alert("Link Copied: " + copyText.value);
}
function rightFunction() {
  var copyText = document.getElementById("rightLink");
  copyText.select();
  document.execCommand("copy");
  alert("Link Copied: " + copyText.value);
}
</script>