<a class="button-shadow mobile-center" href="#"> TAKE PROFIT PLAN </a>
        <table class="table-type-one">
          <tr>
            <th>Plant Cycle</th>
            <th>Quantity</th>
            <th>SP</th>
            <th>Cross Profit</th>
            <th>POI</th>
          </tr>
          <tr>
            <td>CY-1</td>
            <td>500kg</td>
            <td>$200C</td>
            <td>Solo</td>
            <td>60%</td>
          </tr>
        </table>
      <div class="row">
        <table class="table-type-two">
          <tr class="border-dotted-both">
            <th class="table-width-adj">&nbsp; </th>
              <th>Start date</th>
              <th>End Date</th>
              <th>Total Points</th>
              <th>Deposit</th>
              <th>Profit</th>
              <th>Sell</th>
              <th>Total Revennue</th>
            <th class="table-width-adj">&nbsp; </th>
          </tr>
          <tr>
            <td  class="table-width-adj">&nbsp; </td>
              <td>May-30-2018</td>
              <td>Aug-15-2018</td>
              <td>5</td>
              <td>$20</td>
              <td>6</td>
              <td>5%</td>
              <td>50$</td>
            <td  class="table-width-adj">&nbsp; </td>
          </tr>
        </table>
      </div>


        <div class="row plant-plan-row">
            <div class="col-md-6 col-sm-8 plant-plan mobile-adjust">
                <div class="col-md-4 col-sm-6">
                    <img src="/images/tree.png" />
                </div>
                <div class="col-md-6 col-sm-6">
                   <span> Plant Plan </span>
                   <a class="button-shadow small" href="#">20-500</a>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="plant-plan-right">
                    <a class="button-shadow small" href="#">Plant Quantity</a>
                    <a class="tr-button" href="#">TR</a>
                </div>
                <div class="range-600">
                	<div class="scale-values">
            	<div class="start">$0</div>
                <div class="end">$600</div>
                </div>
                <img src="/images/range600.png" />
                </div>
            </div>

        </div>