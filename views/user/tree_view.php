<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Utils;
use yii\helpers\Url;
use app\models\User;

$this->title = 'Tree View';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $UserProfile = User::findOne([
          'id' => Yii::$app->user->id
      ]); 

//echo '<pre>'; print_r($UserProfile->username); exit; 
          ?>
<div class="right_col" role="main" style="min-height: 202px;">
    <div class="clearfix"></div>
    <div class="header-title-breadcrumb element-box-shadow">
        <div class="container" style="height: 36px;">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12 text-left">
                    <h3><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></h3>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12 hide-on-tablet">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo Url::to(['user/dashboard']); ?>">Dashboard</a></li> 
                        <li class="active"><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>

    <div class="row chart-section1">
        <div class="col-md-12">
            <div class="panel panel-default table-transactions">
                <?php // echo Url::to(['site/register', 'ref' => $UserProfile->username, 'pos' => 'L']); exit; ?>
                
                 <div class="container" style="margin-bottom: 30px; margin-left: 15px;">
        <div class="col-md-6">
            <h2 class="text-center" style="font-weight: bold;">Left Referral Link </h2>
            <input style="padding: 5px;width: 75%;" type="text" value="<?php echo "https://www.tradeoclub.com" . Url::to(['site/register', 'ref' => $UserProfile->username, 'pos' => 'L']); ?>" id="leftLink">
            <button class="btn btn-primary  btn-sm" style="width:20%" onclick="leftFunction()">Copy Link</button>
        </div>
        <div class="col-md-6">
            <h2 class="text-center" style="font-weight: bold;">Right Referral Link </h2>
            <input style="padding: 5px;width: 75%;" type="text" value="<?php echo "https://www.tradeoclub.com" . Url::to(['site/register', 'ref' => $UserProfile->username, 'pos' => 'R']); ?>" id="rightLink">
            <button class="btn btn-primary  btn-sm" style="width:20%" onclick="rightFunction()">Copy Link</button>
        </div>    
    </div>
                
                <div class="panel-body padding_30">
                    <?php $form = ActiveForm::begin(['id' => 'search-user-tree-form']); ?>
                    <div class="col-md-6">
                        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default tables-crypto-stats">
                            <div class="panel-body padding_30">
                                <table class="table table-striped no-margin" style="min-width:245px">
                                    <tbody>
                                        <!-- <tr>
                                            <td>Total Left Users</td>
                                            <td><?php echo $downlineData['left_user_count']; ?></td>
                                        </tr> -->
                                        <tr>
                                            <td>Total Left Purchase Count</td>
                                            <td><?php echo $downlineData['left_user_package_purchase_count']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Total Left Business</td>
                                            <td><i class="fa fa-btc"> </i> <?php echo $downlineData['left_user_total_package_purchase_amount']; ?></td>
                                        </tr>
                                        <!-- <tr>
                                            <td>Total Right Users</td>
                                            <td><?php echo $downlineData['right_user_count']; ?></td>
                                        </tr> -->
                                        <tr>
                                            <td>Total Right Purchase Count</td>
                                            <td><?php echo $downlineData['right_user_package_purchase_count']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Total Right Business</td>
                                            <td><i class="fa fa-btc"> </i> <?php echo $downlineData['right_user_total_package_purchase_amount']; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>


                    <div class="col-sm-12 col-md-9 col-md-offset-3">

                        <div class="tree-view">

                            <ul>
                                <li>
                                    <div class="firstNode"><?php echo Utils::getUserForBtree($tree_data[0]); ?> </button></div>

                                    <ul class="firstNodeExt">

                                        <li class="leftSubNode">
                                            <div><?php echo Utils::getUserForBtree($tree_data[1], $tree_data[0], 'L'); ?></div>
                                            <?php if ($tree_data[1] != 'N'): ?>

                                                <ul>
                                                    <li>
                                                        <div><?php echo Utils::getUserForBtree($tree_data[3], $tree_data[1], 'L'); ?></div>
                                                    </li>

                                                    <li>
                                                        <div><?php echo Utils::getUserForBtree($tree_data[4], $tree_data[1], 'R'); ?></div>
                                                    </li>
                                                </ul>

                                            <?php endif; ?>
                                        </li>

                                        <li class="rightSubNode">
                                            <div><?php echo Utils::getUserForBtree($tree_data[2], $tree_data[0], 'R'); ?></div>

                                            <?php if (!empty($tree_data[5]) && $tree_data[2] != 'N'): ?>
                                                <ul>
                                                    <li>
                                                        <div><?php echo Utils::getUserForBtree($tree_data[5], $tree_data[2], 'L'); ?></div>
                                                    </li>

                                                    <li>
                                                        <div><?php echo Utils::getUserForBtree($tree_data[6], $tree_data[2], 'R'); ?></div>
                                                    </li>
                                                </ul>
                                            <?php endif; ?>
                                        </li>

                                    </ul>

                                </li>
                            </ul>

                        </div>


                    </div>
                    <!--            <div class="col-sm-12 col-xs-12 col-md-4">
                    
                                  <div class="box box-widget widget-user-2">
                                     Add the bg color to the header using any of the bg-* classes 
                                    <div class="widget-user-header">
                                        <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                                                    User Downline Statistics
                                        </h4>
                                      </div>
                                    <div class="box-footer no-padding">
                                      <ul class="nav nav-stacked">
                                        <li><a href="#">Total Left Users <span class="pull-right badge bg-blue"><?php echo $downlineData['left_user_count']; ?></span></a></li>
                                        <li><a href="#">Total Left Purchase Count<span class="pull-right badge bg-green"><?php echo $downlineData['left_user_package_purchase_count']; ?></span></a></li>
                                        <li><a href="#">Total Left Purchase Amount<span class="pull-right badge bg-green"> <i class="fa fa-btc"> </i> <?php echo $downlineData['left_user_total_package_purchase_amount']; ?></span></a></li>
                                        <li><a href="#">Total Right Users <span class="pull-right badge bg-blue"><?php echo $downlineData['right_user_count']; ?></span></a></li>
                                        <li><a href="#">Total Right Purchase Count<span class="pull-right badge bg-green"><?php echo $downlineData['right_user_package_purchase_count']; ?></span></a></li>
                                        <li><a href="#">Total Right Purchase Amount<span class="pull-right badge bg-green"> <i class="fa fa-btc"> </i> <?php echo $downlineData['right_user_total_package_purchase_amount']; ?></span></a></li>
                                      </ul>
                                    </div>
                                  </div>
                    
                              </div>-->

                </div>
            </div>
        </div>
    </div>
</div>

<script>
function leftFunction() {
  var copyText = document.getElementById("leftLink");
  copyText.select();
  document.execCommand("copy");
  alert("Link Copied: " + copyText.value);
}
function rightFunction() {
  var copyText = document.getElementById("rightLink");
  copyText.select();
  document.execCommand("copy");
  alert("Link Copied: " + copyText.value);
}
</script>