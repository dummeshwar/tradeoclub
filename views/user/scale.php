<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Utils;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;

$this->title = 'User Dashboard';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row tree-area">
        <div class="col-md-9 col-sm-12 plant-table">
        <div class="doller-img">
            <img src="/images/almond-doller.png" />
        </div>
        <div class="plant-details">
            <table class="table-type-three up">
                <tr>
                    <td>Plant Purchased</td>
                    <td style="text-align: center;" >100</td>
                    <td>500</td>
                </tr>
            </table>

            <table class="table-type-three down">
                <tr>
                    <td>Plant Cost</td>
                    <td>50</td>
                </tr>
                <tr>
                    <td>Plant Volume</td>
                    <td>250</td>
                </tr>
                <tr>
                    <td>Plant Share</td>
                    <td>30%</td>
                </tr>
                <tr>
                    <td>Total Earnings</td>
                    <td>250-1500</td>
                </tr>
                <tr>
                    <td> <button class="buttons grean" type="button">HELP</button> </td>
                    <td><button class="buttons blue" type="button">CASHOUT</button></td>
                </tr>
            </table>
        </div>
        </div>
        <div class="col-md-3 col-sm-12 tree-table">
          <table class="table-type-four">
            <tr>
              <td>Plant Purchased</td>
              <td>100</td>
              <td>250rs</td>
            </tr>
            <tr>
              <td>Present Plants</td>
              <td>20</td>
              <td>40</td>
            </tr>
            <tr>
              <td>Refered Commission</td>
              <td>100</td>
              <td><button class="buttons blue small" type="button">Cash Out</button></td>
            </tr>
            <tr>
              <td>Binary comission</td>
              <td>100</td>
              <td><button class="buttons blue small" type="button">Cash Out</button></td>
            </tr>
            <tr>
              <td colspan="3">
                  <button class="buttons grean medium" type="button">SUMMARY</button>
                  <button class="buttons blue medium" type="button">SHARES</button>
              </td>
            </tr>
          </table>
        </div>
    </div>
    <div class="row">
      <div class="scale-range">
<div class="scale-left">
<img src="/images/leftPlant.png"  />
</div>

    <div class="scale-middle">

    	<div class="range-bar-flex range-bar-1">
        	<div class="top-leaf"><img src="/images/topLeaf.png" /> </div>
            <div class="top-text">0</div>
            <div class="middle-bar"> <img src="/images/scaleBars.png"  /></div>
            <div class="bottom-text"><div class="sub-top-text"> Iron </div> <div class="sub-bottom-text"> 20</div></div>
        </div>

        <div class="range-bar-flex range-bar-2">
            <div class="top-text">100</div>
            <div class="middle-bar"> <img src="/images/scaleBars.png"  /></div>
            <div class="bottom-text"><div class="sub-top-text"> Iron </div> <div class="sub-bottom-text"> 20</div></div>
        </div>


        <div class="range-bar-flex range-bar-3">
            <div class="top-text">500</div>
            <div class="middle-bar"> <img src="/images/scaleBars.png"  /></div>
            <div class="bottom-text"><div class="sub-top-text"> Iron </div> <div class="sub-bottom-text"> 20</div></div>
        </div>


        <div class="range-bar-flex range-bar-4">
            <div class="top-text">600</div>
            <div class="middle-bar"> <img src="/images/scaleBars.png"  /></div>
            <div class="bottom-text"><div class="sub-top-text"> Iron </div> <div class="sub-bottom-text"> 20</div></div>
        </div>


        <div class="range-bar-flex range-bar-5">
            <div class="top-text">900</div>
            <div class="middle-bar"> <img src="/images/scaleBars.png"  /></div>
            <div class="bottom-text"><div class="sub-top-text"> Iron </div> <div class="sub-bottom-text"> 20</div></div>
        </div>

        <div class="range-bar-flex range-bar-6">
            <div class="top-text">1100</div>
            <div class="middle-bar"> <img src="/images/scaleBars.png"  /></div>
            <div class="bottom-text"><div class="sub-top-text"> Iron </div> <div class="sub-bottom-text"> 20</div></div>
        </div>

        <div class="range-bar-flex range-bar-7">
            <div class="top-text">1400</div>
            <div class="middle-bar"> <img src="/images/scaleBars.png"  /></div>
            <div class="bottom-text"><div class="sub-top-text"> Iron </div> <div class="sub-bottom-text"> 20</div></div>
        </div>

        <div class="range-bar-flex range-bar-8">
            <div class="top-text">2000</div>
            <div class="middle-bar"> <img src="/images/scaleBars.png"  /></div>
            <div class="bottom-text"><div class="sub-top-text"> Iron </div> <div class="sub-bottom-text"> 20</div></div>
        </div>

    </div>

<div class="scale-right"><img src="/images/rightPlant.png"  /></div>

</div>
    </div>
