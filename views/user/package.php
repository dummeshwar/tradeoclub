<div class="row ">
        <div class="col-md-6 col-sm-8 justify-content-center mobile-adjust">
            <a class="button-shadow height-90 button-flex" href="#">
                <div class="item">
                    <img src="/images/cp-icon.png" />
                </div>
                <div class="item">
                    COMPOUND PROFIT
                </div>
            </a>
            <a class="button-shadow height-90 button-flex" href="#">
                <div class="item">
                    <img src="/images/nc-icon.png" />
                </div>
                <div class="item">
                    NUMBER OF CYCLES <img src="/images/nc-icon2.png" />
                </div>
            </a>
        </div>
        <div class="col-md-6 col-sm-8 justify-content-center top-bottom-margin-mobile mobile-adjust">
            <a class="button-shadow height-45 button-flex top-margin" href="#"> TAKE PROFIT PLAN </a>
            <div>
            	<div class="scale-values">
            	<div class="start">0</div>
                <div class="end">150</div>
                </div>
                <img style="width: 100%; max-width: 580px;" width="100%" src="/images/range150.png" />
            </div>
        </div>
    </div>

<div class="row calculate border-dotted-top">
    <button class="calculate-button-leaf" type="button">CALCULATE</button>
</div>

<div class="row" >
  <table class="table-type-two">
    <tr>
      <th class="table-width-adj">&nbsp; </th>
        <th>Plant Cycle</th>
        <th>Quantiy</th>
        <th>SP</th>
        <th>Cross Profit</th>
        <th>POI<sup>2</sup></th>
      <th class="table-width-adj">&nbsp; </th>
    </tr>
    <tr>
      <td  class="table-width-adj">&nbsp; </td>
        <td>C1</td>
        <td>50</td>
        <td>$500</td>
        <td>$01</td>
        <td>96%</td>
      <td  class="table-width-adj">&nbsp; </td>
    </tr>
    <tr>
        <td  class="table-width-adj">&nbsp; </td>
        <td>C2</td>
        <td>100</td>
        <td>15000</td>
        <td>$0.2</td>
        <td>69%</td>
        <td  class="table-width-adj">&nbsp; </td>
    </tr>
    <tr class="border-dotted-top">
        <td  class="table-width-adj">&nbsp; </td>
        <td>SUM</td>
        <td>150</td>
        <td>$2000</td>
        <td>$0.3</td>
        <td>59%</td>
        <td  class="table-width-adj">&nbsp; </td>
    </tr>

  </table>
</div>