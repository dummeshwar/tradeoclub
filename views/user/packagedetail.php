        
<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Single Product';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .home-banner { display:none; }
    .paddingBtn { padding: 10px; top: 10px; position: relative; }
    
    
        
        
        .confirm_class{
        padding: 30px 10px;
        margin: 30px 0 auto;
    }
    
   .price-plan-block {
    position: relative;
    color: #FFF;
    box-shadow: 2px 2px 5px #5B5B5B;
    padding: 30px 10px;
    background: linear-gradient(rgba(48, 48, 48, 0.8), rgba(48, 48, 48, 0.8)), rgba(48, 48, 48, 0.8) url(../images/price-01.jpg) center;
    background-size: auto 100%;
    background-position: center;
    text-align: center;
    height: 284px;
    /*height: 100%;*/
    margin: 30px 0 auto;
    border-bottom: 2px solid transparent;
    -webkit-transition: all 0.5s ease-in-out;
    margin-bottom: 30px
}

.price-plan-block:hover {
    background-size: auto 110%;
    -webkit-transition: all 0.5s ease-in-out;
    border-bottom: 2px solid #FF8E31;
    box-shadow: 4px 4px 8px #5B5B5B;
}



.price-icon {
    position: absolute;
    top: -30px;
    left: 0;
    right: 0;
    margin: 0 auto;
    background-color: #333;
    border-radius: 50%;
    width: 64px;
    height: 64px;
}
.price-subheading {
    color: #FFF;
    margin: 20px 0;
}
h5 {
    font-size: 18px;
    font-family: 'Roboto', sans-serif;
    font-weight: 900;
}

.price-heading {
    color: #FF8E31;
    margin: 10px 0;
}

h2 {
    font-size: 34px;
        font-family: 'Roboto', sans-serif;
    font-weight: 900;
}

.btn-default {
    border-radius: 0;
    font-family: 'Roboto', sans-serif;
    font-size: 13px;
    font-weight: 900;
    line-height: 1.69;
    letter-spacing: 0.3px;
    text-align: center;
    color: #FFF;
    border: none;
    padding: 12px;
    text-transform: uppercase;
    background-color: #FF8E31;
    -webkit-transition: all 0.5s ease;
    -ms-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    transition: all 0.5s ease;
        margin-top: 27px;
}

.bottom_margin{
    margin-bottom: 30px;
}

</style>






<div class="right_col" role="main" style="min-height: 202px;">
    <div class="clearfix"></div>
    <div class="header-title-breadcrumb element-box-shadow">
        <div class="container" style="height: 36px;">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12 text-left">
                    <h3><?php echo !empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></h3>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12 hide-on-tablet">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo Url::to(['user/dashboard']); ?>">Dashboard</a></li> 
                        <li class="active"><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>

    <div class="row chart-section1">
        <div class="col-md-12">
            <div class="panel panel-default table-transactions">
                <div class="panel-body padding_30">
                   

                    

        <div class="col-lg-12 col-md-12 col-sm-12">
                        
        <div class="col-sm-6"> 
          <div class="price-plan-block">
            <div class="price-icon"><img src="/images/icons/bitcoin-icon.png" alt="bitcoin-icon"></div>
            <h5 class="price-subheading"><?= $package->name ?></h5>
            <h2 class="price-heading">$<?= $package->price ?></h2>
            <!--<div class="price-text">You Get 1 Bitcoin</div>-->   
         
           
           
          </div>
            
                       
        </div>
            <div class="col-sm-6" style="margin-top: 30px;">
                       <form class="form-horizontal row" method="post">
                                        <div class="col-sm-12">
                                            <?php if ($is_user_has_sufficient_fund) { ?>
                                                <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                                                <?= Html::a('Confirm Booking', Url::toRoute(['/site/confirm-booking', 'id' => base64_encode($package->id)]), ['data-method' => 'post', 'class' => "btn-primary paddingBtn"]) ?>
                                            <?php } else { ?>
                                                <div class="alert alert-danger">
                                                    <strong>Notice!</strong> You don't have sufficient balance to purchase this package. Please recharge your cash wallet.
                                                </div>
                                                <?= Html::a('Add Fund', Url::toRoute(['/user/add-fund']), ['class' => "btn-primary paddingBtn"]) ?>
                                            <?php } ?>
                                        </div>
                                    </form>
            </div>
                       
                    </div>
                    
                    
                </div>
            </div>
        </div>
    </div>
    
 
</div>