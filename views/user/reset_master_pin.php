<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset Key Pin';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-reset-master-pin container">
    <?php if (Yii::$app->session->hasFlash('master-pin-reset')): ?>
        <div class="alert alert-success">
            <?= Yii::$app->session->getFlash('master-pin-reset'); ?>
        </div>
    <?php endif; ?>
    
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'reset-master-pin-form']); ?>

            <?= $form->field($model, 'master_pin')->passwordInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'master_pin_repeat')->passwordInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>