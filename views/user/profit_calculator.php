<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Utils;
use yii\helpers\Url;


$this->title = 'Profit Calculator';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="right_col" role="main" style="min-height: 202px;">
    <div class="clearfix"></div>
    <div class="header-title-breadcrumb element-box-shadow">
        <div class="container" style="height: 36px;">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12 text-left">
                    <h3><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></h3>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12 hide-on-tablet">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo Url::to(['user/dashboard']); ?>">Dashboard</a></li> 
                        <li class="active"><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>

    <div class="row chart-section1">
        <div class="col-md-12">
            <div class="panel panel-default table-transactions">
                <div class="panel-body padding_30">
                    <div class="col-lg-3 col-md-3">
            <?php $form = ActiveForm::begin(['id' => 'profit-calculator-form']); ?>
                <?php echo $form->field($model,'amount'); ?>
                <div class="form-group">
                    <?= Html::submitButton('Calculate', ['class' => 'btn btn-primary']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>

        <?php if ($success) {?>
          <table class="table" style="background: #fff">
            <thead class="thead-light" style="background: #eee">
              <tr>
                <th>Package Value ($)</th>
                <th>Quantity</th>
                <th>No. of days</th>
                <th>Return per Week ($)</th>
                <th>Total Profit ($)</th>
              </tr>
            </thead>
            <tbody>
              <?php if ($results) {?>
                <?php foreach ($results as $result) { ?>
                  <?php $quantity = floor($model->amount / $result->price); ?>
                  <tr>
                    <td> $ <?php echo number_format($result->price, 2); ?></td>
                    <td><?php echo $quantity; ?></td>
                    <td><?php echo $result->no_of_days; ?></td>
                    <td><?php echo number_format($result->daily_returns, 2); ?></td>
                    <td>$ <?php echo number_format((($result->no_of_days / 7 ) * $result->daily_returns * $quantity), 2); ?></td>
                  </tr>
                <?php }?>
              <?php } else { ?>
                <tr>
                  <td>No packages are found</td>
                </tr>
              <?php }?>
            </tbody>
          </table>

        <?php }?>
                </div>
            </div>
        </div>
    </div>
</div>