<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $user app\models\LoginForm */

use borales\extensions\phoneInput\PhoneInput;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Url;
?>


<?php    
$this->title = 'Payment Gateways';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- breadcrumb start here -->
<!-- <div class="bread-crumb">
	<div class="container">
		<h2>Thank You</h2>
		<ul class="list-inline">
			<li><a href="/">home</a></li>
			<li><a href="<?php echo Url::toRoute('site/payment');?>">Payment Gateways</a></li>
		</ul>
	</div>
</div> -->
<!-- breadcrumb end here -->
<style>
    .layout{
        border:1px solid #6f7175;
    background: #f4f6f9;
    margin-top: 25px;
    padding:10px;
    margin-bottom: 20px;
    }
    
    .logo{
        padding-top: 20px;
    }
    
    .bttn{
            margin-top: 22px;
    padding: 7px;
    }

</style>    
<!-- main container start here -->
<div class="container">
    
    
<div class="placetop layout">
<!--	<div class="container">-->
		<div class="row">
    
   
<!--			<div class="col-sm-12">
				<div class="thanks">
						<img class="" src="/images/payeer.png" alt="payeer" />
					<div class="">
						<h3>Deposit via Payeer</h3>
						<p>No fees. Processed immediately.</p>
					</div>
                                                <div style="float:right">
						<button class="btn-primary" type="button">  P********</button>
					</div>
				</div>
			</div>-->
			<div class="col-sm-12">
			<div class="col-sm-4">
				
						<img class="logo" src="/images/payeer.png" alt="payeer" />
                        </div>
                                        <div class="col-sm-4">

						<h3>Deposit via Payeer</h3>
						<p>No fees. Processed immediately.</p>
					</div>
                                        <div class="col-sm-4">

						<button class="btn-primary pull-right btn-lg bttn" type="button">P********</button>
					</div>
				</div>
			</div>
		</div>
	<!--</div>-->

<div class="placetop layout">
	<!--<div class="container">-->
		<div class="row">
			<div class="col-sm-12">
			<div class="col-sm-4">
				
				<img class="logo" src="/images/bitcoin.png" alt="bitcoin" />
                        </div>
			<div class="col-sm-4">		
						<h3>Deposit via Bitcoin(USD)</h3>
						<p>Bitcoin address for deposit :</p>
                        </div>
					<div class="text-right">
						<button class="btn-primary pull-right btn-lg bttn" type="button">*******bitcoinaddress*******</button>
					</div>
				</div>
			</div>
		</div>
	<!--</div>-->

<div class="placetop layout">
	<!--<div class="container">-->
		<div class="row">
			<div class="col-sm-12">
			<div class="col-sm-4">
				
				<img class="logo" src="/images/perfectmoney.png" alt="perfectmoney" />
                                
                        </div>
					<div class="col-sm-4">
						<h3>Deposit via Perfect Money</h3>
						<p>No fees. Processed immediately.</p>
					</div>
					<div class="text-right">
						<button class="btn-primary pull-right btn-lg bttn" type="button">U********</button>
					</div>
				</div>
			</div>
		</div>
	<!--</div>-->
</div>
    
