<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Utils;
use yii\helpers\Url;

$this->title = 'Market Capitalizations';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="right_col" role="main" style="min-height: 202px;">
    <div class="clearfix"></div>
    <div class="header-title-breadcrumb element-box-shadow">
        <div class="container" style="height: 36px;">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12 text-left">
                    <h3><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></h3>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12 hide-on-tablet">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo Url::to(['user/dashboard']); ?>">Dashboard</a></li> 
                        <li class="active"><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>

    <div class="row chart-section1">
        <div class="col-md-12">
            <div class="panel panel-default table-transactions">
                <div class="panel-body">
                    <div class="table-market" role="main">
                        <!--<div class="spacer_30"></div>-->
                        <div class="clearfix"></div>
                        
                        <div class="chart-section1">
                            <div class="panel panel-default table-market-capitalization">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Cryptocurrency Market Capitalizations</h3>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-cryptic dataTable no-footer" id="dataTable_crypto" role="grid">
                                        <thead>
                                            <tr role="row">
                                                <th colspan="2" rowspan="1">Currency <i class="fa fa-sort"></i></th>
                                                <th class="text-right">Price <i class="fa fa-sort"></i></th>
                                                <th class="text-right">Market Cap <i class="fa fa-sort-amount-desc"></i></th>
                                                <th class="text-right">Volume 24H <i class="fa fa-sort"></i></th>
                                                <th class="text-right">Change % (1H) <i class="fa fa-sort"></i></th>
                                                <th class="text-right">Change % (24H) <i class="fa fa-sort"></i></th>
                                                <th class="text-right">Change % (7D) <i class="fa fa-sort"></i></th>
                                                <!--<th class="text-right">Chart <i class="fa fa-sort"></i></th>-->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc BTC" title="BTC"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Bitcoin</a></h6>
                                                    <small class="text-muted">BTC</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 11,723.40</p></td>
                                                <td class="text-right"><p><span>$</span> 197,078,267,295</p></td>
                                                <td class="text-right"><p><span>$</span> 17,950,900,000</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -1.82%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 17.96%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -15.20%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc ETH" title="ETH"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Ethereum</a></h6>
                                                    <small class="text-muted">ETH</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 1,070.39</p></td>
                                                <td class="text-right"><p><span>$</span> 103,892,495,504</p></td>
                                                <td class="text-right"><p><span>$</span> 7,564,310,000</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -1.28%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 26.13%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -11.74%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,5,5,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc XRP" title="XRP"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Ripple</a></h6>
                                                    <small class="text-muted">XRP</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 1.64</p></td>
                                                <td class="text-right"><p><span>$</span> 63,391,183,730</p></td>
                                                <td class="text-right"><p><span>$</span> 10,143,400,000</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -0.12%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 66.62%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -16.44%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,20,20,10,10,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc ADA" title="ADA"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Cardano</a></h6>
                                                    <small class="text-muted">ADA</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 0.68</p></td>
                                                <td class="text-right"><p><span>$</span> 17,643,890,043</p></td>
                                                <td class="text-right"><p><span>$</span> 1,687,430,000</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -2.40%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 40.76%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -5.91%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,20,20,10,10,14,16,15,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc LTC" title="LTC"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Litecoin</a></h6>
                                                    <small class="text-muted">LTC</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 198.88</p></td>
                                                <td class="text-right"><p><span>$</span> 10,901,285,520</p></td>
                                                <td class="text-right"><p><span>$</span> 1,235,390,000</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -2.56%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 26.96%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -15.74%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">18,14,10,10,11,14,14,20,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc XEM" title="XEM"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> NEM</a></h6>
                                                    <small class="text-muted">XEM</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 1.11</p></td>
                                                <td class="text-right"><p><span>$</span> 9,996,569,999</p></td>
                                                <td class="text-right"><p><span>$</span> 153,335,000</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -1.35%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 43.32%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -19.62%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,20,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc NEO" title="NEO"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> NEO</a></h6>
                                                    <small class="text-muted">NEO</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 149.15</p></td>
                                                <td class="text-right"><p><span>$</span> 9,694,490,000</p></td>
                                                <td class="text-right"><p><span>$</span> 1,304,130,000</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -4.32%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 36.90%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 31.05%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,20,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc DASH" title="DASH"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Dash</a></h6>
                                                    <small class="text-muted">DASH</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 865.27</p></td>
                                                <td class="text-right"><p><span>$</span> 6,771,308,110</p></td>
                                                <td class="text-right"><p><span>$</span> 193,439,000</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -0.94%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 30.83%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -16.47%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,20,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc EOS" title="EOS"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> EOS</a></h6>
                                                    <small class="text-muted">EOS</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 10.57</p></td>
                                                <td class="text-right"><p><span>$</span> 6,460,354,540</p></td>
                                                <td class="text-right"><p><span>$</span> 1,566,560,000</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -4.17%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 25.84%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -6.43%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,20,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc XMR" title="XMR"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Monero</a></h6>
                                                    <small class="text-muted">XMR</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 336.11</p></td>
                                                <td class="text-right"><p><span>$</span> 5,249,235,883</p></td>
                                                <td class="text-right"><p><span>$</span> 176,650,000</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -1.92%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 28.27%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -9.78%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,20,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc ETC" title="ETC"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Ethereum Classic</a></h6>
                                                    <small class="text-muted">ETC</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 31.35</p></td>
                                                <td class="text-right"><p><span>$</span> 3,109,936,842</p></td>
                                                <td class="text-right"><p><span>$</span> 550,273,000</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -4.03%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 24.92%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -8.33%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,20,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc QTUM" title="QTUM"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Qtum</a></h6>
                                                    <small class="text-muted">QTUM</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 38.18</p></td>
                                                <td class="text-right"><p><span>$</span> 2,817,991,874</p></td>
                                                <td class="text-right"><p><span>$</span> 888,043,000</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -3.16%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 26.87%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -21.11%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,20,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc LSK" title="LSK"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Lisk</a></h6>
                                                    <small class="text-muted">LSK</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 23.77</p></td>
                                                <td class="text-right"><p><span>$</span> 2,784,607,027</p></td>
                                                <td class="text-right"><p><span>$</span> 94,214,400</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -1.97%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 38.82%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -9.49%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,20,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc OMG" title="OMG"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> OmiseGO</a></h6>
                                                    <small class="text-muted">OMG</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 18.79</p></td>
                                                <td class="text-right"><p><span>$</span> 1,916,950,969</p></td>
                                                <td class="text-right"><p><span>$</span> 101,669,000</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -2.55%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 38.28%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -17.04%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,20,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc USDT" title="USDT"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Tether</a></h6>
                                                    <small class="text-muted">USDT</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 1.03</p></td>
                                                <td class="text-right"><p><span>$</span> 1,672,345,408</p></td>
                                                <td class="text-right"><p><span>$</span> 4,941,850,000</p></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 0.78%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 1.93%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 3.13%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc XVG" title="XVG"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Verge</a></h6>
                                                    <small class="text-muted">XVG</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 0.11</p></td>
                                                <td class="text-right"><p><span>$</span> 1,663,900,911</p></td>
                                                <td class="text-right"><p><span>$</span> 261,147,000</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -6.63%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 70.25%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -25.29%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,20,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc ZEC" title="ZEC"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Zcash</a></h6>
                                                    <small class="text-muted">ZEC</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 530.40</p></td>
                                                <td class="text-right"><p><span>$</span> 1,636,048,635</p></td>
                                                <td class="text-right"><p><span>$</span> 145,861,000</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -0.95%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 25.30%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -20.25%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,20,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc STRAT" title="STRAT"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Stratis</a></h6>
                                                    <small class="text-muted">STRAT</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 15.53</p></td>
                                                <td class="text-right"><p><span>$</span> 1,532,582,626</p></td>
                                                <td class="text-right"><p><span>$</span> 55,016,000</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -1.87%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 34.40%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -8.87%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc ARDR" title="ARDR"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Ardor</a></h6>
                                                    <small class="text-muted">ARDR</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 1.48</p></td>
                                                <td class="text-right"><p><span>$</span> 1,482,804,960</p></td>
                                                <td class="text-right"><p><span>$</span> 261,149,000</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -4.61%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 29.43%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 7.45%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc BCN" title="BCN"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Bytecoin</a></h6>
                                                    <small class="text-muted">BCN</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 0.01</p></td>
                                                <td class="text-right"><p><span>$</span> 1,465,618,587</p></td>
                                                <td class="text-right"><p><span>$</span> 10,811,700</p></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 0.63%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 54.15%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -21.15%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc STEEM" title="STEEM"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Steem</a></h6>
                                                    <small class="text-muted">STEEM</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 4.45</p></td>
                                                <td class="text-right"><p><span>$</span> 1,100,959,745</p></td>
                                                <td class="text-right"><p><span>$</span> 25,037,000</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -1.88%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 42.02%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -6.83%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc BTS" title="BTS"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> BitShares</a></h6>
                                                    <small class="text-muted">BTS</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 0.36</p></td>
                                                <td class="text-right"><p><span>$</span> 947,934,004</p></td>
                                                <td class="text-right"><p><span>$</span> 89,824,700</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -3.65%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 35.07%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -39.46%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc DOGE" title="DOGE"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Dogecoin</a></h6>
                                                    <small class="text-muted">DOGE</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 0.01</p></td>
                                                <td class="text-right"><p><span>$</span> 941,142,659</p></td>
                                                <td class="text-right"><p><span>$</span> 63,248,100</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -2.44%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 39.99%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -30.79%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc WAVES" title="WAVES"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Waves</a></h6>
                                                    <small class="text-muted">WAVES</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 8.78</p></td>
                                                <td class="text-right"><p><span>$</span> 877,976,000</p></td>
                                                <td class="text-right"><p><span>$</span> 39,506,800</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -2.76%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 22.61%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -23.42%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc REP" title="REP"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Augur</a></h6>
                                                    <small class="text-muted">REP</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 66.95</p></td>
                                                <td class="text-right"><p><span>$</span> 736,479,700</p></td>
                                                <td class="text-right"><p><span>$</span> 30,136,300</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -2.58%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 31.27%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -36.46%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc KMD" title="KMD"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Komodo</a></h6>
                                                    <small class="text-muted">KMD</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 6.81</p></td>
                                                <td class="text-right"><p><span>$</span> 708,469,055</p></td>
                                                <td class="text-right"><p><span>$</span> 13,785,800</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -2.38%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 48.65%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -11.35%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc DGB" title="DGB"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> DigiByte</a></h6>
                                                    <small class="text-muted">DGB</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 0.07</p></td>
                                                <td class="text-right"><p><span>$</span> 665,577,239</p></td>
                                                <td class="text-right"><p><span>$</span> 30,695,200</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -2.55%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 40.31%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -30.83%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc ARK" title="ARK"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Ark</a></h6>
                                                    <small class="text-muted">ARK</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 6.65</p></td>
                                                <td class="text-right"><p><span>$</span> 652,050,748</p></td>
                                                <td class="text-right"><p><span>$</span> 12,076,300</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -0.91%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 42.48%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -24.47%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc SALT" title="SALT"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> SALT</a></h6>
                                                    <small class="text-muted">SALT</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 8.99</p></td>
                                                <td class="text-right"><p><span>$</span> 639,586,223</p></td>
                                                <td class="text-right"><p><span>$</span> 17,684,300</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -0.53%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 35.98%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -24.19%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc BAT" title="BAT"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Basic Attention Token</a></h6>
                                                    <small class="text-muted">BAT</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 0.63</p></td>
                                                <td class="text-right"><p><span>$</span> 627,186,000</p></td>
                                                <td class="text-right"><p><span>$</span> 39,758,900</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -4.29%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 63.56%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -10.68%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc GNT" title="GNT"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Golem</a></h6>
                                                    <small class="text-muted">GNT</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 0.73</p></td>
                                                <td class="text-right"><p><span>$</span> 608,448,133</p></td>
                                                <td class="text-right"><p><span>$</span> 17,969,100</p></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 2.46%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 39.84%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -15.94%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc DCR" title="DCR"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Decred</a></h6>
                                                    <small class="text-muted">DCR</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 89.31</p></td>
                                                <td class="text-right"><p><span>$</span> 587,798,364</p></td>
                                                <td class="text-right"><p><span>$</span> 2,615,730</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -2.64%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 26.37%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -15.64%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc GBYTE" title="GBYTE"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Byteball Bytes</a></h6>
                                                    <small class="text-muted">GBYTE</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 882.48</p></td>
                                                <td class="text-right"><p><span>$</span> 569,397,493</p></td>
                                                <td class="text-right"><p><span>$</span> 2,505,740</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -1.06%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 54.71%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 24.96%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc PIVX" title="PIVX"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> PIVX</a></h6>
                                                    <small class="text-muted">PIVX</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 9.66</p></td>
                                                <td class="text-right"><p><span>$</span> 534,842,023</p></td>
                                                <td class="text-right"><p><span>$</span> 9,726,770</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -2.13%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 22.92%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -22.99%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc FCT" title="FCT"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Factom</a></h6>
                                                    <small class="text-muted">FCT</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 48.78</p></td>
                                                <td class="text-right"><p><span>$</span> 426,580,828</p></td>
                                                <td class="text-right"><p><span>$</span> 16,881,500</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -1.58%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 38.03%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -21.28%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc RDD" title="RDD"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> ReddCoin</a></h6>
                                                    <small class="text-muted">RDD</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 0.01</p></td>
                                                <td class="text-right"><p><span>$</span> 422,219,857</p></td>
                                                <td class="text-right"><p><span>$</span> 19,938,500</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -3.70%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 71.16%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -24.34%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc DGD" title="DGD"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> DigixDAO</a></h6>
                                                    <small class="text-muted">DGD</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 192.04</p></td>
                                                <td class="text-right"><p><span>$</span> 384,070,000</p></td>
                                                <td class="text-right"><p><span>$</span> 10,433,200</p></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 0.25%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 18.68%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -3.47%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc SYS" title="SYS"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Syscoin</a></h6>
                                                    <small class="text-muted">SYS</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 0.70</p></td>
                                                <td class="text-right"><p><span>$</span> 371,041,676</p></td>
                                                <td class="text-right"><p><span>$</span> 7,089,720</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -2.43%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 37.39%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -10.68%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc MONA" title="MONA"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> MonaCoin</a></h6>
                                                    <small class="text-muted">MONA</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 6.53</p></td>
                                                <td class="text-right"><p><span>$</span> 370,827,916</p></td>
                                                <td class="text-right"><p><span>$</span> 6,624,600</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -0.56%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 27.45%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -17.58%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc MAID" title="MAID"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> MaidSafeCoin</a></h6>
                                                    <small class="text-muted">MAID</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 0.76</p></td>
                                                <td class="text-right"><p><span>$</span> 342,003,814</p></td>
                                                <td class="text-right"><p><span>$</span> 6,891,280</p></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 0.18%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 26.44%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -21.25%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc NXT" title="NXT"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Nxt</a></h6>
                                                    <small class="text-muted">NXT</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 0.34</p></td>
                                                <td class="text-right"><p><span>$</span> 335,585,059</p></td>
                                                <td class="text-right"><p><span>$</span> 27,466,200</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -3.02%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 39.71%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -19.76%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc XZC" title="XZC"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> ZCoin</a></h6>
                                                    <small class="text-muted">XZC</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 80.10</p></td>
                                                <td class="text-right"><p><span>$</span> 312,352,936</p></td>
                                                <td class="text-right"><p><span>$</span> 6,700,890</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -1.86%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 27.82%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -19.19%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc GAME" title="GAME"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> GameCredits</a></h6>
                                                    <small class="text-muted">GAME</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 4.83</p></td>
                                                <td class="text-right"><p><span>$</span> 311,078,970</p></td>
                                                <td class="text-right"><p><span>$</span> 7,081,870</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -2.93%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 34.00%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -18.16%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc PART" title="PART"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Particl</a></h6>
                                                    <small class="text-muted">PART</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 33.83</p></td>
                                                <td class="text-right"><p><span>$</span> 299,053,213</p></td>
                                                <td class="text-right"><p><span>$</span> 972,687</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -1.80%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 32.53%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 32.35%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc GNO" title="GNO"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Gnosis</a></h6>
                                                    <small class="text-muted">GNO</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 254.20</p></td>
                                                <td class="text-right"><p><span>$</span> 280,783,464</p></td>
                                                <td class="text-right"><p><span>$</span> 4,359,600</p></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 0.95%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 53.11%</span></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -26.89%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                            <tr role="row">
                                                <td><span><a href="candlestick-charts.html"><i class="cc EMC" title="EMC"></i></a></span></td>
                                                <td>
                                                    <h6><a href="candlestick-charts.html" class="d-none d-md-block d-lg-block d-xl-block"> Emercoin</a></h6>
                                                    <small class="text-muted">EMC</small>
                                                </td>
                                                <td class="text-right"><p><span>$</span> 6.52</p></td>
                                                <td class="text-right"><p><span>$</span> 268,738,319</p></td>
                                                <td class="text-right"><p><span>$</span> 4,293,630</p></td>
                                                <td class="no-wrap text-right"><span class="change-down label label-light-danger"><i class="fa fa-chevron-down"></i> -2.01%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 30.30%</span></td>
                                                <td class="no-wrap text-right"><span class="change-up label label-light-success"><i class="fa fa-chevron-up"></i> 2.43%</span></td>
                                                <!--<td><a href="candlestick-charts.html"><span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span></a></td>-->
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="buy_now">
                            <a href="https://modeltheme.com/get-cryptic-ui-kit.php">
                                <div class="btn-hover-text"><p class="element-box-shadow text-bold">Purchase for 24$</p></div>
                                <button class="button_buy_now text-bold element-box-shadow"><i class="icon-basket icons"></i>
                                </button>
                            </a>
                            <div class="btn-small">$24</div>
                        </div>
                        <a href="#" class="scrollToTop"><i class="fa fa-chevron-up text-white" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>