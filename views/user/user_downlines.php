<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;
use yii\widgets\LinkPager;
use yii\helpers\Utils;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PackageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Direct Referrals';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="right_col" role="main" style="min-height: 202px;">
    <div class="clearfix"></div>
    <div class="header-title-breadcrumb element-box-shadow">
        <div class="container" style="height: 36px;">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12 text-left">
                    <h3><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></h3>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12 hide-on-tablet">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo Url::to(['user/dashboard']); ?>">Dashboard</a></li> 
                        <li class="active"><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>

    <div class="row chart-section1">
        <div class="col-md-12">
            <div class="panel panel-default table-transactions">
                <div class="panel-body padding_30">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>User</th>
                                <th>Package Prices (<i class="fa fa-btc"> </i>)</th>
                                <th>Status</th>
                                <th>Position</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php if (!empty($users)) : ?>
                                <?php $i = (1 * ($pages->getPage() * Yii::$app->params['pageSize'])) + 1; ?>
                                <?php foreach ($users as $user): ?>
                                    <tr class="<?php echo ($user->status == 0) ? 'danger' : 'success' ?>">
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $user->username; ?></td>
                                        <td>
                                            <?php $orders = Utils::getUserPackages($user->id); ?>
                                            <?php if (!empty($orders)): ?>
                                                <?php foreach ($orders as $order): ?>
                                                    <?php echo "<i class='fa fa-btc'> </i> " . $order->package->price; ?>, &nbsp;
                                                <?php endforeach; ?>
                                            <?php else: ?>
                                                <p> None </p>
                                            <?php endif; ?>
                                        </td>
                                        <td><?php echo ($user->status == 0) ? "Inactive" : "Active"; ?></td>
                                        <td><?php echo ($user->position == 'L') ? "Left" : "Right"; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr class="center">
                                    <td> No results found!!! </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>

                    <!--- PAGINATION BLOCK -->
                    <?php
                    // display pagination
                    echo LinkPager::widget([
                        'pagination' => $pages,
                    ]);
                    ?>
                    <!-- End -->
                </div>
            </div>
        </div>
    </div>
</div>
