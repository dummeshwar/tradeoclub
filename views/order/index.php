<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;


/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'My Orders';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="right_col" role="main" style="min-height: 202px;">
    <div class="clearfix"></div>
    <div class="header-title-breadcrumb element-box-shadow">
        <div class="container" style="height: 36px;">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12 text-left">
                    <h3><?php echo !empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></h3>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12 hide-on-tablet">
                    <ol class="breadcrumb text-right">
                        <li><a href="<?php echo Url::to(['user/dashboard']); ?>">Dashboard</a></li> 
                        <li class="active"><?php echo!empty($this->params['breadcrumbs'][0]) ? $this->params['breadcrumbs'][0] : '' ?></li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>

    <div class="row chart-section1">
        <div class="col-md-12">
            <div class="panel panel-default table-transactions">
                <div class="panel-body padding_30">
                    
    <?php if (Yii::$app->session->hasFlash('order-success')): ?>
      <div class="alert alert-success alert-dismissable">
            <p><i class="icon fa fa-check"></i><?= Yii::$app->session->getFlash('order-success') ?></p>
      </div>
    <?php endif; ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'user_id',
            [
                    'label' => "Package Name",
                    'attribute' => 'package',
                    'value' => function($model) {
                        return !empty($model->travelPackage->name) ? $model->travelPackage->name : $model->package->name;
                    }
            ],

            [
                    'header' => 'Package Price',
                    //'label' => "Package Price",
                    'attribute' => 'package',
                    'value' => 'package.price',
                    'value' => function($model) {
                        return !empty($model->travelPackage->price) ? $model->travelPackage->price : $model->package->price;
                    }
            ],

//                   commented by vin start
            [
                'attribute' => 'start_date',
                'label' => 'Purchased on/before',
                'filter' => '<div class="drp-container input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>' .
                DateRangePicker::widget([
                    'name' => 'startDate',
                    'model' => $searchModel,
                    'convertFormat' => true,
                    'attribute' => 'start_date',
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'Y-m-d',
                            'separator' => ' to ',
                        ],
                        'opens' => 'left'
                    ]
                ]) . '</div>'
            ],

            [
                'attribute' => 'end_date',
                'label' => 'Expire on/before',
                'filter' => '<div class="drp-container input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>' .
                DateRangePicker::widget([
                    'name' => 'endDate',
                    'model' => $searchModel,
                    'convertFormat' => true,
                    'attribute' => 'end_date',
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'Y-m-d',
                            'separator' => ' to ',
                        ],
                        'opens' => 'left'
                    ]
                ]) . '</div>'
            ],
//                    commented by vin END

            [
                    'label' => "Status",
                    'attribute' => 'orderStatus',
                    'format'=>'raw',
//                    'value' => Html::a('Click me', ['site/index'], ['class' => 'btn btn-success btn-xs']),
                    'value' => function($model) {
                        $status = '';
                        $class = '';
//                        echo '<pre>'; print_r($model); exit;
//                        return !empty($model->status) ? $model->status : '9999';
                        if(!empty($model->status) && ($model->status == 1)) {
                            $status = 'Success';
                            $class = 'label label-success';
                        } else {
                            $status = 'Expired';
                            $class = 'label label-danger';
                        }
                        return '<span class="'.$class.'" style="top: 5px; position: relative;">'.$status.'</span>';
                    }

                    // 'format' => 'html',
                    // 'filter' => ['1' => 'Success', '0' => 'Failded'],
            ],

            // 'updated_at',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>