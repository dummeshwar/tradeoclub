<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Forgot Password';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .borderform{
     box-shadow: 5px 5px 5px 5px #888888;
    padding: 15px;
    margin-bottom: 40px;
    border-radius: 6px;
    padding-top: 30px;
    
}
input#passwordresetrequestform-email {
    width: 300px;
}
.bt1 {
    padding: 12px 18px;
    font-size: 20px;
    margin-left: -22px;
    
}
h1 {
    margin-bottom: 20px;
    text-align: center;
    margin-top: -39px;
}
</style>
 <section class="section lb page-title1 little-pad">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-big-title clearfix text-center">
                	                    <div class="title-banner-bred">
                        <div class="title-subtitle-content">
                            <h3>My Account</h3>
                            
                        </div>                    
                                                    <div class="breadcrumb-content">
                                <ul class="breadcrumb">
			<li><a href="/">Home</a></li><li class="active">Forgot Password</li>
		</ul>                            </div>
                                            </div>
                                    </div>
            </div>
        </div>
    </div>
</section>

<div class="request-new-password container">
    
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('error')): ?>
        <p class="lead text-danger"> <strong>Error!</strong>. <?= Yii::$app->session->getFlash('error'); ?>.</p>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <p class="lead text-success"> <strong>Successful!</strong>. <?= Yii::$app->session->getFlash('success'); ?>.</p>
    <?php endif; ?>

    <?php
    $form = ActiveForm::begin([
                'id' => 'forgot-password-form',
                'layout' => 'horizontal',
                'fieldConfig' => [
//                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
//                    'labelOptions' => ['class' => 'col-lg-1 control-label'],
                ],
    ]);
    ?>
<div class="col-lg-6 col-lg-offset-3">
        <div class="borderform">
    <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Submit', ['class' => 'bt1 btn btn-block', 'name' => 'forgot-password-button', 'style'=>'background:#ee821a;color:#fff']) ?>
        </div>
    </div>
        </div>
</div>
    <?php ActiveForm::end(); ?>
</div>



<!--
<style>
    
    .login-center {
    background: #fff;
    min-width: 20rem;
    padding: 5px 0px;
    border-radius: 5px;
    margin: 1.55714em 0;
    border: 1px solid blue;
    }
    .input-group1{
        padding: 3px;
        margin:15px;
    }
    .input-group2{
       margin:5px 25px;
    }

    .bt1{
        padding: 20px 18px;
        font-size: 20px;
    }
    
    #loginlogo{
        background: #60af2f;
    padding: 10px;
    text-align: center;
    height: 66px;
    margin-left: 133px;
    margin-top: 15px;
    }

    .inpt{
        padding: 24px 10px;
    }
    .fpass{
        margin-left: 165px;
    }
    h1{
        text-align: center;
    }
    </style>

    
<div class="row"     style="background: rebeccapurple;margin-top: -30px;">
    <div class="col-lg-4 col-lg-offset-4">
        <div class="login-center">
      

<h1>Forgot Password</h1>
<div class="input-group1">
<label>Email Id</label><font color="red" size="2">*</font>
            <input type="text" class="form-control inpt" /> 
            
        </div>
        
        
        
        <div class="input-group2">
            <a href="#" class="bt1 btn btn-block">Submit</a>
        </div>
        
    </div> 
</div> 
    </div>
-->