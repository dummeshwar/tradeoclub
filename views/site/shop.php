<?php // echo '<pre>'; print_r($packages); exit; ?>
<!--  page banner -->

<?php

use borales\extensions\phoneInput\PhoneInput;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Url;
use yii\helpers\Utils;
use yii\widgets\LinkPager;

?>

  <section id="page-banner" class="page-banner-main-block" style="background-image: url('images/bg/page-banner.jpg')"> 
    <div class="overlay-bg"></div>
    <div class="container">
      <h1 class="page-banner-heading">Shop</h1>
    </div>
  </section>
<!--  end page banner  -->
<!--  breadcrumb -->
  <section class="breadcrumb-block">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li>Shop</li>
          </ol>
        </div>
        <div class="col-sm-4">
          <div class="breadcrumb-btn">
            <a href="contact.html" class="btn btn-default">Get In Touch</a>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- end breadcrumb -->
<!--  shop -->
  <section id="shop-page" class="shop-page-main-block">
    <div class="container">
      <div class="row">

        <div class="col-sm-12">
          <div class="product-filter">
            <div class="row">
              <div class="col-sm-6">
                <div class="showing-result">
                  Showing 1–9 of <a href="#">15</a> results       
                </div>
              </div>
              <div class="col-sm-6 text-right">
                <div class="filter-by">
                  <form action="#">
                    <div class="select-filter">
                        <select>
                          <option selected disabled value="default">Default Sorting</option>
                          <option value="ascending">Ascending</option>
                          <option value="descending">Descending</option>
                        </select> 
                    </div>
                  </form>               
                </div>
              </div>              
            </div>
          </div>
          <div class="row">
              <?php if (!empty($packages)) { ?>
                <?php foreach($packages as $package): ?>
            <div class="col-md-4 col-sm-6">
              <div class="product">
                <div class="product-img">
                  <!--<a href="single-product.html"><img src="images/shop/shop-1.jpg" class="img-responsive" alt="product"></a>-->
                      <?php $link_to_package = Url::to(['site/package-detail', 'id' => base64_encode($package->id)]); ?>
                                                <?php if (!empty($package->image)) { ?>
                                                        <a href="<?= $link_to_package; ?>">
                                                                <?= Html::img(Yii::$app->params['packageImagesUpload'] . $package->image , ['alt' => $package->name, 'title' => $package->name, 'class' => "img-responsive"]) ?>
                                                        </a>
                                                <?php } else {?>
                                                        <a href="<?= $link_to_package; ?>">
                                                                <?= Html::img(Yii::$app->params['packagePlaceHolderImage'] , ['alt' => $package->name, 'title' => $package->name, 'class' => "img-responsive"]) ?>
                                                        </a>
                                                <?php } ?>
                </div>
                <div class="product-dtl">
                  <!--<a href="single-product.html"><h6 class="product-name">Blue-tooth Headphones</h6></a>-->
                  <a href="<?= $link_to_package; ?>"> <h6 class="product-name"><?= $package->name ?></h6></a>
                  <div class="rating">     
                    <ul>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li class="disable"><i class="fa fa-star" aria-hidden="true"></i></li>
                    </ul>
                  </div> 
                  <div class="product-price">$<?= $package->price ?></div>
                  <!--<a href="#" class="btn btn-default btn-grey">Add To cart</a>-->
                  <a class="btn btn-default btn-grey"  href="<?= $link_to_package; ?>">Buy Now</a> 
                </div>
              </div>
            </div>
              <?php endforeach; ?>
                                                        <?php } ?>
<!--            <div class="col-md-4 col-sm-6">
              <div class="product">
                <div class="product-img">
                  <a href="single-product.html"><img src="images/shop/shop-2.jpg" class="img-responsive" alt="product"></a>
                </div>
                <div class="product-dtl">
                  <a href="single-product.html"><h6 class="product-name">Camera Lance</h6></a>
                  <div class="rating">     
                    <ul>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star-half-empty" aria-hidden="true"></i></li>
                    </ul>
                  </div> 
                  <div class="product-price">$ 999.00</div>
                  <a href="#" class="btn btn-default btn-grey">Add To cart</a>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="product">
                <div class="product-img">
                  <a href="single-product.html"><img src="images/shop/shop-3.jpg" class="img-responsive" alt="product"></a>
                </div>
                <div class="product-dtl">
                  <a href="single-product.html"><h6 class="product-name">Mobile Device</h6></a>
                  <div class="rating">     
                    <ul>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li class="disable"><i class="fa fa-star" aria-hidden="true"></i></li>
                    </ul>
                  </div> 
                  <div class="product-price">$ 850.00</div>
                  <a href="#" class="btn btn-default btn-grey">Add To cart</a>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="product">
                <div class="product-img">
                  <a href="single-product.html"><img src="images/shop/shop-4.jpg" class="img-responsive" alt="product"></a>
                </div>
                <div class="product-dtl">
                  <a href="single-product.html"><h6 class="product-name">NFC Speakers </h6></a>
                  <div class="rating">     
                    <ul>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    </ul>
                  </div> 
                  <div class="product-price">$ 600.00</div>
                  <a href="#" class="btn btn-default btn-grey">Add To cart</a>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="product">
                <div class="product-img">
                  <a href="single-product.html"><img src="images/shop/shop-5.jpg" class="img-responsive" alt="product"></a>
                </div>
                <div class="product-dtl">
                  <a href="single-product.html"><h6 class="product-name">Blue-tooth Headphones</h6></a>
                  <div class="rating">     
                    <ul>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li class="disable"><i class="fa fa-star" aria-hidden="true"></i></li>
                    </ul>
                  </div> 
                  <div class="product-price">$ 540.00</div>
                  <a href="#" class="btn btn-default btn-grey">Add To cart</a>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="product">
                <div class="product-img">
                  <a href="single-product.html"><img src="images/shop/shop-6.jpg" class="img-responsive" alt="product"></a>
                </div>
                <div class="product-dtl">
                  <a href="single-product.html"><h6 class="product-name">Portable Speaker</h6></a>
                  <div class="rating">     
                    <ul>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    </ul>
                  </div> 
                  <div class="product-price">$ 600.00</div>
                  <a href="#" class="btn btn-default btn-grey">Add To cart</a>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="product">
                <div class="product-img">
                  <a href="single-product.html"><img src="images/shop/shop-7.jpg" class="img-responsive" alt="product"></a>
                </div>
                <div class="product-dtl">
                  <a href="single-product.html"><h6 class="product-name">USB Cable </h6></a>
                  <div class="rating">     
                    <ul>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li class="disable"><i class="fa fa-star" aria-hidden="true"></i></li>
                    </ul>
                  </div> 
                  <div class="product-price">$ 100.00</div>
                  <a href="#" class="btn btn-default btn-grey">Add To cart</a>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="product">
                <div class="product-img">
                  <a href="single-product.html"><img src="images/shop/shop-1.jpg" class="img-responsive" alt="product"></a>
                </div>
                <div class="product-dtl">
                  <a href="single-product.html"><h6 class="product-name">Blue-tooth Headphones</h6></a>
                  <div class="rating">     
                    <ul>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li class="disable"><i class="fa fa-star" aria-hidden="true"></i></li>
                    </ul>
                  </div> 
                  <div class="product-price">$ 780.00</div>
                  <a href="#" class="btn btn-default btn-grey">Add To cart</a>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6">
              <div class="product">
                <div class="product-img">
                  <a href="single-product.html"><img src="images/shop/shop-9.jpg" class="img-responsive" alt="product"></a>
                </div>
                <div class="product-dtl">
                  <a href="single-product.html"><h6 class="product-name">Watch</h6></a>
                  <div class="rating">     
                    <ul>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                    </ul>
                  </div> 
                  <div class="product-price">$ 699.00</div>
                  <a href="#" class="btn btn-default btn-grey">Add To cart</a>
                </div>
              </div>
            </div>-->
          </div>
          <hr>
          <nav aria-label="Page navigation" class="shop-pagination text-center">
            <ul class="pagination">
              <li>
                <a href="#" aria-label="Previous">
                  <span aria-hidden="true"><i class="fa fa-angle-left" aria-hidden="true"></i></span>
                </a>
              </li>
              <li class="active"><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li>
                <a href="#" aria-label="Next">
                  <span aria-hidden="true"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                </a>
              </li>
            </ul>
          </nav> 
        </div>
      </div>
    </div>
  </section>

<!-- jquery -->
<script src="js/jquery.min.js"></script> <!-- jquery library js -->
<script src="js/bootstrap.min.js"></script> <!-- bootstrap js -->
<script src="js/menumaker.js"></script> <!-- menumaker js -->
<script src="js/owl.carousel.js"></script> <!-- owl carousel js -->
<script src="js/jquery.magnific-popup.min.js"></script> <!-- magnify popup js -->
<script src="js/jquery.elevatezoom.js"></script> <!-- product zoom js -->
<script src="js/jquery.ajaxchimp.js"></script> <!-- mail chimp js -->
<script src="js/smooth-scroll.js"></script> <!-- smooth scroll js -->
<script src="js/waypoints.min.js"></script> <!-- facts count js required for jquery.counterup.js file -->
<script src="js/jquery.counterup.js"></script> <!-- facts count js-->
<script src="js/price-slider.js"></script> <!-- price slider / filter js-->
<script src="js/theme.js"></script> <!-- custom js -->  
<!-- end jquery -->
</body>
<!--body end -->
</html>