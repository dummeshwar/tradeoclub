<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Forgot password and key creation link ?';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resend-password-security-create container">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('error')): ?>
        <p class="lead text-danger"> <strong>Error!</strong>. <?= Yii::$app->session->getFlash('error'); ?>.</p>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <p class="lead text-success"> <strong>Successful!</strong>. <?= Yii::$app->session->getFlash('success'); ?>.</p>
    <?php endif; ?>

    <?php
    $form = ActiveForm::begin([
                'id' => 'resend-password-security-create-form',
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8 col-lg-offset-2\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-2 control-label'],
                ],
    ]);
    ?>

    <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
    <?= $form->field($model, 'email_repeat')->textInput(['autofocus' => true]) ?>

    <div class="form-group">
        <div class="col-lg-offset-2 col-lg-11">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'resend-password-security-create-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
