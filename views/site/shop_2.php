

<body id="bg">
	
    <div class="page-wraper">
        	
        

        
        <!-- CONTENT START -->
        <div class="page-content">
        
            <!-- INNER PAGE BANNER -->
            <div class="wt-bnr-inr overlay-wraper" style="background-image:url(images/banner/product-banner.jpg);">
            	<div class="overlay-main bg-black opacity-07"></div>
                <div class="container">
                    <div class="wt-bnr-inr-entry">
                        <h1 class="text-white">Product</h1>
                    </div>
                </div>
            </div>
            <!-- INNER PAGE BANNER END -->
            
            <!-- BREADCRUMB ROW -->                            
            <div class="bg-gray-light p-tb20">
            	<div class="container">
                    <ul class="wt-breadcrumb breadcrumb-style-2">
                        <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
                        <li>Product</li>
                    </ul>
                </div>
            </div>
            <!-- BREADCRUMB ROW END -->
            
            <!-- SECTION CONTENT START -->
            <div class="section-full p-t80 p-b50">
                <div class="container">
                    <div class="section-content">
                    	<div class="row">
                            <!-- SIDE BAR END --> 
                            <div class="col-md-9">                   
                                <!-- TITLE START -->
                                <div class="p-b10">
                                    <h2 class="text-uppercase">Our Products</h2>
                                    <div class="wt-separator-outer m-b30">
                                        <div class="wt-separator bg-primary"></div>
                                    </div>
                                </div>
                                <!-- TITLE END -->
                                
                                <div class="row">
                                    <!-- COLUMNS 1 -->
                                    <div class="col-md-4 col-sm-4 col-xs-6 col-xs-100pc m-b30">
                                        <div class="wt-box wt-product-box p-a10 bg-gray">
                                            <div class="wt-thum-bx wt-img-overlay1 wt-img-effect zoom">
                                                <img src="images/products/pic-1.jpg" alt="">
                                                <div class="overlay-bx">
                                                    <div class="overlay-icon">
                                                        <a href="javascript:void(0);">
                                                            <i class="fa fa-cart-plus wt-icon-box-xs"></i>
                                                        </a>
                                                        <a class="mfp-link" href="javascript:void(0);">
                                                            <i class="fa fa-heart wt-icon-box-xs"></i>
                                                        </a>
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="wt-info  text-center">
                                                 <div class="p-a10 bg-white">
                                                    <h4 class="wt-title">
                                                        <a href="javascript:;">One Product</a>
                                                    </h4>
                                                    <span class="price">
                                                        <del>
                                                             <span><span class="Price-currencySymbol">£</span>3.00</span>
                                                        </del> 
                                                        <ins>
                                                            <span><span class="Price-currencySymbol">£</span>5.00</span>
                                                        </ins>
                                                    </span>
                                                    <div class="rating-bx">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="p-t10">
                                                        <button class="site-button button-sm font-weight-600" type="button">ADD TO CART</button>
                                                    </div>
                                                    
                                                    
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- COLUMNS 2 -->
                                    <div class="col-md-4 col-sm-4 col-xs-6 col-xs-100pc m-b30">
                                        <div class="wt-box wt-product-box p-a10 bg-gray">
                                            <div class="wt-thum-bx wt-img-overlay1 wt-img-effect zoom">
                                                <img src="images/products/pic-2.jpg" alt="">
                                                <div class="overlay-bx">
                                                    <div class="overlay-icon">
                                                        <a href="javascript:void(0);">
                                                            <i class="fa fa-cart-plus wt-icon-box-xs"></i>
                                                        </a>
                                                        <a class="mfp-link" href="javascript:void(0);">
                                                            <i class="fa fa-heart wt-icon-box-xs"></i>
                                                        </a>
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="wt-info  text-center">
                                                 <div class="p-a10 bg-white">
                                                    <h4 class="wt-title">
                                                        <a href="javascript:;">Two Product </a>
                                                    </h4>
                                                    <span class="price">
                                                        <del>
                                                             <span><span class="Price-currencySymbol">£</span>3.00</span>
                                                        </del> 
                                                        <ins>
                                                            <span><span class="Price-currencySymbol">£</span>7.00</span>
                                                        </ins>
                                                    </span>
                                                    <div class="rating-bx">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="p-t10">
                                                        <button class="site-button button-sm font-weight-600" type="button">ADD TO CART</button>
                                                    </div>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- COLUMNS 3 -->
                                    <div class="col-md-4 col-sm-4 col-xs-6 col-xs-100pc m-b30">
                                        <div class="wt-box wt-product-box p-a10 bg-gray">
                                            <div class="wt-thum-bx wt-img-overlay1 wt-img-effect zoom">
                                                <img src="images/products/pic-3.jpg" alt="">
                                                <div class="overlay-bx">
                                                    <div class="overlay-icon">
                                                        <a href="javascript:void(0);">
                                                            <i class="fa fa-cart-plus wt-icon-box-xs"></i>
                                                        </a>
                                                        <a class="mfp-link" href="javascript:void(0);">
                                                            <i class="fa fa-heart wt-icon-box-xs"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wt-info  text-center">
                                                 <div class="p-a10 bg-white">
                                                    <h4 class="wt-title">
                                                        <a href="javascript:;">Three Product</a>
                                                    </h4>
                                                    <span class="price">
                                                        <del>
                                                             <span><span class="Price-currencySymbol">£</span>3.00</span>
                                                        </del> 
                                                        <ins>
                                                            <span><span class="Price-currencySymbol">£</span>10.00</span>
                                                        </ins>
                                                    </span>
                                                    <div class="rating-bx">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="p-t10">
                                                        <button class="site-button button-sm font-weight-600" type="button">ADD TO CART</button>
                                                    </div>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- COLUMNS 4 -->
                                    <div class="col-md-4 col-sm-4 col-xs-6 col-xs-100pc m-b30">
                                        <div class="wt-box wt-product-box p-a10 bg-gray">
                                            <div class="wt-thum-bx wt-img-overlay1 wt-img-effect zoom">
                                                <img src="images/products/pic-4.jpg" alt="">
                                                <div class="overlay-bx">
                                                    <div class="overlay-icon">
                                                        <a href="javascript:void(0);">
                                                            <i class="fa fa-cart-plus wt-icon-box-xs"></i>
                                                        </a>
                                                        <a class="mfp-link" href="javascript:void(0);">
                                                            <i class="fa fa-heart wt-icon-box-xs"></i>
                                                        </a>
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="wt-info  text-center">
                                                 <div class="p-a10 bg-white">
                                                    <h4 class="wt-title">
                                                        <a href="javascript:;">Four Product</a>
                                                    </h4>
                                                    <span class="price">
                                                        <del>
                                                             <span><span class="Price-currencySymbol">£</span>3.00</span>
                                                        </del> 
                                                        <ins>
                                                            <span><span class="Price-currencySymbol">£</span>19.00</span>
                                                        </ins>
                                                    </span>
                                                    <div class="rating-bx">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="p-t10">
                                                        <button class="site-button button-sm font-weight-600" type="button">ADD TO CART</button>
                                                    </div>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- COLUMNS 5 -->
                                    <div class="col-md-4 col-sm-4 col-xs-6 col-xs-100pc m-b30">
                                        <div class="wt-box wt-product-box p-a10 bg-gray">
                                            <div class="wt-thum-bx wt-img-overlay1 wt-img-effect zoom">
                                                <img src="images/products/pic-5.jpg" alt="">
                                                <div class="overlay-bx">
                                                    <div class="overlay-icon">
                                                        <a href="javascript:void(0);">
                                                            <i class="fa fa-cart-plus wt-icon-box-xs"></i>
                                                        </a>
                                                        <a class="mfp-link" href="javascript:void(0);">
                                                            <i class="fa fa-heart wt-icon-box-xs"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wt-info  text-center">
                                                 <div class="p-a10 bg-white">
                                                    <h4 class="wt-title">
                                                        <a href="javascript:;">Five Product</a>
                                                    </h4>
                                                    <span class="price">
                                                        <del>
                                                             <span><span class="Price-currencySymbol">£</span>3.00</span>
                                                        </del> 
                                                        <ins>
                                                            <span><span class="Price-currencySymbol">£</span>22.00</span>
                                                        </ins>
                                                    </span>
                                                    <div class="rating-bx">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="p-t10">
                                                        <button class="site-button button-sm font-weight-600" type="button">ADD TO CART</button>
                                                    </div>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- COLUMNS 6 -->
                                    <div class="col-md-4 col-sm-4 col-xs-6 col-xs-100pc m-b30">
                                        <div class="wt-box wt-product-box p-a10 bg-gray">
                                            <div class="wt-thum-bx wt-img-overlay1 wt-img-effect zoom">
                                                <img src="images/products/pic-6.jpg" alt="">
                                                <div class="overlay-bx">
                                                    <div class="overlay-icon">
                                                        <a href="javascript:void(0);">
                                                            <i class="fa fa-cart-plus wt-icon-box-xs"></i>
                                                        </a>
                                                        <a class="mfp-link" href="javascript:void(0);">
                                                            <i class="fa fa-heart wt-icon-box-xs"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wt-info  text-center">
                                                <div class="p-a10 bg-white">
                                                    <h4 class="wt-title">
                                                        <a href="javascript:;">Six Product</a>
                                                    </h4>
                                                    <span class="price">
                                                        <del>
                                                             <span><span class="Price-currencySymbol">£</span>3.00</span>
                                                        </del> 
                                                        <ins>
                                                            <span><span class="Price-currencySymbol">£</span>44.00</span>
                                                        </ins>
                                                    </span>
                                                    <div class="rating-bx">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="p-t10">
                                                        <button class="site-button button-sm font-weight-600" type="button">ADD TO CART</button>
                                                    </div>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- COLUMNS 7 -->
                                    <div class="col-md-4 col-sm-4 col-xs-6 col-xs-100pc m-b30">
                                        <div class="wt-box wt-product-box p-a10 bg-gray">
                                            <div class="wt-thum-bx wt-img-overlay1 wt-img-effect zoom">
                                                <img src="images/products/pic-7.jpg" alt="">
                                                <div class="overlay-bx">
                                                    <div class="overlay-icon">
                                                        <a href="javascript:void(0);">
                                                            <i class="fa fa-cart-plus wt-icon-box-xs"></i>
                                                        </a>
                                                        <a class="mfp-link" href="javascript:void(0);">
                                                            <i class="fa fa-heart wt-icon-box-xs"></i>
                                                        </a>
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="wt-info  text-center">
                                                 <div class="p-a10 bg-white">
                                                    <h4 class="wt-title">
                                                        <a href="javascript:;">Seven Product</a>
                                                    </h4>
                                                    <span class="price">
                                                        <del>
                                                             <span><span class="Price-currencySymbol">£</span>3.00</span>
                                                        </del> 
                                                        <ins>
                                                            <span><span class="Price-currencySymbol">£</span>56.00</span>
                                                        </ins>
                                                    </span>
                                                    <div class="rating-bx">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="p-t10">
                                                        <button class="site-button button-sm font-weight-600" type="button">ADD TO CART</button>
                                                    </div>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- COLUMNS 8 -->
                                    <div class="col-md-4 col-sm-4 col-xs-6 col-xs-100pc m-b30">
                                        <div class="wt-box wt-product-box p-a10 bg-gray">
                                            <div class="wt-thum-bx wt-img-overlay1 wt-img-effect zoom">
                                                <img src="images/products/pic-8.jpg" alt="">
                                                <div class="overlay-bx">
                                                    <div class="overlay-icon">
                                                        <a href="javascript:void(0);">
                                                            <i class="fa fa-cart-plus wt-icon-box-xs"></i>
                                                        </a>
                                                        <a class="mfp-link" href="javascript:void(0);">
                                                            <i class="fa fa-heart wt-icon-box-xs"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wt-info  text-center">
                                                 <div class="p-a10 bg-white">
                                                    <h4 class="wt-title">
                                                        <a href="javascript:;">Eight Product</a>
                                                    </h4>
                                                    <span class="price">
                                                        <del>
                                                             <span><span class="Price-currencySymbol">£</span>3.00</span>
                                                        </del> 
                                                        <ins>
                                                            <span><span class="Price-currencySymbol">£</span>2.00</span>
                                                        </ins>
                                                    </span>
                                                    <div class="rating-bx">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="p-t10">
                                                        <button class="site-button button-sm font-weight-600" type="button">ADD TO CART</button>
                                                    </div>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- COLUMNS 9 -->
                                    <div class="col-md-4 col-sm-4 col-xs-6 col-xs-100pc m-b30">
                                        <div class="wt-box wt-product-box p-a10 bg-gray">
                                            <div class="wt-thum-bx wt-img-overlay1 wt-img-effect zoom">
                                                <img src="images/products/pic-9.jpg" alt="">
                                                <div class="overlay-bx">
                                                    <div class="overlay-icon">
                                                        <a href="javascript:void(0);">
                                                            <i class="fa fa-cart-plus wt-icon-box-xs"></i>
                                                        </a>
                                                        <a class="mfp-link" href="javascript:void(0);">
                                                            <i class="fa fa-heart wt-icon-box-xs"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wt-info  text-center">
                                                 <div class="p-a10 bg-white">
                                                    <h4 class="wt-title">
                                                        <a href="javascript:;">Nine Product</a>
                                                    </h4>
                                                    <span class="price">
                                                        <del>
                                                             <span><span class="Price-currencySymbol">£</span>3.00</span>
                                                        </del> 
                                                        <ins>
                                                            <span><span class="Price-currencySymbol">£</span>2.00</span>
                                                        </ins>
                                                    </span>
                                                    <div class="rating-bx">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="p-t10">
                                                        <button class="site-button button-sm font-weight-600" type="button">ADD TO CART</button>
                                                    </div>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                                
                                <!-- ADD BLOCK -->
                                <div class="p-tb30">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="wt-box pro-banner">
                                                <img src="images/add/pic1.jpg" alt="">
                                                <div class="pro-banner-disc p-a10 text-white">
                                                    <h2 class="text-uppercase m-a0 m-b10">Best time to buy</h2>
                                                    <h4 class="m-a0 m-b10">Our Product</h4>
                                                    <h3 class="text-uppercase m-a0 m-b10">UP TO</h3>
                                                    <h5 class="text-uppercase m-a0 m-b10">10% Cashback</h5>
                                                    <a href="#" class="site-button button-sm ">ADD TO CART </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="wt-box pro-banner">
                                                <img src="images/add/pic2.jpg" alt="">
                                                <div class="pro-banner-disc p-a10 text-white">
                                                    <h2 class="text-uppercase m-a0 m-b10">Best time to buy</h2>
                                                    <h4 class="m-a0 m-b10">Two Product</h4>
                                                    <h3 class="text-uppercase m-a0 m-b10">UP TO</h3>
                                                    <h5 class="text-uppercase m-a0 m-b10">40% Cashback</h5>
                                                    <a href="#" class="site-button button-sm">ADD TO CART</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ADD BLOCK -->
                                
                            </div>                        
                            <!-- SIDE BAR START -->
                            <div class="col-md-3">
                            
                                <aside  class="side-bar">
                                    
                                        <!-- 13. SEARCH -->
                                        <div class="widget bg-white ">
                                            <h4 class="widget-title">Search</h4>
                                            <div class="search-bx">
                                                <form role="search" method="post">
                                                    <div class="input-group">
                                                        <input name="news-letter" type="text" class="form-control" placeholder="Write your text">
                                                        <span class="input-group-btn">
                                                            <button type="submit" class="site-button"><i class="fa fa-search"></i></button>
                                                        </span>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- 7. OUR CLIENT -->
                                        <div class="widget">
                                            <h4 class="widget-title">Our Client</h4>
                                            <div class="owl-carousel widget-client p-t10">
                                            
                                                <!-- COLUMNS 1 --> 
                                                <div class="item">
                                                    <div class="ow-client-logo">
                                                        <div class="client-logo wt-img-effect on-color">
                                                            <a href="#"><img src="images/client-logo/logo1.png" alt=""></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- COLUMNS 2 --> 
                                                <div class="item">
                                                    <div class="ow-client-logo">
                                                        <div class="client-logo wt-img-effect on-color">
                                                            <a href="#"><img src="images/client-logo/logo2.png" alt=""></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- COLUMNS 3 --> 
                                                <div class="item">
                                                    <div class="ow-client-logo">
                                                        <div class="client-logo wt-img-effect on-color">
                                                            <a href="#"><img src="images/client-logo/logo3.png" alt=""></a>
                                                        </div>
                                                    </div>
                                                </div>
                              
                                            </div>
                                        </div>                                                                        
                                        <!-- 4. OUR GALLERY  -->
                                        <div class="widget widget_gallery mfp-gallery">
                                            <h4 class="widget-title">Our Gallery</h4>
                                            <ul>
                                                <li>
                                                    <div class="wt-post-thum">
                                                        <a href="images/gallery/pic1.jpg" class="mfp-link" ><img src="images/gallery/thumb/pic1.jpg" alt=""></a>
                                                    </div>
                                                </li>
                                                
                                                <li>
                                                    <div class="wt-post-thum ">
                                                        <a href="images/gallery/pic2.jpg" class="mfp-link"><img src="images/gallery/thumb/pic2.jpg" alt=""></a>
                                                    </div>
                                                </li>
                                                
                                                <li>
                                                    <div class="wt-post-thum  ">
                                                        <a href="images/gallery/pic3.jpg" class="mfp-link"><img src="images/gallery/thumb/pic3.jpg" alt=""></a>
                                                    </div>
                                                </li>
                                                
                                                <li>
                                                    <div class="wt-post-thum ">
                                                        <a href="images/gallery/pic4.jpg" class="mfp-link"><img src="images/gallery/thumb/pic4.jpg" alt=""></a>
                                                    </div>
                                                </li>
                                                
                                                <li>
                                                    <div class="wt-post-thum ">
                                                        <a href="images/gallery/pic5.jpg" class="mfp-link"><img src="images/gallery/thumb/pic5.jpg" alt=""></a>
                                                    </div>
                                                </li>
                                                
                                                <li>
                                                    <div class="wt-post-thum ">
                                                        <a href="images/gallery/pic6.jpg" class="mfp-link"><img src="images/gallery/thumb/pic6.jpg" alt=""></a>
                                                    </div>
                                                </li>
                                                
                                                <li>
                                                    <div class="wt-post-thum">
                                                        <a href="images/gallery/pic7.jpg" class="mfp-link" ><img src="images/gallery/thumb/pic7.jpg" alt=""></a>
                                                    </div>
                                                </li>
                                                
                                                <li>
                                                    <div class="wt-post-thum ">
                                                        <a href="images/gallery/pic8.jpg" class="mfp-link"><img src="images/gallery/thumb/pic8.jpg" alt=""></a>
                                                    </div>
                                                </li>
                                                
                                                <li>
                                                    <div class="wt-post-thum  ">
                                                        <a href="images/gallery/pic7.jpg" class="mfp-link"><img src="images/gallery/thumb/pic7.jpg" alt=""></a>
                                                    </div>
                                                </li>
                                                
                                                <li>
                                                    <div class="wt-post-thum ">
                                                        <a href="images/gallery/pic6.jpg" class="mfp-link"><img src="images/gallery/thumb/pic6.jpg" alt=""></a>
                                                    </div>
                                                </li>
                                                
                                                <li>
                                                    <div class="wt-post-thum ">
                                                        <a href="images/gallery/pic5.jpg" class="mfp-link"><img src="images/gallery/thumb/pic5.jpg" alt=""></a>
                                                    </div>
                                                </li>
                                                
                                                <li>
                                                    <div class="wt-post-thum ">
                                                        <a href="images/gallery/pic4.jpg" class="mfp-link"><img src="images/gallery/thumb/pic4.jpg" alt=""></a>
                                                    </div>
                                                </li>
                                            </ul>
                                           
                                        </div> 
                                         <!-- 6. NEWSLETTER -->
                                        <div class="widget widget_newsletter-2 bg-white  ">
                                            <h4 class="widget-title">Newsletter</h4>
                                            <div class="newsletter-bx p-a30">
                                                <div class="newsletter-icon">
                                                    <i class="fa fa-envelope-o"></i>
                                                </div>
                                                
                                                <div class="newsletter-content">
                                                    <i>Enter your e-mail and subscribe to our newsletter. Sit amet of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis</i>
                                                 </div>
                                                 <div class="m-t20">
                                                    <form role="search" method="post">
                                                    <div class="input-group">
                                                        <input name="news-letter" class="form-control" placeholder="ENTER YOUR EMAIL" type="text">
                                                        <span class="input-group-btn">
                                                            <button type="submit" class="site-button"><i class="fa fa-paper-plane-o"></i></button>
                                                        </span>
                                                    </div>
                                                </form>
                                                </div>
                                            </div>
                                        </div> 
                                        <!-- 2. RECENT POSTS -->
                                        <div class="widget bg-white  recent-posts-entry">
                                            <h4 class="widget-title">Posts</h4>
                                            <div class="section-content">
                                                <div class="wt-tabs tabs-default border">
                                                    <ul class="nav nav-tabs">
                                                        <li class="active"><a data-toggle="tab" href="#web-design-1">Recent</a></li>
                                                        <li><a data-toggle="tab" href="#graphic-design-1">Popular</a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                    
                                                        <div id="web-design-1" class="tab-pane active ">
                                                            <div class="widget-post-bx">
                                                                <div class="widget-post clearfix bg-gray">
                                                                    <div class="wt-post-media">
                                                                        <img src="images/blog/recent-blog/pic1.jpg"  alt="" class="radius-bx">
                                                                    </div>
                                                                    <div class="wt-post-info">
                                                                        <div class="wt-post-header">
                                                                            <h6 class="post-title">Lorem ipsum dolor sit amet</h6>
                                                                        </div>
                                                                        <div class="wt-post-meta">
                                                                            <ul>
                                                                                <li class="post-author">25 Dec</li>
                                                                                <li class="post-comment"><i class="fa fa-comments"></i> 20comment</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="widget-post clearfix bg-gray">
                                                                    <div class="wt-post-media">
                                                                        <img src="images/blog/recent-blog/pic2.jpg" alt=""  class="radius-bx">
                                                                    </div>
                                                                    <div class="wt-post-info">
                                                                        <div class="wt-post-header">
                                                                            <h6 class="post-title">Debitis nihil placeat, illum est </h6>
                                                                        </div>
                                                                        <div class="wt-post-meta">
                                                                            <ul>
                                                                                <li class="post-author">25 Dec</li>
                                                                                <li class="post-comment"><i class="fa fa-comments"></i> 15 comment</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="widget-post clearfix bg-gray">
                                                                    <div class="wt-post-media">
                                                                        <img src="images/blog/recent-blog/pic3.jpg" alt=""  class="radius-bx">
                                                                    </div>
                                                                    <div class="wt-post-info">
                                                                        <div class="wt-post-header">
                                                                            <h6 class="post-title">Elit Assumenda vel amet </h6>
                                                                        </div>
                                                                        <div class="wt-post-meta">
                                                                            <ul>
                                                                                <li class="post-author">25 Dec</li>
                                                                                <li class="post-comment"><i class="fa fa-comments"></i> 07 comment</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div id="graphic-design-1" class="tab-pane">
                                                            <div class="widget-post-bx">
                                                                <div class="widget-post clearfix bg-gray">
                                                                    <div class="wt-post-media">
                                                                        <img src="images/blog/recent-blog/pic3.jpg" alt=""  class="radius-bx">
                                                                    </div>
                                                                    <div class="wt-post-info">
                                                                        <div class="wt-post-header">
                                                                            <h6 class="post-title">Blog post title doler sit amet</h6>
                                                                        </div>
                                                                        <div class="wt-post-meta">
                                                                            <ul>
                                                                                <li class="post-author">25 Dec</li>
                                                                                <li class="post-comment"><i class="fa fa-comments"></i> 10 comment</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="widget-post clearfix bg-gray">
                                                                    <div class="wt-post-media">
                                                                        <img src="images/blog/recent-blog/pic1.jpg"  alt=""  class="radius-bx">
                                                                    </div>
                                                                    <div class="wt-post-info">
                                                                        <div class="wt-post-header">
                                                                            <h6 class="post-title">Blog post title lorem ipsum</h6>
                                                                        </div>
                                                                        <div class="wt-post-meta">
                                                                            <ul>
                                                                                <li class="post-author">25 Dec</li>
                                                                                <li class="post-comment"><i class="fa fa-comments"></i> 29 comment</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="widget-post clearfix bg-gray">
                                                                    <div class="wt-post-media">
                                                                        <img src="images/blog/recent-blog/pic2.jpg" alt=""  class="radius-bx">
                                                                    </div>
                                                                    <div class="wt-post-info">
                                                                        <div class="wt-post-header">
                                                                            <h6 class="post-title">Blog post title a dummy text</h6>
                                                                        </div>
                                                                        <div class="wt-post-meta">
                                                                            <ul>
                                                                                <li class="post-author">25 Dec</li>
                                                                                <li class="post-comment"><i class="fa fa-comments"></i> 05 comment</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 12. TAGS -->
                                        <div class="widget bg-white  widget_tag_cloud">
                                            <h4 class="widget-title">Tags</h4>
                                            <div class="tagcloud">
                                                <a href="javascript:void(0);">Trouble </a>
                                                <a href="javascript:void(0);">Programmers</a>
                                                <a href="javascript:void(0);">Never</a>
                                                <a href="javascript:void(0);">Tell</a>
                                                <a href="javascript:void(0);">Doing</a>
                                                <a href="javascript:void(0);">Person</a>
                                                <a href="javascript:void(0);">Inventors Tag</a>
                                                <a href="javascript:void(0);">Between </a>
                                                <a href="javascript:void(0);">Abilities</a>
                                                <a href="javascript:void(0);">Fault </a>
                                                <a href="javascript:void(0);">Gets </a>
                                                <a href="javascript:void(0);">Macho</a>
                                            </div>
                                        </div> 
                                    
                                   </aside>
        
                            </div>

                       </div> 
                   </div>
                 </div>
             </div>
             <!-- SECTION CONTENT END -->
        
        </div>
        <!-- CONTENT END -->
        
        <!-- FOOTER START -->
        <footer class="site-footer bg-no-repeat bg-full-height bg-center"  style="background-image:url(images/background/footer-bg.jpg);">
            <!-- FOOTER BLOCKES START -->  
            <div class="footer-top overlay-wraper">
                <div class="overlay-main bg-black opacity-05"></div>
                <div class="container">
                    <div class="row">
                        <!-- ABOUT COMPANY -->
                        <div class="col-md-3 col-sm-6">  
                            <div class="widget widget_about">
                                <h4 class="widget-title text-white">About Company</h4>
                                <div class="logo-footer clearfix p-b15">
                                    <a href="index.html"><img src="images/footer-logo.png" width="230" height="67" alt=""/></a>
                                </div>
                                <p>Thewebmax ipsum dolor sit amet, consectetuer adipiscing elit,
                                      sed diam nonummy nibh euismod tincidunt ut laoreet dolore agna aliquam erat .   
                                      wisi enim ad minim veniam, quis tation. sit amet, consec tetuer.
                                      ipsum dolor sit amet, consectetuer adipiscing.ipsum dolor sit .
                                </p>  
                            </div>
                        </div> 
                        <!-- RESENT POST -->
                        <div class="col-md-3 col-sm-6">
                            <div class="widget recent-posts-entry-date">
                                <h4 class="widget-title text-white">Resent Post</h4>
                                <div class="widget-post-bx">
                                    <div class="bdr-light-blue widget-post clearfix  bdr-b-1 m-b10 p-b10">
                                        <div class="wt-post-date text-center text-uppercase text-white p-t5">
                                            <strong>20</strong>
                                            <span>Mar</span>
                                        </div>
                                        <div class="wt-post-info">
                                            <div class="wt-post-header">
                                                <h6 class="post-title"><a href="blog-single.html">Blog title first </a></h6>
                                            </div>
                                            <div class="wt-post-meta">
                                                <ul>
                                                    <li class="post-author"><i class="fa fa-user"></i>By Admin</li>
                                                    <li class="post-comment"><i class="fa fa-comments"></i> 28</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bdr-light-blue widget-post clearfix  bdr-b-1 m-b10 p-b10">
                                        <div class="wt-post-date text-center text-uppercase text-white p-t5">
                                            <strong>30</strong>
                                            <span>Mar</span>
                                        </div>
                                        <div class="wt-post-info">
                                            <div class="wt-post-header">
                                                <h6 class="post-title"><a href="blog-single.html">Blog title first </a></h6>
                                            </div>
                                            <div class="wt-post-meta">
                                                <ul>
                                                    <li class="post-author"><i class="fa fa-user"></i>By Admin</li>
                                                    <li class="post-comment"><i class="fa fa-comments"></i> 29</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bdr-light-blue widget-post clearfix  bdr-b-1 m-b10 p-b10">
                                        <div class="wt-post-date text-center text-uppercase text-white p-t5">
                                            <strong>31</strong>
                                            <span>Mar</span>
                                        </div>
                                        <div class="wt-post-info">
                                            <div class="wt-post-header">
                                                <h6 class="post-title"><a href="blog-single.html">Blog title first </a></h6>
                                            </div>
                                            <div class="wt-post-meta">
                                                <ul>
                                                    <li class="post-author"><i class="fa fa-user"></i>By Admin</li>
                                                    <li class="post-comment"><i class="fa fa-comments"></i> 30</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>      
                        <!-- USEFUL LINKS -->
                        <div class="col-md-3 col-sm-6">
                            <div class="widget widget_services">
                                <h4 class="widget-title text-white">Useful links</h4>
                                <ul>
                                    <li><a href="about-1.html">About</a></li>
                                    <li><a href="faq-1.html">FAQ</a></li>
                                    <li><a href="our-team.html">Our Team</a></li>
                                    <li><a href="services-1.html">Services</a></li>
                                    <li><a href="contact-1.html">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- NEWSLETTER -->
                        <div class="col-md-3 col-sm-6">
                            <div class="widget widget_newsletter">
                                <h4 class="widget-title text-white">Newsletter</h4>
                                <div class="newsletter-bx">
                                    <form role="search" method="post">
                                        <div class="input-group">
                                        <input name="news-letter" class="form-control" placeholder="ENTER YOUR EMAIL" type="text">
                                        <span class="input-group-btn">
                                            <button type="submit" class="site-button"><i class="fa fa-paper-plane-o"></i></button>
                                        </span>
                                    </div>
                                     </form>
                                </div>
                            </div>
                            <!-- SOCIAL LINKS -->
                            <div class="widget widget_social_inks">
                                <h4 class="widget-title text-white">Social Links</h4>
                                <ul class="social-icons social-square social-darkest">
                                    <li><a href="javascript:void(0);" class="fa fa-facebook"></a></li>
                                    <li><a href="javascript:void(0);" class="fa fa-twitter"></a></li>
                                    <li><a href="javascript:void(0);" class="fa fa-linkedin"></a></li>
                                    <li><a href="javascript:void(0);" class="fa fa-rss"></a></li>
                                    <li><a href="javascript:void(0);" class="fa fa-youtube"></a></li>
                                    <li><a href="javascript:void(0);" class="fa fa-instagram"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    
                       <div class="col-md-3 col-sm-6  p-tb20">
                           <div class="wt-icon-box-wraper left  bdr-1 bdr-gray-dark p-tb15 p-lr10 clearfix">
                                <div class="icon-md text-primary">
                                    <span class="iconmoon-travel"></span>
                                </div>
                                <div class="icon-content text-white">
                                    <h5 class="wt-tilte text-uppercase m-b0">Address</h5>
                                    <p>No.123 Chalingt Gates, Supper market New York</p>
                                </div>
                           </div>
                        </div>
                       <div class="col-md-3 col-sm-6  p-tb20 ">
                           <div class="wt-icon-box-wraper left  bdr-1 bdr-gray-dark p-tb15 p-lr10 clearfix ">
                                <div class="icon-md text-primary">
                                    <span class="iconmoon-smartphone-1"></span>
                                </div>
                                <div class="icon-content text-white">
                                    <h5 class="wt-tilte text-uppercase m-b0">Phone</h5>
                                    <p class="m-b0">+41 555 888 9585</p>
                                    <p>+41 555 888 9585</p>
                                </div>
                           </div>
                       </div>
                       <div class="col-md-3 col-sm-6  p-tb20">
                           <div class="wt-icon-box-wraper left  bdr-1 bdr-gray-dark p-tb15 p-lr10 clearfix">
                                <div class="icon-md text-primary">
                                    <span class="iconmoon-fax"></span>
                                </div>
                                <div class="icon-content text-white">
                                    <h5 class="wt-tilte text-uppercase m-b0">Fax</h5>
                                    <p class="m-b0">FAX: (123) 123-4567</p>
                                    <p>FAX: (123) 123-4567</p>
                                </div>
                            </div>
                        </div>
                       <div class="col-md-3 col-sm-6 p-tb20">
                           <div class="wt-icon-box-wraper left  bdr-1 bdr-gray-dark p-tb15 p-lr10 clearfix">
                                <div class="icon-md text-primary">
                                    <span class="iconmoon-email"></span>
                                </div>
                                <div class="icon-content text-white">
                                    <h5 class="wt-tilte text-uppercase m-b0">Email</h5>
                                    <p class="m-b0">info@demo.com</p>
                                    <p>info@demo1234.com</p>
                                </div>
                            </div>
                        </div>

                  </div>
                </div>
            </div>
            <!-- FOOTER COPYRIGHT -->
            <div class="footer-bottom  overlay-wraper">
                <div class="overlay-main"></div>
                <div class="constrot-strip"></div>
                <div class="container p-t30">
                    <div class="row">
                        <div class="wt-footer-bot-left">
                            <span class="copyrights-text">© 2018. All Rights Reserved.</span>
                        </div>
                        <div class="wt-footer-bot-right">
                            <ul class="copyrights-nav pull-right"> 
                                <li><a href="javascript:void(0);">Terms  & Condition</a></li>
                                <li><a href="javascript:void(0);">Privacy Policy</a></li>
                                <li><a href="contact-1.html">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- FOOTER END -->

        <!-- BUTTON TOP START -->
        <button class="scroltop"><span class=" iconmoon-house relative" id="btn-vibrate"></span>Top</button>

        <!-- MODAL  LOGIN -->
        <div id="Login-form" class="modal fade " role="dialog">
          <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-white">Login Your Account</h4>
              </div>
              <div class="modal-body p-a30">
                <form id="log-form">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input class="form-control" placeholder="Enter Username" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input class="form-control" placeholder="Enter email" type="email">
                        </div>
                    </div>
                    <button type="button" class="site-button-secondry text-uppercase btn-block m-b10">Submit</button>
                    <!--<span class="font-12">Don't have an account? <a href="javascript:;" class="text-primary">Register Here</a></span>-->
                </form>
              </div>
              <div class="modal-footer text-center">
                <div class="text-center"><img src="images/logo.png" alt=""></div>
              </div>
            </div>
          </div>
        </div>
        
        <!-- MODAL  REGISTER -->
        <div id="Register-form" class="modal fade " role="dialog">
          <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-white">Register here</h4>
              </div>
              <div class="modal-body p-a30">
                <form id="reg-form">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input class="form-control" placeholder="Enter Username" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input class="form-control" placeholder="Enter email" type="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-key"></i></span>
                            <input class="form-control" placeholder="Enter Password" type="email">
                        </div>
                    </div>
                    <button type="button" class="site-button-secondry text-uppercase btn-block m-b10">Submit</button>
                    <!--<span class="font-12">Already Have an Account? <a href="javascript:;" class="text-primary">Login</a></span>-->
                </form>
              </div>
              <div class="modal-footer text-center">
                <div class="text-center"><img src="images/logo.png" alt=""></div>
              </div>
            </div>
          </div>
        </div> 
        
    </div>
    

<!-- JAVASCRIPT  FILES ========================================= --> 
<script   src="js/jquery-1.12.4.min.js"></script><!-- JQUERY.MIN JS -->
<script   src="js/bootstrap.min.js"></script><!-- BOOTSTRAP.MIN JS -->

<script   src="js/bootstrap-select.min.js"></script><!-- FORM JS -->
<script   src="js/jquery.bootstrap-touchspin.min.js"></script><!-- FORM JS -->

<script   src="js/magnific-popup.min.js"></script><!-- MAGNIFIC-POPUP JS -->

<script   src="js/waypoints.min.js"></script><!-- WAYPOINTS JS -->
<script   src="js/counterup.min.js"></script><!-- COUNTERUP JS -->
<script   src="js/waypoints-sticky.min.js"></script><!-- COUNTERUP JS -->

<script  src="js/isotope.pkgd.min.js"></script><!-- MASONRY  -->

<script   src="js/owl.carousel.min.js"></script><!-- OWL  SLIDER  -->

<script   src="js/stellar.min.js"></script><!-- PARALLAX BG IMAGE   --> 
<script   src="js/scrolla.min.js"></script><!-- ON SCROLL CONTENT ANIMTE   --> 

<script   src="js/custom.js"></script><!-- CUSTOM FUCTIONS  -->
<script   src="js/shortcode.js"></script><!-- SHORTCODE FUCTIONS  -->
<script   src="js/switcher.js"></script><!-- SWITCHER FUCTIONS  -->
<script  src="js/jquery.bgscroll.js"></script><!-- BACKGROUND SCROLL -->
<script  src="js/tickerNews.min.js"></script><!-- TICKERNEWS-->



<!-- LOADING AREA START ===== -->
<div class="loading-area">
    <div class="loading-box"></div>
    <div class="loading-pic">
        <div class="cssload-container">
            <div class="cssload-dot bg-primary"><i class="fa fa-bitcoin"></i></div>
            <div class="step" id="cssload-s1"></div>
            <div class="step" id="cssload-s2"></div>
            <div class="step" id="cssload-s3"></div>
        </div>
    </div>
</div>
<!-- LOADING AREA  END ====== -->

<!-- STYLE SWITCHER  ======= --> 
<div class="styleswitcher">

    <div class="switcher-btn-bx">
        <a class="switch-btn">
            <span class="fa fa-cog fa-spin"></span>
        </a>
    </div>
    
    <div class="styleswitcher-inner">
    
        <h6 class="switcher-title">Color Skin</h6>
        <ul class="color-skins">
            <li><a class="theme-skin skin-1" href="producta39b.html?theme=css/skin/skin-1" title="default Theme"></a></li>
            <li><a class="theme-skin skin-2" href="product61e7.html?theme=css/skin/skin-2" title="pink Theme"></a></li>
            <li><a class="theme-skin skin-3" href="productcce5.html?theme=css/skin/skin-3" title="sky Theme"></a></li>
            <li><a class="theme-skin skin-4" href="product13f7.html?theme=css/skin/skin-4" title="green Theme"></a></li>
            <li><a class="theme-skin skin-5" href="product19a6.html?theme=css/skin/skin-5" title="red Theme"></a></li>
            <li><a class="theme-skin skin-6" href="productfe5c.html?theme=css/skin/skin-6" title="orange Theme"></a></li>
            <li><a class="theme-skin skin-7" href="productab47.html?theme=css/skin/skin-7" title="purple Theme"></a></li>
            <li><a class="theme-skin skin-8" href="product5f8d.html?theme=css/skin/skin-8" title="blue Theme"></a></li>
            <li><a class="theme-skin skin-9" href="product5663.html?theme=css/skin/skin-9" title="gray Theme"></a></li>
            <li><a class="theme-skin skin-10" href="product28ac.html?theme=css/skin/skin-10" title="brown Theme"></a></li>
            <li><a class="theme-skin skin-11" href="product26b8.html?theme=css/skin/skin-11" title="gray Theme"></a></li>
            <li><a class="theme-skin skin-12" href="product7f4c.html?theme=css/skin/skin-12" title="golden Theme"></a></li>
        </ul>   
        
        <h6 class="switcher-title">Nav</h6>
        <ul class="nav-view">
            <li class="nav-light active">Light</li>
            <li class="nav-dark">Dark</li>
        </ul>
        
        <h6 class="switcher-title">Nav</h6>
        <ul class="nav-width">
            <li class="nav-boxed active">Boxed</li>
            <li class="nav-wide">Wide</li>
        </ul>   
        
        <h6 class="switcher-title">Header</h6>
        <ul class="header-view">
            <li class="header-fixed active">Fixed</li>
            <li class="header-static">Static</li>
        </ul> 
        
        <h6 class="switcher-title">Layout</h6>
        <ul class="layout-view">
            <li class="wide-layout active">Wide</li>
            <li class="boxed">Boxed</li>
        </ul>   
                    
        <h6 class="switcher-title">Background Image</h6>
        <ul class="background-switcher">
            <li><img src="images/switcher/switcher-bg/thum/bg1.jpg" rel="images/switcher/switcher-bg/large/bg1.jpg" alt=""></li>
            <li><img src="images/switcher/switcher-bg/thum/bg2.jpg" rel="images/switcher/switcher-bg/large/bg2.jpg"  alt=""></li>
            <li><img src="images/switcher/switcher-bg/thum/bg3.jpg" rel="images/switcher/switcher-bg/large/bg3.jpg"  alt=""></li>
            <li><img src="images/switcher/switcher-bg/thum/bg4.jpg" rel="images/switcher/switcher-bg/large/bg4.jpg"  alt=""></li>
        </ul>
        
        <h6 class="switcher-title">Background Pattern</h6>
        <ul class="pattern-switcher">
            <li><img src="images/switcher/switcher-patterns/thum/bg1.jpg"  rel="images/switcher/switcher-patterns/large/pt1.jpg" alt=""></li>
            <li><img src="images/switcher/switcher-patterns/thum/bg2.jpg"  rel="images/switcher/switcher-patterns/large/pt2.jpg" alt=""></li>
            <li><img src="images/switcher/switcher-patterns/thum/bg3.jpg"  rel="images/switcher/switcher-patterns/large/pt3.jpg" alt=""></li>
            <li><img src="images/switcher/switcher-patterns/thum/bg4.jpg"  rel="images/switcher/switcher-patterns/large/pt4.jpg" alt=""></li>
            <li><img src="images/switcher/switcher-patterns/thum/bg5.jpg"  rel="images/switcher/switcher-patterns/large/pt5.jpg" alt=""></li>
            <li><img src="images/switcher/switcher-patterns/thum/bg6.jpg"  rel="images/switcher/switcher-patterns/large/pt6.jpg" alt=""></li>
            <li><img src="images/switcher/switcher-patterns/thum/bg7.jpg"  rel="images/switcher/switcher-patterns/large/pt7.jpg" alt=""></li>
            <li><img src="images/switcher/switcher-patterns/thum/bg8.jpg"  rel="images/switcher/switcher-patterns/large/pt8.jpg" alt=""></li>
            <li><img src="images/switcher/switcher-patterns/thum/bg9.jpg"  rel="images/switcher/switcher-patterns/large/pt9.jpg" alt=""></li>
            <li><img src="images/switcher/switcher-patterns/thum/bg10.jpg"  rel="images/switcher/switcher-patterns/large/pt10.jpg" alt=""></li>
            <li><img src="images/switcher/switcher-patterns/thum/bg11.jpg"  rel="images/switcher/switcher-patterns/large/pt11.jpg" alt=""></li>
            <li><img src="images/switcher/switcher-patterns/thum/bg12.jpg"  rel="images/switcher/switcher-patterns/large/pt12.jpg" alt=""></li>
        </ul>
        
        
    </div>    
</div>
<!-- STYLE SWITCHER END ==== -->


 
  


 


</body>


