<section class="section lb page-title1 little-pad" style="margin-top: 79px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-big-title clearfix text-center">
                	                    <div class="title-banner-bred">
                        <div class="title-subtitle-content">
                            <h3>NurseryPlant Gardening</h3>
                            <p>A basic standard blog page example.</p>
                        </div>                    
                                                    <div class="breadcrumb-content">
                                <ul class="breadcrumb">
			<li><a href="/">Home</a></li><li><a href="../../../../category/garden/index.html">Garden</a>  </li><li class="active">Gardening</li>
		</ul>                            </div>
                                            </div>
                                    </div>
            </div>
        </div>
    </div>
</section>


 <section class="section main-container">

<div class="container">    
    <div class="row">    
    	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        	<div class="content">
        
		
				 <div id="post-179" class="post-179 post type-post status-publish format-standard has-post-thumbnail hentry category-garden tag-buy tag-featured tag-new tag-plant tag-sale tag-shop tag-store">
		<div class="blog-item">
    	                <div class="post-media">
        	        	<div class="entry has-thumb">
        	 <div class="date-container">
             	                07 Apr 2017                            </div><!-- end magnifier -->
                        <img src="images/blog44-900x560.jpg" alt="blog44">                        </div>
        </div><!-- end media -->
                <div class="blog-meta">
            <ul class="list-inline blog-meta-list">
                <li><a href="../../../../author/nursery/index.html"><i class="fa fa-user"></i> nursery</a></li>
                <li><a href="index.html#respond"><i class="fa fa-comments-o"></i> 0</a></li>
                                <li><a href="index.html"><i class="fa fa-eye"></i> 292</a></li>
                                
                <li><i class="fa fa-folder-open-o"></i> <a href="../../../../category/garden/index.html" rel="category tag">Garden</a></li>
            </ul><!-- end inline -->
                        	<h4>Gardening</h4>
                                            
                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
<p><span id="more-179"></span></p>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
<blockquote><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p></blockquote>
<p><a href="#"><img class="size-medium wp-image-636 alignleft" src="images/gal6-300x200.jpg" alt="" width="300" height="200" srcset="http://theme-stall.com/nurseryplant/demos/wp-content/uploads/2017/05/gal6-300x200.jpg 300w, http://theme-stall.com/nurseryplant/demos/wp-content/uploads/2017/05/gal6.jpg 500w" sizes="(max-width: 300px) 100vw, 300px" /></a> Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. <a href="#"><img class="alignright size-medium wp-image-637" src="images/blog5-300x169.jpg" alt="" width="300" height="169" srcset="http://theme-stall.com/nurseryplant/demos/wp-content/uploads/2017/05/blog5-300x169.jpg 300w, http://theme-stall.com/nurseryplant/demos/wp-content/uploads/2017/05/blog5-768x432.jpg 768w, http://theme-stall.com/nurseryplant/demos/wp-content/uploads/2017/05/blog5.jpg 1000w" sizes="(max-width: 300px) 100vw, 300px" /></a></p>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
<p>&nbsp;</p>
<div class="post-views post-179 entry-meta">
			<span class="post-views-icon dashicons dashicons-chart-bar"></span>
			<span class="post-views-label">Post Views: </span>
			<span class="post-views-count">292</span>
			</div>                        
                    </div><!-- end blog-meta -->
                					
				<div class="blog-tags">				
					<ul class="list-inline cat_list"><li><a href="../../../../tag/buy/index.html">buy</a></li><li><a href="../../../../tag/featured/index.html">featured</a></li><li><a href="../../../../tag/new/index.html">new</a></li><li><a href="../../../../tag/plant/index.html">plant</a></li><li><a href="../../../../tag/sale/index.html">sale</a></li><li><a href="../../../../tag/shop/index.html">shop</a></li><li><a href="../../../../tag/store/index.html">store</a></li></ul>						
				</div>						
			            <hr>
            	<ul class="list-inline social">
		<li class="facebook">
		   <!--fb-->
		   <a target="_blank" href="http://www.facebook.com/share.php?u=http://theme-stall.com/nurseryplant/demos/blog/2017/04/07/gardening/&amp;title=Gardening" title="Share this post on Facebook!"><i class="fa fa-facebook"></i></a>
		</li>
		<li class="twitter">
		   <!--twitter-->
		   <a target="_blank" href="http://twitter.com/home?status=Gardening:http://theme-stall.com/nurseryplant/demos/blog/2017/04/07/gardening/" title="Share this post on Twitter!"><i class="fa fa-twitter"></i></a>
		</li>
		<li class="google-plus">
		   <!--g+-->
		   <a target="_blank" href="https://plus.google.com/share?url=http://theme-stall.com/nurseryplant/demos/blog/2017/04/07/gardening/" title="Share this post on Google Plus!"><i class="fa fa-google-plus"></i></a>
		</li>
		<li class="linkedin">
		   <!--linkedin-->
		   <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://theme-stall.com/nurseryplant/demos/blog/2017/04/07/gardening/&amp;title=&amp;source=LinkedIn" title="Share this post on Linkedin!"><i class="fa fa-linkedin"></i></a>
		</li>
		<li class="pinterest">
		   <!--Pinterest-->
		   <a target="_blank" href="http://pinterest.com/pin/create/button/?url=http://theme-stall.com/nurseryplant/demos/blog/2017/04/07/gardening/&amp;media=http://theme-stall.com/nurseryplant/demos/wp-content/uploads/2017/04/blog44.jpg&amp;description=Gardening%20on%20Nursery%20Plant%20http://theme-stall.com/nurseryplant/demos" class="pin-it-button" count-layout="horizontal" title="Share on Pinterest"><i class="fa fa-pinterest"></i></a>
		</li>
		<li class="email-share">
		   <!--Email-->
		   <a title="Share by email" href="mailto:?subject=Check this post - Gardening &body= http://theme-stall.com/nurseryplant/demos/blog/2017/04/07/gardening/&title="Gardening" email"=""><i class="fa fa-envelope"></i></a>
		</li>
	</ul>
	                </div><!-- end blog-item -->
</div><!-- end content -->				 
				     <div class="blog-item next-prev-pagi">
    <div class="row">
        <div class="col-md-12">
            <div class="section-button text-center">
                <div class="absolute-pager">
                    <nav>
										
                            <a class="btn btn-primary previous" href="../new-plant/index.html"><small>Previous</small></a>
                                        						
						<a class="btn btn-primary next" href="../../../05/06/gardening-2/index.html"><small>Next</small></a>
					                    </nav>
                </div><!-- end left -->
            </div><!-- end button -->
        </div><!-- end col -->
    </div><!-- end row -->
    </div>
                     
                 
				
<div class="blog-item">
	
    		

    				<div class="blog-items-new">
					
                
				<div class="commentform row" id="respond">
                
									<form action="http://theme-stall.com/nurseryplant/demos/wp-comments-post.php" method="post" id="commentform" class="contact-form search-top" novalidate>
                    <div class="widget-title col-md-12">
                        <h4>Comment Form                        <a rel="nofollow" id="cancel-comment-reply-link" href="index.html#respond" style="display:none;">Cancel Reply</a></h4>
                    </div>
                    						
					                            <div class="col-md-4 col-sm-12">
		<input id="author" name="author" type="text" placeholder="Name" value=""  aria-required='true' class="form-control" /></div>
<div class="col-md-4 col-sm-12">
		<input id="email" name="email" type="email" value=""  aria-required='true' class="form-control" placeholder="Email" /></div>
<div class="col-md-4 col-sm-12">
		<input id="url" name="url" type="text" value="" class="form-control" placeholder="Website" /></div>
                            <div class="col-md-12 col-sm-12">
	<textarea id="comment" name="comment" aria-required="true" cols="30" rows="5" class="form-control" placeholder="Comment"></textarea></div>						                        
						<div class="col-md-12 col-sm-12"><input class="btn btn-primary" type="submit" id="submit" value="Send Comment" /></div>
                        <input type='hidden' name='comment_post_ID' value='179' id='comment_post_ID' />
<input type='hidden' name='comment_parent' id='comment_parent' value='0' />
											</form>
				                </div>
			
		        </div><!--.widget-->
		
    </div>

				</div>
</div>

	    
    <div id="sidebar" class="col-md-4 col-sm-12 col-xs-12">    
    <div class="sidebar">
		<div class="widget-area">
        	<div id="black-studio-tinymce-1" class="widget clearfix widget_black_studio_tinymce"><div class="widget-title"><h4>New Arrival</h4></div><div class="textwidget"><p><div class="container-invis woocommerce product-carousel-lists">
                    <div class="list-wrapper products clearfix">
                
                <div class="owl-hotels owl-carousel owl-theme owl-custom" data-column="1" data-rtl="false">
                                    <div class="owl-item">
                        <div class="list-item">
                            <a href="../../../../../product/apple/index.html" title="">
                            <div class="entry">
                                                           </div><!-- end entry -->
                            </a>
                            <div class="product-magni-desc">
                                <h4><a href="../../../../../product/apple/index.html" title="">Envy Apple</a></h4>
                                                                <div class="short-description-product">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi&hellip;</div>
                            </div>

                         <!-- end list-desc -->
                        </div><!-- end list-item -->
                    </div><!-- end col -->
                                        <div class="owl-item">
                        <div class="list-item">
                            <a href="../../../../../product/whitelily/index.html" title="">
                            <div class="entry">
                                                         </div><!-- end entry -->
                            </a>
                            <div class="product-magni-desc">
                                <h4><a href="../../../../../product/whitelily/index.html" title="">White Lily</a></h4>
                                                                <div class="short-description-product">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi&hellip;</div>
                            </div>

                         <!-- end list-desc -->
                        </div><!-- end list-item -->
                    </div><!-- end col -->
                                        <div class="owl-item">
                        <div class="list-item">
                            <a href="../../../../../product/strawberry/index.html" title="">
                            <div class="entry">
                                                           </div><!-- end entry -->
                            </a>
                            <div class="product-magni-desc">
                                <h4><a href="../../../../../product/strawberry/index.html" title="">Strawberry</a></h4>
                                                                <div class="short-description-product">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi&hellip;</div>
                            </div>

                         <!-- end list-desc -->
                        </div><!-- end list-item -->
                    </div><!-- end col -->
                        
                </div><!-- end row -->
                <div class="product-section-title clearfix">
                    <h3>Products 
                                        <span class="item-number">Items - 3</span>
                    </h3>
                </div><!-- end section-title -->
            </div><!-- end list-wrapper -->
            </div></p>
</div></div>        <div id="recent-posts-3" class="widget clearfix widget_recent_entries">        <div class="widget-title"><h4>Recent Posts</h4></div>        <div class="recentposts recent">
        <ul class="list-unstyled">
                    <li>
            	                <div class="thumb">
                    <img src="images/blog3-400x400_c.jpg" alt="blog3">                </div><!-- end entry -->
                                
                <div class="text">
                    <h4><a href="../../../05/06/gardening-2/index.html" title="Flower Gardening">Flower Gardening</a></h4>
                                             <h6>06 May 2017</h6>                        
                                    </div>
            </li>
                    <li>
            	                <div class="thumb">
                    <img src="images/blog44-400x400_c.jpg" alt="blog44">                </div><!-- end entry -->
                                
                <div class="text">
                    <h4><a href="index.html" title="Gardening">Gardening</a></h4>
                                             <h6>07 Apr 2017</h6>                        
                                    </div>
            </li>
                    <li>
            	                <div class="thumb">
                    <img src="images/blog22-400x400_c.jpg" alt="blog22">                </div><!-- end entry -->
                                
                <div class="text">
                    <h4><a href="../new-plant/index.html" title="New Plant">New Plant</a></h4>
                                             <h6>07 Apr 2017</h6>                        
                                    </div>
            </li>
                    <li>
            	                <div class="thumb">
                    <img src="/images/blog11-400x400_c.jpg" alt="blog11">                </div><!-- end entry -->
                                
                <div class="text">
                    <h4><a href="../garden-care/index.html" title="Garden Care">Garden Care</a></h4>
                                             <h6>07 Apr 2017</h6>                        
                                    </div>
            </li>
                </ul>
        </div>
        </div><div id="black-studio-tinymce-2" class="widget clearfix widget_black_studio_tinymce"><div class="widget-title"><h4>Discount</h4></div><div class="textwidget"><a href="#"><img class="alignleft wp-image-625 size-full" src="images/siderbar-banner.png" alt="" width="350" height="233" /></a></div></div><div id="tag_cloud-1" class="widget clearfix widget_tag_cloud"><div class="widget-title"><h4>Popular Tags</h4></div><div class="tagcloud"><a href="../../../../tag/book/index.html" class="tag-cloud-link tag-link-18 tag-link-position-1" style="font-size: 8pt;" aria-label="book (1 item)">book</a>
<a href="../../../../tag/buy/index.html" class="tag-cloud-link tag-link-19 tag-link-position-2" style="font-size: 14.3pt;" aria-label="buy (2 items)">buy</a>
<a href="../../../../tag/featured/index.html" class="tag-cloud-link tag-link-44 tag-link-position-3" style="font-size: 8pt;" aria-label="featured (1 item)">featured</a>
<a href="../../../../tag/garden/index.html" class="tag-cloud-link tag-link-20 tag-link-position-4" style="font-size: 14.3pt;" aria-label="garden (2 items)">garden</a>
<a href="../../../../tag/new/index.html" class="tag-cloud-link tag-link-43 tag-link-position-5" style="font-size: 8pt;" aria-label="new (1 item)">new</a>
<a href="../../../../tag/nursery/index.html" class="tag-cloud-link tag-link-21 tag-link-position-6" style="font-size: 18.5pt;" aria-label="nursery (3 items)">nursery</a>
<a href="../../../../tag/plant/index.html" class="tag-cloud-link tag-link-22 tag-link-position-7" style="font-size: 22pt;" aria-label="plant (4 items)">plant</a>
<a href="../../../../tag/sale/index.html" class="tag-cloud-link tag-link-45 tag-link-position-8" style="font-size: 14.3pt;" aria-label="sale (2 items)">sale</a>
<a href="../../../../tag/shop/index.html" class="tag-cloud-link tag-link-23 tag-link-position-9" style="font-size: 18.5pt;" aria-label="shop (3 items)">shop</a>
<a href="../../../../tag/store/index.html" class="tag-cloud-link tag-link-24 tag-link-position-10" style="font-size: 14.3pt;" aria-label="store (2 items)">store</a></div>
</div>        </div>    
    </div>                
</div>	
    
         
    </div>
    
</div>
<!-- Content Wrap -->            

            </section>
