<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Reset key pin';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset-password container">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'reset-master-pin-form']); ?>
            
                <?= $form->field($model, 'master_pin')->passwordInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'master_pin_repeat')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Reset', ['class' => 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>