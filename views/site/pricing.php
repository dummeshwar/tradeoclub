<head>
    <link rel="alternate" type="text/xml+oembed" href="../wp-json/oembed/1.0/embed756b.html?url=http%3A%2F%2Ftheme-stall.com%2Fnurseryplant%2Fdemos%2Fpricing-plan%2F&amp;format=xml" />
	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
					<style type="text/css" media="all"
				       id="siteorigin-panels-layouts-head">/* Layout 530 */ #pgc-530-0-0 , #pgc-530-1-0 , #pgc-530-2-0 { width:100%;width:calc(100% - ( 0 * 30px ) ) } #pg-530-0 , #pg-530-1 , #pg-530-2 , #pl-530 .so-panel:last-child { margin-bottom:0px } #pl-530 .so-panel { margin-bottom:30px } #pg-530-0> .panel-row-style { padding:0px 0px 70px 0px } #pg-530-0.panel-no-style, #pg-530-0.panel-has-style > .panel-row-style , #pg-530-1.panel-no-style, #pg-530-1.panel-has-style > .panel-row-style , #pg-530-2.panel-no-style, #pg-530-2.panel-has-style > .panel-row-style { -webkit-align-items:flex-start;align-items:flex-start } #pg-530-1> .panel-row-style { background-image:url(images/bg5.jpg);background-attachment:fixed;background-position:center center;background-size:cover;padding:70px 0px 70px 0px } #pg-530-2> .panel-row-style { padding:70px 0px 0px 0px } @media (max-width:780px){ #pg-530-0.panel-no-style, #pg-530-0.panel-has-style > .panel-row-style , #pg-530-1.panel-no-style, #pg-530-1.panel-has-style > .panel-row-style , #pg-530-2.panel-no-style, #pg-530-2.panel-has-style > .panel-row-style { -webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column } #pg-530-0 .panel-grid-cell , #pg-530-1 .panel-grid-cell , #pg-530-2 .panel-grid-cell { margin-right:0 } #pg-530-0 .panel-grid-cell , #pg-530-1 .panel-grid-cell , #pg-530-2 .panel-grid-cell { width:100% } #pl-530 .panel-grid-cell { padding:0 } #pl-530 .panel-grid .panel-grid-cell-empty { display:none } #pl-530 .panel-grid .panel-grid-cell-mobile-last { margin-bottom:0px } #pg-530-0> .panel-row-style { padding:0px 0px 15px 0px } #pg-530-1> .panel-row-style { padding:15px 0px 15px 0px } #pg-530-2> .panel-row-style { padding:15px 0px 0px 0px }  } </style><link rel="icon" href="../wp-content/uploads/2017/05/cropped-favicon-icon-32x32.png" sizes="32x32" />

</head>



<section class="section lb page-title1 little-pad" style="margin-top: 79px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-big-title clearfix text-center">
                	                    <div class="title-banner-bred">
                        <div class="title-subtitle-content">
                            <h3>Pricing &#038; Plan</h3>
                            <p>This is our pricing page showing two, three and four columns </p>
                        </div>                    
                                                    <div class="breadcrumb-content">
                                <ul class="breadcrumb">
			<li><a href="/">Home</a></li><li class="active">Pricing &#038; Plan</li>
		</ul>                            </div>
                                            </div>
                                    </div>
            </div>
        </div>
    </div>
</section>



        <section class="section main-container">

<div class="container">    
    <div class="row">    
    	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        	<div class="content">
		<div class="content-page blog-item">
	<div class="post-padding clearfix">
	<div id="pl-530"  class="panel-layout" ><div id="pg-530-0"  class="panel-grid panel-has-style" ><div class="panel-row-style panel-row-style-for-530-0" ><div id="pgc-530-0-0"  class="panel-grid-cell" ><div id="panel-530-0-0-0" class="so-panel widget widget_themestall themestall panel-first-child panel-last-child" data-index="0" ><div class="textwidget"><div class="section-big-title text-center title-subtitle" style="margin-bottom: 50px;">
                    <h3 style="font-size: 42px; color: #6bb42f;">Pricing Two Column</h3><p style="color: #1f2839;">This is an example page. It’s different from a blog post because it will</p><div class="divider-title text-center" style="background: #6bb42f"><span class="span-1" style="background: #6bb42f"></span></div></div>
<div class="ts-pricing pricing-tables text-center row">
	                
                                        <div id="ts-pricing-541" class="col-md-6 col-sm-6 col-xs-12 featured-off">
                    	<div class="pricing-box dark">
                            <div class="pricing-header">
                            	<h3>Business Package</h3>                            </div>  
                            <div class="pricing-price">
                            	                                <p><sub>$</sub>500.99</p>
                            </div><!-- end price -->
                            <div class="pricing-desc text-center">
                                <p>It was popularised in the with the release of Letraset sheets containing Lorem Ipsum passages</p>
                            </div><!-- end desc -->
                            <div class="panel-group" role="tablist" aria-multiselectable="true">                               
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse5411" aria-expanded="false" aria-controls="collapse5411">WordPress Installation</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse5411" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse5412" aria-expanded="false" aria-controls="collapse5412">Plugin Setting</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse5412" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse5413" aria-expanded="false" aria-controls="collapse5413">Google XML Sitemap</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse5413" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse5414" aria-expanded="false" aria-controls="collapse5414">Working Contact Form</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse5414" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse5415" aria-expanded="false" aria-controls="collapse5415">Unlimited Colors</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse5415" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse5416" aria-expanded="false" aria-controls="collapse5416">Dedicated support</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse5416" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.</p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                            </div><!-- end panel-group -->
                            
                            <div class="pricing-footer text-center">
                            	                                <a href="#" class="btn btn-white">Order Now</a>
                            </div><!-- end desc -->
                        </div>
                    </div>

				                
                                        <div id="ts-pricing-142" class="col-md-6 col-sm-6 col-xs-12 featured-off">
                    	<div class="pricing-box dark">
                            <div class="pricing-header">
                            	<h3>Beginner Package</h3>                            </div>  
                            <div class="pricing-price">
                            	                                <p><sub>$</sub>400.99</p>
                            </div><!-- end price -->
                            <div class="pricing-desc text-center">
                                <p>It was popularised in the with the release of Letraset sheets containing Lorem Ipsum passages</p>
                            </div><!-- end desc -->
                            <div class="panel-group" role="tablist" aria-multiselectable="true">                               
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1421" aria-expanded="false" aria-controls="collapse1421">WordPress Installation</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1421" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1422" aria-expanded="false" aria-controls="collapse1422">Plugin Setting</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1422" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1423" aria-expanded="false" aria-controls="collapse1423">Google XML Sitemap</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1423" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1424" aria-expanded="false" aria-controls="collapse1424">Working Contact Form</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1424" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1425" aria-expanded="false" aria-controls="collapse1425">Unlimited Colors</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1425" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1426" aria-expanded="false" aria-controls="collapse1426">Dedicated support</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1426" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1427" aria-expanded="false" aria-controls="collapse1427">Lifetime Update</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1427" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                            </div><!-- end panel-group -->
                            
                            <div class="pricing-footer text-center">
                            	                                <a href="#" class="btn btn-white">Order Now</a>
                            </div><!-- end desc -->
                        </div>
                    </div>

				</div></div></div></div></div></div><div id="pg-530-1"  class="panel-grid panel-has-style" ><div class="siteorigin-panels-stretch panel-row-style panel-row-style-for-530-1" data-stretch-type="full" data-overlay="107,180,47" data-opacity="0.8" ><div id="pgc-530-1-0"  class="panel-grid-cell" ><div id="panel-530-1-0-0" class="so-panel widget widget_themestall themestall panel-first-child panel-last-child" data-index="1" ><div class="textwidget"><div class="section-big-title text-center title-subtitle" style="margin-bottom: 50px;">
                    <h3 style="font-size: 42px; color: #fff;">Pricing Three Column</h3><p style="color: #fff;">This is an example page. It’s different from a blog post because it will</p><div class="divider-title text-center" style="background: #fff"><span class="span-1" style="background: #fff"></span></div></div><div class="ts-pricing pricing-tables text-center row">
	                
                                        <div id="ts-pricing-541" class="col-md-4 col-sm-6 col-xs-12 featured-off">
                    	<div class="pricing-box light">
                            <div class="pricing-header">
                            	<h3>Business Package</h3>                            </div>  
                            <div class="pricing-price">
                            	                                <p><sub>$</sub>500.99</p>
                            </div><!-- end price -->
                            <div class="pricing-desc text-center">
                                <p>It was popularised in the with the release of Letraset sheets containing Lorem Ipsum passages</p>
                            </div><!-- end desc -->
                            <div class="panel-group" role="tablist" aria-multiselectable="true">                               
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse5411" aria-expanded="false" aria-controls="collapse5411">WordPress Installation</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse5411" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse5412" aria-expanded="false" aria-controls="collapse5412">Plugin Setting</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse5412" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse5413" aria-expanded="false" aria-controls="collapse5413">Google XML Sitemap</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse5413" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse5414" aria-expanded="false" aria-controls="collapse5414">Working Contact Form</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse5414" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse5415" aria-expanded="false" aria-controls="collapse5415">Unlimited Colors</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse5415" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse5416" aria-expanded="false" aria-controls="collapse5416">Dedicated support</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse5416" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.</p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                            </div><!-- end panel-group -->
                            
                            <div class="pricing-footer text-center">
                            	                                <a href="#" class="btn btn-white">Order Now</a>
                            </div><!-- end desc -->
                        </div>
                    </div>

				                
                                        <div id="ts-pricing-142" class="col-md-4 col-sm-6 col-xs-12 featured-off">
                    	<div class="pricing-box light">
                            <div class="pricing-header">
                            	<h3>Beginner Package</h3>                            </div>  
                            <div class="pricing-price">
                            	                                <p><sub>$</sub>400.99</p>
                            </div><!-- end price -->
                            <div class="pricing-desc text-center">
                                <p>It was popularised in the with the release of Letraset sheets containing Lorem Ipsum passages</p>
                            </div><!-- end desc -->
                            <div class="panel-group" role="tablist" aria-multiselectable="true">                               
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1421" aria-expanded="false" aria-controls="collapse1421">WordPress Installation</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1421" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1422" aria-expanded="false" aria-controls="collapse1422">Plugin Setting</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1422" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1423" aria-expanded="false" aria-controls="collapse1423">Google XML Sitemap</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1423" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1424" aria-expanded="false" aria-controls="collapse1424">Working Contact Form</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1424" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1425" aria-expanded="false" aria-controls="collapse1425">Unlimited Colors</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1425" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1426" aria-expanded="false" aria-controls="collapse1426">Dedicated support</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1426" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1427" aria-expanded="false" aria-controls="collapse1427">Lifetime Update</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1427" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                            </div><!-- end panel-group -->
                            
                            <div class="pricing-footer text-center">
                            	                                <a href="#" class="btn btn-white">Order Now</a>
                            </div><!-- end desc -->
                        </div>
                    </div>

				                
                                        <div id="ts-pricing-140" class="col-md-4 col-sm-6 col-xs-12 featured-off">
                    	<div class="pricing-box light">
                            <div class="pricing-header">
                            	<h3>Premium Package</h3>                            </div>  
                            <div class="pricing-price">
                            	                                <p><sub>$</sub>560.99</p>
                            </div><!-- end price -->
                            <div class="pricing-desc text-center">
                                <p>It was popularised in the with the release of Letraset sheets containing Lorem Ipsum passages</p>
                            </div><!-- end desc -->
                            <div class="panel-group" role="tablist" aria-multiselectable="true">                               
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1401" aria-expanded="false" aria-controls="collapse1401">WordPress Installation</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1401" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1402" aria-expanded="false" aria-controls="collapse1402">Plugin Setting</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1402" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1403" aria-expanded="false" aria-controls="collapse1403">Google XML Sitemap</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1403" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1404" aria-expanded="false" aria-controls="collapse1404">Working Contact Form</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1404" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1405" aria-expanded="false" aria-controls="collapse1405">Unlimited Colors</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1405" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1406" aria-expanded="false" aria-controls="collapse1406">Lifetime Update</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1406" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                            </div><!-- end panel-group -->
                            
                            <div class="pricing-footer text-center">
                            	                                <a href="#" class="btn btn-white">Order Now</a>
                            </div><!-- end desc -->
                        </div>
                    </div>

				</div></div></div></div></div></div><div id="pg-530-2"  class="panel-grid panel-has-style" ><div class="panel-row-style panel-row-style-for-530-2" ><div id="pgc-530-2-0"  class="panel-grid-cell" ><div id="panel-530-2-0-0" class="so-panel widget widget_themestall themestall panel-first-child panel-last-child" data-index="2" ><div class="textwidget"><div class="section-big-title text-center title-subtitle" style="margin-bottom: 50px;">
                    <h3 style="font-size: 42px; color: #6bb42f;">Pricing Four Column</h3><p style="color: #1f2839;">This is an example page. It’s different from a blog post because it will</p><div class="divider-title text-center" style="background: #6bb42f"><span class="span-1" style="background: #6bb42f"></span></div></div>
<div class="ts-pricing pricing-tables text-center row">
	                
                                        <div id="ts-pricing-541" class="col-md-3 col-sm-6 col-xs-12 featured-off">
                    	<div class="pricing-box dark">
                            <div class="pricing-header">
                            	<h3>Business Package</h3>                            </div>  
                            <div class="pricing-price">
                            	                                <p><sub>$</sub>500.99</p>
                            </div><!-- end price -->
                            <div class="pricing-desc text-center">
                                <p>It was popularised in the with the release of Letraset sheets containing Lorem Ipsum passages</p>
                            </div><!-- end desc -->
                            <div class="panel-group" role="tablist" aria-multiselectable="true">                               
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse5411" aria-expanded="false" aria-controls="collapse5411">WordPress Installation</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse5411" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse5412" aria-expanded="false" aria-controls="collapse5412">Plugin Setting</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse5412" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse5413" aria-expanded="false" aria-controls="collapse5413">Google XML Sitemap</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse5413" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse5414" aria-expanded="false" aria-controls="collapse5414">Working Contact Form</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse5414" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse5415" aria-expanded="false" aria-controls="collapse5415">Unlimited Colors</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse5415" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse5416" aria-expanded="false" aria-controls="collapse5416">Dedicated support</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse5416" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.</p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                            </div><!-- end panel-group -->
                            
                            <div class="pricing-footer text-center">
                            	                                <a href="#" class="btn btn-white">Order Now</a>
                            </div><!-- end desc -->
                        </div>
                    </div>

				                
                                        <div id="ts-pricing-142" class="col-md-3 col-sm-6 col-xs-12 featured-off">
                    	<div class="pricing-box dark">
                            <div class="pricing-header">
                            	<h3>Beginner Package</h3>                            </div>  
                            <div class="pricing-price">
                            	                                <p><sub>$</sub>400.99</p>
                            </div><!-- end price -->
                            <div class="pricing-desc text-center">
                                <p>It was popularised in the with the release of Letraset sheets containing Lorem Ipsum passages</p>
                            </div><!-- end desc -->
                            <div class="panel-group" role="tablist" aria-multiselectable="true">                               
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1421" aria-expanded="false" aria-controls="collapse1421">WordPress Installation</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1421" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1422" aria-expanded="false" aria-controls="collapse1422">Plugin Setting</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1422" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1423" aria-expanded="false" aria-controls="collapse1423">Google XML Sitemap</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1423" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1424" aria-expanded="false" aria-controls="collapse1424">Working Contact Form</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1424" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1425" aria-expanded="false" aria-controls="collapse1425">Unlimited Colors</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1425" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1426" aria-expanded="false" aria-controls="collapse1426">Dedicated support</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1426" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1427" aria-expanded="false" aria-controls="collapse1427">Lifetime Update</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1427" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                            </div><!-- end panel-group -->
                            
                            <div class="pricing-footer text-center">
                            	                                <a href="#" class="btn btn-white">Order Now</a>
                            </div><!-- end desc -->
                        </div>
                    </div>

				                
                                        <div id="ts-pricing-140" class="col-md-3 col-sm-6 col-xs-12 featured-off">
                    	<div class="pricing-box dark">
                            <div class="pricing-header">
                            	<h3>Premium Package</h3>                            </div>  
                            <div class="pricing-price">
                            	                                <p><sub>$</sub>560.99</p>
                            </div><!-- end price -->
                            <div class="pricing-desc text-center">
                                <p>It was popularised in the with the release of Letraset sheets containing Lorem Ipsum passages</p>
                            </div><!-- end desc -->
                            <div class="panel-group" role="tablist" aria-multiselectable="true">                               
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1401" aria-expanded="false" aria-controls="collapse1401">WordPress Installation</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1401" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1402" aria-expanded="false" aria-controls="collapse1402">Plugin Setting</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1402" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1403" aria-expanded="false" aria-controls="collapse1403">Google XML Sitemap</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1403" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1404" aria-expanded="false" aria-controls="collapse1404">Working Contact Form</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1404" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1405" aria-expanded="false" aria-controls="collapse1405">Unlimited Colors</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1405" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1406" aria-expanded="false" aria-controls="collapse1406">Lifetime Update</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1406" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                            </div><!-- end panel-group -->
                            
                            <div class="pricing-footer text-center">
                            	                                <a href="#" class="btn btn-white">Order Now</a>
                            </div><!-- end desc -->
                        </div>
                    </div>

				                
                                        <div id="ts-pricing-138" class="col-md-3 col-sm-6 col-xs-12 featured-off">
                    	<div class="pricing-box dark">
                            <div class="pricing-header">
                            	<h3>Ultimate Package</h3>                            </div>  
                            <div class="pricing-price">
                            	                                <p><sub>$</sub>1220.99</p>
                            </div><!-- end price -->
                            <div class="pricing-desc text-center">
                                <p>It was popularised in the with the release of Letraset sheets containing Lorem Ipsum passages</p>
                            </div><!-- end desc -->
                            <div class="panel-group" role="tablist" aria-multiselectable="true">                               
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1381" aria-expanded="false" aria-controls="collapse1381">WordPress Installation</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1381" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1382" aria-expanded="false" aria-controls="collapse1382">Plugin Setting</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1382" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1383" aria-expanded="false" aria-controls="collapse1383">Google XML Sitemap</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1383" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1384" aria-expanded="false" aria-controls="collapse1384">Working Contact Form</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1384" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1385" aria-expanded="false" aria-controls="collapse1385">Unlimited Colors</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1385" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                                <div class="panel panel-default animation revealOnScroll">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <button type="button" data-toggle="collapse" data-target="#collapse1386" aria-expanded="false" aria-controls="collapse1386">Dedicated support</button>                                            
                                        </h4>
                                    </div><!-- end heading -->
                                    
                                                                        <div id="collapse1386" class="panel-collapse collapse" role="tabpanel">
                                        <div class="panel-body">
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </p>
                                        </div>
                                    </div>
                                    
                                                                        
                                </div><!-- end panel -->
                                                            </div><!-- end panel-group -->
                            
                            <div class="pricing-footer text-center">
                            	                                <a href="#" class="btn btn-white">Order Now</a>
                            </div><!-- end desc -->
                        </div>
                    </div>

				</div></div></div></div></div></div></div>    
        </div>    
</div>
        
    	</div>
</div>

	     
    </div>
    
</div>
<!-- Content Wrap -->

            </section>