<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $user app\models\LoginForm */

use borales\extensions\phoneInput\PhoneInput;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Url;
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.ui.min.js"></script>
    
    <style>
    .close{
    margin-right: -16px;
    opacity: 1 !important;
}
.close:hover{
    color:black;
}
.modal-header .close {
    margin-top: -39px;
}
.modal-open > .container > .costumModal > .title {
    filter: blur(2px);
}
.modal-content{
    position:fixed;
    border: 3px solid cadetblue;
    -ms-transform: translate(0%,60%);
    -moz-transform:translate(0%,60%);
    -webkit-transform: translate(0%,60%);
    transform: translate(0%,60%);
    
}
.modal-backdrop.in {
    opacity: 0 !important;
}
.modal-backdrop {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1040;   
}
.down{
    width: 47px;
}
.wats{
    width: 55px;
    margin-left: 10px;
}
.mes{
    width: 41px;
    margin-left: 6px;
}
.promo {
    padding: 5px 8px 5px 8px;
    border: 1px dashed;
    border-color: blue;
}
.refer{
    margin-bottom: 18px;
    margin-top: 10px;
    font-weight: bold;
}
    
    </style>
        
<?php    
//$this->title = 'Payment Gateways';
//$this->params['breadcrumbs'][] = $this->title;
?>

<!-- breadcrumb start here -->
<!-- <div class="bread-crumb">
	<div class="container">
		<h2>Thank You</h2>
		<ul class="list-inline">
			<li><a href="/">home</a></li>
			<li><a href="<?php echo Url::toRoute('site/payment');?>">Payment Gateways</a></li>
		</ul>
	</div>
</div> -->
<!-- breadcrumb end here -->
<style>
    .layout{
        border:1px solid #6f7175;
    background: #f4f6f9;
    margin-top: 12px;
    padding:10px;
    margin-bottom: 20px;
    }
    
    .logo{
        padding-top: 20px;
    }
    
    .bttn{
            margin-top: 22px;
    padding: 7px;
    }
.section.lb {
	top: -69px;
}
.sociallink{
   background-color: #fed602;
}
</style>    
<!-- main container start here -->

    <section class="section lb page-title1 little-pad">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-big-title clearfix text-center">
                	                    <div class="title-banner-bred">
                        <div class="title-subtitle-content">
                            <!--<h3>Payment Options</h3>-->
                            <!--<p>NurseryPlant subtitle goes here</p>-->
                        </div>                    
                                                    <div class="breadcrumb-content">
                                <ul class="breadcrumb">
			<li><a href="/">Home</a></li><li class="active">Payment Options</li>
		</ul>                            </div>
                                            </div>
                                    </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    
    
<!--<div class="placetop layout">

		<div class="row">
    
   
			<div class="col-sm-12">
			<div class="col-sm-4">
				
						<img class="logo" src="/images/payeer.png" alt="payeer" />
                        </div>
                                        <div class="col-sm-4">

						<h3>Invest via Payeer</h3>
						<p>No fees. Instant Processing.</p>
					</div>
                                        <div class="col-sm-4">

						<button class="btn-primary pull-right btn-lg bttn btnShowPayeer" type="button">P********</button>
					</div>
				</div>
			</div>
		</div>-->
	

<div class="placetop layout">
	<!--<div class="container">-->
		<div class="row">
			<div class="col-sm-12">
			<div class="col-sm-4">
				
				<img class="logo" src="/images/bitcoin.png" alt="bitcoin" />
                        </div>
			<div class="col-sm-4">		
						<h3>Invest via Bitcoin(USD)</h3>
						<p>Instant Processing at Zero Fees.</p>
                        </div>
					<div class="text-right">
						<button class="btn-primary pull-right btn-lg bttn btnShowBTC" type="button">*******bitcoinaddress*******</button>
					</div>
				</div>
			</div>
		</div>
	<!--</div>-->

<!--<div class="placetop layout">
	
		<div class="row">
			<div class="col-sm-12">
			<div class="col-sm-4">
				
				<img class="logo" src="/images/perfectmoney.png" alt="perfectmoney" />
                                
                        </div>
					<div class="col-sm-4">
						<h3>Invest via Perfect Money</h3>
						<p>Instant Processing at Zero Fees.</p>
					</div>
					<div class="text-right">
						<button class="btn-primary pull-right btn-lg bttn btnShowPerfectMoney"   type="button">U********</button>              
					</div>
				</div>
			</div>
		</div>-->
	
</div>
    

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
<link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/blitzer/jquery-ui.css"
    rel="stylesheet" type="text/css" />
<script type="text/javascript">
/*    $(function () {
        $(".dialogBTC").dialog({
            modal: true,
            autoOpen: false,
            title: "BITCOIN Payment Details",
            width: 300,
            height: 150,
            
        });
        $(".dialogPayeer").dialog({
            modal: true,
            autoOpen: false,
            title: "PAYEER Payment Details",
            width: 300,
            height: 150,
            
        });
        $(".dialogPerfectMoney").dialog({
            modal: true,
            autoOpen: false,
            title: "PERFECT MONEY Payment Details",
            width: 300,
            height: 150,
            
        });
        $(".btnShowBTC").click(function () {
            $('.dialogBTC').dialog('open');
        });
        $(".btnShowPayeer").click(function () {
            $('.dialogPayeer').dialog('open');
        });
        $(".btnShowPerfectMoney").click(function () {
            $('.dialogPerfectMoney').dialog('open');
        });
    }); */
</script>
<style>
.ui-dialog-titlebar.ui-widget-header.ui-corner-all.ui-helper-clearfix {
    background: #30d044;
}    
</style>
    
<!--<div class="dialogBTC" style="display: none; top: 30px;" align = "center">
    <b> 1ErffrRHbitcoinaddressbR76b5T </b>
</div>

<div class="dialogPayeer" style="display: none; top: 30px;" align = "center">
    <b> P12345678 </b>
</div>

<div class="dialogPerfectMoney" style="display: none; top: 30px;" align = "center">
    <b> U1234567 </b>
</div>-->

      