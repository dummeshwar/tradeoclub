<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Forgot Key Pin';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-new-master-pin container">
    
    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php if (Yii::$app->session->hasFlash('error')): ?>
        <p class="lead text-danger"> <strong>Error!</strong>. <?= Yii::$app->session->getFlash('error'); ?>.</p>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <p class="lead text-success"> <strong>Successful!</strong>. <?= Yii::$app->session->getFlash('success'); ?>.</p>
    <?php endif; ?>

    <?php
    $form = ActiveForm::begin([
                'id' => 'forgot-master-form',
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-1 control-label'],
                ],
    ]);
    ?>

    <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'forgot-master-pin-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
