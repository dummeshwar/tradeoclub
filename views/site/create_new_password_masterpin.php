<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Set your password';
//$this->params['breadcrumbs'][] = $this->title;
?>

<div class="create-new-password-masterpin container">
    <?= Html::tag('h1', Html::encode($this->title), ['class' => 'form-title', 'style'=>'font-size:30px;padding:20px;']) ?>

    
    <?php
    $form = ActiveForm::begin([
                'id' => 'create-new-password-masterpin',
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-2',
                        'offset' => 'col-sm-offset-4',
                    ],
                ],
    ]);

    echo $form->field($model, 'password', [
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-3',
        ]
    ])->passwordInput(['autofocus' => true]);

    echo $form->field($model, 'password_repeat', [
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-3',
        ]
    ])->passwordInput();

    echo $form->field($model, 'master_pin', [
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-3',
        ]
    ])->hiddenInput(['value'=>'123456'])->label(false);

    echo $form->field($model, 'master_pin_repeat', [
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-3',
        ]
    ])->hiddenInput(['value'=>'123456'])->label(false);
    ?>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'new-pwd-masterpin-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>