<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

//$this->title = 'Login';
//$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    
    .login-center {
/*    background: #fff;
    min-width: 20rem;
    padding: 5px 0px;
    border-radius: 5px;
    margin: 1.55714em 0;
    border: 1px solid blue;*/


/* background: #fff;
    min-width: 20rem;
    border-radius: 5px;
    margin: 1.55714em 0;*/
    /* border: 1px solid blue; */
    box-shadow: 5px 5px 5px 5px #888888;
    padding: 15px;
    
    
    }
    .input-group1{
        padding: 3px;
        margin:15px;
    }
    .input-group2{
       margin:5px 25px;
    }

    .bt1{
        padding: 8px 18px;
        font-size: 20px;
            margin-left: 17px;
            width: 90%;
    
    }
    
    #loginlogo{
        background: #60af2f;
    padding: 10px;
    text-align: center;
    height: 66px;
    margin-left: 133px;
    margin-top: 15px;
    }

    .inpt{
        padding: 24px 10px;
    }
    .fpass{
        margin-left: 165px;
    }
    
   

label.control-label.col-sm-3 {
    margin-left: 15px;
}

.borderform{
     box-shadow: 5px 5px 5px 5px #888888;
    padding: 17px;
    margin-bottom: 40px;
    border-radius: 6px;
    margin-top: -60px;
    
}
a.forgot-passwordwd-link {
    
    margin-left: 95px;
}


element.style {
}
.form-group.field-loginform-username.required {
    padding-top: 14px;
}
    </style>



 


    <section class="section lb page-title1 little-pad">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-big-title clearfix text-center">
                	                    <div class="title-banner-bred">
                        <div class="title-subtitle-content">
                            <h3 style="padding-top:40px">My Account</h3>
                            <!--<p>NurseryPlant subtitle goes here</p>-->
                        </div>                    
<!--                                                    <div class="breadcrumb-content">
                                <ul class="breadcrumb">
			<li><a href="/">Home</a></li><li class="active">My Account</li>
		</ul>                            </div>-->
                                            </div>
                                    </div>
            </div>
        </div>
    </div>
</section>
<br/>
<br/>
<br/>

<div class="site-login container">
    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?php if (Yii::$app->session->hasFlash('successNewPwdMasterPin')): ?>
        <p class="lead text-success"> <strong>Successful!</strong>. Please login here!!!.</p>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <!--<p class="lead text-success"> <strong>Successful!</strong>. <?= Yii::$app->session->getFlash('success'); ?>.</p>-->
    <?php endif; ?>


    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
//            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
//            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

   
      
        <div class="borderform col-lg-4 col-lg-offset-4">
             <?= $form->field($model, 'username')->textInput(['autofocus' => true,'style'=>'width:220px']) ?>
      

        <?= $form->field($model, 'password')->passwordInput(['style'=>'width:220px']) ?>

        
            
         <?= Html::submitButton('Login', ['class' => 'bt1 btn btn-block', 'name' => 'login-button' ,'style'=>'background:#ee821a;color:#fff']) ?>     <br/>
         <?= Html::a('Forgot your password ?', ['site/request-password-reset'], ['class' => 'forgot-passwordwd-link']) ?>
         <br/>
         <br/>
          <?= Html::a('Register', ['site/register'], ['class' => 'bt1 btn btn-block','style'=>'background:#2f2f2f;color:#fff']) ?>
         <br/>
         
        </div>
       <!-- <div class="form-group">
            <div class="col-lg-offset-1 col-lg-1">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
            <div class="col-lg-1">
                <?= Html::a('Register', ['site/register'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-1 col-sm-10 sign-in-block-links">
                <?= Html::a('Forgot your password ?', ['site/request-password-reset'], ['class' => 'forgot-passwordwd-link']) ?>
            <div>
                </div>
            </div>
        </div>-->

    <?php ActiveForm::end(); ?>

</div>



    
    <!--<div class="row" style="margin-top: -70px;">-->
<!--    <div class="col-lg-4 col-lg-offset-4">
        <div class="login-center">
        
           
<div class="input-group1">
<label>USER ID</label><font color="red" size="2">*</font>
            <input type="text" name="username" class="form-control inpt" /> 
            
        </div>
        
        <div class="input-group1">
            <label>PASSWORD</label><font color="red" size="2">*</font>
            <input type="password" name="password" class="form-control inpt" /> 
            
        </div> 
        
        <div class="input-group1" >
            <label>KEY PIN</label><font color="red" size="2">*</font>
            <input type="text" name="master_pin" class="form-control inpt" /> 
            
        </div> 
        <br/>
        
        <div class="input-group2">
            <a href="site/login" name="login-button" class="bt1 btn btn-block">Login</a>
        </div>
        <br/>
        <a href="#" class="fpass">Forgot your password ?</a>
        <br/>
        <br/>
        
        <div class="input-group2">
             <a href="/site/register" class="btn bt1 btn-block" style="background:  #5959f1;">Register</a>
        </div>
            </form>
    </div> 
</div> 
    </div>
    
   -->