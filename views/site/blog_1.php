<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\FrontPageAsset;

use yii\helpers\Url;

?>

<!--  page banner -->
  <section id="page-banner" class="page-banner-main-block" style="background-image: url('images/bg/page-banner.jpg')"> 
    <div class="overlay-bg"></div>
    <div class="container">
      <h1 class="page-banner-heading">Blog</h1>
    </div>
  </section>  
<!--  end page banner  -->
<!--  breadcrumb -->
  <section class="breadcrumb-block">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            
            <li>Blog</li>
          </ol>
        </div>
        <div class="col-sm-4">
          <div class="breadcrumb-btn">
            <?= Html::a('Get In Touch', ['site/contact'], ['class' => 'btn btn-default']) ?>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- end breadcrumb -->

<!--  single post -->
  <section id="single-post" class="single-post-main-block">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="news-list-block single-post-block">
            
            <div class="news-list-dtl">
              <h3 class="news-list-heading">Facts about Bitcoin</h3>
            </div>
              <br/>
            <div class="single-post-point">
              <h5 class="post-point-heading">What Is Bitcoin?</h5>
              
              <div class="row">
                <div class="col-sm-12">
                  <p>Bitcoin is both a digital currency and a payment system. It can be sent around the world in seconds, at almost no cost. This has value because of all the good things you can do with it. Whether you need to quickly transfer money internationally, shop online without a bank account, accept payment from a compromised land with no risk of fraud, or have a store of value you can easily convert into local currency, Bitcoin is the answer.</p>
                </div>
<!--                <div class="col-sm-4">
                  <div class="post-point-img">
                    <img src="images/blog/news-dtl.jpg" class="img-responsive" alt="post-point">
                  </div>
                  <div class="post-point-img-caption">
                    This infographic explores how Formation of Strategic Partnerships Works basicaly.
                  </div>
                </div>-->
              </div>
              
            </div>
            <div class="single-post-point">
              <h5 class="post-point-heading">Is Bitcoin Safe To Use?</h5>
              <p>Similar to online banking information, logins and passwords to Bitcoin services must be kept 100 percent private. Best practices include using a password manager to store unique, long passwords for each site and enabling two-factor authentication using a smartphone whenever possible.</p>
            </div>
            <div class="single-post-point">
              <h5 class="post-point-heading">Do Merchants Accept Bitcoin</h5>
              <p>Multi-billion dollar companies are beginning to accept Bitcoin, and more are doing so every day. Bitcoin offers numerous advantages for businesses. Merchants are able to save the fees that are generally charged by credit card companies, never have to deal with chargebacks, and can accept payment without requiring personal information from their customers. And with payment processors, merchants don't have to worry about Bitcoin's volatility.</p>
            </div>
            <div class="single-post-point">
              <h5 class="post-point-heading">What Is Bitcoin's Legal Status?</h5>
              <p>Bitcoin has been treated as a currency and a commodity by the United States government, meaning most Bitcoin companies fall under existing law and regulations. The New York Department of Financial Services in the process of establishing regulatory guidelines for Bitcoin companies in the State of New York.</p>
            </div>
            <div class="single-post-point">
              <h5 class="post-point-heading">How Do I Buy Bitcoin?</h5>
              <p>Bitcoin can be bought, mined or exchanges for goods and services. Buying Bitcoin is the simplest way to acquire them. You can buy Bitcoins in person, direct from another bitcoin holder, purchase them online through an exchange, or buy them through a growing number of Bitcoin ATMs.</p>
            </div>
            <div class="single-post-point">
              <h5 class="post-point-heading">What Is Most The Secure Way To Store Bitcoin?</h5>
              <p>Currently, there are a number of measures you can take to completely secure large Bitcoin funds, but they can be complex to achieve. For now, it is best to consult with one of the well-known security companies in the Bitcoin space. Meanwhile, those same companies and others are busy innovating, and greater ease and efficiency are on the way.</p>
            </div>
                       
          </div> 
            
           

            
            
            
           
        </div>
                      
      </div>
    </div>
      
      
       <!-- market  -->
  <div id="market" class="market-main-block" style="background-image: url('images/bg/market-bg.jpg')">
    <div class="overlay-bg"></div>
    <div class="container">
      <div class="row">
        <div class="col-sm-6"> 
          <div class="market-block">
            <h3 class="market-heading">About Bitcoin</h3>  
            <p>Bitcoin is the mother of all Cryptocurrencies.Bitcoin is digital money and overall instalment framework. It is the principal decentralised advanced money, as the framework works without a national bank or single chairman. The system is distributed and exchanges occur between clients specifically using cryptography, without a delegate. Bitcoins are made as a reward for a procedure known as mining. They can be traded for different monetary standards, items, and administrations.
Value of all the cryptocurrencies are directly influenced according to the value of Bitcoin.<p>
            
            <a href="#" class="btn btn-default">Read More</a>
          </div>   
        </div>
        <div class="col-sm-6">
          <div class="market-chart">
            <div class="btcwdgt-chart">
            </div>
          </div> 
        </div>  
      </div>
    </div>
  </div>
<!-- end market  -->



<div class="container">
      <div class="row">
        <div class="col-sm-12">
<div class="single-post-tag-block">
            
          </div> 
          <div class="single-post-written"> 
            <div class="row">
              <div class="col-sm-2">
                <div class="written-img">
                  <img src="images/blog/written.jpg" class="img-responsive" alt="written">
                </div>
              </div>
              <div class="col-sm-10">
                <div class="written-dtl">
                  <h6 class="written-by">Written by Admin</h6>
                  <p>Edison may have been behind the invention of the electric light bulb, but he did not work alone.  models and companies have been forged through.</p>
                </div>
              </div>
            </div>
          </div>
              </div>
            </div>
          </div>
  </section>
<!--  end single post -->