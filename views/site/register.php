<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $userRegistrationModelForm app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;
use yii\captcha\Captcha;
use yii\helpers\Url;
use borales\extensions\phoneInput\PhoneInput;

//$this->title = 'Create Account';
//$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .section.lb {
	top: -69px;
}
.borderform{
     box-shadow: 5px 5px 5px 5px #888888;
    padding: 15px;
    margin-bottom: 60px;
    border-radius: 6px;
    padding-top: 30px;
    margin-top: -31px;
    
}
input#registrationform-parent_id {
    width: 230px;
}
input#registrationform-username {
    width: 305px;
}
input#registrationform-email {
    width: 305px;
}
input#registrationform-verifycode {
    width: 304px;
    margin-left: 8px;
    border-color: #6bb42f;
    background: none;
    border-radius: 3px;
    box-shadow: none!important;
    color: #6bb42f;
    padding: 11px 13px;
}
.bt1 {
    padding: 12px 18px;
    font-size: 20px;
    margin-left: -75px;
    width: 95%;
}
.innerForm{
        margin-left: 29px;
    margin-right: -35px;
}

    </style>
    
    <section class="section lb page-title1 little-pad">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-big-title clearfix text-center">
                	                    <div class="title-banner-bred">
                        <div class="title-subtitle-content">
                            <h3 style="padding-top:40px">My Account</h3>
                            <!--<p>NurseryPlant subtitle goes here</p>-->
                        </div>                    
                                                    <div class="breadcrumb-content">
                                <ul class="breadcrumb">
			<li><a href="/">Home</a></li><li class="active">My Account</li>
		</ul>                            </div>
                                            </div>
                                    </div>
            </div>
        </div>
    </div>
</section>
<div class="site-register container">
    <?= Html::tag('h1', Html::encode($this->title), ['class' => 'form-title']) ?>

    <?php if (Yii::$app->session->hasFlash('registrationSuccessful')): ?>
        <div class="alert alert-success">
            Registration is <strong>Successful!</strong> . Please check your mail for further details.
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('registrationFailed')): ?>
        <div class="alert alert-danger">
            Registration is <strong>Failed!</strong> . Something went wrong.
        </div>
    <?php endif; ?>


    <?php
        $form = ActiveForm::begin([
                    'id' => 'site-register-form',
                    'layout' => 'horizontal',
                    'fieldConfig' => [
//                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
//                        'horizontalCssClasses' => [
//                            'label' => 'col-sm-2',
//                            'offset' => 'col-sm-offset-4',
//                        ],
                    ],
        ]);
    ?>

<div class="borderform col-lg-6 col-lg-offset-3">
    <div class="innerForm">
    <?php
        // TypeaheadBasic usage with ActiveForm and model
        echo $form->field($userRegistrationModelForm, 'parent_id')->widget(TypeaheadBasic::classname(), [
            'data' => $activeUsers,
            'pluginOptions' => ['highlight' => true],
            'options' => ['placeholder' => 'Sponsor Id ...', 'wrapper' => ''],
        ]);
    ?>



    <div class="form-group">
        <!--<div class="col-sm-offset-2 col-sm-10 col-sm-3">-->
            <?= Html::button('Get Sponsor Id', ['class' => 'btn btn-primary btn-xs', 'style' => 'margin:0;bottom: 10px;position: relative;margin-top: -60px;padding: 12px;float: right;margin-right: 107px;', 'onclick' => " $.ajax({
                        type     :'POST',
                        cache    : false,
                        url  : '" . Url::to(['user/getd-default-sponsor-id']) . "',
                        success  : function(response) {
                                console.log('success: '+response.sponsorId);
                                $('#registrationform-parent_id').val(response.sponsorId)
                        },
                        error: function(){
                          console.log('failure : something went wrong while fetching sponsor ID. Please try again later.');
                        }
                        });return false;",]) ?>
        <!--</div>-->
    </div>

    <?php
        echo $form->field($userRegistrationModelForm, 'position', [
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-4',
            ]
        ])->inline()->radioList(array('L' => 'Left', 'R' => 'Right'));


        echo $form->field($userRegistrationModelForm, 'username', [
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-3',
            ]
        ])->textInput();

        echo $form->field($userRegistrationModelForm, 'email', [
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-3',
            ]
        ])->textInput();


        // echo $form->field($userRegistrationModelForm, 'contact_number', [
        //     'horizontalCssClasses' => [
        //         'wrapper' => 'col-sm-3',
        //     ]
        // ])->widget(PhoneInput::className(), [
        //     'jsOptions' => [
        //         'allowExtensions' => true,
        //         'nationalMode' => false,
        //         'allowDropdown' => true,
        //     ]
        // ]);
        ?><div class="form-group">
            <?php
        echo $form->field($userRegistrationModelForm, 'verifyCode', [
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-6',
            ] 
            
        ])->widget(Captcha::className(), [
            'options' => ['placeholder' => 'To change, click on it.'],
        ]);

        ?></div>
    

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton('Register', ['class' => 'bt1 btn btn-block site-button-secondry btn-block m-b10', 'name' => 'register-button','style'=> "background:#ee821a;color:#fff"]) ?>
        </div>
    </div>
</div>
</div>
    <?php ActiveForm::end(); ?>

</div>


<!--<style>
    
    .login-center {
    background: #fff;
    min-width: 20rem;
    border-radius: 5px;
    margin: 1.55714em 0;
    /* border: 1px solid blue; */
    box-shadow: 5px 5px 5px 5px #888888;
    padding: 15px;
    }
    .input-group1{
        padding: 3px;
        margin:15px;
    }
    .input-group2{
       margin:5px 25px;
    }

    .bt1{
        padding: 20px 18px;
        font-size: 20px;
    }
    
    #loginlogo{
        background: #60af2f;
    padding: 10px;
    text-align: center;
    height: 66px;
    margin-left: 133px;
    margin-top: 15px;
    }

    .inpt{
        padding: 20px 10px;
    }
    .fpass{
        margin-left: 165px;
    }
    
    .keybtn{
    margin-top: -42px;
    padding: 11px 16px;
    float: right;
    }
    </style>

    
<div class="row"     style=";margin-top: -30px;">
    <div class="col-lg-4 col-lg-offset-4">
        <div class="login-center">
<!--        <label>USER ID</label>-->
<!-- <img id="loginlogo" src="/images/logo.png" > -->


<!--
    <div class="input-group1">
        <label>PARENT ID</label><font color="red" size="2">*</font>
            <input type="text" class="form-control inpt" /> 
            <div class="input-group-append">
            <button class="btn btn-outline-secondary keybtn" type="button">Get Sponsor Id</button>
            </div>    
        </div>
        
        <div class="input-group1">
            <label>POSITION</label><font color="red" size="2">*</font>
        <select class="form-control inpt" id="inputGroupSelect01">
            <option selected>Choose...</option>
            <option value="1">Left</option>
            <option value="2">Right</option>
        </select>
            
        </div> 
        
        <div class="input-group1" >
            <label>USER NAME</label><font color="red" size="2">*</font>
            <input type="text" class="form-control inpt" /> 
            
        </div> 
        <div class="input-group1" >
            <label>EMAIL</label><font color="red" size="2">*</font>
            <input type="text" class="form-control inpt" /> 
            
        </div> 
        <br/>
        
        <div class="input-group2">
            <a href="login" class="bt1 btn btn-block">Register</a>
        </div>
        
        
        
    </div> 
</div> 
    </div>-->
    
   





