<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\FrontPageAsset;

use yii\helpers\Url;

?>

<!--  page banner -->
  <section id="page-banner" class="page-banner-main-block" style="background-image: url('/images/bg/page-banner.jpg')"> 
    <div class="overlay-bg"></div>
    <div class="container">
      <h1 class="page-banner-heading">Blog</h1>
    </div>
  </section>  
<!--  end page banner  -->
<!--  breadcrumb -->
  <section class="breadcrumb-block">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            
            <li>Blog</li>
          </ol>
        </div>
        <div class="col-sm-4">
          <div class="breadcrumb-btn">
            <?= Html::a('Get In Touch', ['site/contact'], ['class' => 'btn btn-default']) ?>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- end breadcrumb -->

<!--  news -->
  <section id="news-list" class="news-list-main-block">
    <div class="container">
      <div class="row">
        <div class="col-sm-9">
          <div class="news-list-block">
            <div class="news-list-img">
                <img src="/images/blog/list-news-1.jpg" class="img-responsive" alt="news">
            </div>
            <div class="news-list-dtl">
              <h3 class="news-list-heading">What Is Bitcoin?</h3>
              <div class="meta-tag">
                <ul>
                  <li><i class="ti-timer" aria-hidden="true"></i> <a href="#">January 01, 2019</a></li>
                  <li><i class="ti-user" aria-hidden="true"></i> <a href="#">Admin</a></li>
                  <li><i class="ti-comment" aria-hidden="true"></i> <a href="#">25</a></li>
                </ul>
              </div>
              <p>Bitcoin is both a digital currency and a payment system. It can be sent around the world in seconds, at almost no cost. This has value because of all the good things you can do with it. Whether you need to quickly transfer money internationally, shop online without a bank account, accept payment from a compromised land with no risk of fraud, or have a store of value you can easily convert into local currency, Bitcoin is the answer.</p>             
              <!--<a href="single-post.html" class="btn btn-default">Read More</a>-->
            </div>
          </div>
          <div class="news-list-block">
            <div class="news-list-img">
              <!--<a href="#"><img src="/images/blog/list-news-2.jpg" class="img-responsive" alt="news"></a>-->
              <img src="/images/blog/list-news-2.jpg" class="img-responsive" alt="news">
            </div>
            <div class="news-list-dtl">
              <h3 class="news-list-heading">Is Bitcoin Safe To Use?</h3>
              <div class="meta-tag">
                <ul>
                  <li><i class="ti-timer" aria-hidden="true"></i> <a href="#">January 05, 2019</a></li>
                  <li><i class="ti-user" aria-hidden="true"></i> <a href="#">Admin</a></li>
                  <li><i class="ti-comment" aria-hidden="true"></i> <a href="#">45</a></li>
                </ul>
              </div>
              <p>Similar to online banking information, logins and passwords to Bitcoin services must be kept 100 percent private. Best practices include using a password manager to store unique, long passwords for each site and enabling two-factor authentication using a smartphone whenever possible.</p>             
              <!--<a href="single-post.html" class="btn btn-default">Read More</a>-->
            </div>
          </div>
          <div class="news-list-block">
            <div class="news-list-img">
             <img src="/images/blog/list-news-3.jpg" class="img-responsive" alt="news">
            </div>
            <div class="news-list-dtl">
              <h3 class="news-list-heading">Do Merchants Accept Bitcoin</h3>
              <div class="meta-tag">
                <ul>
                  <li><i class="ti-timer" aria-hidden="true"></i> <a href="#">January 06, 2019</a></li>
                  <li><i class="ti-user" aria-hidden="true"></i> <a href="#">Admin</a></li>
                  <li><i class="ti-comment" aria-hidden="true"></i> <a href="#">14</a></li>
                </ul>
              </div>
              <p>Multi-billion dollar companies are beginning to accept Bitcoin, and more are doing so every day. Bitcoin offers numerous advantages for businesses. Merchants are able to save the fees that are generally charged by credit card companies, never have to deal with chargebacks, and can accept payment without requiring personal information from their customers. And with payment processors, merchants don't have to worry about Bitcoin's volatility.</p>             
              <!--<a href="single-post.html" class="btn btn-default">Read More</a>-->
            </div>
          </div>
          <div class="news-list-block">
            <div class="news-list-img">
              <img src="/images/blog/list-news-4.jpg" class="img-responsive" alt="news">
            </div>
            <div class="news-list-dtl">
             <h3 class="news-list-heading">What Is Bitcoin's Legal Status?</h3>
              <div class="meta-tag">
                <ul>
                  <li><i class="ti-timer" aria-hidden="true"></i> <a href="#">January 08, 2019</a></li>
                  <li><i class="ti-user" aria-hidden="true"></i> <a href="#">Admin</a></li>
                  <li><i class="ti-comment" aria-hidden="true"></i> <a href="#">15</a></li>
                </ul>
              </div>
              <p>Bitcoin has been treated as a currency and a commodity by the United States government, meaning most Bitcoin companies fall under existing law and regulations. The New York Department of Financial Services in the process of establishing regulatory guidelines for Bitcoin companies in the State of New York.</p>             
              <!--<a href="single-post.html" class="btn btn-default">Read More</a>-->
            </div>
          </div>
          <div class="news-list-block">
            <div class="news-list-img">
              <img src="/images/blog/list-news-5.jpg" class="img-responsive" alt="news">
            </div>
            <div class="news-list-dtl">
              <h3 class="news-list-heading">How Do I Buy Bitcoin?</h3>
              <div class="meta-tag">
                <ul>
                  <li><i class="ti-timer" aria-hidden="true"></i> <a href="#">January 08, 2019</a></li>
                  <li><i class="ti-user" aria-hidden="true"></i> <a href="#">Admin</a></li>
                  <li><i class="ti-comment" aria-hidden="true"></i> <a href="#">20</a></li>
                </ul>
              </div>
              <p>Currently, there are a number of measures you can take to completely secure large Bitcoin funds, but they can be complex to achieve. For now, it is best to consult with one of the well-known security companies in the Bitcoin space. Meanwhile, those same companies and others are busy innovating, and greater ease and efficiency are on the way.</p>             
<!--              <a href="single-post.html" class="btn btn-default">Read More</a>-->
            </div>
          </div>
          <div class="news-list-block">
            <div class="news-list-img">
              <img src="/images/blog/list-news-3.jpg" class="img-responsive" alt="news">
            </div>
            <div class="news-list-dtl">
              <h3 class="news-list-heading">What Is Most The Secure Way To Store Bitcoin?</h3>
              <div class="meta-tag">
                <ul>
                  <li><i class="ti-timer" aria-hidden="true"></i> <a href="#">January 08, 2019</a></li>
                  <li><i class="ti-user" aria-hidden="true"></i> <a href="#">Admin</a></li>
                  <li><i class="ti-comment" aria-hidden="true"></i> <a href="#">20</a></li>
                </ul>
              </div>
              <p>Bitcoin can be bought, mined or exchanges for goods and services. Buying Bitcoin is the simplest way to acquire them. You can buy Bitcoins in person, direct from another bitcoin holder, purchase them online through an exchange, or buy them through a growing number of Bitcoin ATMs.</p>             
<!--              <a href="single-post.html" class="btn btn-default">Read More</a>-->
            </div>
          </div>
         
        </div>
        <div class="col-sm-3">
          <div class="news-list-sidebar">
<!--            <div class="sidebar-widget search-widget">
              <form class="search-form" method="post" action="#">
                <div class="form-group">
                  <input type="text" class="form-control" id="search" placeholder="Search Here.....">
                  <a href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
                </div>
              </form>
            </div>-->
            <div class="sidebar-widget rct-news-widget">
              <h6 class="widget-heading">Recent Post</h6>
              <ul>
                <li>
                  <h6 class="rct-news-title">What Is Bitcoin?</h6>
                  <div class="date"><i class="ti-timer" aria-hidden="true"></i> <a href="#">January 05, 2019</a></div>
                </li>
                <li>
                  <a href="#"><h6 class="rct-news-title">Is Bitcoin Safe To Use?</h6></a>
                  <div class="date"><i class="ti-timer" aria-hidden="true"></i> <a href="#">January 06, 2019</a></div>
                </li>
                <li>
                  <a href="#"><h6 class="rct-news-title">Do Merchants Accept Bitcoin</h6></a>
                  <div class="date"><i class="ti-timer" aria-hidden="true"></i> <a href="#">January 08, 2019</a></div>
                </li>
              </ul>
            </div>            
            <div class="sidebar-widget rct-news-widget">
              <h6 class="widget-heading">Popular Post</h6>
              <ul>
                <li>
                  <a href="#"><h6 class="rct-news-title">What Is Bitcoin's Legal Status?</h6></a>
                  <div class="date"><i class="ti-timer" aria-hidden="true"></i> <a href="#">January 05, 2019</a></div>
                </li>
                <li>
                  <a href="#"><h6 class="rct-news-title">How Do I Buy Bitcoin?</h6></a>
                  <div class="date"><i class="ti-timer" aria-hidden="true"></i> <a href="#">January 06, 2019</a></div>
                </li>
                <li>
                  <a href="#"><h6 class="rct-news-title">What Is Most The Secure Way To Store Bitcoin?</h6></a>
                  <div class="date"><i class="ti-timer" aria-hidden="true"></i> <a href="#">January 08, 2019</a></div>
                </li>
              </ul>
            </div>  
            
             
          </div>
        </div>               
      </div>
    </div>
      
       <!-- market  -->
  <!--<div id="market" class="market-main-block" style="background-image: url('/images/bg/market-bg.jpg')">
    <div class="overlay-bg"></div>
    <div class="container">
      <div class="row">
        <div class="col-sm-6"> 
          <div class="market-block">
            <h3 class="market-heading">About Bitcoin</h3>  
            <p>Bitcoin is the mother of all Cryptocurrencies.Bitcoin is digital money and overall instalment framework. It is the principal decentralised advanced money, as the framework works without a national bank or single chairman. The system is distributed and exchanges occur between clients specifically using cryptography, without a delegate. </p>
                <p>Bitcoins are made as a reward for a procedure known as mining. They can be traded for different monetary standards, items, and administrations.
Value of all the cryptocurrencies are directly influenced according to the value of Bitcoin.</p>
            
            
          </div>   
        </div>
        <div class="col-sm-6">
          <div class="market-chart">
            <div class="btcwdgt-chart">
            </div>
          </div> 
        </div>  
      </div>
    </div>
  </div>-->
<!-- end market  -->
  </section>
<!--  end news -->