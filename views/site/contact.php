<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

//$this->title = 'Contact';
//$this->params['breadcrumbs'][] = $this->title;
?>

<!--  page banner -->
  <section id="page-banner" class="page-banner-main-block" style="background-image: url('/images/bg/contact_banner.png')"> 
    <!--<div class="overlay-bg"></div>-->
    <div class="container">
      <h1 class="page-banner-heading"></h1>
    </div>
  </section>
<!--  end page banner  -->
<!--  breadcrumb -->
  <section class="breadcrumb-block">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-sm-7">
          <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li>Contact</li>
          </ol>
        </div>
        <div class="col-md-4 col-sm-5">
          <div class="breadcrumb-btn">
            <a href="contact.html" class="btn btn-default">Get In Touch</a>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- end breadcrumb -->
<!--  map -->
  <!--<div id="location" class="contact-map"></div>-->
<!--  end map -->
<!-- contact  -->


<?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Thank you for contacting us. We will respond to you as soon as possible.
        </div>

        <p>
            Note that if you turn on the Yii debugger, you should be able
            to view the mail message on the mail panel of the debugger.
            <?php if (Yii::$app->mailer->useFileTransport): ?>
                Because the application is in development mode, the email is not sent but saved as
                a file under <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
                Please configure the <code>useFileTransport</code> property of the <code>mail</code>
                application component to be false to enable email sending.
            <?php endif; ?>
        </p>

    <?php else: ?>

<!--        <p>
            If you have business inquiries or other questions, please fill out the following form to contact us.
            Thank you.
        </p>-->
        
        
  <section id="contact" class="contact-page-main-block">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <div class="contact-section">
            <h5 class="contact-heading">Get in touch</h5>
            <p>If you have business inquiries or other questions, please fill out the following form to contact us.
            Thank you.</p>
          </div>
          <!--<form class="contact-form" method="post" action="contact-us.php">-->
           <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?> 
            <div class="row">
              <div class="col-sm-6">          
                <div class="form-group">
<!--                  <label>Your Name <span>*</span></label>
                  <input type="text" class="form-control" id="name-contact" placeholder="" required>-->
<?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
                </div>
                <div class="form-group">
<!--                  <label>Your Phone Number</label>
                  <input type="text" class="form-control" id="phone-contact" placeholder="">-->
<?= $form->field($model, 'email') ?>
                </div>
              </div>
<!--              <div class="col-sm-6">
                <div class="form-group">
                  <label>Your Email <span>*</span></label>
                  <input type="email" class="form-control" id="email-contact" placeholder="" required>
                </div>          
                <div class="form-group">
                  <label>Company Name</label>
                  <input type="text" class="form-control" id="cmy-name" placeholder="">
                </div>
              </div>-->
              <div class="col-sm-12">
                <div class="form-group">
<!--                  <label>Subject</label>
                  <input type="text" class="form-control" id="subject-contact" placeholder="">-->
<?= $form->field($model, 'subject') ?>
                </div>
                <div class="form-group">
<!--                  <label>Your Message</label>
                  <textarea class="form-control" id="message-contact" placeholder=""></textarea>-->
 <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
               <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ]) ?>
                </div>
                
              </div>
            </div>
            <!--<button type="submit" class="btn btn-default">Send Message</button>-->
               <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
          <!--</form>--> 
          
           <?php ActiveForm::end(); ?>
        </div>
          <?php endif; ?>
        <div class="col-sm-4">
          <div class="address-block">
            <div class="address-top-block">
              <h5 class="address-heading">Industrial Contacts</h5>
            </div>
            <div class="address-dtl-block">
              <h5 class="address-dtl-heading">Tradeoclub pvt.ltd</h5>
              <ul>
                <li>140 W 106th Street, New York, NY 10025 USA.</li>
                <li><span>T</span>  <a href="tel:">+1 (201)590-7038</a></li>
                <li><span>E</span>  <a href="mailto:">info@tradeoclub.com</a></li>
              </ul>
              <!--<a href="#">View on Google Map</a>-->
            </div>
            <hr>
<!--            <div class="address-dtl-block">
              <h5 class="address-dtl-heading">Industrial Solution Inc.</h5>
              <ul>
                <li>516 Newton Street Avenue, Stanford, England</li>
                <li>T  <a href="tel:">+ 215 623 7532</a></li>
                <li>E  <a href="mailto:">info@tradeoclub.com</a></li>
              </ul>
              <a href="#">View on Google Map</a>
            </div>-->
            <hr>
            <div class="address-arrow">
              <ul>                
                <li><a href="#"><i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-angle-up" aria-hidden="true"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- end contact -->

<!-- footer top  -->
  <section id="footer-top" class="footer-top-main-block marginbtm100">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 pad-0">
          <div class="footer-map">
            <div class="row">
              <div class="col-xs-4">
                <div class="footer-map-img">
                  <img src="/images/footer-map.png" class="img-responsive" alt="footer-map">
                </div>
              </div>
              <div class="col-xs-8">
                <div class="footer-map-dtl">
                  <h5 class="footer-map-heading">100 + Support Desk</h5>
                  <p>Our solutions used in over 200 + countries of million customers.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
         <div class="col-sm-6 pad-0">
          <div class="footer-subscribe">
            <form id="subscribe-form" class="subscribe-form">
              <div class="form-group">
                <label>Sign up our newsletter for Keep up to date.</label>
                <input type="email" class="form-control" id="mc-email" placeholder="Your Email Address">
              </div>
              <button type="submit" class="btn btn-default">Submit</button>
              <label for="mc-email" class="submit-label"></label>
            </form>
          </div>
        </div>
      </div>
    </div> 
  </section>
<!-- end footer top -->

 
<script>
  $(function($) {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "http://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
    document.body.appendChild(script);
  });
  function initialize(){
    var myLatLng = {lat: 25.3500, lng: 74.6333}; // Insert Your Latitude and Longitude For Footer Wiget Map
    var mapOptions = {
      center: myLatLng, 
      zoom: 15,
      disableDefaultUI: true,
      scrollwheel: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]     
    }
    // For Footer Widget Map
    var map = new google.maps.Map(document.getElementById("location"), mapOptions);
    var image = '/images/icons/map.png';
    var beachMarker = new google.maps.Marker({
      position: myLatLng, 
      map: map,   
      icon: image
    });    
  }
</script>
<!-- end jquery -->
</body>
<!--body end -->
</html>