        
<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Single Product';
$this->params['breadcrumbs'][] = $this->title;
?>


<section class="section main-container">
<div class="container">    
    <div class="row">    
    	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        	<div class="content">
		

<div id="product-96" class="post-96 product type-product status-publish has-post-thumbnail product_cat-aflower first instock sale shipping-taxable purchasable product-type-simple">

    <div class="row" style="margin-top:40px; margin-bottom: 20px">
        <div class="col-md-6">

	<figure class="woocommerce-product-gallery__wrapper">
		
            <!--<img src="/images/new2-180x180.jpg" alt="sale image">-->    
            <div data-thumb="images/new2-180x180.jpg" class="woocommerce-product-gallery__image">
                <a data-rel="prettyPhoto[product]" href="images/new2.jpg"><img width="600" height="600" src="images/new2-600x600.jpg" class="attachment-shop_single size-shop_single wp-post-image" alt="" title="" data-src="images/new2.jpg" data-large_image="images/new2.jpg" data-large_image_width="1000" data-large_image_height="700" srcset="images/new2-600x600.jpg 600w, images/new2-150x150.jpg 150w, images/new2-180x180.jpg 180w, images/new2-300x300.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" /></a>
            </div>
                
      
        </figure>
</div>
        
<div class="col-md-6">
	<div class="summary entry-summary">

		<h1 class="product_title entry-title">White Lily</h1>
	<div class="woocommerce-product-rating">
		<div class="star-rating"><span style="width:100%">Rated <strong class="rating">5.00</strong> out of 5 based on <span class="rating">2</span> customer ratings</span></div>		<a href="#reviews" class="woocommerce-review-link" rel="nofollow">(<span class="count">2</span> customer reviews)</a>	</div>

                <p class="price" style="padding: 4px;font-size: 29px;font-weight:  bold;"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>16.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>12.00</span></ins></p>
<div class="woocommerce-product-details__short-description">
    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
</div>

	
	<form class="" method="post" enctype='multipart/form-data'>
			
	
		<button type="submit" name="add-to-cart" value="96" class="btn">Buy Now</button>

			</form>

	

<!--<div class="yith-wcwl-add-to-wishlist add-to-wishlist-96">
		    <div class="yith-wcwl-add-button show" style="display:block">

	        
<a href="index07bb.html?add_to_wishlist=96" rel="nofollow" data-product-id="96" data-product-type="simple" class="add_to_wishlist" >
        Add to Wishlist</a>
<img src="../../wp-content/plugins/yith-woocommerce-wishlist/assets/images/wpspin_light.gif" class="ajax-loading" alt="loading" width="16" height="16" style="visibility:hidden" />
	    </div>

	    <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
	        <span class="feedback">Product added!</span>
	        <a href="../../wishlist/index.html" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
	        <span class="feedback">The product is already in the wishlist!</span>
	        <a href="../../wishlist/index.html" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div style="clear:both"></div>
	    <div class="yith-wcwl-wishlistaddresponse"></div>
	
</div>-->

<div class="clear"></div>

<!--<div class="product_meta">

	
	
	<span class="posted_in">Category: <a href="../../product-category/aflower/index.html" rel="tag">Flower</a></span>
	
	
</div>-->


	</div><!-- .summary -->

    </div>
        </div>
    </div>
        
        
	
	<div class="woocommerce-tabs wc-tabs-wrapper">
<!--		<ul class="tabs wc-tabs" role="tablist">
							<li class="description_tab" id="tab-title-description" role="tab" aria-controls="tab-description">
					<a href="#tab-description">Description</a>
				</li>
							<li class="reviews_tab" id="tab-title-reviews" role="tab" aria-controls="tab-reviews">
					<a href="#tab-reviews">Reviews (2)</a>
				</li>
					</ul>-->
					<div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab" id="tab-description" role="tabpanel" aria-labelledby="tab-title-description">
				
  <h2>Description</h2>

<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
			</div>
					<div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--reviews panel entry-content wc-tab" id="tab-reviews" role="tabpanel" aria-labelledby="tab-title-reviews">
				<div id="reviews" class="woocommerce-Reviews">
	
                                    
                                    
                              

	
	<div class="clear"></div>
</div>
			</div>
			</div>


	<section class="related products">

		<h2>Related products</h2>

		<div class="list-wrapper clearfix">
<div class="products row">

			
				<div class="col-md-4 col-sm-4 post-105 product type-product status-publish has-post-thumbnail product_cat-aflower first instock shipping-taxable purchasable product-type-simple">
	<div class="list-item">
	            <div class="entry">
    	<a href="../sun-flower/index.html"><img src="images/best3-1000x1000_c.jpg" alt="best3"></a>
        <div class="price-with-title">
        	<h4>Sun Flower</h4>
        	                <p class="product-price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>17.00</span></p>
                    </div>
        <div class="magnifier">
            <div class="price-with-title hover-des">
                        <a href="#" class="button yith-wcqv-button button-quick-view" data-product_id="105"><i class="fa fa-eye"></i></a>
            			<a rel="nofollow" href="index3f1f.html?add-to-cart=105" data-quantity="1" data-product_id="105" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart"><i class="fa fa-shopping-cart"></i></a>            
<div class="yith-wcwl-add-to-wishlist add-to-wishlist-105">
		    <div class="yith-wcwl-add-button show" style="display:block">

	        
<a href="index3486.html?add_to_wishlist=105" rel="nofollow" data-product-id="105" data-product-type="simple" class="add_to_wishlist" >
        Add to Wishlist</a>
<img src="../../wp-content/plugins/yith-woocommerce-wishlist/assets/images/wpspin_light.gif" class="ajax-loading" alt="loading" width="16" height="16" style="visibility:hidden" />
	    </div>

	    <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
	        <span class="feedback">Product added!</span>
	        <a href="../../wishlist/index.html" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
	        <span class="feedback">The product is already in the wishlist!</span>
	        <a href="../../wishlist/index.html" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div style="clear:both"></div>
	    <div class="yith-wcwl-wishlistaddresponse"></div>
	
</div>

<div class="clear"></div>			
            </div>
        </div><!-- end magnifier -->
    </div>
        </div>
</div>

			
				<div class="col-md-4 col-sm-4 post-107 product type-product status-publish has-post-thumbnail product_cat-aflower  instock shipping-taxable purchasable product-type-simple">
	<div class="list-item">
	            <div class="entry">
    	<a href="../pink-rose/index.html"><img src="images/best11-1000x1000_c.jpg" alt="best11"></a>
        <div class="price-with-title">
        	<h4>Pink Rose</h4>
        	                <p class="product-price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>18.00</span></p>
                    </div>
        <div class="magnifier">
            <div class="price-with-title hover-des">
                        <a href="#" class="button yith-wcqv-button button-quick-view" data-product_id="107"><i class="fa fa-eye"></i></a>
            			<a rel="nofollow" href="indexeea2.html?add-to-cart=107" data-quantity="1" data-product_id="107" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart"><i class="fa fa-shopping-cart"></i></a>            
<div class="yith-wcwl-add-to-wishlist add-to-wishlist-107">
		    <div class="yith-wcwl-add-button show" style="display:block">

	        
<a href="indexbcc3.html?add_to_wishlist=107" rel="nofollow" data-product-id="107" data-product-type="simple" class="add_to_wishlist" >
        Add to Wishlist</a>
<img src="../../wp-content/plugins/yith-woocommerce-wishlist/assets/images/wpspin_light.gif" class="ajax-loading" alt="loading" width="16" height="16" style="visibility:hidden" />
	    </div>

	    <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
	        <span class="feedback">Product added!</span>
	        <a href="../../wishlist/index.html" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
	        <span class="feedback">The product is already in the wishlist!</span>
	        <a href="../../wishlist/index.html" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div style="clear:both"></div>
	    <div class="yith-wcwl-wishlistaddresponse"></div>
	
</div>

<div class="clear"></div>			
            </div>
        </div><!-- end magnifier -->
    </div>
        </div>
</div>

			
				<div class="col-md-4 col-sm-4 post-106 product type-product status-publish has-post-thumbnail product_cat-aflower last instock shipping-taxable purchasable product-type-simple">
	<div class="list-item">
	            <div class="entry">
    	<a href="../jasmine/index.html"><img src="images/best2-1000x1000_c.jpg" alt="best2"></a>
        <div class="price-with-title">
        	<h4>Jasmine</h4>
        	                <p class="product-price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>20.00</span></p>
                    </div>
        <div class="magnifier">
            <div class="price-with-title hover-des">
                        <a href="#" class="button yith-wcqv-button button-quick-view" data-product_id="106"><i class="fa fa-eye"></i></a>
            			<a rel="nofollow" href="indexa07a.html?add-to-cart=106" data-quantity="1" data-product_id="106" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart"><i class="fa fa-shopping-cart"></i></a>            
<div class="yith-wcwl-add-to-wishlist add-to-wishlist-106">
		    <div class="yith-wcwl-add-button show" style="display:block">

	        
<a href="indexd1a9.html?add_to_wishlist=106" rel="nofollow" data-product-id="106" data-product-type="simple" class="add_to_wishlist" >
        Add to Wishlist</a>
<img src="../../wp-content/plugins/yith-woocommerce-wishlist/assets/images/wpspin_light.gif" class="ajax-loading" alt="loading" width="16" height="16" style="visibility:hidden" />
	    </div>

	    <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
	        <span class="feedback">Product added!</span>
	        <a href="../../wishlist/index.html" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div class="yith-wcwl-wishlistexistsbrowse hide" style="display:none">
	        <span class="feedback">The product is already in the wishlist!</span>
	        <a href="../../wishlist/index.html" rel="nofollow">
	            Browse Wishlist	        </a>
	    </div>

	    <div style="clear:both"></div>
	    <div class="yith-wcwl-wishlistaddresponse"></div>
	
</div>

<div class="clear"></div>			
            </div>
        </div><!-- end magnifier -->
    </div>
        </div>
</div>

			
		</div>
</div>

	</section>


</div><!-- #product-96 -->

    	</div>
</div>

	     
    </div>
    
</div>
<!-- Content Wrap -->

            </section>