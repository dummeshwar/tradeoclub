        
<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Single Product';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .home-banner { display:none; }
    .paddingBtn { padding: 10px; top: 10px; position: relative; }
</style>

<section class="section main-container">
    <div class="container">    
        <div class="row">    
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div class="content">


                    <div id="product-96" class="post-96 product type-product status-publish has-post-thumbnail product_cat-aflower first instock sale shipping-taxable purchasable product-type-simple">

                        <div class="row" style="margin-top:40px; margin-bottom: 20px">
                            <div class="col-md-6">

                                <figure class="woocommerce-product-gallery__wrapper">

<!--<img src="/images/new2-180x180.jpg" alt="sale image">-->    
                                    <div class="woocommerce-product-gallery__image">
                                        <img width="450" height="450" src="/uploads/package/<?= $package->image ?>" class="attachment-shop_single size-shop_single wp-post-image" alt="" title="" data-large_image_width="1000" data-large_image_height="700" sizes="(max-width: 600px) 100vw, 600px" />
                                    </div>


                                </figure>
                            </div>

                            <div class="col-md-6">
                                <div class="summary entry-summary">

                                    <h1 class="product_title entry-title"><?= $package->name ?></h1>
                                    <div class="woocommerce-product-rating">
                                        <div class="star-rating"><span style="width:100%">Rated <strong class="rating">4.8</strong> out of 5 </span></div>		<a href="#" class="woocommerce-review-link" rel="nofollow">(<span class="count">2456</span> customer reviews)</a>	</div>

                                    <p class="price" style="padding: 4px;font-size: 29px;font-weight:  bold;"><ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"> $ </span><?= $package->price ?></span></ins></p>
                                    <div class="woocommerce-product-details__short-description">
                                        <p><?= $package->description ?></p>
                                    </div>

                                    <div class="clear"></div>
                                    
                                    <!--<h3 class="product_title entry-title">Available Payment Modes:</h3>-->
                                    
                                    

<!--                                    <div class="placetop layout" style="margin-bottom: 10px;">
                                        	<div class="container">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="col-sm-4 col-md-2" style="border: 2px solid #eeeeee;padding: 7px;">
                                                    <img class="logo" src="/images/bitcoin.png" alt="bitcoin" />
                                                </div>
                                                <div class="col-sm-4 col-md-9" style="margin: 0px 10px 10px 10px;font-size: 19px;font-weight: bold;border: 2px solid #eeeeee;padding: 4px;">
                                                    1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="placetop layout" style="margin-bottom: 10px;">
                                        	<div class="container">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="col-sm-4 col-md-2" style="border: 2px solid #eeeeee;padding: 7px;">
                                                    <img class="logo" src="/images/payeer.png" alt="payeer" />
                                                </div>
                                                <div class="col-sm-4 col-md-9" style="margin: 0px 10px 10px 10px;font-size: 19px;font-weight: bold;border: 2px solid #eeeeee;padding: 4px;">
                                                    P12345678
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="placetop layout" style="margin-bottom: 10px;">
                                        	<div class="container">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="col-sm-4 col-md-2" style="border: 2px solid #eeeeee;padding: 7px;">
                                                    <img class="logo" src="/images/perfectmoney.png" alt="perfectmoney" />
                                                </div>
                                                <div class="col-sm-4 col-md-9" style="margin: 0px 10px 10px 10px;font-size: 19px;font-weight: bold;border: 2px solid #eeeeee;padding: 4px;">
                                                    P12345678
                                                </div>
                                            </div>
                                        </div>
                                    </div>-->

                                    <form class="form-horizontal row" method="post">
                                        <div class="col-sm-12">
                                            <?php if ($is_user_has_sufficient_fund) { ?>
                                                <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                                                <?= Html::a('Confirm Booking', Url::toRoute(['/site/confirm-booking', 'id' => base64_encode($package->id)]), ['data-method' => 'post', 'class' => "btn-primary paddingBtn"]) ?>
                                            <?php } else { ?>
                                                <div class="alert alert-danger">
                                                    <strong>Notice!</strong> You don't have sufficient balance to purchase this package. Please recharge your cash wallet.
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </form>
                                    <br />
                                    <div class="clear"></div>

                                </div><!-- .summary -->

                            </div>
                        </div>
                    </div>

                </div><!-- #product-96 -->

            </div>
        </div>


    </div>

</div>
<!-- Content Wrap -->

</section>