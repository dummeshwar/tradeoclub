<?php

use borales\extensions\phoneInput\PhoneInput;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Url;
use yii\helpers\Utils;
use yii\widgets\LinkPager;

$this->title = 'Packages';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
/*    .page-title1.little-pad {
    background: url(/images/blog.jpg);
}*/
.bgimg{
background: url(/images/blog.jpg);
}
.sociallink{
   background-color: #fed602;
}

    </style>
<section class=" bgimg section lb page-title1 little-pad">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-big-title clearfix text-center">
                	                    <div class="title-banner-bred">
                        <div class="title-subtitle-content">
                            <h3>NurseryPlant Shop</h3>
                            <p>NurseryPlant subtitle goes here</p>
                        </div>                    
                                                    <div class="breadcrumb-content">
                                <ul class="breadcrumb">
			<li><a href="/">Home</a></li><li class="active">Product</li>
		</ul>                            </div>
                                            </div>
                                    </div>
            </div>
        </div>
    </div>
</section>

<div id="wrapper">      
	<!--<section class="section main-container ">-->
		<div class="container">    
			<div class="row">    
				<!--<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">-->
					<div class="content">
						<h1 class="page-title"></h1>
						<div class="list-wrapper clearfix">
							
							<?php if (!empty($packages)) { ?>
							<!-- Starts here -->
								<?php foreach($packages as $package): ?>
								<div class="col-md-4 col-sm-4">   
                                                                    <div class="products row" style="padding: 20px;">
									<div class="list-item">
										<!-- <div class="ribbon"><span>Sale!</span></div> -->
											<div class="entry">
												<?php $link_to_package = Url::to(['site/package-detail', 'id' => base64_encode($package->id)]); ?>
												<?php if (!empty($package->image)) { ?>
													<a href="<?= $link_to_package; ?>">
														<?= Html::img(Yii::$app->params['packageImagesUpload'] . $package->image , ['alt' => $package->name, 'title' => $package->name, 'class' => "img-responsive"]) ?>
													</a>
												<?php } else {?>
													<a href="<?= $link_to_package; ?>">
														<?= Html::img(Yii::$app->params['packagePlaceHolderImage'] , ['alt' => $package->name, 'title' => $package->name, 'class' => "img-responsive"]) ?>
													</a>
												<?php } ?>
												<div class="price-with-title">
													<h4><?= $package->name ?></h4>
													<p class="product-price">
													<!-- <del><span class="woocommerce-Price-amount amount">
													<span class="woocommerce-Price-currencySymbol">$ </span>15.00</span></del> -->
													<ins><span class="woocommerce-Price-amount amount">
													<span class="woocommerce-Price-currencySymbol">$ </span><?= $package->price ?></span></ins></p>
												</div>
												<div class="magnifier">
													<div class="price-with-title hover-des">
														<!-- <a href="#" class="button yith-wcqv-button button-quick-view" data-product_id="115"><i class="fa fa-eye"></i></a> -->
														<a rel="nofollow" href="<?= $link_to_package; ?>" data-quantity="1" data-product_id="115" data-product_sku="" class="button1 product_type_simple add_to_cart_button ajax_add_to_cart">Buy Now</a>            		
													</div>
												</div><!-- end magnifier -->
											</div>
										</div>
									</div>	
								</div>
								<?php endforeach; ?>
							<?php } ?>
							</div>
						</div>
					<!--</div>-->
				</div>
			</div>
		</div>
	<!--</section>-->
</div><!-- end wrapper -->
