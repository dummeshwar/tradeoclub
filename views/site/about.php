<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\FrontPageAsset;

use yii\helpers\Url;

?>
<style>
    p{
        text-align: justify;
        font-weight:bold;
    }
    h4{
        font-weight: bold;
    }
    </style>

<!--  page banner -->
  <section id="page-banner" class="page-banner-main-block" style="background-image: url('/images/bg/page-banner.jpg')"> 
    <div class="overlay-bg"></div>
    <div class="container">
      <h1 class="page-banner-heading">About Us</h1>
    </div>
  </section> 
<!--  end page banner  -->

 <section class="breadcrumb-block">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li>About Us</li>
          </ol>
        </div>
        <div class="col-sm-4">
          <div class="breadcrumb-btn">
             <?= Html::a('Get In Touch', ['site/contact'], ['class' => 'btn btn-default']) ?>
          </div>
        </div>
      </div>
    </div>
  </section>

<!-- about us  -->
  <section id="about-page" class="about-page-main-block">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="about-page-block">
            <h2 class="about-page-heading">About Us</h2>
            <p style="text-indent: 50px;">Tradeoclub is a self-running organization which came into action in January 2019 with a vision to become the 1st ever bot based crypto trading platform. The headquarter is situated in New York City, USA. Our first moto is members satisfaction through profit maximization and global leadership.</p>
            <p>As cryptocurrency has evolved so dramatically in recent years we decided to grab this opportunity to make our life better along with our valuable members.</p>
           
            <br/>
            <h4 class="about-page-heading">What we do here?</h4>
            <p>We build wealth from the trending platform I;e cryptocurrency. Bitcoin(BTC), Ethereum(ETH), Dash coin(DASH), Ripple(XRP), Litecoin(LTC) are some of the popular cryptos in the current market, we find trading is  the most profitable way to get involved in this crypto market as mining has not shown satisfactory results in past.</p>
           
            <br/>
            <h4 class="about-page-heading">How we do it?</h4>
            <p>Bot based trading is the finest platform in trading wardrobe. We integrated our latest technology with that platform to develop the most powerful bot in crypto trading named as Tradeobot 2.0, 3.0 & 5.0. As we know bot does not get tired, 2.0 works 24x7, 6 days a week to achieve optimum performance in giving wealth in our pockets. The Profit percentage ranges from 20% according to the member's level.</p>
            
            <p>Our club members consistently share a capacity for rigorous thinking and the kind of visionary outlook that has helped us lead our club into the thick of Crypto revolution. This is also helping us to define a new generation of leaders poised to create a way of commerce and a currency system worth of these dynamic times.</p>
          </div>
        </div>        
        <div class="col-md-6">
          <div class="about-page-img">
            <img src="/images/about/about-img-1.jpg" class="img-responsive" alt="about">
          </div>
          <div class="row">
            <div class="col-xs-6">
              <div class="about-page-img">
                <img src="/images/about/about-img-2.jpg" class="img-responsive" alt="about">
              </div>
            </div>
            <div class="col-xs-6">
              <div class="about-page-img">
                <img src="/images/about/about-img-3.jpg" class="img-responsive" alt="about">
              </div>
            </div>
          </div>
        </div>
      </div>
        <br/>
          <div class="row">
              <div class="col-md-3" >
              <img src="/images/owner.jpg" class="img-responsive" alt="owner">
              <br/>
              <div><strong>Garry Cox (Owner and Senior trader) </strong></div>
          </div> 
              <div class="col-md-9" >
                  <div class="about-page-block" style="margin-top:40px;">
                      <p style="text-indent: 50px;">When considering organizational training as it relates to a Bitcoin integration strategy, the first thing many leaders might focus on is Bitcoin's technical aspects. While technical details and factual background are certainly high value and need to be carefully considered, often the most substantial initiatives is not technical but cultural and psychological.</p>
            
            <p>In 2017, when many Bitcoin startups were focusing on how to get the digital currency into the hands of individual customers, Garry Cox saw the opportunity in Bitcoin trading to generate wealth.</p>
            <p>Garry Cox has his hands in Forex trading as well as Stock trading.</p>
            <p>2011 - till present</p>
            <p>He realised that Cryptocurrency trading can be so profitable if it is done with proper strategy and team. Tradeoclub has a huge background in the field of Cryptocurrency trading and investment and it is the benchmark for the future.</p>
            <p>Tradeoclub officially entered the market in 2019 January and its headquarter is situated in New York City.</p>
            <p>There is a team of highly qualified and experienced traders who make the strategy to get profit even from a bear market.</p>
            <p><strong>The basic trading strategy is:</strong></p>
            
            <p> : Buy low, Sell high. Saying that is 100 times easier than actually doing it. We'll be looking at some ways we can maximize this possibility.</p>
            <p>: Short Term Trading (2 days or less) </p>
            <p>: Long Term Trading (2 days or more)</p>
            <p><strong>We'll also be looking at different approaches to trading.</strong></p>
            <p>: Moving Averages Crossover Trading
            <p>: Momentum Trading</p>
            <p>: Technical Trading</p>
            <p>: Fundamental Trading</p>
            <p>There also is not an easy route. If you want to make good or even great trades you must put in the time and effort it takes to gather the information, look at the patterns, follow the news and finally make the trade. But even when all of those things fall into place you still can make a horrible trade. So Tradeoclub team keeps eye on every scenario and make the best choice so members can get the maximum benefit. </p>
                  </div></div></div>
            <br/>
            <br/>
            <div class="row">
              <div class="col-md-12" >
                  <div class="about-page-block" >
            
                  </div>
              </div>
              
          
          
          
          
          
          
      </div>
    </div>
  </section>
<!-- end about us -->


  <!-- market  -->
  <div id="market" class="market-main-block marginbtm100" style="background-image: url('/images/bg/market-bg.jpg');">
    <div class="overlay-bg"></div>
    <div class="container">
      <div class="row">
        <div class="col-sm-6"> 
          <div class="market-block">
            <h3 class="market-heading">About Bitcoin</h3>  
            <p>Bitcoin is the mother of all Cryptocurrencies.Bitcoin is digital money and overall instalment framework. It is the principal decentralised advanced money, as the framework works without a national bank or single chairman. The system is distributed and exchanges occur between clients specifically using cryptography, without a delegate. </p>
                <p>Bitcoins are made as a reward for a procedure known as mining. They can be traded for different monetary standards, items, and administrations.
Value of all the cryptocurrencies are directly influenced according to the value of Bitcoin.</p>
            
            <!--<a href="#" class="btn btn-default">Read More</a>-->
          </div>   
        </div>
        <div class="col-sm-6">
          <div class="market-chart">
            <div class="btcwdgt-chart">
            </div>
          </div> 
        </div>  
      </div>
    </div>
  </div>
<!-- end market  -->