

	
<!-- Mirrored from theme-stall.com/nurseryplant/demos/my-account/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 08 May 2018 17:53:41 GMT -->
<html>


<body>

<section class="section lb page-title1 little-pad" style="margin-top: 79px;">
    <div class="container1">
        <div class="row">
            <div class="col-md-12">
                <div class="section-big-title clearfix text-center">
                	                    <div class="title-banner-bred">
                        <div class="title-subtitle-content">
                            <h3>My Account</h3>
                            <p></p>
                        </div>                    
                                                    <div class="breadcrumb-content">
                                <ul class="breadcrumb">
			<li><a href="../index.html">Home</a></li><li class="active">My Account</li>
		</ul>                            </div>
                                            </div>
                                    </div>
            </div>
        </div>
    </div>
</section>        
        <section>

<div class="container">    
    <div class="row">    
    	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        	
	<div class="post-padding clearfix">
	<div class="woocommerce">



<!--<div class="u-columns col2-set" id="customer_login">-->

<div class="col-md-6">


		<h2>Login</h2>

		<form  method="post">

			
			<p class=" ">
                            <label for="username">Username or email address <span class="required">*</span></label> <br/>
                            
				<input type="text" class="inputbox" name="username" id="username" value="" />
			</p>
			<p class=" ">
				<label for="password">Password <span class="required">*</span></label> <br/>
				<input class="inputbox" type="password" name="password" id="password" />
			</p>

			
			<p class="form-row">
						<input type="submit" class="woocommerce-Button button" name="login" value="Login" />
				<label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline"> <br/>
					<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span>Remember me</span>
				</label>
			</p>
			<p class="woocommerce-LostPassword lost_password">
				<a href="lost-password/index.html">Lost your password?</a>
			</p>

			
		</form>


	</div>

	<div class="col-md-6">

		<h2>Register</h2>

		<form method="post" class="register">

			
			
				<p class=" ">
					<label for="reg_username">Username <span class="required">*</span></label><br/>
					<input type="text" class="inputbox" name="username" id="reg_username" value="" />
				</p>

			
			<p class=" ">
				<label for="reg_email">Email address <span class="required">*</span></label><br/>
				<input type="email" class="inputbox" name="email" id="reg_email" value="" />
			</p>

			
				<p class=" ">
					<label for="reg_password">Password <span class="required">*</span></label><br/>
					<input type="password" class="inputbox" name="password" id="reg_password" />
				</p>

			
			<!-- Spam Trap -->
			<div style="left: -999em; position: absolute;"><label for="trap">Anti-spam</label><input type="text" name="email_2" id="trap" tabindex="-1" autocomplete="off" /></div>

			
			<p class="woocomerce-FormRow form-row">
				<input type="hidden" id="woocommerce-register-nonce" name="woocommerce-register-nonce" value="fdda74a26d" /><input type="hidden" name="_wp_http_referer" value="/nurseryplant/demos/my-account/" />				<input type="submit" class="woocommerce-Button button" name="register" value="Register" />
			</p>

			
		</form>

	</div>

<!--</div>-->

</div>
    
        </div>    

</div>

	     
    </div>
    
</div>
<!-- Content Wrap -->

            </section>
                        
                        	        
                                            
               
        </div><!-- end wrapper -->
        <div class="dmtop"><a href="#"><i class="fa fa-angle-up"></i></a></div>
        
<div id="yith-quick-view-modal">

	<div class="yith-quick-view-overlay"></div>

	<div class="yith-wcqv-wrapper">

		<div class="yith-wcqv-main">

			<div class="yith-wcqv-head">
				<a href="#" id="yith-quick-view-close" class="yith-wcqv-close">X</a>
			</div>

			<div id="yith-quick-view-content" class="woocommerce single-product"></div>

		</div>

	</div>

</div>				

  
	</body>

<!-- Mirrored from theme-stall.com/nurseryplant/demos/my-account/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 08 May 2018 17:53:41 GMT -->
</html>