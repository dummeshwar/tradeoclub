<?php

use borales\extensions\phoneInput\PhoneInput;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Url;
use yii\helpers\Utils;
use yii\widgets\LinkPager;

?>

<body id="bg">
	
    <div class="page-wraper">
        	
        

        
        <!-- CONTENT START -->
        <div class="page-content">
        
            <!-- INNER PAGE BANNER -->
            <div class="wt-bnr-inr overlay-wraper" style="background-image:url(images/banner/product-banner.jpg);">
            	<div class="overlay-main bg-black opacity-07"></div>
                <div class="container">
                    <div class="wt-bnr-inr-entry">
                        <h1 class="text-white">Shop</h1>
                    </div>
                </div>
            </div>
            <!-- INNER PAGE BANNER END -->
            
            <!-- BREADCRUMB ROW -->                            
            <div class="bg-gray-light p-tb20">
            	<div class="container">
                    <ul class="wt-breadcrumb breadcrumb-style-2">
                        <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
                        <li>Shop</li>
                    </ul>
                </div>
            </div>
            <!-- BREADCRUMB ROW END -->
            
            <!-- SECTION CONTENT START -->
            <div class="section-full p-t80 p-b50">
                <div class="container">
                    <div class="section-content">
                    	<div class="row">
                            <!-- SIDE BAR END --> 
                            <div class="col-md-12">                   
                                <!-- TITLE START -->
                                <div class="p-b10">
                                    <h2 class="text-uppercase">Our Products</h2>
                                    <div class="wt-separator-outer m-b30">
                                        <div class="wt-separator bg-primary"></div>
                                    </div>
                                </div>
                                <!-- TITLE END -->
                                
                                <div class="row">
                                    <!-- COLUMNS 1 -->
                                    <?php if (!empty($packages)) { ?>
							<!-- Starts here -->
								<?php foreach($packages as $package): ?>
                                    <div class="col-md-4 col-sm-4 col-xs-6 col-xs-100pc m-b30">
                                        <div class="wt-box wt-product-box p-a10 bg-gray">
                                            <div class="wt-thum-bx wt-img-overlay1 wt-img-effect zoom">
                                                <!--<img src="images/products/pic-1.jpg" alt="">-->
                                                
                                                <?php $link_to_package = Url::to(['site/package-detail', 'id' => base64_encode($package->id)]); ?>
                                                <?php if (!empty($package->image)) { ?>
                                                        <a href="<?= $link_to_package; ?>">
                                                                <?= Html::img(Yii::$app->params['packageImagesUpload'] . $package->image , ['alt' => $package->name, 'title' => $package->name, 'class' => "img-responsive"]) ?>
                                                        </a>
                                                <?php } else {?>
                                                        <a href="<?= $link_to_package; ?>">
                                                                <?= Html::img(Yii::$app->params['packagePlaceHolderImage'] , ['alt' => $package->name, 'title' => $package->name, 'class' => "img-responsive"]) ?>
                                                        </a>
                                                <?php } ?>
                                                
                                                <div class="overlay-bx">
                                                    <div class="overlay-icon">
<!--                                                        <a href="javascript:void(0);">
                                                            <i class="fa fa-cart-plus wt-icon-box-xs"></i>
                                                        </a>-->
<!--                                                        <a class="mfp-link" href="javascript:void(0);">
                                                            <i class="fa fa-heart wt-icon-box-xs"></i>
                                                        </a>-->
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="wt-info  text-center">
                                                 <div class="p-a10 bg-white">
                                                    <h4 class="wt-title">
                                                        <!--<a href="javascript:;">One Product</a>-->
                                                        <a href="<?= $link_to_package; ?>"> <?= $package->name ?></a>
                                                    </h4>
                                                    <span class="price">
                                                        <del>
                                                             <span><span class="Price-currencySymbol">£</span>3.00</span>
         i                                               </del> 
                                                        <ins>
                                                            <span><span class="Price-currencySymbol">£</span><?= $package->price ?></span>
                                                        </ins>
                                                    </span>
                                                    <div class="rating-bx">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="p-t10">
                                                        <!--<button class="site-button button-sm font-weight-600" type="button">ADD TO CART</button>-->
                                                        <a class="site-button button-sm font-weight-600"  href="<?= $link_to_package; ?>">Buy Now</a> 
                                                    </div>
                                                    
                                                    
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                                        <?php endforeach; ?>
                                                        <?php } ?>
                                </div>
                                
                                <!-- ADD BLOCK -->
                                <div class="p-tb30">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="wt-box pro-banner">
                                                <img src="images/add/pic1.jpg" alt="">
                                                <div class="pro-banner-disc p-a10 text-white">
                                                    <h2 class="text-uppercase m-a0 m-b10">Best time to buy</h2>
                                                    <h4 class="m-a0 m-b10">Our Product</h4>
                                                    <h3 class="text-uppercase m-a0 m-b10">UP TO</h3>
                                                    <h5 class="text-uppercase m-a0 m-b10">10% Cashback</h5>
                                                    <a href="#" class="site-button button-sm ">ADD TO CART </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="wt-box pro-banner">
                                                <img src="images/add/pic2.jpg" alt="">
                                                <div class="pro-banner-disc p-a10 text-white">
                                                    <h2 class="text-uppercase m-a0 m-b10">Best time to buy</h2>
                                                    <h4 class="m-a0 m-b10">Two Product</h4>
                                                    <h3 class="text-uppercase m-a0 m-b10">UP TO</h3>
                                                    <h5 class="text-uppercase m-a0 m-b10">40% Cashback</h5>
                                                    <a href="#" class="site-button button-sm">ADD TO CART</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ADD BLOCK -->
                                
                            </div>                        
                            <!-- SIDE BAR START -->
                       </div> 
                   </div>
                 </div>
             </div>
             <!-- SECTION CONTENT END -->
        
        </div>
        <!-- CONTENT END -->
        


        <!-- BUTTON TOP START -->
        <!--<button class="scroltop"><span class=" iconmoon-house relative" id="btn-vibrate"></span>Top</button>-->

         
        
    </div>
    

<!-- JAVASCRIPT  FILES ========================================= --> 
<script   src="js/jquery-1.12.4.min.js"></script><!-- JQUERY.MIN JS -->
<script   src="js/bootstrap.min.js"></script><!-- BOOTSTRAP.MIN JS -->

<script   src="js/bootstrap-select.min.js"></script><!-- FORM JS -->
<script   src="js/jquery.bootstrap-touchspin.min.js"></script><!-- FORM JS -->

<script   src="js/magnific-popup.min.js"></script><!-- MAGNIFIC-POPUP JS -->

<script   src="js/waypoints.min.js"></script><!-- WAYPOINTS JS -->
<script   src="js/counterup.min.js"></script><!-- COUNTERUP JS -->
<script   src="js/waypoints-sticky.min.js"></script><!-- COUNTERUP JS -->

<script  src="js/isotope.pkgd.min.js"></script><!-- MASONRY  -->

<script   src="js/owl.carousel.min.js"></script><!-- OWL  SLIDER  -->

<script   src="js/stellar.min.js"></script><!-- PARALLAX BG IMAGE   --> 
<script   src="js/scrolla.min.js"></script><!-- ON SCROLL CONTENT ANIMTE   --> 

<script   src="js/custom.js"></script><!-- CUSTOM FUCTIONS  -->
<script   src="js/shortcode.js"></script><!-- SHORTCODE FUCTIONS  -->
<script   src="js/switcher.js"></script><!-- SWITCHER FUCTIONS  -->
<script  src="js/jquery.bgscroll.js"></script><!-- BACKGROUND SCROLL -->
<script  src="js/tickerNews.min.js"></script><!-- TICKERNEWS-->



<!-- LOADING AREA START ===== -->
<div class="loading-area">
    <div class="loading-box"></div>
    <div class="loading-pic">
        <div class="cssload-container">
            <div class="cssload-dot bg-primary"><i class="fa fa-bitcoin"></i></div>
            <div class="step" id="cssload-s1"></div>
            <div class="step" id="cssload-s2"></div>
            <div class="step" id="cssload-s3"></div>
        </div>
    </div>
</div>
<!-- LOADING AREA  END ====== -->

</body>


