<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;


/* @var $this yii\web\View */
/* @var $searchModel app\models\MoneyTransferSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Money Transactions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="money-transfer-index table-responsive">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Send Money', ['transfer'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Transaction Id',
                'attribute' => 'transaction',
                'value' => 'transaction.transaction_id',
            ],

            [
                'label' => 'From wallet',
                'attribute' => 'fromWallet',
                'format'=>'raw',
                'filter'=>false,
                'value' => 'fromWallet.name',
            ],

            [
                'label' => 'To wallet',
                'attribute' => 'toWallet',
                'format'=>'raw',
                'filter'=>false,
                'value' => 'toWallet.name',
            ],

            [
                'label' => 'From User',
                'attribute' => 'fromUser',
                'value' => 'fromUser.username',
            ],

            [
                'label' => 'To User',
                'attribute' => 'toUser',
                'value' => 'toUser.username',
            ],
            [
                'header' => 'Amount (<i class="fa fa-usd">)',
                //'label' => 'Amount',
                'attribute' => 'amount',
                'format'=>'raw',
                'filter'=>false,
            ],
            [
                'label' => 'Comment',
                'attribute' => 'transaction',
                'value' => 'transaction.user_comment',
            ],

            // [
            //     'label' => 'Comment', 
            //     'attribute' => 'transaction',
            //     'format'=>'raw',
            //     'filter'=>false,
            //     'value' => 'transaction.comment',
            // ],

            // [
            //     'label' => 'Mode',
            //     'attribute' => 'transaction',
            //     'value' => 'transaction.transaction_mode',
            // ],


            [
                'attribute' => 'status',
                'format' => 'html',
                'filter'=> false,
                'value' => function($model, $key, $index) {
                    return ($model->status == 1) ? "Sucess" : "Failed";
                },
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Date on/before',
                'filter' => '<div class="drp-container input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>' .
                DateRangePicker::widget([
                    'name' => 'createdAt',
                    'model' => $searchModel,
                    'convertFormat' => true,
                    'attribute' => 'created_at',
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'Y-m-d',
                            'separator' => ' to ',
                        ],
                        'opens' => 'left'
                    ]
                ]) . '</div>'
            ],

        ],
    ]); ?>
</div>
