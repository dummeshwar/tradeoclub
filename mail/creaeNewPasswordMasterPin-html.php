
<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */
$createLink = Yii::$app->urlManager->createAbsoluteUrl(['site/create-new-password-masterpin', 'token' => $new_token]);
?>

    <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                      <td>
                        <div class="contentEditableContainer contentImageEditable">
                          <div class="contentEditable" align='left'>
                            <img class="banner" src="https://www.tradeoclub.com/mail_header.jpg" alt='header' data-default="placeholder" data-max-width='560' width='560' height='242' border="0" />
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td class='bgItem'>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                          <tr><td height='38'></td></tr>
                          <tr>
                            <td class="left_pad">
                              <div class="contentEditableContainer contentTextEditable">
                                <div class="contentEditable">
                                  <h2 style='font-size:38px;text-align:center; line-height:42px;'>Create Password & Key Pin</h2>
                                </div>
                              </div>
                            </td>
                          </tr>
                          <tr><td height='20'></td></tr>
                          <tr><td height='5' align='center'><hr style='width:117px; height:5px; background-color:#d2176e;border: none;'/></td></tr>
                          <tr><td height='20'></td></tr>
                          <tr>
                            <td class="left_pad">
                              <div class="contentEditableContainer contentTextEditable">
                                <div class="contentEditable" style='font-size:16px;color:#555555;text-align:center;line-height:24px;font-style: italic;'>
                                  <p >Follow the link below to create your password and Key pin.</p>
                                  <p><?= Html::a(Html::encode($createLink), $createLink) ?></p>
                                </div>
                              </div>
                            </td>
                          </tr>
                          <tr><td height='38'></td></tr>
                          <tr><td height='10' bgcolor="#ffffff"></td></tr>
                        </table>
                      </td>
                    </tr>
                  </table>
        </div>
        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr><td width='210' height='10' align='right'><img src="imgs/bannerTop.png" width='179' height='10' /></td><td width='350' colspan="2"></td></tr>
                <tr>
                  <td width='210' class='bgItem' align='right'>
                    <table width="155" border="0" cellspacing="0" cellpadding="0" >
                      <tr><td bgcolor="#d2176e" height='50'></td></tr>
                      <tr>
                        <td bgcolor="#d2176e" style='padding-right:15px;padding-left:15px;'>
                          <div class="contentEditableContainer contentTextEditable">
                            <div class="contentEditable" style='font-size:38px;color:#FFFFFF;text-align:center;line-height:38px;font-style: italic;'>
                              <p style='color:#FFFFFF;'>Special Offer!</p>
                            </div>
                          </div>
                        </td>
                      </tr>
                      <tr><td height='50' bgcolor="#d2176e"></td></tr>
                      <tr><td valign="bottom"><img src="/images/bannerBottom.png" width='155' height='24' /></td></tr>
                    </table>
                  </td>
                  <td width='40' bgcolor="#f2f2f2"></td>
                  <td width='260' class='bgItem'>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                      <tr><td height='30'></td></tr>
                      <tr>
                        <td>
                          <div class="contentEditableContainer contentTextEditable">
                            <div class="contentEditable" style='color:#555555;font-size:16px;line-height:24px;text-align:left;font-family: Helvetica Neue;'>
                              <p> Thanks for following us! <br/> Here’s a special <br/> offer for you!</p>
                            </div>
                          </div>
                        </td>
                      </tr>
                      <tr><td height='50'></td></tr>
                    </table>
                  </td>
                  <td width='30' bgcolor="#f2f2f2"></td>
                </tr>
                <tr><td height='10'></td></tr>
              </table>
        </div>