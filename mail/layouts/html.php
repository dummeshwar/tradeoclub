l<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Tradeoclub</title>
  <style type="text/css">
  body {
   padding-top: 0 !important;
   padding-bottom: 0 !important;
   padding-top: 0 !important;
   padding-bottom: 0 !important;
   margin:0 !important;
   width: 100% !important;
   -webkit-text-size-adjust: 100% !important;
   -ms-text-size-adjust: 100% !important;
   -webkit-font-smoothing: antialiased !important;
 }
 .tableContent img {
   border: 0 !important;
   display: block !important;
   outline: none !important;
 }
 a{
  color:#382F2E;
}

p, h1,h2,h3,ul,ol,li,div{
  margin:0;
  padding:0;
}

h1,h2{
  font-weight: normal;
  background:transparent !important;
  border:none !important;
}

.contentEditable h2.big{
  font-size: 30px !important;
}

 .contentEditable h2.bigger{
  font-size: 37px !important;
}

td,table{
  vertical-align: top;
}
td.middle{
  vertical-align: middle;
}

a.link1{
  font-size:14px;
  color:#D4D4D4;
  text-decoration:none;
  font-family: Helvetica Neue;
}

.link2{
font-size:16px;
color:#d2176e;
text-decoration:none;
line-height:24px;
font-family: Helvetica;
font-weight: bold;
}

.link3{
padding:5px 10px;
border-radius: 6px;
background-color: #d2176e;
font-size:13px;
color:#f2f2f2;
text-decoration:none;
line-height:26px;
font-family: Helvetica;
font-weight: bold;
}

.contentEditable li{
  margin-top:10px;
  margin-bottom:10px;
  list-style: none;
  color:#ffffff;
  text-align:center;
  font-size:13px;
  line-height:19px;
}

.appart p{
  font-size:13px;
  line-height:19px;
  color:#aaaaaa !important;
}
h2{
  color:#555555;
  font-weight: normal;
  font-size:28px;
  color:#555555;
  font-family: Georgia;
  line-height: 28px;
  font-style: italic;
}
.bgItem{
background:#f2f2f2;
}
.bgBody{
background:#ffffff;
}

@media only screen and (max-width:480px)
		
{
		
table[class="MainContainer"], td[class="cell"] 
	{
		width: 100% !important;
		height:auto !important; 
	}
td[class="specbundle"] 
	{
		width: 80% !important;
		float:left !important;
		font-size:13px !important;
		line-height:17px !important;
		display:block !important;
		padding-bottom:15px !important;
		text-align:center !important;
		padding-left:20% !important;
	}
td[class="specbundle2"] 
	{
		width:85% !important;
		float:left !important;
		font-size:13px !important;
		line-height:17px !important;
		display:block !important;
		padding-left:10% !important;
		padding-right:5% !important;
		padding-bottom:10px !important;
	}
	
	td[class="specbundle3"] 
	{
		width:20px !important;
		float:left !important;
		display:block !important;
		background-color:#f2f2f2 !important;
	}
	
td[class="specbundle4"] 
	{
		width:90% !important;
		float:left !important;
		font-size:13px !important;
		line-height:17px !important;
		display:block !important;
		padding-left:10% !important;
		padding-right:5% !important;
		padding-left:5% !important;
	}
		
td[class="spechide"] 
	{
		display:none !important;
	}
	    img[class="banner"] 
	{
	          width: 100% !important;
	          height: auto !important;
	}
		td[class="left_pad"] 
	{
			padding-left:15px !important;
			padding-right:15px !important;
	}
		 
}
	
@media only screen and (max-width:540px) 

{
		
table[class="MainContainer"], td[class="cell"] 
	{
		width: 100% !important;
		height:auto !important; 
	}
td[class="specbundle"] 
	{
		width: 80% !important;
		float:left !important;
		font-size:13px !important;
		line-height:17px !important;
		display:block !important;
		padding-bottom:15px !important;
		text-align:center !important;
		padding-left:20% !important;
	}	
td[class="specbundle2"] 
	{
		width:85% !important;
		float:left !important;
		font-size:13px !important;
		line-height:17px !important;
		display:block !important;
		padding-left:10% !important;
		padding-right:5% !important;
		padding-bottom:10px !important;
	}
	
	td[class="specbundle3"] 
	{
		width:20px !important;
		float:left !important;
		display:block !important;
		background-color:#f2f2f2 !important;
	}
	
td[class="specbundle4"] 
	{
		width:90% !important;
		float:left !important;
		font-size:13px !important;
		line-height:17px !important;
		display:block !important;
		padding-left:10% !important;
		padding-right:5% !important;
		padding-left:5% !important;
	}
		
td[class="spechide"] 
	{
		display:none !important;
	}
	    img[class="banner"] 
	{
	          width: 100% !important;
	          height: auto !important;
	}
		td[class="left_pad"] 
	{
			padding-left:15px !important;
			padding-right:15px !important;
	}
	.font{
		font-size:15px !important;
		line-height:19px !important;
		}
		
}
</style>

<script type="colorScheme" class="swatch active">
  {
    "name":"Default",
    "bgBody":"ffffff",
    "link":"d2176e",
    "color":"555555",
    "bgItem":"f2f2f2",
    "title":"555555"
  }
</script>

</head>
<body paddingwidth="0" paddingheight="0" bgcolor="#d1d3d4"  style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center" bgcolor="#FFFFFF" style='font-family:Georgia, serif;'>
    <!-- =============== START HEADER =============== -->
  <tbody>
    <tr>
      <td><table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="MainContainer">
      <!-- =============== END HEADER =============== -->
          <!-- =============== START BODY =============== -->
  <tbody>
    <tr>
      <td class='movableContentContainer'>
      	<div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td height="10"></td>
    </tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td valign="top" class="specbundle4"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td valign="top" width="89" class="specbundle4" align="center"><img src="https://www.tradeoclub.com/images/logo1.jpeg" alt="Logo" title="Logo" width="89" height="88" data-max-width="100"></td>
      <td width="40" valign="top" class="spechide">&nbsp;</td>
      <td valign="middle" class="specbundle4"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td height="25" class="spechide"></td>
    </tr>
    <tr>
      <td class="font"><div class="contentEditableContainer contentTextEditable">
                          <div class="contentEditable" align='left' style='color:#555555;font-size:18px; padding-top:5px;'>
                            <h1 style='line-height: 22px;font-size:18px;'>
                              Tradeoclub
                            </h1>
                          </div>
                        </div></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>
</td>
    </tr>
  </tbody>
</table>
</td>
      <td valign="top" width="20" class="spechide">&nbsp;</td>
      <td valign="middle" style='vertical-align: middle;' width='150' class="left_pad">
                         <!-- <div class='contentEditableContainer contentTextEditable'>
                            <div class='contentEditable' style='text-align: right;'>
                              <a target='_blank' href="[SHOWEMAIL]" class='link1' >Open in your browser</a>
                            </div>
                          </div> -->
                        </td>
    </tr>
  </tbody>
</table></td>
    </tr>
    <tr>
      <td height="10"></td>
    </tr>
  </tbody>
</table>

        </div>

    <?php $this->beginBody() ?>
    <?= $content ?>
    <?php $this->endBody() ?>

  <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
       <td height='10' bgcolor="#ffffff"></td>
    </tr>
    <tr>
      <td bgcolor="#f2f2f2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td valign="top" width="20" class="spechide">&nbsp;</td>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td height="20"></td>
    </tr>
    <tr>
<!--      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td valign="top" width="207" align="center" class="specbundle2"><div class='contentEditableContainer contentImageEditable'>
                            <div class='contentEditable'>
                              <img src="/images/map.jpg" data-default="placeholder" data-max-width='210' width='207' height='189'/>
                            </div>
                          </div></td>
      <td valign="top" width="23" class="specbundle2">&nbsp;</td>
      <td valign="top" class="specbundle2"><div class='contentEditableContainer contentTextEditable'>
                            <div class='contentEditableContainer contentTextEditable'>
                            <div class='contentEditable' style='font-size:16px;color:#555555;text-align:left;line-height:24px;'>
                                  <h2 style='font-size:28px;'>Visit us!</h2>
                                  <br/>
                                  <p>We’re really nice, come and visit us!</p>
                                  <br/>
                                   <span class='link2' style='font-size:24px;'>[CLIENTS.ADDRESS]</span> 
                            </div>
                          </div>
                          </div></td>
    </tr>
  </tbody>
</table>
</td>-->
    </tr>
    <tr>
      <td height="20"></td>
    </tr>
  </tbody>
</table>
</td>
      <td valign="top" width="20" class="spechide">&nbsp;</td>
    </tr>
  </tbody>
</table>
</td>
    </tr>
    <tr>
       <td height='10' bgcolor="#ffffff"></td>
    </tr>
  </tbody>
</table>
        </div>
        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f2f2f2">
  <tbody>
    <tr>
      <td td height='10' bgcolor="#ffffff"></td>
    </tr>
    <tr>
      <td height='50'></td>
    </tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td valign="top" width="50" class="spechide">&nbsp;</td>
      <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
<!--      <td  style='padding-top:14px;' align="center" class="specbundle2"><div class='contentEditableContainer contentTextEditable'>
                            <div class='contentEditable' style='font-size: 16px;color:#555555;line-height: 24px;'>
                              <p>
                                Check us on Facebook, Twitter and Pinterest!
                              </p>
                            </div>
                          </div></td>-->
      <td width='30' class="specbundle2"></td>
<!--      <td valign="top" width="230" class="specbundle2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td valign="top" width="70"><div class='contentEditableContainer contentFacebookEditable'>
                            <div class='contentEditable'>
                              <img src="/images/facebook.png"   data-max-width="70" width='70' height='70' alt='facebook' data-customIcon="true"/>
                            </div>
                          </div></td>
      <td width='10' valign="top"></td>
      <td valign="top" width="70"><div class='contentEditableContainer contentFacebookEditable'>
                            <div class='contentEditable'>
                              <img src="/images/twitter.png"   data-max-width="70" width='70' height='70' alt='facebook' data-customIcon="true"/>
                            </div>
                          </div></td>
      <td width='10' valign="top"></td>
      <td valign="top" width="70"><div class='contentEditableContainer contentFacebookEditable'>
                            <div class='contentEditable'>
                              <img src="/images/pinterest.png"   data-max-width="70" width='70' height='70' alt='facebook' data-customIcon="true"/>
                            </div>
                          </div></td>
    </tr>
  </tbody>
</table>
</td>-->
    </tr>
  </tbody>
</table>
</td>
      <td valign="top" width="50" class="spechide">&nbsp;</td>
    </tr>
  </tbody>
</table>
</td>
    </tr>
    <tr>
      <td height='50'></td>
    </tr>
    <tr>
      <td td height='10' bgcolor="#ffffff"></td>
    </tr>
  </tbody>
</table>

        </div>
        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr><td height='10'></td></tr>

              <tr><td><div style='border-top:1px solid #555555;'></div></td></tr>

              <tr><td height='20'></td></tr>

              <tr>
                <td align='center'>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                      <td width="100%" align="center">
                        <div class="contentEditableContainer contentTextEditable">
                          <div class="contentEditable" style='color:#555555;text-align:center;font-size:13px;line-height:19px;'>
                            <!-- <p>Sent by [SENDER_NAME] <br/>
                              [CLIENTS.ADDRESS] <br/>
                              [CLIENTS.PHONE] <br/>
                              <a target='_blank' href="[FORWARD]" style='color:#555555;'>Forward to a friend</a><br/>
                              </p>
                              <a target='_blank' href="[UNSUBSCRIBE]" style='color:#555555;'>Unsubscribe</a>
                            </p> -->
                          </div>
                        </div>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
        </div>
      </td>
    </tr>
  </tbody>
</table>
</td>
    </tr>
    <tr>
    	<td height='20'>&nbsp;</td>
    </tr>
  </tbody>
</table>


  </body>
  </html>

<?php $this->endPage() ?>
