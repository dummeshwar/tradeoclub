<?php

use yii\db\Migration;

/**
 * Handles adding points to table `package`.
 */
class m180518_072315_add_points_column_to_package_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('package', 'points', $this->decimal(12, 0)->defaultValue(0));
        $this->addCommentOnColumn('package','points','Profitrexpoints for TCG wallet.');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('package', 'points');
    }
}
