<?php

use yii\db\Migration;

/**
 * Handles the creation of table `payment_gateway`.
 */
class m180310_141407_create_payment_gateway_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('payment_gateway', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'mode' => $this->integer(1)->defaultValue(0),
            'account_details' => $this->text(),
            'title' => $this->string(),
            'description' => $this->text(),
            'slug' => $this->string(),
            'min_amount' => $this->decimal(18, 8)->defaultValue(0),
            'withdrawal_min_amount' => $this->decimal(18, 8)->defaultValue(0),
            'request_url' => $this->string(),
            'response_url' => $this->string(),
            'charges' => $this->decimal(18, 8)->defaultValue(0),
            'status' => $this->integer(1)->defaultValue(0),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime() . ' DEFAULT NOW() ',
        ]);


        $this->addCommentOnColumn('payment_gateway','mode','1:ONLINE 0:OFFLINE.');
        $this->addCommentOnColumn('payment_gateway','charges','Percent value. Do not include % in value.');
        $this->addCommentOnColumn('payment_gateway','status','1:Active 0:Inactive.');

        $this->execute("SET foreign_key_checks = 0;");

        $this->insert('payment_gateway', [
            'name' => "via Bitcoin(USD)",
            'mode' => 1, // Travel wallet
            'title' => "Bitcoin",
            'description' => "Bitcoin",
            'slug' => "bitcoin",
            'min_amount' => 100,
            'withdrawal_min_amount' => 100,
            'charges' => 0.1,
            'status' => 1,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
        ]);
        
        $this->execute("SET foreign_key_checks = 1;");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('payment_gateway');
    }
}
