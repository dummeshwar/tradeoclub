<?php

use yii\db\Migration;

/**
 * Handles the creation of table `transaction`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `payment_gateway`
 * - `wallet`
 * - `user`
 * - `user`
 * - `user`
 */
class m180310_142836_create_transaction_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('transaction', [
            'id' => $this->primaryKey(),
            'transaction_id' => $this->string()->notNull(),
            'payment_transaction_id' => $this->string(),
            'user_id' => $this->integer()->notNull(),
            'payment_gateway_id' => $this->integer(),
            'actual_amount' => $this->decimal(18, 8)->defaultValue(0),
            'paid_amount' => $this->decimal(18, 8)->defaultValue(0),
            'transaction_mode' => $this->string()->notNull(),
            'to_wallet_id' => $this->integer(),
            'from_wallet_id' => $this->integer(),
            'to_user_id' => $this->integer(),
            'from_user_id' => $this->integer(),
            'user_comment' => $this->text(),
            'admin_comment' => $this->text(),
            'admin_marked_status' => $this->integer(1)->defaultValue(0),
            'status' => $this->integer(1)->defaultValue(0),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime() . ' DEFAULT NOW() ',
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-transaction-user_id',
            'transaction',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-transaction-user_id',
            'transaction',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `payment_gateway_id`
        $this->createIndex(
            'idx-transaction-payment_gateway_id',
            'transaction',
            'payment_gateway_id'
        );

        // add foreign key for table `payment_gateway`
        $this->addForeignKey(
            'fk-transaction-payment_gateway_id',
            'transaction',
            'payment_gateway_id',
            'payment_gateway',
            'id',
            'CASCADE'
        );

        // creates index for column `to_wallet_id`
        $this->createIndex(
            'idx-transaction-to_wallet_id',
            'transaction',
            'to_wallet_id'
        );

        // add foreign key for table `wallet`
        $this->addForeignKey(
            'fk-transaction-to_wallet_id',
            'transaction',
            'to_wallet_id',
            'wallet',
            'id',
            'CASCADE'
        );

        // creates index for column `from_wallet_id`
        $this->createIndex(
            'idx-transaction-from_wallet_id',
            'transaction',
            'from_wallet_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-transaction-from_wallet_id',
            'transaction',
            'from_wallet_id',
            'wallet',
            'id',
            'CASCADE'
        );

        // creates index for column `to_user_id`
        $this->createIndex(
            'idx-transaction-to_user_id',
            'transaction',
            'to_user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-transaction-to_user_id',
            'transaction',
            'to_user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `from_user_id`
        $this->createIndex(
            'idx-transaction-from_user_id',
            'transaction',
            'from_user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-transaction-from_user_id',
            'transaction',
            'from_user_id',
            'user',
            'id',
            'CASCADE'
        );
        $this->addCommentOnColumn('transaction','transaction_id','Unique id for each transaction.');
        $this->addCommentOnColumn('transaction','payment_transaction_id','Response (unique ID) from online transactions.');
        $this->addCommentOnColumn('transaction','user_id','Initiated the transaction.');
        $this->addCommentOnColumn('transaction','payment_gateway_id','Gateway ID ex: Bitcoin. Null meaning no Gateway used.');
        $this->addCommentOnColumn('transaction','actual_amount','Actual amount without commission (IN BTC).');
        $this->addCommentOnColumn('transaction','paid_amount','Full amount inlcudes commission (IN BTC).');
        $this->addCommentOnColumn('transaction','transaction_mode','Modes of transactions - ex : Credit, Debit, Trasfer etc. Check it in config -> params file.');
        $this->addCommentOnColumn('transaction','admin_marked_status','0:Failed 1:Success/Approved, 2:Pendning.');
        $this->addCommentOnColumn('transaction','status','0:Failed 1:Success/Approved, 2:Pendning.');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-transaction-user_id',
            'transaction'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-transaction-user_id',
            'transaction'
        );

        // drops foreign key for table `payment_gateway`
        $this->dropForeignKey(
            'fk-transaction-payment_gateway_id',
            'transaction'
        );

        // drops index for column `payment_gateway_id`
        $this->dropIndex(
            'idx-transaction-payment_gateway_id',
            'transaction'
        );

        // drops foreign key for table `wallet`
        $this->dropForeignKey(
            'fk-transaction-to_wallet_id',
            'transaction'
        );

        // drops index for column `to_wallet_id`
        $this->dropIndex(
            'idx-transaction-to_wallet_id',
            'transaction'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-transaction-from_wallet_id',
            'transaction'
        );

        // drops index for column `from_wallet_id`
        $this->dropIndex(
            'idx-transaction-from_wallet_id',
            'transaction'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-transaction-to_user_id',
            'transaction'
        );

        // drops index for column `to_user_id`
        $this->dropIndex(
            'idx-transaction-to_user_id',
            'transaction'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-transaction-from_user_id',
            'transaction'
        );

        // drops index for column `from_user_id`
        $this->dropIndex(
            'idx-transaction-from_user_id',
            'transaction'
        );

        $this->dropTable('transaction');
    }
}
