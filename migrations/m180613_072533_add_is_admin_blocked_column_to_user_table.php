<?php

use yii\db\Migration;

/**
 * Handles adding is_admin_blocked to table `user`.
 */
class m180613_072533_add_is_admin_blocked_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'is_admin_blocked', $this->integer(1)->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'is_admin_blocked');
    }
}
