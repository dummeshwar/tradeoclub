<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `user`
 * - `user`
 * - `user`
 */
class m180102_192946_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'parent_id' => $this->integer(),
            'current_parent_id' => $this->integer(),
            'position' => $this->string(1),
            'password' => $this->string(),
            'password_reset_token' => $this->string(),
            'master_pin' => $this->string(),
            'email' => $this->string()->notNull(),
            'auth_key' => $this->string(),
            'status' => $this->integer(1)->defaultValue(0),
            'is_deleted' => $this->integer(1)->defaultValue(0),
            'is_admin' => $this->integer(1)->defaultValue(0),
            'left_user' => $this->string(),
            'right_user' => $this->string(),
            'left_user_id' => $this->integer(),
            'right_user_id' => $this->integer(),
            'lc_fund' => $this->decimal(18, 8),
            'rc_fund' => $this->decimal(18, 8),
            'binary_calculated_order_ids' => $this->text(),
            'level' => $this->integer()->defaultValue(0),
            'original_password' => $this->string(),
            'original_master_pin' => $this->string(),
            'last_login' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime() . ' DEFAULT NOW() ',
        ]);

        // creates index for column `parent_id`
        $this->createIndex(
            'idx-user-parent_id',
            'user',
            'parent_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user-parent_id',
            'user',
            'parent_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `current_parent_id`
        $this->createIndex(
            'idx-user-current_parent_id',
            'user',
            'current_parent_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user-current_parent_id',
            'user',
            'current_parent_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `left_user_id`
        $this->createIndex(
            'idx-user-left_user_id',
            'user',
            'left_user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user-left_user_id',
            'user',
            'left_user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `right_user_id`
        $this->createIndex(
            'idx-user-right_user_id',
            'user',
            'right_user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user-right_user_id',
            'user',
            'right_user_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->addCommentOnColumn('user','parent_id','Direct Parent User ID in the tree.');
        $this->addCommentOnColumn('user','current_parent_id','Immidiate Parent User ID in the tree.');
        $this->addCommentOnColumn('user','position','L:Left,R:Right');
        $this->addCommentOnColumn('user','status','1:Active,0:In Active');
        $this->addCommentOnColumn('user','is_deleted','1:Deleted,0:Not Deleted');
        $this->addCommentOnColumn('user','is_admin','1:Admin,0:Normal User');
        $this->addCommentOnColumn('user','left_user','Left child name');
        $this->addCommentOnColumn('user','right_user','Right child name');
        $this->addCommentOnColumn('user','left_user_id','Left user ID');
        $this->addCommentOnColumn('user','right_user_id','Right user ID');


        $this->execute("SET foreign_key_checks = 0;");
        $this->insert('user', [
            'username' => 'profitrex',
            'password' => Yii::$app->getSecurity()->generatePasswordHash('123456'),
            'original_password' => '123456',
            'email' => 'admin@profitrex.com',
            'is_admin' => 1,
            'left_user' => "demo_user",
            'left_user_id' => 2,
            'status' => 1,
            'master_pin' => Yii::$app->getSecurity()->generatePasswordHash('123456'),
            'original_master_pin' => '123456',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
        ]);

        $this->insert('user', [
            'username' => 'demo_user',
            'parent_id' => 1,
            'current_parent_id' => 1,
            'level' => 1,
            'position' => 'L',
            'password' => Yii::$app->getSecurity()->generatePasswordHash('123456'),
            'original_password' => '123456',
            'email' => 'dummy@gmail.com',
            'is_admin' => 0,
            'status' => 1,
            'master_pin' => Yii::$app->getSecurity()->generatePasswordHash('123456'),
            'original_master_pin' => '123456',
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
        ]);

        $this->execute("SET foreign_key_checks = 1;");

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user-parent_id',
            'user'
        );

        // drops index for column `parent_id`
        $this->dropIndex(
            'idx-user-parent_id',
            'user'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user-current_parent_id',
            'user'
        );

        // drops index for column `current_parent_id`
        $this->dropIndex(
            'idx-user-current_parent_id',
            'user'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user-left_user_id',
            'user'
        );

        // drops index for column `left_user_id`
        $this->dropIndex(
            'idx-user-left_user_id',
            'user'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user-right_user_id',
            'user'
        );

        // drops index for column `right_user_id`
        $this->dropIndex(
            'idx-user-right_user_id',
            'user'
        );

        $this->dropTable('user');
    }
}
