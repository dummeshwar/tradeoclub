<?php

use yii\db\Migration;

/**
 * Handles the creation of table `admin_pop_ups`.
 */
class m180519_121333_create_admin_pop_ups_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('admin_pop_ups', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'image_desktop' => $this->string(),
            'image_mobile' => $this->string(),
            'status' => $this->integer(1)->defaultValue(0),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime() . ' DEFAULT NOW() ',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('admin_pop_ups');
    }
}
