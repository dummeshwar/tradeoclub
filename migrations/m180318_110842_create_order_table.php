<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `package`
 * - `transaction`
 */
class m180318_110842_create_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'package_id' => $this->integer()->notNull(),
            'transaction_id' => $this->integer()->notNull(),
            'start_date' => $this->date(),
            'end_date' => $this->date(),
            'status' => $this->integer(1)->defaultValue(0),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime() . ' DEFAULT NOW() ',
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-order-user_id',
            'order',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-order-user_id',
            'order',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `package_id`
        $this->createIndex(
            'idx-order-package_id',
            'order',
            'package_id'
        );

        // add foreign key for table `package`
        $this->addForeignKey(
            'fk-order-package_id',
            'order',
            'package_id',
            'package',
            'id',
            'CASCADE'
        );

        // creates index for column `transaction_id`
        $this->createIndex(
            'idx-order-transaction_id',
            'order',
            'transaction_id'
        );

        // add foreign key for table `transaction`
        $this->addForeignKey(
            'fk-order-transaction_id',
            'order',
            'transaction_id',
            'transaction',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-order-user_id',
            'order'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-order-user_id',
            'order'
        );

        // drops foreign key for table `package`
        $this->dropForeignKey(
            'fk-order-package_id',
            'order'
        );

        // drops index for column `package_id`
        $this->dropIndex(
            'idx-order-package_id',
            'order'
        );

        // drops foreign key for table `transaction`
        $this->dropForeignKey(
            'fk-order-transaction_id',
            'order'
        );

        // drops index for column `transaction_id`
        $this->dropIndex(
            'idx-order-transaction_id',
            'order'
        );

        $this->dropTable('order');
    }
}
