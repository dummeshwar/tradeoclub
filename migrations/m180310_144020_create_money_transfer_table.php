<?php

use yii\db\Migration;

/**
 * Handles the creation of table `money_transfer`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `user`
 * - `transaction`
 * - `wallet`
 * - `user`
 */
class m180310_144020_create_money_transfer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('money_transfer', [
            'id' => $this->primaryKey(),
            'to_user_id' => $this->integer()->notNull(),
            'from_user_id' => $this->integer()->notNull(),
            'transaction_id' => $this->integer()->notNull(),
            'to_wallet_id' => $this->integer()->notNull(),
            'from_wallet_id' => $this->integer()->notNull(),
            'amount' => $this->decimal(18, 8)->defaultValue(0),
            'status' => $this->integer(1)->defaultValue(0),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime() . ' DEFAULT NOW() ',
        ]);

        // creates index for column `to_user_id`
        $this->createIndex(
            'idx-money_transfer-to_user_id',
            'money_transfer',
            'to_user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-money_transfer-to_user_id',
            'money_transfer',
            'to_user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `from_user_id`
        $this->createIndex(
            'idx-money_transfer-from_user_id',
            'money_transfer',
            'from_user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-money_transfer-from_user_id',
            'money_transfer',
            'from_user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `transaction_id`
        $this->createIndex(
            'idx-money_transfer-transaction_id',
            'money_transfer',
            'transaction_id'
        );

        // add foreign key for table `transaction`
        $this->addForeignKey(
            'fk-money_transfer-transaction_id',
            'money_transfer',
            'transaction_id',
            'transaction',
            'id',
            'CASCADE'
        );

        // creates index for column `to_wallet_id`
        $this->createIndex(
            'idx-money_transfer-to_wallet_id',
            'money_transfer',
            'to_wallet_id'
        );

        // add foreign key for table `wallet`
        $this->addForeignKey(
            'fk-money_transfer-to_wallet_id',
            'money_transfer',
            'to_wallet_id',
            'wallet',
            'id',
            'CASCADE'
        );

        // creates index for column `from_wallet_id`
        $this->createIndex(
            'idx-money_transfer-from_wallet_id',
            'money_transfer',
            'from_wallet_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-money_transfer-from_wallet_id',
            'money_transfer',
            'from_wallet_id',
            'wallet',
            'id',
            'CASCADE'
        );

        $this->addCommentOnColumn('money_transfer','amount',' In (BTC).');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-money_transfer-to_user_id',
            'money_transfer'
        );

        // drops index for column `to_user_id`
        $this->dropIndex(
            'idx-money_transfer-to_user_id',
            'money_transfer'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-money_transfer-from_user_id',
            'money_transfer'
        );

        // drops index for column `from_user_id`
        $this->dropIndex(
            'idx-money_transfer-from_user_id',
            'money_transfer'
        );

        // drops foreign key for table `transaction`
        $this->dropForeignKey(
            'fk-money_transfer-transaction_id',
            'money_transfer'
        );

        // drops index for column `transaction_id`
        $this->dropIndex(
            'idx-money_transfer-transaction_id',
            'money_transfer'
        );

        // drops foreign key for table `wallet`
        $this->dropForeignKey(
            'fk-money_transfer-to_wallet_id',
            'money_transfer'
        );

        // drops index for column `to_wallet_id`
        $this->dropIndex(
            'idx-money_transfer-to_wallet_id',
            'money_transfer'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-money_transfer-from_wallet_id',
            'money_transfer'
        );

        // drops index for column `from_wallet_id`
        $this->dropIndex(
            'idx-money_transfer-from_wallet_id',
            'money_transfer'
        );

        $this->dropTable('money_transfer');
    }
}
