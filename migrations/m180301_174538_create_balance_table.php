<?php

use yii\db\Migration;

/**
 * Handles the creation of table `balance`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `wallet`
 */
class m180301_174538_create_balance_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('balance', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'wallet_id' => $this->integer()->notNull(),
            'amount' => $this->decimal(18, 8)->defaultValue(0),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime() . ' DEFAULT NOW() ',
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-balance-user_id',
            'balance',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-balance-user_id',
            'balance',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `wallet_id`
        $this->createIndex(
            'idx-balance-wallet_id',
            'balance',
            'wallet_id'
        );

        // add foreign key for table `wallet`
        $this->addForeignKey(
            'fk-balance-wallet_id',
            'balance',
            'wallet_id',
            'wallet',
            'id',
            'CASCADE'
        );

        $this->execute("SET foreign_key_checks = 0;");

          // Admin user        
        $this->insert('balance', [
            'user_id' => 1,
            'wallet_id' => 1, // Cash wallet
            'amount' => 1000.00,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
        ]);
        
        $this->insert('balance', [
            'user_id' => 1,
            'wallet_id' => 2, // ads wallet
            'amount' => 1000.00,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
        ]);
        $this->insert('balance', [
            'user_id' => 1,
            'wallet_id' => 3, // Commission wallet
            'amount' => 1000.00,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
        ]);
        $this->insert('balance', [
            'user_id' => 1,
            'wallet_id' => 4, // Travel wallet
            'amount' => 1000.00,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
        ]);
        
        // Normal user
        $this->insert('balance', [
            'user_id' => 2,
            'wallet_id' => 1, // Cash wallet
            'amount' => 0.00,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
        ]);
        
        $this->insert('balance', [
            'user_id' => 2,
            'wallet_id' => 2, // ads wallet
            'amount' => 0.00,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
        ]);
        $this->insert('balance', [
            'user_id' => 2,
            'wallet_id' => 3, // Commission wallet
            'amount' => 0.00,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
        ]);
        $this->insert('balance', [
            'user_id' => 2,
            'wallet_id' => 4, // Travel wallet
            'amount' => 0.00,
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
        ]);
        
        
        $this->execute("SET foreign_key_checks = 1;");

        $this->addCommentOnColumn('balance','amount', 'In (BTC)');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->execute("SET foreign_key_checks = 0;");
        $this->delete('balance', ['user_id' => 1]);
        $this->delete('balance', ['user_id' => 2]);
        $this->execute("SET foreign_key_checks = 1;");
        
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-balance-user_id',
            'balance'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-balance-user_id',
            'balance'
        );

        // drops foreign key for table `wallet`
        $this->dropForeignKey(
            'fk-balance-wallet_id',
            'balance'
        );

        // drops index for column `wallet_id`
        $this->dropIndex(
            'idx-balance-wallet_id',
            'balance'
        );

        $this->dropTable('balance');
    }
}
