<?php

use yii\db\Migration;

/**
 * Handles the creation of table `broadcast_message`.
 */
class m180613_055735_create_broadcast_message_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('broadcast_message', [
            'id' => $this->primaryKey(),
            'description' => $this->text(),
            'status' => $this->integer(1)->defaultValue(0),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime() . ' DEFAULT NOW() ',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('broadcast_message');
    }
}
