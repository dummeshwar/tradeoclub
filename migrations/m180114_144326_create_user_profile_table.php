<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_profile`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m180114_144326_create_user_profile_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_profile', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'picture' => $this->string(),
            'date_of_birth' => $this->date(),
            'contact_number' => $this->string(),
            'address' => $this->text(),
            'street' => $this->string(),
            'state' => $this->string(),
            'city' => $this->string(),
            'zip_code' => $this->string(),
            'country_code' => $this->string(10),
            'national_id' => $this->string(),
            'passport_id' => $this->string(),
            'bank_statement' => $this->string(),
            'skype_id' => $this->string(),
            'facebook_id' => $this->string(),
            'twitter_id' => $this->string(),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime() . ' DEFAULT NOW() ',
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-user_profile-user_id',
            'user_profile',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_profile-user_id',
            'user_profile',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
        $this->addCommentOnColumn('user_profile','user_id','Reference to parent table - User.');


        $this->execute("SET foreign_key_checks = 0;");
        
        $this->insert('user_profile', [
            'user_id' => 1,
            'first_name' => 'Admin',
            'last_name' => 'Bit',
            'date_of_birth' => new \yii\db\Expression('NOW()'),
            'contact_number' => "8094558048",
            'country_code' => "91",
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
        ]);

        $this->insert('user_profile', [
            'user_id' => 2,
            'first_name' => 'Dummy',
            'last_name' => 'D',
            'date_of_birth' => new \yii\db\Expression('NOW()'),
            'contact_number' => "8094558048",
            'country_code' => "91",
            'created_at' => new \yii\db\Expression('NOW()'),
            'updated_at' => new \yii\db\Expression('NOW()'),
        ]);
        
        $this->execute("SET foreign_key_checks = 1;");   
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user_profile-user_id',
            'user_profile'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-user_profile-user_id',
            'user_profile'
        );

        $this->dropTable('user_profile');
    }
}
