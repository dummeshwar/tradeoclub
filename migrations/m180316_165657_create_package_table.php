<?php

use yii\db\Migration;

/**
 * Handles the creation of table `package`.
 */
class m180316_165657_create_package_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('package', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'price' => $this->decimal(18, 8)->defaultValue(0),
            'image' => $this->string(),
            'description' => $this->text(),
            'no_of_days' => $this->integer()->notNull(),
            'status' => $this->integer(1)->defaultValue(0),
            'daily_returns' => $this->decimal(18, 8)->notNull(),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime() . ' DEFAULT NOW() ',
        ]);


        $this->addCommentOnColumn('package','no_of_days','How long the package will stay active.');
        $this->addCommentOnColumn('package','price',' In (BTC).');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('package');
    }
}
