<?php

use yii\db\Migration;
use yii\db\Expression;

/**
 * Handles the creation of table `wallet`.
 */
class m180114_145358_create_wallet_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('wallet', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'status' => $this->integer(1)->defaultValue(0),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime() . ' DEFAULT NOW() ',
        ]);

        $this->insert('wallet', [
            'name' => 'Cash Wallet',
            'status' => 1,
            'created_at' => new Expression('NOW()'),
            'updated_at' => new Expression('NOW()'),
        ]);
        $this->insert('wallet', [
            'name' => 'Referral Wallet',
            'status' => 1,
            'created_at' => new Expression('NOW()'),
            'updated_at' => new Expression('NOW()'),
        ]);
        $this->insert('wallet', [
            'name' => 'Binary Wallet',
            'status' => 1,
            'created_at' => new Expression('NOW()'),
            'updated_at' => new Expression('NOW()'),
        ]);
        $this->insert('wallet', [
            'name' => 'Profitrex Wallet',
            'status' => 1,
            'created_at' => new Expression('NOW()'),
            'updated_at' => new Expression('NOW()'),
        ]);


        $this->addCommentOnColumn('wallet','name','1: Cash Wallet, 2: Referral Wallet (Direct), 3: Binary Wallet (Binary commission), 4: Profitrex Wallet');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->delete('wallet', ['id' => 1]);
        $this->delete('wallet', ['id' => 2]);
        $this->delete('wallet', ['id' => 3]);
        $this->delete('wallet', ['id' => 4]);
        $this->dropTable('wallet');
    }
}
