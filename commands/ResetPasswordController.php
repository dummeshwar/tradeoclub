<?php


namespace app\commands;

use yii\console\Controller;
use app\models\User;
use Yii;

class ResetPasswordController extends Controller
{

    public function actionIndex()
    {

    	$query = User::find();
    	$users = $query->all();
        $count = 0;

    	if (!empty($users)) {
    		foreach ($users as $user) {
                if (!empty($user->original_password) && !empty($user->original_master_pin)) {
                    $user->password = Yii::$app->getSecurity()->generatePasswordHash($user->original_password);
                    $user->master_pin = Yii::$app->getSecurity()->generatePasswordHash($user->original_master_pin);
                    $user->save(false);
                    $count++;
                }
    		}
            echo "Total Record updated : " . $count . "\n";
    		echo "Command is success !!!!\n";

    	} else {
    		echo "No records found !!!\n";
    	}
    	echo "Reached the end !!!!!! \n";
    }
}
