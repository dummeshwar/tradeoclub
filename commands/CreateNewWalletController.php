<?php


namespace app\commands;

use yii\console\Controller;
use app\models\Wallet;
use app\models\User;
use app\models\Balance;

/**
 * Creates new wallet.
 *
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CreateNewWalletController extends Controller
{
    /**
     * Create new wallet and updates to balance w.r.t all users.
     */
     /**
      * This command echoes what you have entered as the message.
      * @param string $message the message to be echoed.
      */
     public function actionIndex($walletName = NULL)
     {
         if ($walletName == NULL) {
           echo "Please provide wallet name.";
           die;
         }
         $wallet = new Wallet();
         $wallet->name = $walletName;
         $wallet->status = 1;
         $wallet->status = 1;
         $wallet->created_at = date('Y-m-d H:i:s', time());
         $wallet->updated_at = date('Y-m-d H:i:s', time());
         if (!$wallet->save()) {
             throw new Exception("Something went wrong!. Please try again later.");
         }

         // Get all active users including admin.
         $users = User::find()->all();
         if (!empty($users)) {
           foreach ($users as $user) {
             // Wallet.
             $balance = new Balance();
             $balance->user_id = $user->id;
             $balance->wallet_id = \Yii::$app->params['secondary_wallets']['PROMO_WALLET'];
             $balance->created_at = date('Y-m-d H:i:s', time());
             $balance->updated_at = date('Y-m-d H:i:s', time());
             if (!$balance->save()) {
                 throw new Exception("Something went wrong!. Please try again later.");
             }
           }
         } else {
           echo "\n*************** No records found !!! ************** \n";
           die;
         }
         echo "\n =================== \n";
         echo $walletName . " : Wallet created\n";
         echo "\n ---------- Success -------- \n";
         echo "\n ======================== \n";
     }
}
