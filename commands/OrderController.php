<?php


namespace app\commands;

use yii\console\Controller;
use app\models\Order;

/**
 * Checks whether the order is expired or not.
 *
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class OrderController extends Controller
{
    /**
     * This command helps us to change the order status to expire based on
     * the package purckage expire date (enddate - startdate).
     */
    public function actionChangeStatus()
    {
    	$today = date('Y-m-d');
    	$query = Order::find()->where(['<', 'end_date', $today]);
    	$query->andWhere(['status' => 1]);
    	$orders = $query->all();

    	$count = 1;
    	if (!empty($orders)) {
    		foreach ($orders as $order) {
    			// Make that order as expired by changing its status.
    			$order->status = 0;
    			$order->save(false);

          echo "Record updated : " . $count++ . "\n";
    		}

    		echo "Command is success !!!!\n";

    	} else {
    		echo "No records found !!!\n";
    	}

    	echo "Reached the end !!!!!! \n";
    }
}
