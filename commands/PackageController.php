<?php


namespace app\commands;
use Yii;
use yii\console\Controller;
use app\models\Order;
use app\models\Transaction;
use app\models\Balance;

/**
 * Checks whether the order is expired or not.
 *
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PackageController extends Controller
{
    /**
     * This command helps us to provide daily returns
     */
    public function actionDailyReturns()
    {
    	$today = date('Y-m-d');
      // ENDdate >= startdate - Meeaning order not expired.
    	$query = Order::find()->where(['>=', 'end_date', $today]);
      // start_date != todayDate - Consider one day older orders.
      $query->andWhere(['!=', 'start_date', $today]);
    	$query->andWhere(['status' => 1]);
    	$orders = $query->all();

    	$count = 1;
    	if (!empty($orders)) {
        foreach ($orders as $order) {

          if (!empty($order->package->daily_returns))  {
            // Create a transaction.
            // $daily_returns = $order->package->daily_returns;
            $daily_returns = ($order->package->price * $order->package->daily_returns) / 100;
            $TransactionObj = Transaction::createNewTransaction($order->user_id, Yii::$app->params['wallets']['TRADEOCLUB_WALLET'], Yii::$app->params['wallets']['TRADEOCLUB_WALLET'], Yii::$app->params['transaction_modes']['daily_returns'], $daily_returns, $daily_returns, Yii::$app->params['transaction_modes']['daily_returns']);
            // To user balance update
            Balance::findAndUpdate($order->user_id, Yii::$app->params['wallets']['TRADEOCLUB_WALLET'],  $daily_returns, 'CREDIT');
          }
        }
    		echo "Command is success !!!!\n";
    	} else {
    		echo "No records found !!!\n";
    	}
    	echo "Reached the end !!!!!! \n";
    }
    
    public function actionWeeklyReturns()
    {
    	$today = date('Y-m-d');
      // ENDdate >= startdate - Meeaning order not expired.
    	$query = Order::find()->where(['>=', 'end_date', $today]);
      // start_date != todayDate - Consider one day older orders.
      $query->andWhere(['!=', 'start_date', $today]);
    	$query->andWhere(['status' => 1]);
    	$orders = $query->all();

    	$count = 1;
    	if (!empty($orders)) {
        foreach ($orders as $order) {

          if (!empty($order->package->daily_returns))  {
            // Create a transaction.
            // $daily_returns = $order->package->daily_returns;
//            $daily_returns = ($order->package->price * $order->package->daily_returns) / 100;
              $weekly_returns = $order->package->daily_returns; /* It's weekly return for every week till 156 Weeks */
            $TransactionObj = Transaction::createNewTransaction($order->user_id, Yii::$app->params['wallets']['TRADEOCLUB_WALLET'], Yii::$app->params['wallets']['TRADEOCLUB_WALLET'], Yii::$app->params['transaction_modes']['weekly_returns'], $weekly_returns, $weekly_returns, Yii::$app->params['transaction_modes']['weekly_returns']);
            // To user balance update
            Balance::findAndUpdate($order->user_id, Yii::$app->params['wallets']['TRADEOCLUB_WALLET'],  $weekly_returns, 'CREDIT');
          }
        }
    		echo "Command is success !!!!\n";
    	} else {
    		echo "No records found !!!\n";
    	}
    	echo "Reached the end !!!!!! \n";
    }

}
