<?php


namespace app\commands;

use yii\console\Controller;
use app\models\Order;
use app\models\User;
use yii\helpers\ArrayHelper;
use app\models\Package;
use app\models\Transaction;
use app\models\Balance;
use Yii;

/**
 * Binary commission calculation.
 *
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BinaryCommissionController extends Controller
{
    public $binary_calculated_order_ids = [];

    /**
     * This command calculates the binary commission of each user.
     */
    public function actionCalculate() {
      // Get all active users including admin.
      $users = User::find()->where(['status' => 1])->all();

      if (!empty($users)) {
        foreach ($users as $user) {
          // $user = User::findOne(3);
          // echo "\n " . $user->username . "\n";
          $Btree_users = $this->getBinaryTreeUsers($user);
           // print_r($Btree_users);
           // echo "\n ******************* \n";
           //echo count($Btree_users);
           //continue;
           // exit;

           if (!empty($Btree_users) && (count($Btree_users) > 0)) {
              $this->processUserTree($Btree_users, count($Btree_users), $user);
           }
        }
      } else {
        echo "*************** No records found !!! ************** \n";
        echo "*************** No binary calculation !!! ************** \n";
      }
      echo "************** Reached the end !!!!!! ************* \n";
    }

    /**
     * Get users level by level.
     */
    public function getBinaryTreeUsers($rootNode) {

        $children = $root_node_children_ids = ArrayHelper::getColumn(User::find()->where(['parent_id' => $rootNode->id])->all(), 'id');

        $q = new \SplQueue();
        $q->enqueue($rootNode->username);
        $level = 0;

        $users_at_each_level = [];

        while (1) {
            $node_count = count($q);


            if ($node_count == 0) {
                return array_filter($users_at_each_level);
                break;
            }

            $users_at_each_level_temp = [];
            $flag = false;

            // Dequeue all nodes of current level and Enqueue all
            // nodes of next level
            while ($node_count > 0)                    //T,
            {
                $username = $q->dequeue();
                // 2. Consider both active and inctive users.
                $node = User::findByUsername($username, 2);

                // Below steps : Check whether user(both left and right) purchased any package or not.
                // This should be equal to number nodes at each level.
                if (!empty($node->left_user_id) &&  $node->left_user_id!= NULL) {

                    // Current left user object.
                    $leftUserObj = User::findByUsername($node->left_user, 2);

                    if ( (isset($node->id) && isset($leftUserObj->parent_id) && isset($rootNode->id)) && (($node->id == $leftUserObj->parent_id) || ($rootNode->id == $leftUserObj->parent_id) || (in_array($leftUserObj->parent_id, $root_node_children_ids)) || (in_array($leftUserObj->parent_id, $children))) ) {
                      array_push($users_at_each_level_temp, $leftUserObj->username);
                      array_push($children, $leftUserObj->id);
                      $q->enqueue($leftUserObj->username);
                    } else {

                      // Loop until you find direct referral in tree.
                      while (isset($leftUserObj->left_user_id) && $leftUserObj->left_user_id != null) {
                        $leftUserObj = User::findOne($leftUserObj->left_user_id);

                        if ( (isset($node->id) && isset($leftUserObj->parent_id) && isset($rootNode->id)) && (($node->id == $leftUserObj->parent_id) || ($rootNode->id == $leftUserObj->parent_id) || (in_array($leftUserObj->parent_id, $root_node_children_ids)) || (in_array($leftUserObj->parent_id, $children))) ) {
                          $flag = true;
                          $q->enqueue($leftUserObj->username);
                          break;
                        }
                      }

                      if ($flag) {
                        array_push($users_at_each_level_temp, $leftUserObj->username);
                        array_push($children, $leftUserObj->id); // Push all children.
                      } else {
                        array_push($users_at_each_level_temp, 'X'); // X: Place holder for empty child in each level.
                      }

                      $flag = false;
                    }
                } else {
                  array_push($users_at_each_level_temp, 'X'); // X: Place holder for empty child in each level.
                }

                if (!empty($node->right_user_id) &&  $node->right_user_id!= NULL) {

                    // Current right user object.
                    $rightUserObj = User::findByUsername($node->right_user, 2);

                    if ( (isset($node->id) && isset($rightUserObj->parent_id) && isset($rootNode->id)) && (($node->id == $rightUserObj->parent_id) || ($rootNode->id == $rightUserObj->parent_id) || (in_array($rightUserObj->parent_id, $root_node_children_ids)) || (in_array($rightUserObj->parent_id, $children))) ) {
                      array_push($users_at_each_level_temp, $rightUserObj->username);
                      array_push($children, $rightUserObj->id);
                      $q->enqueue($rightUserObj->username);
                    } else {
                      // Loop until you find direct referral in tree.
                      while (isset($rightUserObj->right_user_id) && $rightUserObj->right_user_id != null) {
                        $rightUserObj = User::findByUsername($rightUserObj->right_user, 2);

                        if ( (isset($node->id) && isset($rightUserObj->parent_id) && isset($rootNode->id)) && (($node->id == $rightUserObj->parent_id) || ($rootNode->id == $rightUserObj->parent_id) || (in_array($rightUserObj->parent_id, $root_node_children_ids)) || (in_array($rightUserObj->parent_id, $children))) ) {
                          $flag = true;
                          $q->enqueue($rightUserObj->username);
                          break;
                        }
                      }

                      if ($flag) {
                        array_push($users_at_each_level_temp, $rightUserObj->username);
                        array_push($children, $rightUserObj->id);
                      } else {
                        array_push($users_at_each_level_temp, 'X'); // X: Place holder for empty child in each level.
                      }

                      $flag = false;
                    }
                } else {
                  array_push($users_at_each_level_temp, 'X'); // X : Place holder for empty child in each level.

                }

                $node_count--;
            }

            // Increase the level.
            $level++;

            $result_arr = array_fill (0 , pow(2, $level) , 'X' );
            $result_arr = array_replace($result_arr, $users_at_each_level_temp);
            if (count(array_unique($result_arr)) === 1 && end($result_arr) === 'X') {
              // If array has same value. meaning if it has all empty placeholders 'X'
            } else {
              // Assign level users to array.
              // Do this. atleast if we have one user in each level.
              $users_at_each_level[$level] = implode(',', $result_arr);
            }
        }
    }

    /**
     * Process tree.
     */
    public function processUserTree($users, $level, $curUserObj) {
      $lc_fund = 0;
      $rc_fund = 0;
      foreach ($users as $key => $user_string) {
        if ($key > $level) break;

        $nodes = explode(',', $user_string);
        $nodes_count = count($nodes);
        $node_devider_key = $nodes_count / 2;

        $lc_fund += $this->processTreeChildren($nodes, 0, $node_devider_key - 1, $curUserObj);
        $rc_fund += $this->processTreeChildren($nodes, $node_devider_key, $nodes_count - 1, $curUserObj);

      }

      // Final LC and RC fund after adding carry.
      $lc_fund = $lc_fund + $curUserObj->lc_fund;
      $rc_fund = $rc_fund + $curUserObj->rc_fund;

      $this->processBinaryCommission($lc_fund, $rc_fund, $curUserObj);
    }


    /**
     * Calculate package sum.
     */
    public function processTreeChildren($nodes, $start, $end, $rootNode) {

      $children_fund = 0;

      while( $end >=  $start) {

        $userObj = User::findByUsername($nodes[$start], 2);

        if (empty($userObj)) {
          // Not an user.
          $start++;
          continue;
        }

        // Find any packages purchased.
        $package_ids = ArrayHelper::getColumn(Order::find()->where(['status' => 1, 'user_id' => $userObj->id])->all(), 'package_id');
        // Get order ids.
        $order_ids = ArrayHelper::getColumn(Order::find()->where(['status' => 1, 'user_id' => $userObj->id])->all(), 'id');


        // Get binary calculated order_ids from the user.
        $binary_cal_order_ids = [];
        if (!empty($rootNode->binary_calculated_order_ids)) {
          $binary_cal_order_ids = explode(",", $rootNode->binary_calculated_order_ids);
        }
        $result_ids = array_diff($order_ids, $binary_cal_order_ids);

        if (count($result_ids) > 0) {
            // Get only newly created packages.
            $package_ids = ArrayHelper::getColumn(Order::find()->where(['id' => $result_ids])->all(), 'package_id');
            $children_fund += Package::find()->where(['id' => $package_ids])->sum('price');
            $this->binary_calculated_order_ids = array_merge($this->binary_calculated_order_ids, $result_ids);
        }


        $start++;
      }

      return $children_fund;
    }


    /**
     * Give binary commission.
     */
    public function processBinaryCommission($lc_fund, $rc_fund, $curUserObj) {

        if ( ($lc_fund == 0) && ($rc_fund == 0) ) {
            // Do nothing.
            return;
        }

        $binary_commission_perc = Yii::$app->params['binaryCommission'];
        $binary_commission_fund = null;
        if ($lc_fund < $rc_fund) {

            $binary_commission_fund = ($binary_commission_perc * $lc_fund) / 100;
            $right_carry_fund = $rc_fund - $lc_fund;
            $curUserObj->rc_fund = $right_carry_fund;
            $curUserObj->lc_fund = 0;

        } else if ($lc_fund > $rc_fund) {

            $binary_commission_fund = ($binary_commission_perc * $rc_fund) / 100;
            $left_carry_fund = $lc_fund - $rc_fund;
            $curUserObj->lc_fund = $left_carry_fund;
            $curUserObj->rc_fund = 0;

        } else {
            // Both are equal.
            $binary_commission_fund = ($binary_commission_perc * $rc_fund) / 100;
            $curUserObj->lc_fund = 0;
            $curUserObj->rc_fund = 0;
        }


        // Do db operations when binary_commission is not zero.
        // Check for binary calculated order ids.
        if (!empty($binary_commission_fund) && !empty($this->binary_calculated_order_ids)) {

            // Remove duplcaite ids.
            $order_ids = array_unique($this->binary_calculated_order_ids);
            if (!empty($curUserObj->binary_calculated_order_ids)) {
                $existing_order_ids = explode(",", $curUserObj->binary_calculated_order_ids);
                $order_ids = array_merge($existing_order_ids, $order_ids);
            }

            $curUserObj->binary_calculated_order_ids = implode(',', $order_ids);
            $curUserObj->save(false);

            // Make a transaction and update balance.
            $transactionObj = Transaction::createNewTransaction($curUserObj->id, Yii::$app->params['wallets']['BINARY_WALLET'], Yii::$app->params['wallets']['BINARY_WALLET'], Yii::$app->params['transaction_modes']['binary_commission'], $binary_commission_fund, $binary_commission_fund, Yii::$app->params['transaction_modes']['binary_commission'], Yii::$app->params['admin_user_id']);

            // To user balance update
            Balance::findAndUpdate($curUserObj->id, Yii::$app->params['wallets']['BINARY_WALLET'], $binary_commission_fund, 'CREDIT');

            // Finally reset binary_calculated_order_ids variable.
            $this->binary_calculated_order_ids = [];

        }

        $message = "Successfull!!!";
    }
}
