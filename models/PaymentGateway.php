<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment_gateway".
 *
 * @property int $id
 * @property string $name
 * @property int $mode 1:ONLINE 0:OFFLINE.
 * @property string $account_details
 * @property string $title
 * @property string $description
 * @property string $slug
 * @property double $min_amount
 * @property double $withdrawal_min_amount
 * @property string $request_url
 * @property string $response_url
 * @property double $charges Percent value. Do not include % in value.
 * @property int $status 1:Active 0:Inactive.
 * @property string $updated_at
 * @property string $created_at
 *
 * @property Transaction[] $transactions
 */
class PaymentGateway extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_gateway';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['mode', 'status'], 'integer'],
            [['account_details', 'description'], 'string'],
            [['min_amount', 'withdrawal_min_amount', 'charges'], 'number'],
            [['updated_at', 'created_at'], 'safe'],
            [['name', 'title', 'slug', 'request_url', 'response_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'mode' => 'Mode',
            'account_details' => 'Account Details',
            'title' => 'Title',
            'description' => 'Description',
            'slug' => 'Slug',
            'min_amount' => 'Min Amount',
            'withdrawal_min_amount' => 'Withdrawal Min Amount',
            'request_url' => 'Request Url',
            'response_url' => 'Response Url',
            'charges' => 'Charges',
            'status' => 'Status',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['payment_gateway_id' => 'id']);
    }
}
