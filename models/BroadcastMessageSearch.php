<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BroadcastMessage;

/**
 * BroadcastMessageSearch represents the model behind the search form of `app\models\BroadcastMessage`.
 */
class BroadcastMessageSearch extends BroadcastMessage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['description', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BroadcastMessage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            // 'updated_at' => $this->updated_at,
            // 'created_at' => $this->created_at,
        ]);

        //$query->andFilterWhere(['like', 'description', $this->description]);

        // Date condition - start - end.
       if (!empty($this->created_at)) {
           $date_range = explode('to', $this->created_at);
           $query->andFilterWhere(['between', 'created_at', $date_range[0], $date_range[1]]);
       }

        return $dataProvider;
    }
}
