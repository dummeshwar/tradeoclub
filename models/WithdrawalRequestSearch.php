<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Transaction;

/**
 * TransactionSearch represents the model behind the search form about `app\models\Transaction`.
 */
class WithdrawalRequestSearch extends Transaction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'payment_gateway_id', 'actual_amount', 'paid_amount', 'admin_marked_status', 'status', 'from_wallet_id', 'to_wallet_id'], 'integer'],
            [['transaction_id', 'payment_transaction_id', 'transaction_mode', 'created_at', 'updated_at', 'user_comment', 'admin_comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            //'id' => $this->id,
            //'user_id' => $this->user_id,
            //'user_id' => Yii::$app->user->identity->id,
            //'payment_gateway_id' => $this->payment_gateway_id,
            //'actual_amount' => $this->actual_amount,
            //'paid_amount' => $this->paid_amount,
            'admin_marked_status' => $this->admin_marked_status,
            'status' => $this->status,
            'transaction_mode' => Yii::$app->params['transaction_modes']['withdrawal_request'],
            //'created_at' => $this->created_at,
            //'updated_at' => $this->updated_at,
            // 'from_wallet_id' => $this->from_wallet_id,
            // 'to_wallet_id' => $this->to_wallet_id,
        ]);

        // Filter based on not an admin.
        if (!Yii::$app->user->identity->is_admin) {
          $query->andFilterWhere([
              'user_id' => Yii::$app->user->identity->id]);
        }


        //$query->andFilterWhere(['like', 'transaction_id', $this->transaction_id])
            //->andFilterWhere(['like', 'payment_transaction_id', $this->payment_transaction_id])
            //->andFilterWhere(['like', 'comment', $this->comment])
            //->andFilterWhere(['like', 'user_comment', $this->user_comment])
            $query->andFilterWhere(['like', 'admin_comment', $this->admin_comment]);

        // Date condition - start - end.
        if (!empty($this->created_at)) {
            $date_range = explode('to', $this->created_at);
            $query->andFilterWhere(['between', 'created_at', $date_range[0], $date_range[1]]);
        }

        return $dataProvider;
    }
}
