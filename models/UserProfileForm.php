<?php

namespace app\models;

use borales\extensions\phoneInput\PhoneInputValidator;
use yii\helpers\ArrayHelper;
use Yii;
/**
 * Model - UserProfile Form.
 */
/**
 * This is the model class for table "user_profile".
 *
 */
class UserProfileForm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['date_of_birth', 'updated_at', 'created_at'], 'safe'],
            [['address'], 'string'],
            [['first_name', 'last_name', 'contact_number', 'street', 'state', 'city', 'zip_code', 'national_id', 'passport_id', 'skype_id', 'facebook_id', 'twitter_id'], 'string', 'max' => 255],
            [['picture'], 'file', 'extensions' => 'jpg, png, jpeg', 'skipOnEmpty' => true, 'mimeTypes' => 'image/jpeg, image/png',],
            [['bank_statement'], 'file', 'extensions' => 'jpg, png, jpeg', 'skipOnEmpty' => true, 'mimeTypes' => 'image/jpeg, image/png',],
            //[['country_code'], 'string', 'max' => 2],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'picture' => 'Picture',
            'date_of_birth' => 'Date Of Birth',
            'contact_number' => 'Contact Number',
            'address' => 'Address',
            'street' => 'Country Name',
            'state' => 'State',
            'city' => 'City',
            'zip_code' => 'Zip Code',
            'country_code' => 'Country Code',
            'national_id' => 'National ID',
            'passport_id' => 'Passport ID',
            'bank_statement' => 'Bank Statement',
            'skype_id' => 'Skype ID',
            'facebook_id' => 'Facebook ID',
            'twitter_id' => 'Twitter ID',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    public function upload() {
        if ($this->validate()) {
            $this->picture->saveAs('uploads/profile_pics/' . $this->picture->baseName . '.' . $this->picture->extension);
            return true;
        } else {
            return false;
        }
    }

    public function bankStatementUpload() {
        if ($this->validate()) {
            $this->bank_statement->saveAs('uploads/bank-statements/' . $this->bank_statement->baseName . '.' . $this->bank_statement->extension);
            return true;
        } else {
            return false;
        }
    }
}

