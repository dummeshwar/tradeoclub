<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ProfitCalculatorForm extends Model
{
    public $amount;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['amount'], 'required'],
            [['amount'], 'number', 'min'=>50, 'max'=>1000000],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'amount' => 'Amount ($)'
        ];
    }
}
