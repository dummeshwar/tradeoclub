<?php

namespace app\models;
use yii\helpers\ArrayHelper;

use Yii;

/**
 * This is the model class for table "wallet".
 *
 * @property int $id
 * @property string $name 1: Cash Wallet, 2: Referral Wallet (Direct), 3: Binary Wallet (Binary commission), 4: Profitrex Wallet
 * @property int $status
 * @property string $updated_at
 * @property string $created_at
 *
 * @property Balance[] $balances
 */
class Wallet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wallet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBalances()
    {
        return $this->hasMany(Balance::className(), ['wallet_id' => 'id']);
    }

     /**
     * Get all active wallets.
     */
    public static function getAllActiveWallets() {
        return ArrayHelper::getColumn(Wallet::find()->where(['status' => 1])->all(), 'name');
    }
}
