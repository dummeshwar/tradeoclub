<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class SearchUserTreeForm extends Model
{
    public $username;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['username'], 'required'],
            ['username', 'isUserExist'],
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'username'  => 'Username',
        ];
    }

    /**
     * Check user exist or not.
     */
    public function isUserExist($attribute, $params) {
        if (!User::findByUserName($this->username, 2)) {
            $this->addError($attribute, 'User does not exist.');
        }
    }
}
