<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Utils;

/**
 * CreditWalletForm is the model behind the contact form.
 */
class AddFundForm extends Model
{
    public $to_user;
    public $to_wallet;
    public $amount;
    public $comment;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['to_user', 'to_wallet', 'amount'], 'required'],
            [['amount'], 'number', 'min'=>0.01, 'max'=>1000000],
            [['comment'], 'string', 'max' => 255],
            ['to_user', 'isUserExist'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'to_user' => 'Select User',
            'to_wallet' => 'Wallet Name',
            'amount' => 'Amount ($)',
            'comment' => 'Comment',
        ];
    }

    public function isUserExist($attribute, $params)
    {
        if (!Utils::findByUserName($this->to_user)) {
            $this->addError($attribute, 'User does not exist.');
        }
    }

}
