<?php

namespace app\models;

use yii\base\Model;
use yii\base\InvalidParamException;
use app\models\User;

/**
 * ContactForm is the model behind the contact form.
 */
class CreateNewPasswordMasterPinForm extends Model {

    public $password;
    public $password_repeat;
    public $master_pin;
    public $master_pin_repeat;
    public $original_password;
    public $original_master_pin;

    /**
     * @var app\models\User
     */
    //private $_user;

    public $_user;

    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = []) {

        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Token cannot be blank to set new password and Key pin.');
        }
        $this->_user = User::findByTokenTOCreatePasswordMasterPin($token);
        if (!$this->_user) {
            throw new InvalidParamException('Wrong token.');
        }
        parent::__construct($config);
    }

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            [['password', 'password_repeat', 'master_pin', 'master_pin_repeat'], 'required'],
            [['password', 'password_repeat', 'master_pin', 'master_pin_repeat', 'original_password', 'original_master_pin'], 'string', 'min' => 6],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => "Passwords don't match"],
            ['master_pin_repeat', 'compare', 'compareAttribute' => 'master_pin', 'message' => "Key Pins don't match"],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels() {
        return [
            'password' => 'Password',
            'master_pin' => 'Key Pin',
        ];
    }

    /**
     * Set password and mater pin by token.
     *
     * @return bool if password was reset.
     */
    public function createNewPasswordMasterPinByToken() {
        $user = $this->_user;

        // ON requirement. This is not good.
        $user->setOriginalPwdMasterPin($this->password, $this->master_pin);

        $user->setPassword($this->password);
        $user->setMasterPin($this->master_pin);
        $user->removePasswordResetToken();
        
        // Make user active active after creating his/her password and key pin
        $user->status = 1;
        return $user->save(false);
    }

}
