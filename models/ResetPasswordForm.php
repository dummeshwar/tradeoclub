<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\base\InvalidParamException;
use app\models\User;


class ResetPasswordForm extends Model {

    public $password;
    public $password_repeat;
    public $original_password;

    /**
     * @var app\models\User
     */
    private $_user;

    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($user_logged_in = FALSE, $token, $config = []) {

        if ($user_logged_in) {
            $this->_user = $user_logged_in;
        } else {
            if (empty($token) || !is_string($token)) {
                throw new InvalidParamException('Password reset token cannot be blank.');
            }
            $this->_user = User::findByPasswordResetToken($token);
            if (!$this->_user) {
                throw new InvalidParamException('Wrong password reset token.');
            }
        }
        parent::__construct($config);
    }

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            [['password', 'password_repeat'], 'required'],
            [['password', 'password_repeat'], 'string', 'min' => 6],
            [['original_password'], 'string'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => "Passwords don't match"],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels() {
        return [
            'password' => 'Password',
        ];
    }

    /**
     * Resets password by token (Not logged in users).
     *
     * @return bool if password was reset.
     */
    public function resetPasswordByToken() {
        $user = $this->_user;
        $user->setOriginalPassword($this->password);
        $user->setPassword($this->password);
        $user->removePasswordResetToken();
        if (!$user->save(false)) {
            throw new Exception("Something went wrong!. Please try again later.");
        }
        return true;
    }

    /**
     * Reset password for logged in users.
     */
    public function resetPasswordForLoggedInUser() {
        $user = $this->_user;
        $user->setOriginalPassword($this->password);
        $user->setPassword($this->password);
        if (!$user->save(false)) {
            throw new Exception("Something went wrong!. Please try again later.");
        }
        return true;
    }

}
