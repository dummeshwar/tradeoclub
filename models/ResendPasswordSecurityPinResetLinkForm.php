<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\base\InvalidParamException;
use app\models\User;


class ResendPasswordSecurityPinResetLinkForm extends Model {

    public $email;
    public $email_repeat;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            [['email', 'email_repeat'], 'required'],
            [['email', 'email_repeat'], 'email'],
            ['email_repeat', 'compare', 'compareAttribute' => 'email', 'message' => "Emails don't match"],
            ['email', 'exist',
                'targetClass' => 'app\models\User',
                'filter' => ['status' => User::STATUS_INACTIVE, 'is_admin_blocked' => 0],
                'message' => 'There is no user with this email address.'
            ],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels() {
        return [
            'email' => 'Email Id',
            'email_repeat' => 'Repeat Email Id',
        ];
    }

}
