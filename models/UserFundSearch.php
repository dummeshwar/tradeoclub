<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Transaction;

/**
 * TransactionSearch represents the model behind the search form of `app\models\Transaction`.
 */
class UserFundSearch extends Transaction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'payment_gateway_id', 'to_wallet_id', 'from_wallet_id', 'to_user_id', 'from_user_id', 'admin_marked_status', 'status'], 'integer'],
            [['transaction_id', 'payment_transaction_id', 'transaction_mode', 'user_comment', 'admin_comment', 'updated_at', 'created_at'], 'safe'],
            [['actual_amount', 'paid_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }



        // grid filtering conditions
        $query->andFilterWhere([
            // 'id' => $this->id,
            // 'user_id' => $this->user_id,
            // 'payment_gateway_id' => $this->payment_gateway_id,
            // 'actual_amount' => $this->actual_amount,
            // 'paid_amount' => $this->paid_amount,
            // 'to_wallet_id' => $this->to_wallet_id,
            // 'from_wallet_id' => $this->from_wallet_id,
            'user_id' => Yii::$app->user->identity->id,
            // 'from_user_id' => $this->from_user_id,
            // 'admin_marked_status' => $this->admin_marked_status,
            // 'status' => $this->status,
            // 'updated_at' => $this->updated_at,
            // 'created_at' => $this->created_at,
            'transaction_mode' => Yii::$app->params['transaction_modes']['user_add_fund']
        ]);

        // $query->orFilterWhere([
        //     'to_user_id' => Yii::$app->user->identity->id]);


        if (!empty($this->created_at)) {
            $date_range = explode('to', $this->created_at);
            $query->andFilterWhere(['between', 'created_at', $date_range[0], $date_range[1]]);
        }

        return $dataProvider;
    }
}
