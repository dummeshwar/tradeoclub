<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Utils;

/**
 * PackagePurchaseForm is the model behind the contact form.
 */
class BuyTcgPointsForm extends Model
{
    public $points;  // Profitrexpoints.

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['points'], 'required'],
            [['points'], 'number', 'min'=>1, 'max'=>1000000],
            [['points'], 'verifySufficientBalance'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'points' => 'TGC Tokens ( 1 TGC Token = ' . rtrim(sprintf('%.15F', Yii::$app->params['points_to_BTC']), '0')  . ' BTC)'
        ];
    }

    /**
     *
     */
    public function verifySufficientBalance($attribute, $params) {
      $btc_val_of_tcg_point = Yii::$app->params['points_to_BTC']; // 1 tcg point equals.
      $btc_val = $btc_val_of_tcg_point * $this->points;
      $wallet_balance = Utils::walletMoney(Yii::$app->params['wallets']['CASH_WALLET']);
      if ($btc_val >  $wallet_balance) {
          $this->addError('points', 'Please recharge your wallet. You do not have sufficient balance.');
      }
    }

}
