<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ManageUsers;

/**
 * ManageUsersSearch represents the model behind the search form of `app\models\ManageUsers`.
 */
class ManageUsersSearch extends ManageUsers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'current_parent_id', 'status', 'is_deleted', 'is_admin', 'left_user_id', 'right_user_id', 'level'], 'integer'],
            [['username', 'position', 'password', 'password_reset_token', 'master_pin', 'email', 'auth_key', 'left_user', 'right_user', 'binary_calculated_order_ids', 'original_password', 'original_master_pin', 'last_login', 'updated_at', 'created_at'], 'safe'],
            [['lc_fund', 'rc_fund'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ManageUsers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        // grid filtering conditions
        // Status - Active OR Blocked
        $query->andFilterWhere([
            'status' => $this->status,
            'is_admin' => 0, //$this->is_admin,
            //'is_admin' => 0, // Not admin users.
            'is_deleted' => 0,
        ]);

        // Username and email.
        $query->andFilterWhere(['like', 'username', $this->username])
                ->andFilterWhere(['like', 'email', $this->email]);

        // Date condition - start - end.
        if (!empty($this->created_at)) {
            $date_range = explode('to', $this->created_at);
            $query->andFilterWhere(['between', 'created_at', $date_range[0], $date_range[1]]);
        }

        return $dataProvider;
    }
}
