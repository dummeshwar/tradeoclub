<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\base\InvalidParamException;
use app\models\User;


class ResetMasterPinForm extends Model {

    public $master_pin;
    public $master_pin_repeat;
    public $original_master_pin;

    /**
     * @var app\models\User
     */
    private $_user;

    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($user_logged_in = FALSE, $token, $config = []) {

        if ($user_logged_in) {
            $this->_user = $user_logged_in;
        } else {
            if (empty($token) || !is_string($token)) {
                throw new InvalidParamException('Password reset token cannot be blank.');
            }
            $this->_user = User::findByPasswordResetToken($token);
            if (!$this->_user) {
                throw new InvalidParamException('Wrong password reset token.');
            }
        }
        parent::__construct($config);
    }

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            [['master_pin', 'master_pin_repeat'], 'required'],
            [['master_pin', 'master_pin_repeat'], 'string', 'min' => 6],
            [['original_master_pin'], 'string'],
            ['master_pin_repeat', 'compare', 'compareAttribute' => 'master_pin', 'message' => "Key pin don't match"],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels() {
        return [
            'master_pin' => 'Key Pin',
            'master_pin_repeat' => 'Key Pin Repeat',
        ];
    }

    /**
     * Reset key pin by token (Not logged in users).
     *
     * @return bool if password was reset.
     */
    public function resetMasterPinByToken() {
        $user = $this->_user;
        $user->setOriginalMasterPin($this->master_pin);
        $user->setMasterPin($this->master_pin);
        $user->removePasswordResetToken();

        if (!$user->save(false)) {
            throw new Exception("Something went wrong!. Please try again later.");
        }
        return true;
    }

    /**
     * Reset MasterPin for logged in users.
     */
    public function resetMasterPinForLoggedInUser() {
        $user = $this->_user;
        $user->setOriginalMasterPin($this->master_pin);
        $user->setMasterPin($this->master_pin);
        if (!$user->save(false)) {
            throw new Exception("Something went wrong!. Please try again later.");
        }
        return true;
    }

}
