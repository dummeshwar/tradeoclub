<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AdminPopUps;

/**
 * AdminPopUpsSearch represents the model behind the search form of `app\models\AdminPopUps`.
 */
class AdminPopUpsSearch extends AdminPopUps
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'image_desktop', 'image_mobile', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AdminPopUps::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'updated_at' => $this->updated_at,
            // 'created_at' => $this->created_at,
        ]);

        // $query->andFilterWhere(['like', 'name', $this->name])
        //     ->andFilterWhere(['like', 'image_desktop', $this->image_desktop])
        //     ->andFilterWhere(['like', 'image_mobile', $this->image_mobile]);

            $query->andFilterWhere(['like', 'name', $this->name]);
                // ->andFilterWhere(['like', 'image', $this->image])
                // ->andFilterWhere(['like', 'description', $this->description]);

             // Date condition - start - end.
            if (!empty($this->created_at)) {
                $date_range = explode('to', $this->created_at);
                $query->andFilterWhere(['between', 'created_at', $date_range[0], $date_range[1]]);
            }

        return $dataProvider;
    }
}
