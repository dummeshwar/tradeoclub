<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "package".
 *
 * @property int $id
 * @property string $name
 * @property double $price
 * @property string $image
 * @property string $description
 * @property int $no_of_days How long the package will stay active.
 * @property int $status
 * @property double $daily_returns
 * @property string $updated_at
 * @property string $created_at
 * @property string $points Profitrexpoints for TGC wallet.
 */
class Package extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'package';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
//        return [
//            [['name', 'no_of_days', 'daily_returns', 'price', 'points'], 'required'],
//            [['price', 'daily_returns', 'points'], 'number'],
//            ['price', 'compare', 'compareValue' => 0, 'operator' => '>', 'type' => 'number'],
//            ['price', 'compare', 'compareValue' => 100000, 'operator' => '<=', 'type' => 'number'],
//            ['daily_returns', 'compare', 'compareValue' => 100, 'operator' => '<=', 'type' => 'number'],
//            ['daily_returns', 'compare', 'compareValue' => 0, 'operator' => '>', 'type' => 'number'],
//            ['points', 'compare', 'compareValue' => 0, 'operator' => '>=', 'type' => 'number'],
//            [['description'], 'string'],
//            [['no_of_days', 'status'], 'integer'],
//            [['no_of_days'], 'integer', 'min'=>7, 'max'=>2000],
//            [['updated_at', 'created_at'], 'safe'],
//            [['name'], 'string', 'max' => 255],
//            [['image'], 'file', 'extensions' => 'jpg, png, jpeg', 'skipOnEmpty' => true, 'mimeTypes' => 'image/jpeg, image/png',],
//        ];
        
        return [
            [['name', 'daily_returns', 'price'], 'required'],
            [['price', 'daily_returns'], 'number'],
            ['price', 'compare', 'compareValue' => 0, 'operator' => '>', 'type' => 'number'],
            ['price', 'compare', 'compareValue' => 100000, 'operator' => '<=', 'type' => 'number'],
            ['daily_returns', 'compare', 'compareValue' => 100, 'operator' => '<=', 'type' => 'number'],
            ['daily_returns', 'compare', 'compareValue' => 0, 'operator' => '>', 'type' => 'number'],
//            ['points', 'compare', 'compareValue' => 0, 'operator' => '>=', 'type' => 'number'],
            [['description'], 'string'],
//            [['no_of_days', 'status'], 'integer'],
//            [['no_of_days'], 'integer', 'min'=>7, 'max'=>2000],
            [['updated_at', 'created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'jpg, png, jpeg', 'skipOnEmpty' => true, 'mimeTypes' => 'image/jpeg, image/png',],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
            'image' => 'Image',
            'description' => 'Description',
            'no_of_days' => 'No Of Days',
            'status' => 'Status',
            'daily_returns' => 'Daily Returns (%)',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
            'points' => 'Points',
        ];
    }

    public function upload() {
        if ($this->validate()) {
            $this->image->saveAs('uploads/package/' . $this->image->baseName . '.' . $this->image->extension);
            return true;
        } else {
            return false;
        }
    }

    /**
     * It returns whether package is active OR blocked.
     * @return type string.
     */
    public function getPackageStatus() {
        return $this->status === 1 ? "Active" : "Blocked";
    }

}
