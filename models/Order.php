<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $user_id
 * @property int $package_id
 * @property int $transaction_id
 * @property string $start_date
 * @property string $end_date
 * @property int $status
 * @property string $updated_at
 * @property string $created_at
 *
 * @property Package $package
 * @property Transaction $transaction
 * @property User $user
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'package_id', 'transaction_id'], 'required'],
            [['user_id', 'package_id', 'transaction_id', 'status'], 'integer'],
            [['start_date', 'end_date', 'updated_at', 'created_at'], 'safe'],
            [['package_id'], 'exist', 'skipOnError' => true, 'targetClass' => Package::className(), 'targetAttribute' => ['package_id' => 'id']],
            [['transaction_id'], 'exist', 'skipOnError' => true, 'targetClass' => Transaction::className(), 'targetAttribute' => ['transaction_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'package_id' => 'Package ID',
            'transaction_id' => 'Transaction ID',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'status' => 'Status',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackage()
    {
        return $this->hasOne(Package::className(), ['id' => 'package_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['id' => 'transaction_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    /**
     * Create new order.
     */

    public static function createNewOrder($user_id, $transaction_id, $packageObj) {
        $order = new Order();

        $order->user_id = $user_id;
        $order->package_id = $packageObj->id;
        $order->transaction_id = $transaction_id;
        $order->start_date = date('Y-m-d', time());

        // Calulate expire date for the order.
        $todayDate = date('Y-m-d');
        $no_of_days = "+ " . $packageObj->no_of_days . "days";
        $newdate = date('Y-m-d', strtotime($todayDate . $no_of_days));
        $order->end_date = $newdate;
        $order->status = 1;

        if (!$order->save()) {
            throw new Exception("Something went wrong!. Please try again later.");
        }
        return $order;

    }

    public function createPackageOrder($userObj, $model, $transactionObj) {
        $this->user_id = $userObj->id;
        $this->transaction_id = $transactionObj->id;
        $this->package_id = $model->id;        
        $this->start_date = date('Y-m-d', time());

        $todayDate = date('Y-m-d');
        $no_of_days = "+ " . $model->no_of_days . "days";
        $newdate = date('Y-m-d', strtotime($todayDate . $no_of_days));
        $this->end_date = $newdate; // add package expire days.
        $this->status = 1;
    }

    /**
     * order status.
     * @return type string.
     */
    public function getOrderStatus() {
        return $this->status === 1 ? "Success" : "Expired";
        ;
    }

}
