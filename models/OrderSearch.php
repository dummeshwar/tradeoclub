<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;

/**
 * OrderSearch represents the model behind the search form of `app\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'package_id', 'transaction_id', 'status'], 'integer'],
            [['start_date', 'end_date', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            // 'id' => $this->id,
             'user_id' => Yii::$app->user->identity->id,
            // 'package_id' => $this->package_id,
            // 'transaction_id' => $this->transaction_id,
            // 'start_date' => $this->start_date,
            // 'end_date' => $this->end_date,
            // 'status' => $this->status,
            // 'updated_at' => $this->updated_at,
            // 'created_at' => $this->created_at,
        ]);

        // Date condition - start - end.
        if (!empty($this->start_date)) {
            $date_range = explode('to', $this->start_date);
            $query->andFilterWhere(['between', 'start_date', $date_range[0], $date_range[1]]);
        }

        // Date condition - start - end.
        if (!empty($this->end_date)) {
            $date_range = explode('to', $this->end_date);
            $query->andFilterWhere(['between', 'end_date', $date_range[0], $date_range[1]]);
        }

        return $dataProvider;
    }
}
