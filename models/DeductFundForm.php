<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Utils;

/**
 * CreditWalletForm is the model behind the contact form.
 */
class DeductFundForm extends Model
{
    public $from_user;
    public $from_wallet;
    public $amount;
    public $comment;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['from_user', 'from_wallet', 'amount'], 'required'],
            [['amount'], 'number', 'min'=>0.01, 'max'=>1000000],
            ['from_user', 'isUserExist'],
            [['amount'], 'haveSufficientBalance'],
            [['comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'from_user' => 'Select User',
            'from_wallet' => 'Wallet Name',
            'amount' => 'Amount ($)',
            'comment' => 'Comment',
        ];
    }

    public function isUserExist($attribute, $params)
    {
        if (!Utils::findByUserName($this->from_user)) {
            $this->addError($attribute, 'User does not exist.');
        }
    }

    /**
     * Check for sufficient balance user has.
     */
    public function haveSufficientBalance($attribute, $params) {
        $fromUserObj = Utils::findByUserName($this->from_user);

        if ($fromUserObj) {
            $wallet_balance = Utils::walletMoney(($this->from_wallet + 1), $fromUserObj->id); // Wallet id  coming from zero so  + 1.
            if ($this->amount > $wallet_balance) {
               $this->addError($attribute, 'User does not have that balance which you entered.');
            } 
        } else {
            $this->addError('from_user', 'User does not exist.');
        }
    }

}
