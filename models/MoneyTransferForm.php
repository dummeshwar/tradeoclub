<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Utils;

/**
 * ContactForm is the model behind the contact form.
 */
class MoneyTransferForm extends Model
{
    public $from_wallet;
    public $to_user;
    public $to_wallet;
    public $amount;
    public $comment;
    public $to_myself;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['from_wallet', 'to_wallet', 'amount', 'comment'], 'required'],
            ['to_user', 'isUserExist'],
            ['amount', 'number', 'min' => 0.01, 'max' => 1000000],
            [['amount', 'from_wallet'], 'haveSufficientBalance'],
            [['comment'], 'string', 'max' => 255],
            ['to_myself', 'boolean'],
            [['to_myself', 'from_wallet', 'to_wallet'], 'checkForSameWallet'],
            [['to_myself', 'to_user'], 'checkForYourSelf'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'to_user' => 'Select User',
            'from_wallet' => 'From Wallet Type',
            'to_wallet' => 'To Wallet Type',
            'amount' => 'Amount',
            'comment' => 'Comment',
        ];
    }


    /**
     * Check for sufficient balance user has.
     */
    public function haveSufficientBalance($attribute, $params) {
        $wallet_balance = Utils::walletMoney($this->from_wallet);
        $final_amount_after_admin_charges = $this->amount + ($this->amount * Yii::$app->params['transaction_charges']['money_transfer'] / 100);
        if ($final_amount_after_admin_charges > $wallet_balance) {
            $this->addError('amount', 'Please check your wallent balance and transfer (Admin charges - ' . Yii::$app->params['transaction_charges']['money_transfer'] . '%).');
        }
    }

    /**
     *
     */
    public function checkForSameWallet($attribute, $params) {
        if ($this->to_myself && ($this->from_wallet == $this->to_wallet)) {
            $this->addError('to_myself', 'Please change - From Wallet / To Wallet. Same cannot be possible for same user.');
        }
    }

    /**
     * Check user exist or not.
     */
    public function isUserExist($attribute, $params) {
        if (!User::findByUserName($this->to_user)) {
            $this->addError($attribute, 'User does not exist.');
        }
    }

    public function checkForYourSelf($attribute, $params) {
        if (!$this->to_myself && empty($this->to_user)) {
            $this->addError('to_user', 'Please select user.');
        }
    }

}
