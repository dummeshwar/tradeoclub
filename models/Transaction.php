<?php

namespace app\models;
use yii\base\Exception;

use Yii;

/**
 * This is the model class for table "transaction".
 *
 * @property int $id
 * @property string $transaction_id Unique id for each transaction.
 * @property string $payment_transaction_id Response (unique ID) from online transactions.
 * @property int $user_id Initiated the transaction.
 * @property int $payment_gateway_id Gateway ID ex: Bitcoin. Null meaning no Gateway used.
 * @property double $actual_amount Actual amount without commission.
 * @property double $paid_amount Full amount inlcudes commission.
 * @property string $transaction_mode Modes of transactions - ex : Credit, Debit, Trasfer etc. Check it in config -> params file.
 * @property int $to_wallet_id
 * @property int $from_wallet_id
 * @property int $to_user_id
 * @property int $from_user_id
 * @property string $user_comment
 * @property string $admin_comment
 * @property int $admin_marked_status 0:Failed 1:Success/Approved, 2:Pendning.
 * @property int $status 0:Failed 1:Success/Approved, 2:Pendning.
 * @property string $updated_at
 * @property string $created_at
 *
 * @property MoneyTransfer[] $moneyTransfers
 * @property User $fromUser
 * @property Wallet $fromWallet
 * @property PaymentGateway $paymentGateway
 * @property User $toUser
 * @property Wallet $toWallet
 * @property User $user
 */
class Transaction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['transaction_id', 'user_id', 'transaction_mode'], 'required'],
            [['user_id', 'payment_gateway_id', 'to_wallet_id', 'from_wallet_id', 'to_user_id', 'from_user_id', 'admin_marked_status', 'status'], 'integer'],
            [['actual_amount', 'paid_amount'], 'number'],
            [['user_comment', 'admin_comment'], 'string'],
            [['updated_at', 'created_at'], 'safe'],
            [['payment_transaction_id', 'transaction_mode'], 'string', 'max' => 255],
            [['from_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['from_user_id' => 'id']],
            [['from_wallet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Wallet::className(), 'targetAttribute' => ['from_wallet_id' => 'id']],
            //[['payment_gateway_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentGateway::className(), 'targetAttribute' => ['payment_gateway_id' => 'id']],
            [['to_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['to_user_id' => 'id']],
            [['to_wallet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Wallet::className(), 'targetAttribute' => ['to_wallet_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'transaction_id' => 'Transaction ID',
            'payment_transaction_id' => 'Payment Transaction ID',
            'user_id' => 'User ID',
            'payment_gateway_id' => 'Payment Gateway ID',
            'actual_amount' => 'Actual Amount',
            'paid_amount' => 'Paid Amount',
            'transaction_mode' => 'Transaction Mode',
            'to_wallet_id' => 'To Wallet ID',
            'from_wallet_id' => 'From Wallet ID',
            'to_user_id' => 'To User ID',
            'from_user_id' => 'From User ID',
            'user_comment' => 'User Comment',
            'admin_comment' => 'Admin Comment',
            'admin_marked_status' => 'Admin Marked Status',
            'status' => 'Status',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoneyTransfers()
    {
        return $this->hasMany(MoneyTransfer::className(), ['transaction_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromUser()
    {
        return $this->hasOne(User::className(), ['id' => 'from_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromWallet()
    {
        return $this->hasOne(Wallet::className(), ['id' => 'from_wallet_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentGateway()
    {
        return $this->hasOne(PaymentGateway::className(), ['id' => 'payment_gateway_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToUser()
    {
        return $this->hasOne(User::className(), ['id' => 'to_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToWallet()
    {
        return $this->hasOne(Wallet::className(), ['id' => 'to_wallet_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    /**
     * Get transaction status.
     */
    public function getTransactionStatus() {
        if ($this->status == 0 ) return "Rejected";
        if ($this->status == 1 ) return "Success";
        if ($this->status == 2 ) return "Pending";
    }

    /**
     * Get transaction status by admin.
     */
    public function getTransactionStatusGivenByAdmin() {
        if ($this->admin_marked_status == 0 ) return "Rejected";
        if ($this->admin_marked_status == 1 ) return "Success";
        if ($this->admin_marked_status == 2 ) return "Pending";
    }

    /**
     * Get wallets.
     */
    public function getFromWalletNames() {
        if (!empty(Yii::$app->params['wallets_by_keys'][$this->from_wallet_id])) {
            return Yii::$app->params['wallets_by_keys'][$this->from_wallet_id];
        } else {
            return "None";
        }
    }


    /**
     * Get wallets.
     */
    public function getToWalletNames() {
        if (!empty(Yii::$app->params['wallets_by_keys'][$this->to_wallet_id])) {
            return Yii::$app->params['wallets_by_keys'][$this->to_wallet_id];
        } else {
            return "None";
        }
    }


    /**
     * Create transaction for user.
     */
     public static function createNewTransaction($to_user_id, $from_wallet, $to_wallet, $comment, $actual_amount, $paid_amount, $transaction_mode, $from_user_id = Null) {

        $transaction = new Transaction();
        $transaction->transaction_id = mt_rand();

        $transaction->actual_amount = $actual_amount;
        $transaction->paid_amount = $paid_amount;
        $transaction->transaction_mode = $transaction_mode;

        switch ($transaction_mode) {
            case 'MONEY TRANSFER':
                $transaction->from_wallet_id = $from_wallet;
                $transaction->to_wallet_id = $to_wallet;
                $transaction->from_user_id = Yii::$app->user->identity->id;
                $transaction->to_user_id = $to_user_id;
                $transaction->user_comment = $comment;
                $transaction->admin_marked_status = 1;
                $transaction->status = 1;
                $transaction->user_id = Yii::$app->user->identity->id;

                break;

            case 'WITHDRAWAL REQUEST':
                $transaction->from_wallet_id = $from_wallet;
                $transaction->admin_marked_status = 2;
                $transaction->user_comment = $comment;
                $transaction->status = 2;
                $transaction->user_id = Yii::$app->user->identity->id;
                break;

            case 'PACKAGE PURCHASE':
                $transaction->from_wallet_id = $from_wallet;
                $transaction->admin_marked_status = 1;
                $transaction->user_comment = $comment;
                $transaction->status = 1;
                $transaction->user_id = Yii::$app->user->identity->id;
                break;

            case "DIRECT COMMISSION":
                $transaction->from_wallet_id = $from_wallet;
                $transaction->to_wallet_id = $to_wallet;
                $transaction->admin_marked_status = 1;
                $transaction->user_comment = $comment;
                $transaction->to_user_id = $to_user_id;
                $transaction->user_id = $transaction->from_user_id = 1; // Admin , assume admin given commission to user.
                $transaction->status = 1;
                break;

            case "BINARY COMMISSION":
                $transaction->from_wallet_id = $from_wallet;
                $transaction->to_wallet_id = $to_wallet;
                $transaction->admin_marked_status = 1;
                $transaction->user_comment = $comment;
                $transaction->to_user_id = $to_user_id;
                $transaction->user_id = $transaction->from_user_id = 1; // Admin , assume admin given commission to user.
                $transaction->status = 1;
                break;

            case "ADMIN: AMOUNT CREDITED":
                $transaction->from_wallet_id = $from_wallet;
                $transaction->to_wallet_id = $to_wallet;
                $transaction->admin_marked_status = 1;
                $transaction->user_comment = $comment;
                $transaction->to_user_id = $to_user_id;
                $transaction->user_id = $transaction->from_user_id = 1;
                $transaction->status = 1;
                break;

            case "ADMIN: AMOUNT DEBITED":
                $transaction->from_wallet_id = $from_wallet;
                $transaction->to_wallet_id = $to_wallet;
                $transaction->admin_marked_status = 1;
                $transaction->user_comment = $comment;
                $transaction->to_user_id = $to_user_id;
                $transaction->from_user_id = $from_user_id;
                $transaction->user_id = 1;
                $transaction->status = 1;
                break;

            case "ADMIN: WITHDRAWAL REQUEST PROCESSED":
                $transaction->from_wallet_id = $from_wallet;
                $transaction->to_wallet_id = $to_wallet;
                $transaction->admin_marked_status = 1;
                $transaction->user_comment = $comment;
                $transaction->to_user_id = $to_user_id;
                $transaction->from_user_id = $from_user_id;
                //$transaction->user_id = 1;
                $transaction->status = 1;
                break;

            case "ADMIN: WITHDRAWAL REQUEST REJECTED":
                $transaction->from_wallet_id = $from_wallet;
                $transaction->to_wallet_id = $to_wallet;
                $transaction->admin_marked_status = 1;
                $transaction->user_comment = $comment;
                $transaction->admin_comment = "MONEY PAID BACK";
                $transaction->to_user_id = $to_user_id;
                $transaction->from_user_id = $from_user_id;
                $transaction->user_id = 1;
                $transaction->status = 1;
                break;


            case "USER: ADD FUND":
                $transaction->to_wallet_id = $to_wallet;
                $transaction->admin_marked_status = 2;
                $transaction->user_comment = $comment;
                $transaction->user_id = $to_user_id;
                $transaction->to_user_id = $to_user_id;
                $transaction->status = 2;
                break;

            case "DAILY RETURNS":
                $transaction->to_wallet_id = $to_wallet;
                $transaction->admin_marked_status = 1;
                $transaction->user_comment = $comment;
                $transaction->to_user_id = $to_user_id;
                $transaction->user_id = $to_user_id;
                $transaction->from_user_id = 1; // Admin , assume admin given commission to user.
                $transaction->status = 1;
                break;

              case 'TGC TOKENS PURCHASED':
                  $transaction->from_wallet_id = $from_wallet;
                  $transaction->admin_marked_status = 1;
                  $transaction->user_comment = $comment;
                  $transaction->status = 1;
                  $transaction->user_id = Yii::$app->user->identity->id;
                  break;

              case "TGC TOKENS ADDED":
                  $transaction->to_wallet_id = $to_wallet;
                  $transaction->admin_marked_status = 1;
                  $transaction->user_comment = $comment;
                  $transaction->user_id = $to_user_id;
                  $transaction->to_user_id = $to_user_id;
                  $transaction->from_user_id = 1; // Admin , assume admin given commission to user.
                  $transaction->status = 1;
                  break;

              case 'TGC Tokens From Package Purchase':
                  $transaction->from_wallet_id = $from_wallet;
                  $transaction->to_wallet_id = $to_wallet;
                  $transaction->user_id = $to_user_id;
                  $transaction->to_user_id = $to_user_id;
                  $transaction->from_user_id = $from_user_id;
                  $transaction->admin_marked_status = 1;
                  $transaction->user_comment = $comment;
                  $transaction->status = 1;
                  break;

            default:
                break;
        }


        if (!$transaction->save()) {
            print_r($transaction->getErrors()); exit;
            throw new Exception("Something went wrong!. Please try again later.");
        }
        return $transaction;
    } 

    /**
     * [createNewTransaction description]
     * @param  [type] $curUserId       [integer - who is doing this transaction]
     * @param  [type] $modelObj         [description]
     * @param  string $transaction_mode [description]
     * @return [type]                   [description]
     */
    /* public function createNewTransaction($curUserId, $currentModel, $actual_amount, $paid_amount, $transaction_mode) {
        $this->user_id = $curUserId; // Who is doing transaction - user id
        //$this->payment_gateway_id = \Yii::$app->params['payment_gateways']['manual_payment']; // Default will be manual as of now.
        $this->payment_gateway_id = !empty($currentModel->e_currency) ? ($currentModel->e_currency + 1) : \Yii::$app->params['payment_gateways']['manual_payment'];

        // Actaul and paid amount.
        $this->actual_amount = $actual_amount;
        $this->paid_amount = $paid_amount;
        $this->transaction_mode = $transaction_mode;


        switch ($transaction_mode) {
                case 'Add Fund':
                    $this->admin_marked_status = 1;
                    $this->status = 1;
                    break;

                case 'Purchase':
                    $this->admin_marked_status = 1;
                    $this->status = 1;
                    break;

                case 'Withdrawal Request':
                    $this->from_wallet_id = $currentModel->from_wallet;
                    $this->admin_marked_status = 2;
                    $this->user_comment = $currentModel->comment;
                    $this->status = 2;
                    break;

                case 'Fund Transfer':
                    $this->from_wallet_id = $currentModel->from_wallet;
                    $this->to_wallet_id = $currentModel->to_wallet;
                    $this->user_comment = $currentModel->comment;
                    $this->admin_marked_status = 1;
                    $this->status = 1;
                    break;

                default:
                    # code...
                    break;
            }
        } */


    /**
     * Update exsting transaction - For withdrawal request process.
     */

    public static function updateExistingTransaction($transactionObj, $currentModel) {
        $transactionObj->status = $currentModel->is_approved; // assign status.
        $transactionObj->admin_marked_status = $currentModel->is_approved; // assign status.
        $transactionObj->admin_comment = $currentModel->comment;

        if (!$transactionObj->save(FALSE)) {
            throw new Exception("Something went wrong!. Please try again later.");
        }
    }

    /**
     * Create new entry to transaction.
     */
    public function createNewEntryInTransaction($user_id, $payment_gateway_id, $actual_amount, $paid_amount, $transaction_mode, $admin_marked_status, $status, $user_comment, $admin_comment, $from_wallet_id, $to_wallet_id, $from_user_id, $to_user_id) {
        $this->user_id = $user_id;
        $this->payment_gateway_id = $payment_gateway_id;
        $this->actual_amount = $actual_amount;
        $this->paid_amount = $paid_amount;
        $this->transaction_mode = $transaction_mode;
        $this->admin_marked_status = $admin_marked_status;
        $this->status = $status;
        $this->user_comment = $user_comment;
        $this->admin_comment = $admin_comment;
        $this->from_wallet_id = $from_wallet_id;
        $this->to_wallet_id = $to_wallet_id;
        $this->from_user_id = $from_user_id;
        $this->to_user_id = $to_user_id;
        $this->save();
    }
}
