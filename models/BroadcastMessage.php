<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "broadcast_message".
 *
 * @property int $id
 * @property string $description
 * @property int $status
 * @property string $updated_at
 * @property string $created_at
 */
class BroadcastMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'broadcast_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'required'],
            [['description'], 'string'],
            [['status'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'status' => 'Status',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * It returns whether package is active OR blocked.
     * @return type string.
     */
    public function getMessageStatus() {
        return $this->status === 1 ? "Active" : "Blocked";
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        // Set default values for User.
        if ($this->isNewRecord) {
            // New record.
            $this->created_at = date('Y-m-d H:i:s', time());
            $this->updated_at = date('Y-m-d H:i:s', time());
        } else {
            // Updated record.
            $this->updated_at = date('Y-m-d H:i:s', time());
        }
        return parent::beforeSave($insert);
    }

}
