<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "admin_pop_ups".
 *
 * @property int $id
 * @property string $name
 * @property string $image_desktop
 * @property string $image_mobile
 * @property int $status
 * @property string $updated_at
 * @property string $created_at
 */
class AdminPopUps extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin_pop_ups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['updated_at', 'created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['status'], 'integer'],
            [['image_desktop', 'image_mobile'], 'file', 'extensions' => 'jpg, png, jpeg', 'skipOnEmpty' => true, 'mimeTypes' => 'image/jpeg, image/png',],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'image_desktop' => 'Upload Desktop Image',
            'image_mobile' => 'Upload Mobile Image',
            'status' => 'Status',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        // Set default values for User.
        if ($this->isNewRecord) {
            // New record.
            $this->created_at = date('Y-m-d H:i:s', time());
            $this->updated_at = date('Y-m-d H:i:s', time());
        } else {
            // Updated record.
            $this->updated_at = date('Y-m-d H:i:s', time());
        }
        return parent::beforeSave($insert);
    }

    public function upload_desktop_image() {
        if ($this->validate()) {
            $this->image_desktop->saveAs('uploads/admin-pop-ups/' . $this->image_desktop->baseName . '.' . $this->image_desktop->extension);
            return true;
        } else {
            return false;
        }
    }

    public function upload_mobile_image() {
        if ($this->validate()) {
            $this->image_mobile->saveAs('uploads/admin-pop-ups/' . $this->image_mobile->baseName . '.' . $this->image_mobile->extension);
            return true;
        } else {
            return false;
        }
    }

    /**
     * It returns whether package is active OR blocked.
     * @return type string.
     */
    public function getPopUpStatus() {
        return $this->status === 1 ? "Active" : "Blocked";
    }

}
