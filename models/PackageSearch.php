<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Package;

/**
 * PackageSearch represents the model behind the search form of `app\models\Package`.
 */
class PackageSearch extends Package
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'no_of_days', 'status'], 'integer'],
            [['name', 'image', 'description', 'updated_at', 'created_at'], 'safe'],
            [['price', 'daily_returns'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Package::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'price' => $this->price,
            // 'no_of_days' => $this->no_of_days,
            'status' => $this->status,
            // 'daily_returns' => $this->daily_returns,
            // 'updated_at' => $this->updated_at,
            // 'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
            // ->andFilterWhere(['like', 'image', $this->image])
            // ->andFilterWhere(['like', 'description', $this->description]);

         // Date condition - start - end.
        if (!empty($this->created_at)) {
            $date_range = explode('to', $this->created_at);
            $query->andFilterWhere(['between', 'created_at', $date_range[0], $date_range[1]]);
        }
        return $dataProvider;
    }
}
