<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Utils;

/**
 * CreditWalletForm is the model behind the contact form.
 */
class UserAddFundForm extends Model
{
    public $amount;
 

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['amount'], 'required'],
            [['amount'], 'number', 'min'=>0.0001, 'max'=>100],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'amount' => 'Amount (BTC)',
        ];
    }
    
}
