<?php

namespace app\models;
use yii\base\Exception;

use Yii;

/**
 * This is the model class for table "balance".
 *
 * @property int $id
 * @property int $user_id
 * @property int $wallet_id
 * @property double $amount
 * @property string $updated_at
 * @property string $created_at
 *
 * @property User $user
 * @property Wallet $wallet
 */
class Balance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'balance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'wallet_id'], 'required'],
            [['wallet_id'], 'integer'],
            [['amount'], 'number', 'min'=>10, 'max'=>10000],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['wallet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Wallet::className(), 'targetAttribute' => ['wallet_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'wallet_id' => 'Wallet ID',
            'amount' => 'Amount',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWallet()
    {
        return $this->hasOne(Wallet::className(), ['id' => 'wallet_id']);
    }

    /**
     * Create user wallets based on userObj.
     */
    public static function createWallets($userObject) {
        // create all wallets with 0.00 balance for new user.
        // CASH WALLET.
        $balance = new Balance();
        $balance->user_id = $userObject->id;
        $balance->wallet_id = \Yii::$app->params['wallets']['CASH_WALLET'];
        if (!$balance->save()) {
            throw new Exception("Something went wrong!. Please try again later.");
        }


        // ADS WALLET.
        $balance = new Balance();
        $balance->user_id = $userObject->id;
        $balance->wallet_id = \Yii::$app->params['wallets']['REFERRAL_WALLET'];
        if (!$balance->save()) {
            throw new Exception("Something went wrong!. Please try again later.");
        }

        // COMMISSION WALLET.
        $balance = new Balance();
        $balance->user_id = $userObject->id;
        $balance->wallet_id = \Yii::$app->params['wallets']['BINARY_WALLET'];
        if (!$balance->save()) {
            throw new Exception("Something went wrong!. Please try again later.");
        }

        // TRAVEL_WALLET WALLET.
        $balance = new Balance();
        $balance->user_id = $userObject->id;
        $balance->wallet_id = \Yii::$app->params['wallets']['TRADEOCLUB_WALLET'];
        if (!$balance->save()) {
            throw new Exception("Something went wrong!. Please try again later.");
        }

        // PROMO_WALLET WALLET.
        $balance = new Balance();
        $balance->user_id = $userObject->id;
        $balance->wallet_id = \Yii::$app->params['secondary_wallets']['PROMO_WALLET'];
        if (!$balance->save()) {
            throw new Exception("Something went wrong!. Please try again later.");
        }
    }


    /**
     * Get use wallet money. Default cash wallet.
     *
     * @param type $user_id
     * @param type $wallet_id
     * @return type
     */
    public static function getUserWalletMoney($user_id, $wallet_id = 1) {
        return Balance::find()->select('amount')->where(['user_id' => $user_id, 'wallet_id' => $wallet_id])->one()->amount;
    }


    /**
     * Update user balance.
     */
    public static function findAndUpdate($user_id, $wallet_id, $amount, $operation) {
        $balance = Balance::find()->where(['user_id' => $user_id, 'wallet_id' => $wallet_id])->one();

        switch ($operation) {
            case 'DEBIT':
                $balance->amount = $balance->amount - $amount;
                break;
            case 'CREDIT':
                $balance->amount = $balance->amount + $amount;
                break;

            default:
                break;
        }

        if (!$balance->save(FALSE)) {
            throw new Exception("Something went wrong!. Please try again later.");
        }

        return $balance;
    }

    public function updateNewBalance($fund) {
        $this->amount = $fund;
    }

    public static function updateUserBalance($user_id, $wallet_id, $fund) {
        $balanceObj = Balance::find()->where(['user_id' => $user_id, 'wallet_id' => $wallet_id])->one();
        $balanceObj->amount += $fund;
        $balanceObj->save(false);
    }

}
