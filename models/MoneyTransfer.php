<?php

namespace app\models;
use yii\base\Exception;

use Yii;

/**
 * This is the model class for table "money_transfer".
 *
 * @property int $id
 * @property int $to_user_id
 * @property int $from_user_id
 * @property int $transaction_id
 * @property int $to_wallet_id
 * @property int $from_wallet_id
 * @property double $amount
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $fromUser
 * @property Wallet $fromWallet
 * @property User $toUser
 * @property Wallet $toWallet
 * @property Transaction $transaction
 */
class MoneyTransfer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'money_transfer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['to_user_id', 'from_user_id', 'transaction_id', 'to_wallet_id', 'from_wallet_id'], 'required'],
            [['to_user_id', 'from_user_id', 'transaction_id', 'to_wallet_id', 'from_wallet_id', 'status'], 'integer'],
            [['amount'], 'number'],
            [['created_at'], 'safe'],
            [['updated_at'], 'string', 'max' => 255],
            [['from_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['from_user_id' => 'id']],
            [['from_wallet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Wallet::className(), 'targetAttribute' => ['from_wallet_id' => 'id']],
            [['to_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['to_user_id' => 'id']],
            [['to_wallet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Wallet::className(), 'targetAttribute' => ['to_wallet_id' => 'id']],
            [['transaction_id'], 'exist', 'skipOnError' => true, 'targetClass' => Transaction::className(), 'targetAttribute' => ['transaction_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'to_user_id' => 'To User ID',
            'from_user_id' => 'From User ID',
            'transaction_id' => 'Transaction ID',
            'to_wallet_id' => 'To Wallet ID',
            'from_wallet_id' => 'From Wallet ID',
            'amount' => 'Amount',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromUser()
    {
        return $this->hasOne(User::className(), ['id' => 'from_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromWallet()
    {
        return $this->hasOne(Wallet::className(), ['id' => 'from_wallet_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToUser()
    {
        return $this->hasOne(User::className(), ['id' => 'to_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToWallet()
    {
        return $this->hasOne(Wallet::className(), ['id' => 'to_wallet_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['id' => 'transaction_id']);
    }

    /**
     * Create new Money Transfer Entry.
     */
    public static function createNewMoneyTransferEntry($fromUserId, $toUserId, $currentModel, $from_wallet_id, $to_wallet_id, $transactionObj, $amount, $status = 0) {

            $moneyTransfer =  new MoneyTransfer();
            $moneyTransfer->to_user_id = $toUserId;
            $moneyTransfer->from_user_id = $fromUserId;
            $moneyTransfer->transaction_id = $transactionObj->id;
            $moneyTransfer->from_wallet_id = $from_wallet_id;
            $moneyTransfer->to_wallet_id = $to_wallet_id;
            $moneyTransfer->amount = $amount;
            $moneyTransfer->status = $status;
            //$moneyTransfer->comment = $currentModel->comment;
            $moneyTransfer->status = $status;

            if (!$moneyTransfer->save()) {
                throw new Exception("Something went wrong!. Please try again later.");
            }
    }
}
