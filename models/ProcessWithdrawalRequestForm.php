<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Utils;

/**
 * CreditWalletForm is the model behind the contact form.
 */
class ProcessWithdrawalRequestForm extends Model
{
    public $comment;
    public $is_approved;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['is_approved', 'comment'],  'required'],
            ['is_approved',  'integer'],
            [['comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'comment' => 'Your comment ( Remarks )',
            'is_approved' => 'Select status',
        ];
    }

}
