<?php

namespace app\models;

use Yii;
use yii\base\Model;
use borales\extensions\phoneInput\PhoneInputValidator;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RegistrationForm extends Model
{
    public $parent_id;
    public $position;
    //public $contact_number;
    public $username;
    public $email;
    public $verifyCode;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            //[['position', 'contact_number', 'username', 'email'], 'required'],
            [['position','username', 'email'], 'required'],
            ['parent_id', 'required', 'message' => 'Sponsor Id is required.'],
            //[['contact_number'], 'string'],
            //[['contact_number'], PhoneInputValidator::className()],
            ['parent_id', 'isValidSponsor'],
            ['email', 'email'],
            ['username', 'isUserUnique'],
            ['email', 'isEmailUnique'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * Custom Validater
     * @param type $attribute
     * @param type $params
     * @param type $validator
     */
    public function isValidSponsor($attribute, $params, $validator) {
        if (!in_array($this->$attribute, User::getActiveUsers())) {
            $this->addError($attribute, "Sponsor Id doesn't exist");
        }
    }

    /**
     * Custom Validater
     * @param type $attribute
     * @param type $params
     * @param type $validator
     */
    public function isUserUnique($attribute, $params, $validator) {
        if (User::findByUsername($this->$attribute, 2) !== null) {
            $this->addError($attribute, "User already exist.");
        }
    }

    /**
     * Custom Validater
     * @param type $attribute
     * @param type $params
     * @param type $validator
     */
    public function isEmailUnique($attribute, $params, $validator) {
        if ( User::findByEmailId($this->$attribute, 2) !== null) {
            $this->addError($attribute, "Email ID already exist.");
        }
    }
}
