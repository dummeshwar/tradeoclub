<?php

namespace app\models;

use Yii;
use Yii\helpers\Utils;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property int $parent_id Direct Parent User ID in the tree.
 * @property int $current_parent_id Immidiate Parent User ID in the tree.
 * @property string $position L:Left,R:Right
 * @property string $password
 * @property string $password_reset_token
 * @property string $master_pin
 * @property string $email
 * @property string $auth_key
 * @property int $status 1:Active,0:In Active
 * @property int $is_deleted 1:Deleted,0:Not Deleted
 * @property int $is_admin 1:Admin,0:Normal User
 * @property string $left_user Left child name
 * @property string $right_user Right child name
 * @property int $left_user_id Left user ID
 * @property int $right_user_id Right user ID
 * @property string $lc_fund
 * @property string $rc_fund
 * @property string $binary_calculated_order_ids
 * @property int $level
 * @property string $original_password
 * @property string $original_master_pin
 * @property string $last_login
 * @property string $updated_at
 * @property string $created_at
 *
 * @property Balance[] $balances
 * @property MoneyTransfer[] $moneyTransfers
 * @property MoneyTransfer[] $moneyTransfers0
 * @property Order[] $orders
 * @property Transaction[] $transactions
 * @property Transaction[] $transactions0
 * @property Transaction[] $transactions1
 * @property ManageUsers $currentParent
 * @property ManageUsers[] $manageUsers
 * @property ManageUsers $leftUser
 * @property ManageUsers[] $manageUsers0
 * @property ManageUsers $parent
 * @property ManageUsers[] $manageUsers1
 * @property ManageUsers $rightUser
 * @property ManageUsers[] $manageUsers2
 * @property UserProfile[] $userProfiles
 */
class ManageUsers extends \yii\db\ActiveRecord
{
    const SCENARIO_USER_CREATION = 'create_user';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'parent_id', 'position', 'email'], 'required'],
            [['password', 'master_pin'], 'required', 'on' => self::SCENARIO_USER_CREATION],
            ['email', 'email'],

            [['current_parent_id', 'status', 'is_deleted', 'is_admin', 'left_user_id', 'right_user_id', 'level'], 'integer'],
            [['lc_fund', 'rc_fund'], 'number'],
            [['binary_calculated_order_ids'], 'string'],
            [['last_login', 'updated_at', 'created_at'], 'safe'],
            [['username', 'password', 'password_reset_token', 'master_pin', 'email', 'auth_key', 'left_user', 'right_user', 'original_password', 'original_master_pin'], 'string', 'max' => 255],
            [['position'], 'string', 'max' => 1],
            ['parent_id', 'isValidSponsor', 'on' => self::SCENARIO_USER_CREATION],
            ['username', 'isUserUnique', 'on' => self::SCENARIO_USER_CREATION],
            ['email', 'isEmailUnique', 'on' => self::SCENARIO_USER_CREATION],
            // [['current_parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => ManageUsers::className(), 'targetAttribute' => ['current_parent_id' => 'id']],
            // [['left_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ManageUsers::className(), 'targetAttribute' => ['left_user_id' => 'id']],
            // [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => ManageUsers::className(), 'targetAttribute' => ['parent_id' => 'id']],
            // [['right_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => ManageUsers::className(), 'targetAttribute' => ['right_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'parent_id' => 'Parent ID',
            'current_parent_id' => 'Current Parent ID',
            'position' => 'Position',
            'password' => 'Password',
            'password_reset_token' => 'Password Reset Token',
            'master_pin' => 'Key Pin',
            'email' => 'Email',
            'auth_key' => 'Auth Key',
            'status' => 'Status',
            'is_deleted' => 'Is Deleted',
            'is_admin' => 'Is Admin',
            'left_user' => 'Left User',
            'right_user' => 'Right User',
            'left_user_id' => 'Left User ID',
            'right_user_id' => 'Right User ID',
            'lc_fund' => 'Lc Fund',
            'rc_fund' => 'Rc Fund',
            'binary_calculated_order_ids' => 'Binary Calculated Order Ids',
            'level' => 'Level',
            'original_password' => 'Original Password',
            'original_master_pin' => 'Original Key Pin',
            'last_login' => 'Last Login',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBalances()
    {
        return $this->hasMany(Balance::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoneyTransfers()
    {
        return $this->hasMany(MoneyTransfer::className(), ['from_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoneyTransfers0()
    {
        return $this->hasMany(MoneyTransfer::className(), ['to_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['from_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions0()
    {
        return $this->hasMany(Transaction::className(), ['to_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions1()
    {
        return $this->hasMany(Transaction::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentParent()
    {
        return $this->hasOne(ManageUsers::className(), ['id' => 'current_parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManageUsers()
    {
        return $this->hasMany(ManageUsers::className(), ['current_parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeftUser()
    {
        return $this->hasOne(ManageUsers::className(), ['id' => 'left_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManageUsers0()
    {
        return $this->hasMany(ManageUsers::className(), ['left_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(ManageUsers::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManageUsers1()
    {
        return $this->hasMany(ManageUsers::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRightUser()
    {
        return $this->hasOne(ManageUsers::className(), ['id' => 'right_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManageUsers2()
    {
        return $this->hasMany(ManageUsers::className(), ['right_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfiles() {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'id']);
    }

    /**
     * It returns whether user is active OR blocked.
     * @return type string.
     */
    public function getUserStatus() {
        return $this->status === 1 ? "Active" : "Blocked";
        ;
    }

    /**
     * It returns the created date of User in the format datetime.
     * @return type string.
     */
    public function getCreatedDate() {
        return Utils::sayItNever($this->created_at);
    }

    /**
     * It returns the updated date of User in the format datetime..
     * @return type string.
     */
    public function getUpdatedDate() {
        return Utils::sayItNever($this->updated_at);
    }

    /**
     * Set original password and key pin for some purpose.
     */
    public function setOriginalPwdMasterPin($password, $master_pin) {
        $this->original_password =  $password;
        $this->original_master_pin = $master_pin;
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    /**
     * Generates password hash from key pin and sets it to the model
     *
     * @param string $password
     */
    public function setMasterPin($master_pin) {
        $this->master_pin = Yii::$app->getSecurity()->generatePasswordHash($master_pin);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        // Set default values for User.
        if ($this->isNewRecord) {
            // New record.
            $this->created_at = date('Y-m-d H:i:s', time());
            $this->updated_at = date('Y-m-d H:i:s', time());

            // Set parent id
            $ancestor = User::findByUsername($this->parent_id);
            $this->parent_id = $ancestor->id;
            // Send a mail
             $this->sendEmail();
        } else {
            // Updated record.
            $this->updated_at = date('Y-m-d H:i:s', time());
        }
        return parent::beforeSave($insert);
    }

    /**
     * Custom Validater
     * @param type $attribute
     * @param type $params
     * @param type $validator
     */
    public function isValidSponsor($attribute, $params, $validator) {
        if (!in_array($this->$attribute, User::getActiveUsers())) {
            $this->addError($attribute, "Sponsor Id doesn't exist");
        }
    }

    /**
     * Custom Validater
     * @param type $attribute
     * @param type $params
     * @param type $validator
     */
    public function isUserUnique($attribute, $params, $validator) {
        if (User::findByUsername($this->$attribute, 2) !== null) {
            $this->addError($attribute, "User already exist.");
        }
    }

    /**
     * Custom Validater
     * @param type $attribute
     * @param type $params
     * @param type $validator
     */
    public function isEmailUnique($attribute, $params, $validator) {
        if ( User::findByEmailId($this->$attribute, 2) !== null) {
            $this->addError($attribute, "Email ID already exist.");
        }
    }

    /**
     * Create new user.
     */
    public function createNewUser() {
        // Normal and Encrypt the password and key pin.
        $this->setOriginalPwdMasterPin($this->password, $this->master_pin);
        $this->setPassword($this->password);
        $this->setMasterPin($this->master_pin);

        if (!$this->save()) {
            throw new Exception("Something went wrong!. Please try again later.");
        }
    }

    /**
     * Update user.
     */
    public function updateUserByAdmin() {
        if (!empty($this->password)) {
            $this->setOriginalPassword($this->password);
            $this->setPassword($this->password);
        } else {
            // While update user, if password empty, unset that field to avoid empty record insertion.
            unset($this->password);
        }

        if (!empty($this->master_pin)) {
            $this->setOriginalMasterPin($this->master_pin);
            $this->setMasterPin($this->master_pin);
        } else {
            unset($this->master_pin);
        }
        $this->is_admin_blocked = !$this->status;

        if (!$this->save(false)) {
            throw new Exception("Something went wrong!. Please try again later.");
        }
    }


    /**
     * Set original password.
     */
    public function setOriginalPassword($password) {
        $this->original_password =  $password;
    }

    /**
     * Set original key pin.
     */
    public function setOriginalMasterPin($master_pin) {
        $this->original_master_pin = $master_pin;
    }


}
