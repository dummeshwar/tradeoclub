<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MoneyTransfer;

/**
 * MoneyTransferSearch represents the model behind the search form of `app\models\MoneyTransfer`.
 */
class MoneyTransferSearch extends MoneyTransfer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'to_user_id', 'from_user_id', 'transaction_id', 'to_wallet_id', 'from_wallet_id', 'status'], 'integer'],
            [['amount'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MoneyTransfer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        // $query->andFilterWhere([
        //     'id' => $this->id,
        //     'to_user_id' => $this->to_user_id,
        //     'from_user_id' => $this->from_user_id,
        //     'transaction_id' => $this->transaction_id,
        //     'to_wallet_id' => $this->to_wallet_id,
        //     'from_wallet_id' => $this->from_wallet_id,
        //     'amount' => $this->amount,
        //     'status' => $this->status,
        //     'created_at' => $this->created_at,
        // ]);

        $query->orFilterWhere([
            'from_user_id' => Yii::$app->user->identity->id
        ]);

        $query->orFilterWhere([
            'to_user_id' => Yii::$app->user->identity->id]);

//        $query->andFilterWhere(['like', 'updated_at', $this->updated_at]);

        // Date condition - start - end.
        if (!empty($this->created_at)) {
            $date_range = explode('to', $this->created_at);
            $query->andFilterWhere(['between', 'created_at', $date_range[0], $date_range[1]]);
        }
        
        return $dataProvider;
    }
}
