<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use borales\extensions\phoneInput\PhoneInputValidator;
use yii\web\UploadedFile;

/**
 * This is the model class for table "user_profile".
 *
 * @property int $id
 * @property int $user_id Reference to parent table - User.
 * @property string $first_name
 * @property string $last_name
 * @property string $picture
 * @property string $date_of_birth
 * @property string $contact_number
 * @property string $address
 * @property string $street
 * @property string $state
 * @property string $city
 * @property string $zip_code
 * @property string $country_code
 * @property string $national_id
 * @property string $passport_id
 * @property string $bank_statement
 * @property string $skype_id
 * @property string $facebook_id
 * @property string $twitter_id
 * @property string $updated_at
 * @property string $created_at
 *
 * @property User $user
 */
class UserProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['date_of_birth', 'updated_at', 'created_at'], 'safe'],
            [['address'], 'string'],
            [['first_name', 'last_name', 'contact_number', 'street', 'state', 'city', 'zip_code', 'national_id', 'passport_id', 'skype_id', 'facebook_id', 'twitter_id'], 'string', 'max' => 255],
            //[['country_code'], 'string', 'max' => 2],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['picture'], 'file', 'extensions' => 'jpg, png, jpeg', 'skipOnEmpty' => true, 'mimeTypes' => 'image/jpeg, image/png',],
            [['bank_statement'], 'file', 'extensions' => 'jpg, png, jpeg', 'skipOnEmpty' => true, 'mimeTypes' => 'image/jpeg, image/png',],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'picture' => 'Picture',
            'date_of_birth' => 'Date Of Birth',
            'contact_number' => 'Contact Number',
            'address' => 'Address',
            'street' => 'Street',
            'state' => 'State',
            'city' => 'City',
            'zip_code' => 'Zip Code',
            'country_code' => 'Country Code',
            'national_id' => 'National ID',
            'passport_id' => 'Passport ID',
            'bank_statement' => 'Bank Statement',
            'skype_id' => 'Skype ID',
            'facebook_id' => 'Facebook ID',
            'twitter_id' => 'Twitter ID',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Create a new user profile using relation name.
     */
    public static function createNewUserProfile($relatonName, $targetActiveRecordObj, $currentModelAttributes) {
        $userProfile = new UserProfile();
        $userProfile->link($relatonName, $targetActiveRecordObj);
        $userProfile->attributes = $currentModelAttributes;

        // $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        //
        // try {
        //     $numberProto = $phoneUtil->parse($currentModelAttributes['contact_number'], "CH");
        //     // Get coutry code.
        //     $userProfile->country_code = $numberProto->getCountryCode();
        // } catch (\libphonenumber\NumberParseException $e) {
        //     var_dump($e);
        //     exit;
        // }

        if (!$userProfile->save()) {
            throw new Exception("Something went wrong!. Please try again later.");
        }
        return true;
    }

    public function upload() {
        if ($this->validate()) {
            $this->picture->saveAs('uploads/profile_pics/' . $this->picture->baseName . '.' . $this->picture->extension);
            return true;
        } else {
            return false;
        }
    }

    public function bankStatementUpload() {
        if ($this->validate()) {
            $this->bank_statement->saveAs('uploads/bank-statements/' . $this->bank_statement->baseName . '.' . $this->bank_statement->extension);
            return true;
        } else {
            return false;
        }
    }


    /**
     * Create user profile from admin interface.
     */
    public function createNewUserProfileByadmin($userObj) {

        if (!empty($this->date_of_birth)) {
            $this->date_of_birth = date('Y-m-d', strtotime($this->date_of_birth));
        }

        $this->link('user', $userObj);

        if (!empty($this->contact_number)) {
            $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
            try {
                $numberProto = $phoneUtil->parse($this->contact_number, "CH");
                // Get coutry code.
                $this->country_code = $numberProto->getCountryCode();
            } catch (\libphonenumber\NumberParseException $e) {
                var_dump($e);
                exit;
            }
        }


        // User Picture upload
        $this->picture = UploadedFile::getInstance($this, 'picture');
        if (!empty($this->picture) && $this->upload()) {
            $this->picture = $this->picture->name;
        }

        // User bankstament upload
        $this->bank_statement = UploadedFile::getInstance($this, 'bank_statement');
        if (!empty($this->bank_statement) && $this->bankStatementUpload()) {
            $this->bank_statement = $this->bank_statement->name;
        }

        if (!$this->save()) {
            throw new Exception("Something went wrong!. Please try again later.");
        }
    }

    /**
     * Update user profile.
     */
    public function updateUserProfileByadmin($userObj, $exist_profile_image, $exist_statement_image) {

        if (!empty($this->date_of_birth)) {
            $this->date_of_birth = date('Y-m-d', strtotime($this->date_of_birth));
        }

        $this->link('user', $userObj);

        if (!empty($this->contact_number)) {
            $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
            try {
                $numberProto = $phoneUtil->parse($this->contact_number, "CH");
                // Get coutry code.
                $this->country_code = $numberProto->getCountryCode();
            } catch (\libphonenumber\NumberParseException $e) {
                var_dump($e);
                exit;
            }
        }


        // User Picture upload
        $this->picture = UploadedFile::getInstance($this, 'picture');
        if (!empty($this->picture) && $this->upload()) {
            $this->picture = $this->picture->name;
        } else {
            $this->picture = $exist_profile_image;
        }

        // User bankstament upload
        $this->bank_statement = UploadedFile::getInstance($this, 'bank_statement');
        if (!empty($this->bank_statement) && $this->bankStatementUpload()) {
            $this->bank_statement = $this->bank_statement->name;
        } else {
            $this->bank_statement = $exist_statement_image;
        }

        if (!$this->save(false)) {
            throw new Exception("Something went wrong!. Please try again later.");
        }

    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        // Set default values for User.
        if ($this->isNewRecord) {
            // New record.
            $this->created_at = date('Y-m-d H:i:s', time());
            $this->updated_at = date('Y-m-d H:i:s', time());
        } else {
            // Updated record.
            $this->updated_at = date('Y-m-d H:i:s', time());
        }
        return parent::beforeSave($insert);
    }
}
