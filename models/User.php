<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;
use yii\base\Exception;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property int $parent_id Direct Parent User ID in the tree.
 * @property int $current_parent_id Immidiate Parent User ID in the tree.
 * @property string $position L:Left,R:Right
 * @property string $password
 * @property string $password_reset_token
 * @property string $master_pin
 * @property string $email
 * @property string $auth_key
 * @property int $status 1:Active,0:In Active
 * @property int $is_deleted 1:Deleted,0:Not Deleted
 * @property int $is_admin 1:Admin,0:Normal User
 * @property string $left_user Left child name
 * @property string $right_user Right child name
 * @property int $left_user_id Left user ID
 * @property int $right_user_id Right user ID
 * @property string $lc_fund
 * @property string $rc_fund
 * @property string $binary_calculated_order_ids
 * @property int $level
 * @property string $original_password
 * @property string $original_master_pin
 * @property string $last_login
 * @property string $updated_at
 * @property string $created_at
 *
 * @property User $currentParent
 * @property User[] $users
 * @property User $leftUser
 * @property User[] $users0
 * @property User $parent
 * @property User[] $users1
 * @property User $rightUser
 * @property User[] $users2
 * @property UserProfile[] $userProfiles
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE_INACTIVE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'parent_id', 'position', 'email'], 'required'],
            [['parent_id', 'current_parent_id', 'status', 'is_deleted', 'is_admin', 'left_user_id', 'right_user_id', 'level'], 'integer'],
            [['lc_fund', 'rc_fund'], 'number'],
            [['binary_calculated_order_ids'], 'string'],
            [['last_login', 'updated_at', 'created_at'], 'safe'],
            [['username', 'password', 'password_reset_token', 'master_pin', 'email', 'auth_key', 'left_user', 'right_user', 'original_password', 'original_master_pin'], 'string', 'max' => 255],
            [['position'], 'string', 'max' => 1],
            [['username'], 'unique'],
/*            [['current_parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['current_parent_id' => 'id']],
            [['left_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['left_user_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['right_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['right_user_id' => 'id']],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'parent_id' => 'Parent ID',
            'current_parent_id' => 'Current Parent ID',
            'position' => 'Position',
            'password' => 'Password',
            'password_reset_token' => 'Password Reset Token',
            'master_pin' => 'Key Pin',
            'email' => 'Email',
            'auth_key' => 'Auth Key',
            'status' => 'Status',
            'is_deleted' => 'Is Deleted',
            'is_admin' => 'Is Admin',
            'left_user' => 'Left User',
            'right_user' => 'Right User',
            'left_user_id' => 'Left User ID',
            'right_user_id' => 'Right User ID',
            'lc_fund' => 'Lc Fund',
            'rc_fund' => 'Rc Fund',
            'binary_calculated_order_ids' => 'Binary Calculated Order Ids',
            'level' => 'Level',
            'original_password' => 'Original Password',
            'original_master_pin' => 'Original Key Pin',
            'last_login' => 'Last Login',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }


        /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username, $status = 1) {
        if ($status == 1) {
            // Active
            return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
        } else if ($status == 2) {
            // Both : active and Inactive
            return static::findOne(['username' => $username]);
        } else {
            // Inactive
            return static::findOne(['username' => $username, 'status' => self::STATUS_INACTIVE]);
        }

    }

    /**
     * Finds user by email id.
     *
     * @param string $emailId
     * @return static|null
     */
    public static function findByEmailId($emailId, $status = 1) {
        if ($status == 1) {
            // Active
            return static::findOne(['email' => $emailId, 'status' => self::STATUS_ACTIVE]);
        } else if ($status == 2) {
            // Both : active and Inactive
            return static::findOne(['email' => $emailId]);
        } else {
            // Inactive
            return static::findOne(['email' => $emailId, 'status' => self::STATUS_INACTIVE]);
        }

    }


    public static function findInactiveUserByUsername($username) {
        return static::findOne(['username' => $username, 'status' => self::STATUS_INACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
                    'password_reset_token' => $token,
                    'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds user by token to create new password and key pin.
     *
     * @param string $token to create password and key pin
     * @return static|null
     */
    public static function findByTokenTOCreatePasswordMasterPin($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
                    'password_reset_token' => $token,
                    'status' => self::STATUS_DELETED,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }
        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validateMasterPin($master_pin) {
        return Yii::$app->getSecurity()->validatePassword($master_pin, $this->master_pin);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    /**
     * Generates password hash from key pin and sets it to the model
     *
     * @param string $password
     */
    public function setMasterPin($master_pin) {
        $this->master_pin = Yii::$app->getSecurity()->generatePasswordHash($master_pin);
    }

    /**
     * Set original password and key pin for some purpose.
     */
    public function setOriginalPwdMasterPin($password, $master_pin) {
        $this->original_password =  $password;
        $this->original_master_pin = $master_pin;
    }

    /**
     * Set original password.
     */
    public function setOriginalPassword($password) {
        $this->original_password =  $password;
    }

    /**
     * Set original key pin.
     */
    public function setOriginalMasterPin($master_pin) {
        $this->original_master_pin = $master_pin;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentParent()
    {
        return $this->hasOne(User::className(), ['id' => 'current_parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['current_parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeftUser()
    {
        return $this->hasOne(User::className(), ['id' => 'left_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers0()
    {
        return $this->hasMany(User::className(), ['left_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(User::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers1()
    {
        return $this->hasMany(User::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRightUser()
    {
        return $this->hasOne(User::className(), ['id' => 'right_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers2()
    {
        return $this->hasMany(User::className(), ['right_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfiles() {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'id']);
    }

    /**
     * Get all active users.
     */
    public static function getActiveUsers() {
        return ArrayHelper::getColumn(User::find()->where(['status' => 1])->all(), 'username');
    }

    /**
     * Create a new user based on user attributes.
     */
    public static function createNewUser($attributes) {
        $user = new User();
        $user->attributes = $attributes;

        // Send email to user to create new password and key pin.
        $user->sendEmail();

        if (!$user->save()) {
            throw new Exception("Something went wrong!. Please try again later.");
        }
        return $user;
    }

    /**
     * Function to send an email - To create new password and key pin.
     *
     *  Sends an email with a link.
     *
     * @return bool whether the email was send
     */
    public function sendEmail($resend_link = false) {

        // Send a mail.
        if (!$resend_link) {
          $this->generatePasswordResetToken();
        }

//        try {
//                Yii::$app
//                    ->mailer
//                    ->compose(
//                            ['html' => 'creaeNewPasswordMasterPin-html'], ['username' => $this->username, 'new_token' => $this->password_reset_token]
//                    )
//                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
//                    ->setTo($this->email)
//                    ->setSubject('Create Password and Key Pin for ' . Yii::$app->name)
//                    ->send();
//        }  catch(\Exception $e) {
//            print_r("Can't send mail");
//            throw $e;
//        }

    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        // Set default values for User.
        if ($this->isNewRecord) {
            // New record.
            $this->created_at = date('Y-m-d H:i:s', time());
            $this->updated_at = date('Y-m-d H:i:s', time());
        } else {
            // Updated record.
            $this->updated_at = date('Y-m-d H:i:s', time());
        }
        return parent::beforeSave($insert);
    }

    /**
     * Imporatant function for building tree.
     *  Keep it at the end.
     */
    public static function buildTree($userObj) {
        switch ($userObj->position) {
            case 'L':

                User::traverseLeft($userObj);
                break;

            case 'R':

                User::traverseRight($userObj);
                break;

            default:

                throw new Exception("Unknown condition caught!!!.");
                break;
        }
    }

    public static function traverseLeft($userObj) {
        $parentUserObj = User::findOne($userObj->parent_id);
        while (isset($parentUserObj->left_user_id) && $parentUserObj->left_user_id !== null) {
            $parentUserObj = User::findOne($parentUserObj->left_user_id);
        }
        //echo '<pre>'; print_r($userObj); exit;
        User::saveLeftChild($parentUserObj, $userObj);
    }


    public static function traverseRight($userObj) {
        $parentUserObj = User::findOne($userObj->parent_id);
        while (isset($parentUserObj->right_user_id) && $parentUserObj->right_user_id !== null) {
            $parentUserObj = User::findOne($parentUserObj->right_user_id);
        }
        User::saveRightChild($parentUserObj, $userObj);
    }

    public static function saveLeftChild($parentUserObj, $userObj) {
        $parentUserObj->left_user_id = $userObj->id;
        $parentUserObj->left_user = $userObj->username;

        if (!$parentUserObj->save(false)) {
            throw new Exception("Something went wrong!. Please try again later.");
        }


        $userObj->current_parent_id = $parentUserObj->id;
        $userObj->level = $parentUserObj->level + 1;
        if (!$userObj->save(false)) {
            throw new Exception("Something went wrong!. Please try again later.");
        }
    }

    public static function saveRightChild($parentUserObj, $userObj) {
        $parentUserObj->right_user_id = $userObj->id;
        $parentUserObj->right_user = $userObj->username;
        if (!$parentUserObj->save(false)) {
            throw new Exception("Something went wrong!. Please try again later.");
        }

        $userObj->current_parent_id = $parentUserObj->id;
        $userObj->level = $parentUserObj->level + 1;
        if (!$userObj->save(false)) {
            throw new Exception("Something went wrong!. Please try again later.");
        }
    }
}
