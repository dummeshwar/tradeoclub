<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Utils;

/**
 * CreditWalletForm is the model behind the contact form.
 */
class WithdrawalRequestForm extends Model
{
    public $from_wallet;
    public $amount;
    public $comment;
    public $account_number;
    public $e_currency;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['from_wallet', 'amount', 'e_currency'], 'required'],
            ['e_currency', 'integer'],
            [['amount'], 'number', 'min'=>0.01, 'max'=>2500000],
            [['account_number'], 'string', 'max' => 255],
            [['comment'], 'string', 'max' => 255],
            [['amount', 'from_wallet'], 'haveSufficientBalance']
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'from_wallet' => 'Wallet Name',
            'amount' => 'Withdrawal Amount',
            'account_number' => 'Account Number',
            'comment' => 'Currency Address',
            'e_currency' => 'E-Currency',
        ];
    }


    public function haveSufficientBalance($attribute, $params)
    {
        $withdrawalCharges = Yii::$app->params['transaction_charges']['withdrawal'];
        $wallet_balance = Utils::walletMoney($this->from_wallet);
        $actual_amount = $this->amount + (($this->amount * $withdrawalCharges) / 100);
        if ($actual_amount > $wallet_balance) {
            $this->addError('amount', 'You have requested more than what you have in your wallet. Admin charges ' . $withdrawalCharges . "(%)");
        }
    }

}
