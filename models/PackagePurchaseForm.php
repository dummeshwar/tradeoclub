<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Utils;

/**
 * PackagePurchaseForm is the model behind the contact form.
 */
class PackagePurchaseForm extends Model
{
    public $package;  // In terms of bit coin value.


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['package'], 'required'],
            [['package'], 'verifySufficientBalance'],
           // [['package'], 'verifyPackageAlreadyPurchased'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'package' => 'Package'
        ];
    }

    /**
     *
     */
    public function verifySufficientBalance($attribute, $params) {

        $price = Utils::extract_package_price($this->package);


        if (!$price) {
            $this->addError('package', 'Some problems with the package. Please select another');
        }

        $wallet_balance = Utils::walletMoney(Yii::$app->params['wallets']['CASH_WALLET']);
        if ($price >  $wallet_balance) {
            $this->addError('package', 'Please recharge your wallet. You do not have sufficient balance.');
        }
    }

    /**
     *
     */
    public function verifyPackageAlreadyPurchased($attribute, $params) {
        $price = Utils::extract_package_price($this->package);
        
        if (!$price) {
            $this->addError('package', 'Some problems with the package. Please select another');
        }

        $packageObj = Package::find()->where(['status' => 1, 'price' => $price])->one();
        $orderObj = Order::find()->where(['status' => 1, 'package_id' => $packageObj->id, 'user_id' => Yii::$app->user->identity->id])->one();
        if ($orderObj) {
            $this->addError('package', 'You have already purchased this package.');
        }
    }

}
