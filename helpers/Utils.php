<?php

namespace yii\helpers;

use Yii;
use app\models\Balance;
use app\models\User;
use app\models\Order;
use yii\helpers\Url;
use yii\helpers\Html;

class Utils {

    /**
     * Function to get user Picture.
     * @return string
     */
    public static function getUserPicture() {
        if (!empty(Yii::$app->user->identity->userProfiles->picture)) {
            return Yii::$app->params['userImagesUpload'] . Yii::$app->user->identity->userProfiles->picture;
        } else {
            return '@web/images/default_user.png';
        }
    }

    public static function findByUserName($name) {
        return User::find()
            ->where(['username' => $name, 'status' => 1, 'is_admin' => 0])
            ->one();
    }

    public static function walletMoney($wallet_type, $userId = null) {

        $userId = ($userId) ? $userId : Yii::$app->user->identity->id;
        return Balance::getUserWalletMoney($userId, $wallet_type);
    }


    public static function convertBTCtoUSD($bitcoin = 0) {
        try {
            $jsonData = file_get_contents(Yii::$app->params['API']['BitcoinToUSD']);
            $jsonObj = json_decode($jsonData);
            return ($jsonObj->USD->last * $bitcoin);
        }  catch(\Exception $e) {
            return null;
        }
    }

    public static function sayItNever($value = NULL, $format = 'datetime') {
        return isset($value) ? \Yii::$app->formatter->format($value, $format) : 'Never';
    }

    /**
     * Get user purchased packages.
     */
    public static function getUserPackages($user_id) {
        return Order::find()
                ->where(['user_id' => $user_id])
                ->orderBy(['created_at' => SORT_DESC])->all();
    }

    /**
     *  Position 1 - Left, 2- Right
     */
    public static function getUserForBtree($username, $parent_username = NULL, $position = NULL, $is_admin = NULL) {
        //echo $username; exit;
        if ($username == 'N') {
            //$url = Url::toRoute(['site/register', 'ref' => $parent_username, 'pos' => $position]);
            $url = Url::toRoute(['site/register', 'ref' => Yii::$app->user->identity->username, 'pos' => $position]);
            return "<a href='" . $url . "'>" . Html::img('@web/images/user_add.png', ['alt' => 'User', 'class' => 'img-responsive', 'title' => '', 'width' => '50px']) . "</a>";
        } else {
            $url = !empty($is_admin) ? Url::toRoute(['admin/user/tree', 'username' => $username]) : Url::toRoute(['user/my-tree', 'username' => $username]);
            $user = User::find()->where(['username' => $username])->one();
            $order = Order::find()->where(['user_id' => $user->id])->one();

            if (!empty($order)) {
                $userData =  "<a href='" . $url . "'>" . Html::img('@web/images/active.png', ['alt' => 'User', 'class' => 'img-responsive', 'title' => '', 'width' => '50px']) . "</a>";
            } else {
                $userData =  "<a href='" . $url . "'>" . Html::img('@web/images/normal.png', ['alt' => 'User', 'class' => 'img-responsive', 'title' => '', 'width' => '50px']) . "</a>";
            }

            $userData .= $username;
            return $userData;
        }
    }


    /**
     * Extract price.
     */
    public static function extract_package_price($price) {
        $price_arr = explode(",", $price);

        if (!empty($price_arr[1])) {
            $package_price = explode(':', $price_arr[1]);

            return !empty($package_price[1]) ? $package_price[1] : null;            
        }

        return null;
    }


}
