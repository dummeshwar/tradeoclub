-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 08, 2018 at 04:50 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `plant`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_pop_ups`
--

CREATE TABLE `admin_pop_ups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image_desktop` varchar(255) DEFAULT NULL,
  `image_mobile` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `balance`
--

CREATE TABLE `balance` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `wallet_id` int(11) NOT NULL,
  `amount` decimal(18,8) DEFAULT '0.00000000' COMMENT 'In (BTC)',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `balance`
--

INSERT INTO `balance` (`id`, `user_id`, `wallet_id`, `amount`, `updated_at`, `created_at`) VALUES
(1, 1, 1, '889.50000000', '2018-08-04 18:17:21', '2018-08-04 18:17:21'),
(2, 1, 2, '1000.00000000', '2018-08-04 18:17:21', '2018-08-04 18:17:21'),
(3, 1, 3, '1016.05000000', '2018-08-04 18:17:21', '2018-08-04 18:17:21'),
(4, 1, 4, '1000.00000000', '2018-08-04 18:17:21', '2018-08-04 18:17:21'),
(5, 2, 1, '91.00000000', '2018-08-04 18:17:21', '2018-08-04 18:17:21'),
(6, 2, 2, '0.00000000', '2018-08-04 18:17:21', '2018-08-04 18:17:21'),
(7, 2, 3, '0.00000000', '2018-08-04 18:17:21', '2018-08-04 18:17:21'),
(8, 2, 4, '0.00000000', '2018-08-04 18:17:21', '2018-08-04 18:17:21'),
(9, 1, 5, '0.00000000', '2018-08-12 13:13:15', '2018-08-12 13:13:15'),
(10, 2, 5, '0.00000000', '2018-08-12 13:13:15', '2018-08-12 13:13:15'),
(11, 4, 1, '0.50000000', NULL, '2018-08-26 11:55:07'),
(12, 4, 2, '0.00000000', NULL, '2018-08-26 11:55:07'),
(13, 4, 3, '0.00000000', NULL, '2018-08-26 11:55:07'),
(14, 4, 4, '0.00000000', NULL, '2018-08-26 11:55:07'),
(15, 4, 5, '0.00000000', NULL, '2018-08-26 11:55:07');

-- --------------------------------------------------------

--
-- Table structure for table `broadcast_message`
--

CREATE TABLE `broadcast_message` (
  `id` int(11) NOT NULL,
  `description` text,
  `status` int(1) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1533386835),
('m180102_192946_create_user_table', 1533386841),
('m180114_144326_create_user_profile_table', 1533386841),
('m180114_145358_create_wallet_table', 1533386841),
('m180301_174538_create_balance_table', 1533386841),
('m180310_141407_create_payment_gateway_table', 1533386841),
('m180310_142836_create_transaction_table', 1533386841),
('m180310_144020_create_money_transfer_table', 1533386841),
('m180316_165657_create_package_table', 1533386841),
('m180318_110842_create_order_table', 1533386841),
('m180518_072315_add_points_column_to_package_table', 1533386841),
('m180519_121333_create_admin_pop_ups_table', 1533386841),
('m180613_055735_create_broadcast_message_table', 1533386841),
('m180613_072533_add_is_admin_blocked_column_to_user_table', 1533386841);

-- --------------------------------------------------------

--
-- Table structure for table `money_transfer`
--

CREATE TABLE `money_transfer` (
  `id` int(11) NOT NULL,
  `to_user_id` int(11) NOT NULL,
  `from_user_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `to_wallet_id` int(11) NOT NULL,
  `from_wallet_id` int(11) NOT NULL,
  `amount` decimal(18,8) DEFAULT '0.00000000' COMMENT ' In (BTC).',
  `status` int(1) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `money_transfer`
--

INSERT INTO `money_transfer` (`id`, `to_user_id`, `from_user_id`, `transaction_id`, `to_wallet_id`, `from_wallet_id`, `amount`, `status`, `updated_at`, `created_at`) VALUES
(1, 4, 1, 1, 1, 1, '1.00000000', 1, NULL, '2018-08-26 11:56:24'),
(2, 1, 4, 2, 1, 1, '0.50000000', 1, NULL, '2018-08-26 11:58:12');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `user_id`, `package_id`, `transaction_id`, `start_date`, `end_date`, `status`, `updated_at`, `created_at`) VALUES
(1, 2, 1, 42, '2018-09-05', '2019-03-04', 1, NULL, '2018-09-05 08:09:48');

-- --------------------------------------------------------

--
-- Table structure for table `package`
--

CREATE TABLE `package` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(18,8) DEFAULT '0.00000000' COMMENT ' In (BTC).',
  `image` varchar(255) DEFAULT NULL,
  `description` text,
  `no_of_days` int(11) NOT NULL COMMENT 'How long the package will stay active.',
  `no_of_weeks` int(11) NOT NULL,
  `status` int(1) DEFAULT '0',
  `daily_returns` decimal(18,8) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `points` decimal(12,0) DEFAULT '0' COMMENT 'TradeCO points for TCG wallet.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `package`
--

INSERT INTO `package` (`id`, `name`, `price`, `image`, `description`, `no_of_days`, `no_of_weeks`, `status`, `daily_returns`, `updated_at`, `created_at`, `points`) VALUES
(1, 'Package 1', '1.00000000', 'decoration1-1000x1000_c.jpg', 'Package 1', 180, 100, 1, '12.00000000', NULL, '2018-08-27 21:14:22', '100'),
(2, 'Package 2', '100.00000000', 'furtilizer2-1000x1000_c.jpg', 'The second package', 156, 100, 1, '10.00000000', NULL, '2018-09-02 17:33:31', '10');

-- --------------------------------------------------------

--
-- Table structure for table `payment_gateway`
--

CREATE TABLE `payment_gateway` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mode` int(1) DEFAULT '0' COMMENT '1:ONLINE 0:OFFLINE.',
  `account_details` text,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `slug` varchar(255) DEFAULT NULL,
  `min_amount` decimal(18,8) DEFAULT '0.00000000',
  `withdrawal_min_amount` decimal(18,8) DEFAULT '0.00000000',
  `request_url` varchar(255) DEFAULT NULL,
  `response_url` varchar(255) DEFAULT NULL,
  `charges` decimal(18,8) DEFAULT '0.00000000' COMMENT 'Percent value. Do not include % in value.',
  `status` int(1) DEFAULT '0' COMMENT '1:Active 0:Inactive.',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_gateway`
--

INSERT INTO `payment_gateway` (`id`, `name`, `mode`, `account_details`, `title`, `description`, `slug`, `min_amount`, `withdrawal_min_amount`, `request_url`, `response_url`, `charges`, `status`, `updated_at`, `created_at`) VALUES
(1, 'via Bitcoin(USD)', 1, NULL, 'Bitcoin', 'Bitcoin', 'bitcoin', '100.00000000', '100.00000000', NULL, NULL, '0.10000000', 1, '2018-08-04 18:17:21', '2018-08-04 18:17:21');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL,
  `transaction_id` varchar(255) NOT NULL COMMENT 'Unique id for each transaction.',
  `payment_transaction_id` varchar(255) DEFAULT NULL COMMENT 'Response (unique ID) from online transactions.',
  `user_id` int(11) NOT NULL COMMENT 'Initiated the transaction.',
  `payment_gateway_id` int(11) DEFAULT NULL COMMENT 'Gateway ID ex: Bitcoin. Null meaning no Gateway used.',
  `actual_amount` decimal(18,8) DEFAULT '0.00000000' COMMENT 'Actual amount without commission (IN BTC).',
  `paid_amount` decimal(18,8) DEFAULT '0.00000000' COMMENT 'Full amount inlcudes commission (IN BTC).',
  `transaction_mode` varchar(255) NOT NULL COMMENT 'Modes of transactions - ex : Credit, Debit, Trasfer etc. Check it in config -> params file.',
  `to_wallet_id` int(11) DEFAULT NULL,
  `from_wallet_id` int(11) DEFAULT NULL,
  `to_user_id` int(11) DEFAULT NULL,
  `from_user_id` int(11) DEFAULT NULL,
  `user_comment` text,
  `admin_comment` text,
  `admin_marked_status` int(1) DEFAULT '0' COMMENT '0:Failed 1:Success/Approved, 2:Pendning.',
  `status` int(1) DEFAULT '0' COMMENT '0:Failed 1:Success/Approved, 2:Pendning.',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`id`, `transaction_id`, `payment_transaction_id`, `user_id`, `payment_gateway_id`, `actual_amount`, `paid_amount`, `transaction_mode`, `to_wallet_id`, `from_wallet_id`, `to_user_id`, `from_user_id`, `user_comment`, `admin_comment`, `admin_marked_status`, `status`, `updated_at`, `created_at`) VALUES
(1, '1671322427', NULL, 1, NULL, '1.00000000', '1.00000000', 'ADMIN: AMOUNT CREDITED', 1, 1, 4, 1, 'ADMIN: AMOUNT CREDITED', NULL, 1, 1, NULL, '2018-08-26 11:56:24'),
(2, '1373663044', NULL, 1, NULL, '0.50000000', '0.50000000', 'ADMIN: AMOUNT DEBITED', 1, 1, 1, 4, 'ADMIN: AMOUNT DEBITED', NULL, 1, 1, NULL, '2018-08-26 11:58:12'),
(3, '1608436910', NULL, 1, NULL, '1.00000000', '1.00000000', 'USER: ADD FUND', 1, NULL, 1, NULL, 'USER: ADD FUND', NULL, 2, 2, NULL, '2018-09-02 21:22:37'),
(4, '1408436774', NULL, 1, NULL, '1.00000000', '1.00000000', 'USER: ADD FUND', 1, NULL, 1, NULL, 'USER: ADD FUND', NULL, 2, 2, NULL, '2018-09-02 21:25:39'),
(5, '1976486537', NULL, 1, NULL, '1.00000000', '1.00000000', 'USER: ADD FUND', 1, NULL, 1, NULL, 'USER: ADD FUND', NULL, 2, 2, NULL, '2018-09-02 22:04:30'),
(6, '1174513877', NULL, 1, NULL, '1.00000000', '1.00000000', 'USER: ADD FUND', 1, NULL, 1, NULL, 'USER: ADD FUND', NULL, 2, 2, NULL, '2018-09-02 22:05:37'),
(7, '560651957', NULL, 1, NULL, '1.00000000', '1.00000000', 'USER: ADD FUND', 1, NULL, 1, NULL, 'USER: ADD FUND', NULL, 2, 2, NULL, '2018-09-02 22:06:19'),
(8, '585009475', NULL, 1, NULL, '1.00000000', '1.00000000', 'USER: ADD FUND', 1, NULL, 1, NULL, 'USER: ADD FUND', NULL, 2, 2, NULL, '2018-09-02 22:07:44'),
(9, '100371667', NULL, 1, NULL, '1.00000000', '1.00000000', 'USER: ADD FUND', 1, NULL, 1, NULL, 'USER: ADD FUND', NULL, 2, 2, NULL, '2018-09-02 22:11:06'),
(10, '1994385238', NULL, 1, NULL, '1.00000000', '1.00000000', 'USER: ADD FUND', 1, NULL, 1, NULL, 'USER: ADD FUND', NULL, 2, 2, NULL, '2018-09-02 22:12:46'),
(11, '1284749252', NULL, 1, NULL, '1.00000000', '1.00000000', 'USER: ADD FUND', 1, NULL, 1, NULL, 'USER: ADD FUND', NULL, 2, 2, NULL, '2018-09-02 22:13:52'),
(12, '1104768381', NULL, 1, NULL, '1.00000000', '1.00000000', 'USER: ADD FUND', 1, NULL, 1, NULL, 'USER: ADD FUND', NULL, 2, 2, NULL, '2018-09-02 22:14:54'),
(13, '331967618', NULL, 1, NULL, '1.00000000', '1.00000000', 'USER: ADD FUND', 1, NULL, 1, NULL, 'USER: ADD FUND', NULL, 2, 2, NULL, '2018-09-02 22:15:41'),
(14, '949994329', NULL, 1, NULL, '1.00000000', '1.00000000', 'USER: ADD FUND', 1, NULL, 1, NULL, 'USER: ADD FUND', NULL, 2, 2, NULL, '2018-09-02 22:20:54'),
(15, '1128106747', NULL, 1, NULL, '100.00000000', '100.00000000', 'USER: ADD FUND', 1, NULL, 1, NULL, 'USER: ADD FUND', NULL, 2, 2, NULL, '2018-09-03 19:02:09'),
(16, '1432919856', NULL, 2, NULL, '1.00000000', '1.00000000', 'USER: ADD FUND', 1, NULL, 2, NULL, 'USER: ADD FUND', NULL, 2, 2, NULL, '2018-09-04 22:04:04'),
(18, '1423223322', NULL, 2, 1, '1.00000000', '1.00000000', 'Purchase', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2018-09-05 06:43:55'),
(27, '1999764069', NULL, 2, NULL, '1.00000000', '1.00000000', 'PACKAGE PURCHASE', NULL, 1, NULL, NULL, 'PACKAGE PURCHASE', NULL, 1, 1, NULL, '2018-09-05 07:04:48'),
(28, '595489828', NULL, 2, NULL, '1.00000000', '1.00000000', 'PACKAGE PURCHASE', NULL, 1, NULL, NULL, 'PACKAGE PURCHASE', NULL, 1, 1, NULL, '2018-09-05 07:43:12'),
(29, '733082322', NULL, 2, NULL, '1.00000000', '1.00000000', 'PACKAGE PURCHASE', NULL, 1, NULL, NULL, 'PACKAGE PURCHASE', NULL, 1, 1, NULL, '2018-09-05 07:43:47'),
(30, '1121829337', NULL, 2, NULL, '1.00000000', '1.00000000', 'PACKAGE PURCHASE', NULL, 1, NULL, NULL, 'PACKAGE PURCHASE', NULL, 1, 1, NULL, '2018-09-05 07:44:58'),
(31, '1673361394', NULL, 2, NULL, '1.00000000', '1.00000000', 'PACKAGE PURCHASE', NULL, 1, NULL, NULL, 'PACKAGE PURCHASE', NULL, 1, 1, NULL, '2018-09-05 07:48:14'),
(32, '1970984575', NULL, 2, NULL, '1.00000000', '1.00000000', 'PACKAGE PURCHASE', NULL, 1, NULL, NULL, 'PACKAGE PURCHASE', NULL, 1, 1, NULL, '2018-09-05 07:48:43'),
(33, '105057294', NULL, 2, NULL, '1.00000000', '1.00000000', 'PACKAGE PURCHASE', NULL, 1, NULL, NULL, 'PACKAGE PURCHASE', NULL, 1, 1, NULL, '2018-09-05 07:48:53'),
(34, '1723887262', NULL, 2, NULL, '1.00000000', '1.00000000', 'PACKAGE PURCHASE', NULL, 1, NULL, NULL, 'PACKAGE PURCHASE', NULL, 1, 1, NULL, '2018-09-05 07:49:08'),
(35, '1859462475', NULL, 2, NULL, '1.00000000', '1.00000000', 'PACKAGE PURCHASE', NULL, 1, NULL, NULL, 'PACKAGE PURCHASE', NULL, 1, 1, NULL, '2018-09-05 07:50:01'),
(42, '1031485021', NULL, 2, NULL, '1.00000000', '1.00000000', 'PACKAGE PURCHASE', NULL, 1, NULL, NULL, 'PACKAGE PURCHASE', NULL, 1, 1, NULL, '2018-09-05 08:09:48');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `parent_id` int(11) DEFAULT NULL COMMENT 'Direct Parent User ID in the tree.',
  `current_parent_id` int(11) DEFAULT NULL COMMENT 'Immidiate Parent User ID in the tree.',
  `position` varchar(1) DEFAULT NULL COMMENT 'L:Left,R:Right',
  `password` varchar(255) DEFAULT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `master_pin` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `auth_key` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '0' COMMENT '1:Active,0:In Active',
  `is_deleted` int(1) DEFAULT '0' COMMENT '1:Deleted,0:Not Deleted',
  `is_admin` int(1) DEFAULT '0' COMMENT '1:Admin,0:Normal User',
  `left_user` varchar(255) DEFAULT NULL COMMENT 'Left child name',
  `right_user` varchar(255) DEFAULT NULL COMMENT 'Right child name',
  `left_user_id` int(11) DEFAULT NULL COMMENT 'Left user ID',
  `right_user_id` int(11) DEFAULT NULL COMMENT 'Right user ID',
  `lc_fund` decimal(18,8) DEFAULT NULL,
  `rc_fund` decimal(18,8) DEFAULT NULL,
  `binary_calculated_order_ids` text,
  `level` int(11) DEFAULT '0',
  `original_password` varchar(255) DEFAULT NULL,
  `original_master_pin` varchar(255) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `is_admin_blocked` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `parent_id`, `current_parent_id`, `position`, `password`, `password_reset_token`, `master_pin`, `email`, `auth_key`, `status`, `is_deleted`, `is_admin`, `left_user`, `right_user`, `left_user_id`, `right_user_id`, `lc_fund`, `rc_fund`, `binary_calculated_order_ids`, `level`, `original_password`, `original_master_pin`, `last_login`, `updated_at`, `created_at`, `is_admin_blocked`) VALUES
(1, 'plantation', NULL, NULL, NULL, '$2y$13$opIdtmZSovdyTN2j1tRPL.ticridJES3A6kj122rW5L9bEMTCWZ6y', NULL, '$2y$13$kEjAtCQAiqzthjRhxmProuYkgciN06ggaRiOnj9Onv9JPz863EfYe', 'admin@tradecoglobal.com', NULL, 1, 0, 1, 'demo_user', NULL, 2, NULL, NULL, NULL, NULL, 0, '123456', '123456', NULL, '2018-08-04 18:17:20', '2018-08-04 18:17:20', 0),
(2, 'demo', 1, 1, 'L', '$2y$13$HArM0o5N.T2RObKmtHvoAufkYPksYoK.7ARnrmBA.FBNn0g5qco2q', NULL, '$2y$13$Z1d.MEEA9T60.p8HCjsIOu1LFxn9jFEdRRj8lRyI0ZyuW35OQ6V0O', 'dummy@gmail.com', NULL, 1, 0, 0, 'test1', NULL, 4, NULL, NULL, NULL, NULL, 1, '123456', '123456', NULL, '2018-08-26 08:25:07', '2018-08-04 18:17:21', 0),
(4, 'test1', 1, 2, 'L', '$2y$13$FUNKiRSDfzt7KwamBBpI6.LUOc9aTdDuh1BnsWF2EEWFpZju.KScq', NULL, '$2y$13$/SjEuJniAoKOchXl2eKAQuN2TUL/uTrSeudzkBPiBXl/.zPJ/tale', 'vt12345@mailinator.com', NULL, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, '123456', '123456', NULL, '2018-08-26 08:25:07', '2018-08-26 08:25:07', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE `user_profile` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'Reference to parent table - User.',
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `address` text,
  `street` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `zip_code` varchar(255) DEFAULT NULL,
  `country_code` varchar(10) DEFAULT NULL,
  `national_id` varchar(255) DEFAULT NULL,
  `passport_id` varchar(255) DEFAULT NULL,
  `bank_statement` varchar(255) DEFAULT NULL,
  `skype_id` varchar(255) DEFAULT NULL,
  `facebook_id` varchar(255) DEFAULT NULL,
  `twitter_id` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`id`, `user_id`, `first_name`, `last_name`, `picture`, `date_of_birth`, `contact_number`, `address`, `street`, `state`, `city`, `zip_code`, `country_code`, `national_id`, `passport_id`, `bank_statement`, `skype_id`, `facebook_id`, `twitter_id`, `updated_at`, `created_at`) VALUES
(1, 1, 'Admin', 'Bit', NULL, '2018-08-04', '8094558048', NULL, NULL, NULL, NULL, NULL, '91', NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-04 18:17:21', '2018-08-04 18:17:21'),
(2, 2, 'Dummy', 'D', NULL, '2018-08-04', '8094558048', NULL, NULL, NULL, NULL, NULL, '91', NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-04 18:17:21', '2018-08-04 18:17:21'),
(4, 4, 'Test', '1', 'event_avatar3 (1).jpg', '2018-02-01', '+1 201-323-0123', 'test, test', 'st11', 'ewtwe', 'reeee', '45444', '1', 'werwr33', 'sdfsdfs', NULL, 'skype1', 'fb1', 'tw1', '2018-08-26 08:25:07', '2018-08-26 08:25:07');

-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

CREATE TABLE `wallet` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT '1: Cash Wallet, 2: Referral Wallet (Direct), 3: Binary Wallet (Binary commission), 4: Plantation Wallet',
  `status` int(1) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wallet`
--

INSERT INTO `wallet` (`id`, `name`, `status`, `updated_at`, `created_at`) VALUES
(1, 'Cash Wallet', 1, '2018-08-04 18:17:21', '2018-08-04 18:17:21'),
(2, 'Referral Wallet', 1, '2018-08-04 18:17:21', '2018-08-04 18:17:21'),
(3, 'Binary Wallet', 1, '2018-08-04 18:17:21', '2018-08-04 18:17:21'),
(4, 'Plantation Wallet', 1, '2018-08-04 18:17:21', '2018-08-04 18:17:21'),
(5, 'TGC Wallet', 1, '2018-08-12 13:13:15', '2018-08-12 13:13:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_pop_ups`
--
ALTER TABLE `admin_pop_ups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `balance`
--
ALTER TABLE `balance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-balance-user_id` (`user_id`),
  ADD KEY `idx-balance-wallet_id` (`wallet_id`);

--
-- Indexes for table `broadcast_message`
--
ALTER TABLE `broadcast_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `money_transfer`
--
ALTER TABLE `money_transfer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-money_transfer-to_user_id` (`to_user_id`),
  ADD KEY `idx-money_transfer-from_user_id` (`from_user_id`),
  ADD KEY `idx-money_transfer-transaction_id` (`transaction_id`),
  ADD KEY `idx-money_transfer-to_wallet_id` (`to_wallet_id`),
  ADD KEY `idx-money_transfer-from_wallet_id` (`from_wallet_id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-order-user_id` (`user_id`),
  ADD KEY `idx-order-package_id` (`package_id`),
  ADD KEY `idx-order-transaction_id` (`transaction_id`);

--
-- Indexes for table `package`
--
ALTER TABLE `package`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_gateway`
--
ALTER TABLE `payment_gateway`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-transaction-user_id` (`user_id`),
  ADD KEY `idx-transaction-payment_gateway_id` (`payment_gateway_id`),
  ADD KEY `idx-transaction-to_wallet_id` (`to_wallet_id`),
  ADD KEY `idx-transaction-from_wallet_id` (`from_wallet_id`),
  ADD KEY `idx-transaction-to_user_id` (`to_user_id`),
  ADD KEY `idx-transaction-from_user_id` (`from_user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `idx-user-parent_id` (`parent_id`),
  ADD KEY `idx-user-current_parent_id` (`current_parent_id`),
  ADD KEY `idx-user-left_user_id` (`left_user_id`),
  ADD KEY `idx-user-right_user_id` (`right_user_id`);

--
-- Indexes for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-user_profile-user_id` (`user_id`);

--
-- Indexes for table `wallet`
--
ALTER TABLE `wallet`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_pop_ups`
--
ALTER TABLE `admin_pop_ups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `balance`
--
ALTER TABLE `balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `broadcast_message`
--
ALTER TABLE `broadcast_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `money_transfer`
--
ALTER TABLE `money_transfer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `package`
--
ALTER TABLE `package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `payment_gateway`
--
ALTER TABLE `payment_gateway`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_profile`
--
ALTER TABLE `user_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wallet`
--
ALTER TABLE `wallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `balance`
--
ALTER TABLE `balance`
  ADD CONSTRAINT `fk-balance-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-balance-wallet_id` FOREIGN KEY (`wallet_id`) REFERENCES `wallet` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `money_transfer`
--
ALTER TABLE `money_transfer`
  ADD CONSTRAINT `fk-money_transfer-from_user_id` FOREIGN KEY (`from_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-money_transfer-from_wallet_id` FOREIGN KEY (`from_wallet_id`) REFERENCES `wallet` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-money_transfer-to_user_id` FOREIGN KEY (`to_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-money_transfer-to_wallet_id` FOREIGN KEY (`to_wallet_id`) REFERENCES `wallet` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-money_transfer-transaction_id` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `fk-order-package_id` FOREIGN KEY (`package_id`) REFERENCES `package` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-order-transaction_id` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-order-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `fk-transaction-from_user_id` FOREIGN KEY (`from_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-transaction-from_wallet_id` FOREIGN KEY (`from_wallet_id`) REFERENCES `wallet` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-transaction-payment_gateway_id` FOREIGN KEY (`payment_gateway_id`) REFERENCES `payment_gateway` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-transaction-to_user_id` FOREIGN KEY (`to_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-transaction-to_wallet_id` FOREIGN KEY (`to_wallet_id`) REFERENCES `wallet` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-transaction-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk-user-current_parent_id` FOREIGN KEY (`current_parent_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-user-left_user_id` FOREIGN KEY (`left_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-user-parent_id` FOREIGN KEY (`parent_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-user-right_user_id` FOREIGN KEY (`right_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD CONSTRAINT `fk-user_profile-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
