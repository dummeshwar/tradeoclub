<?php

return [
    'class' => 'yii\swiftmailer\Mailer',
    'transport' => [
        'class' => 'Swift_SmtpTransport',
        'host' => 'tradeoclub.com',
        'username' => 'admin@tradeoclub.com',
        'password' => 'Admin@123',
        'port' => '465',
        'encryption' => 'ssl',
    ],
    // send all mails to a file by default. You have to set
    // 'useFileTransport' to false and configure a transport
    // for the mailer to send real emails.
    'useFileTransport' => false,
];
