<?php

return [
    'adminEmail' => 'admin@tradeoclub.com',
    'supportEmail' => 'admin@tradeoclub.com',
    'user.passwordResetTokenExpire' => 3600,
    'userImagesUpload' => '/uploads/profile_pics/',
    'userBankStatements' => '/uploads/bank-statements/',
    'admin_user_id' => 1,
    'packageImagesUpload' => '/uploads/package/',
    'popUpImagesUpload' => '/uploads/admin-pop-ups/',
    'packagePlaceHolderImage' => 'http://via.placeholder.com/263x97',

    'API' => [
        'BitcoinToUSD' => 'https://blockchain.info/ticker',
    ],
    // Primary Wallets.
    'wallets' => [
      'CASH_WALLET' => 1,
      'REFERRAL_WALLET' => 2,
      'BINARY_WALLET' => 3,
      'TRADEOCLUB_WALLET' => 4,
    ],

    // Secondary wallets.
    'secondary_wallets' => [
      'PROMO_WALLET' => 5,
    ],

    // Points to BTC mapping.
    // 1 Point equals to.
    'points_to_BTC' => 0.0001,

    // Primary wallets.
    'wallets_by_keys' => [
        1 => 'Bitcoin Wallet',
        2 => 'Referral Wallet',
        3 => 'Binary Wallet',
        4 => 'Profit Wallet',
        5 => 'Promo Wallet',
    ],


    'ACTIVE' => 1,
    'INACTIVE' => 0,

    // Transfer allow wallets - from wallets
    'from_wallets' => [
        1 => 'Cash Wallet',
        2 => 'Referral Wallet',
        3 => 'Binary Wallet',
        4 => 'Tradeoclub Wallet',
    ],
    // Transfer allow wallets - from wallets
    'to_wallets' => [
        1 => 'Cash Wallet'
    ],

    // Modes
    'transaction_modes' => [
        'credit' => 'CREDIT',
        'debit' => 'DEBIT',
        'money_trasnfer' => 'MONEY TRANSFER',
        'package_purchase' => 'PACKAGE PURCHASE',
        'withdrawal_request' => 'WITHDRAWAL REQUEST',
        'direct_commission' => 'DIRECT COMMISSION',
        'binary_commission' => 'BINARY COMMISSION',
        'amount_credited' => 'ADMIN: AMOUNT CREDITED',
        'amount_debited' => 'ADMIN: AMOUNT DEBITED',
        'withdrawal_request_processed' => 'ADMIN: WITHDRAWAL REQUEST PROCESSED',
        'withdrawal_request_rejected' => 'ADMIN: WITHDRAWAL REQUEST REJECTED',
        'user_add_fund' => 'USER: ADD FUND',
        'daily_returns' => 'DAILY RETURNS',
        'points_added' => 'POINTS ADDED',
        'points_purchased' => 'POINTS PURCHASED',
        'points_from_package_purchase' => 'Points From Package Purchase',
        'weekly_returns' => 'WEEKLY RETURNS',
    ],

    // Transaction charges - All in percentages - Don't specify % sign
    'transaction_charges' => [
        'money_transfer' => 5,
        'withdrawal' => 2,
    ],

    // Referral Commissions.
    'direct_commission' => 11, // in %
    'binary_commission' => 6, // in %

    'money_withdrawal_allowed_wallets' => [
        //1 => 'Cash Wallet',
        2 => 'Referral Wallet',
        3 => 'Binay Wallet',
        4 => 'Tradeoclub Wallet'
    ],
    // Pagination default pageSize
    'pageSize' => 50,

    // Limit the number of packages to display on fronpage.
    'packageLimit' => 8,

    // Admin Approval Status values
    'adminApprovalStatus' => [
        0 => 'Rejected',
        1 => 'Approval',
        //2 => 'Pending',
    ],

    // Payment gataways.
    'payment_gateways' => [
        'manual_payment' => 1,
    ],

];
